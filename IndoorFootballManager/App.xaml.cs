﻿using IndoorFootballManager.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Windows;
using System;
using System.Threading.Tasks;
using System.Windows;
using Notification.Wpf;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace IndoorFootballManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            AppCenter.Start("8e0e26fb-55ca-4f2a-8ff2-32e6d85db425", typeof(Analytics), typeof(Crashes));

            if (e.Args.Length == 0)
                return;

            var fileName = e.Args[0].Split('\\')[6].Split('.')[0];
            Properties["LastSaved"] = fileName;
            Properties["FileName"] = fileName;
        }

        public App() { }

        private static IFM GameWindow => Current.MainWindow as IFM;
        public static Player Player { get; set; }
        private static GeneralManager UserManager => IFM.UserManager;

        public static object NavigationParameters { get; set; }

        private static double _loadPercentage;
        public static double LoadPercentage
        {
            get => _loadPercentage;
            set
            {
                _loadPercentage = value;
                IFM.NotifyChange("LoadPercentage");
            }
        }

        public static async Task ActivateLoading()
        {
            IFM._isBusy = true;
            LoadPercentage = 0;
            IFM.NotifyChange("IsBusy");
        }

        public static async Task DeactivateLoading()
        {
            await Task.Delay(500);
            IFM._isBusy = false;
            LoadPercentage = 0;
            IFM.NotifyChange("IsBusy");
        }

        public static void ShowNotification(string title, string msg, NotificationType type, double time)
        {
            var content = new NotificationContent
            {
                Title = title,
                Message = msg,
                Type = type
            };
            IFM.NotificationManager.Show(content, areaName: "InfoArea", expirationTime: TimeSpan.FromSeconds(time));
        }

        public static void ShowMessageBox(string title, string msg, NotificationType type, string area, string leftBtnTxt, Action leftBtnClick, string rightBtnTxt, Action rightBtnClick)
        {
            var content = new NotificationContent
            {
                Title = title,
                Message = msg,
                Type = type,
                LeftButtonAction = leftBtnClick,
                RightButtonAction = rightBtnClick,
                LeftButtonContent = leftBtnTxt,
                RightButtonContent = rightBtnTxt
            };
            IFM.NotificationManager.Show(content, areaName: area, expirationTime: null);
        }

        private void PlayerCard_Click(object sender, RoutedEventArgs e)
        {
            if (Player != null)
                GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(Player));
        }

        private void EditPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Player != null)
                GameWindow.MainFrame.NavigationService.Navigate(new EditPlayer(Player));
        }

        private void CutPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerData.CutPlayer(Player);
        }

        private void SignPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UserManager != null)
            {
                var newLeague = Repo.Database.GetLeague(1);
                if (newLeague.IsOffSeason)
                {
                    var offer = new OfferContract(UserManager.Team, Player).ShowDialog();
                    if (offer.HasValue && offer.Value)
                        ShowNotification("Contract Offered", $"An offer has been put in for {Player.NameWithPosition}.",NotificationType.Success, 5);
                }
                else
                {
                    void OfferContract()
                    {
                        PlayerData.OfferInSeasonContract(UserManager.Team, Player);
                        ShowNotification("Contract Offered", $"An offer has been put in for {Player.NameWithPosition}.", NotificationType.Success, 5);
                    }
                    ShowMessageBox("Contract Offer", "Are you sure you wish to offer a contract?", NotificationType.Notification, "NotificationArea", "Yes", OfferContract, string.Empty, null);
                }
            }
            else
                ShowNotification("Warning", "You must be running a team for this option.", NotificationType.Error, 5);
        }

        private void ResignPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            var resign = new ResignPlayer(Player).ShowDialog();
            if (resign.HasValue && resign.Value)
                GameWindow.MainFrame.Refresh();
        }

        private void TradePlayerBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TagPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerData.FranchisePlayer(Player);
        }

        private void TrainPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            var train = new TrainPlayer(Player).ShowDialog();
            if (train.HasValue && train.Value)
                GameWindow.MainFrame.Refresh();
        }

        private void ActivatePlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!Player.InjuredReserve && Player.RosterReserve)
            {
                PlayerData.ActivatePlayer(Player);
                ShowNotification("Player Activated", $"{Player.NameWithPosition} has been activated.", NotificationType.Information, 5);
                GameWindow.MainFrame.Refresh();
            }
            else
                ShowNotification("Error", $"You cannot activate {Player.NameWithPosition} from Injured Reserve.", NotificationType.Error, 5);
        }

        private void DeactivatePlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!Player.InjuredReserve && !Player.RosterReserve)
            {
                PlayerData.AssignToReserves(Player);
                ShowNotification("Player Assigned", $"{Player.NameWithPosition} has been placed on reserve roster.", NotificationType.Information, 5);
                GameWindow.MainFrame.Refresh();
            }
            else
                ShowNotification("Error", "This player is already deactivated or on IR.", NotificationType.Error, 5);
        }

        private void IRPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Player.IsInjured)
            {
                PlayerData.AssignToIr(Player);
                ShowNotification("Player Assigned", $"{Player.NameWithPosition} has been placed on IR.", NotificationType.Information, 5);
                GameWindow.MainFrame.Refresh();
            }
            else
                ShowNotification("Error", "You cannot place an uninjured player on IR.", NotificationType.Error, 5);
        }
    }
}

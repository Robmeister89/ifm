﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Runtime.CompilerServices;
using System.Globalization;
using IndoorFootballManager.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Resources.Fonts;
using IndoorFootballManager.Services;
using IndoorFootballManager.Windows;
using SQLite;
using FluentFTP;
using Notification.Wpf;

namespace IndoorFootballManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class IFM : Window, INotifyPropertyChanged
    {
        private bool _hasAdvanced = true;
        private bool _takeToDraft = false;
        private bool UploadFailed { get; set; } = false;
        private bool UploadTeamFailed { get; set; } = false;
        private bool ImportTeamsFailed { get; set; } = false;

        private static IFM _ifm;
        private static SQLiteConnection Database { get; set; }

        public static readonly NotificationManager NotificationManager = new NotificationManager();

        public static League CurrentLeague = new League();
        public static GeneralManager UserManager { get; set; }
        public static Team TeamListSelect { get; set; }
        public bool CommissionerButtonsEnabled => UserManager != null && UserManager.IsCommissioner;
        public bool TeamButtonsEnabled => UserManager != null && UserManager.TeamId > 0;
        public static DateTime ToDate { get; set; }

        public IFM()
        {
            InitializeComponent();
            DataContext = this;

            _ifm = this;

            Height = Convert.ToDouble(Properties.Settings.Default.Height);
            Width = Convert.ToDouble(Properties.Settings.Default.Width);

            if (SystemParameters.PrimaryScreenHeight < Height)
            {
                WindowState = WindowState.Maximized;
            }
            else
            {
                // set default parameters to larger screen...
                Height = 912;
                Width = 1632;
                Properties.Settings.Default.Height = Height.ToString(CultureInfo.InvariantCulture);
                Properties.Settings.Default.Width = Width.ToString(CultureInfo.InvariantCulture);
                Properties.Settings.Default.Save();
            }

            MainFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;

            if (Properties.Settings.Default.Background != string.Empty) return;
            Properties.Settings.Default.Background = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Images\\background.png";
            Properties.Settings.Default.Save();
        }

        internal void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // not working correctly... loads league but logging into GM is not working???
            if (Application.Current.Properties["FileName"] != null)
            {
                Globals.LeagueAbbreviation = Application.Current.Properties["LastSaved"].ToString();

                ContinueBtn_Click(sender, e);
            }
            else
            {
                if (string.IsNullOrEmpty(Globals.LeagueAbbreviation))
                    NewLeagueScreen();
                else
                    ExistingLeague();
            }
        }

        private void NewLeagueScreen()
        {
            FileMenu.Visibility = Visibility.Hidden;
            HideButtons();
            DisableMenuItems();
            FileBtn.IsEnabled = true;
            SaveBtn.IsEnabled = false;
            CloseBtn.IsEnabled = false;
            OptionsBtn.IsEnabled = true;
            DraftBtn.Visibility = Visibility.Collapsed;
            FreeAgencyBtn.Visibility = Visibility.Collapsed;
            ChangeFormatBtn.Visibility = Visibility.Collapsed;

            if (Properties.Settings.Default.LastSaved == "")
                ContinueBtn.IsEnabled = false;

            LeagueTxt.Visibility = Visibility.Hidden;
            DateTxt.Visibility = Visibility.Hidden;
            ClearHistory(MainFrame);
            Database = null;
            MainFrame.Navigate(null);
        }

        public async void ExistingLeague()
        {
            FileMenu.Visibility = Visibility.Hidden;
            ShowButtons();
            EnableMenuItems(MainMenu);
            EnableMenuItems(FileMenu);
            Database = Repo.Database;
            CurrentLeague = Database.GetLeague(1);

            DraftBtn.Visibility = Visibility.Collapsed;
            FreeAgencyBtn.Visibility = Visibility.Collapsed;
            ChangeFormatBtn.Visibility = Visibility.Collapsed;
            if (CurrentLeague.CurDate == CurrentLeague.DraftDate)
                DraftBtn.Visibility = Visibility.Visible;
            if (CurrentLeague.IsOffSeason && (CurrentLeague.CurDate >= CurrentLeague.NewSeasonDate || CurrentLeague.CurDate < CurrentLeague.InSeasonDate) && CurrentLeague.CurDate != CurrentLeague.DraftDate)
                FreeAgencyBtn.Visibility = Visibility.Visible;
            if (CurrentLeague.CurDate == CurrentLeague.NewSeasonDate)
                ChangeFormatBtn.Visibility = Visibility.Visible;

            CheckLogin(UserManager);

            LeagueTxt.Visibility = Visibility.Visible;
            DateTxt.Visibility = Visibility.Visible;
            LeagueTxt.Text = LeagueNameText;
            DateTxt.Text = DateText;

            await NavigateToPage(new ManagerPage(UserManager), new ManagerEmails(UserManager));

            App.ShowNotification("Success", $"{CurrentLeague.Abbreviation} has loaded successfully.", NotificationType.Success, 5);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                NotifyPropertyChanged();
            }
        }

        private string _leagueNameText = "League Name";
        public string LeagueNameText
        {
            get
            {
                if (CurrentLeague != null)
                    _leagueNameText = CurrentLeague.LeagueName;
                return _leagueNameText;
            }
            set
            {
                _leagueNameText = value;
                NotifyPropertyChanged();
            }
        }

        private string _dateText = "Current Date";
        public string DateText
        {
            get
            {
                if (CurrentLeague != null)
                    _dateText = CurrentLeague.CurDate.ToLongDateString();
                return _dateText;
            }
            set
            {
                _dateText = value;
                NotifyPropertyChanged();
            }
        }

        private int _emailsCount;
        public int EmailsCount
        {
            get
            {
                if (Database != null && CurrentLeague != null && UserManager != null)
                    _emailsCount = Database.GetEmails(UserManager.Id).Count();
                return _emailsCount;
            }
            set
            {
                _emailsCount = value;
                NotifyPropertyChanged();
            }
        }

        private string _loadPercentage;
        public string LoadPercentage
        {
            get
            {
                var percentage = App.LoadPercentage / 100.0;
                _loadPercentage = percentage > 0.0 ? $"{percentage:P1}" : string.Empty;
                return _loadPercentage;
            }
            set
            {
                _loadPercentage = value;
                NotifyPropertyChanged();
            }
        }

        public static void NotifyChange(string s)
        {
            _ifm.NotifyPropertyChanged(s);
        }

        private void HideButtons()
        {
            foreach (var b in ForeScreen.Children.OfType<Button>())
                b.Visibility = Visibility.Hidden;
        }

        private void ShowButtons()
        {
            foreach (var b in ForeScreen.Children.OfType<Button>())
                b.Visibility = Visibility.Visible;
        }

        private void DisableMenuItems()
        {
            foreach (MenuItem m in MainMenu.Items)
                m.IsEnabled = false;
        }

        private static void EnableMenuItems(ItemsControl menu)
        {
            foreach (MenuItem m in menu.Items)
                m.IsEnabled = true;
        }

        private void HideAll()
        {
            foreach (var c in MainScreen.Children.OfType<Canvas>())
                c.Visibility = Visibility.Hidden;
        }

        private async Task NavigateToPage(UserControl control, UserControl secondaryControl = null, object extraData = null)
        {
            _isBusy = true;
            NotifyChange(nameof(IsBusy));

            try
            {
                _ = MainFrame.Navigate(control, extraData);
                if (secondaryControl != null)
                {
                    switch (control)
                    {
                        case TeamPage teamPage:
                            teamPage.TeamFrame.Content = secondaryControl;
                            break;
                        case ManagerPage managerPage:
                            managerPage.GmFrame.Content = secondaryControl;
                            break;
                    }
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
            finally
            {
                await Task.Delay(500);
                _isBusy = false;
                NotifyChange(nameof(IsBusy));
            }
        }

        private async void TeamsBtn_Click(object sender, RoutedEventArgs e)
        {
            var teams = new TeamList().ShowDialog();
            if (teams.HasValue && teams.Value)
                await NavigateToPage(new TeamPage(TeamListSelect));
        }

        private async void ManagersBtn_Click(object sender, RoutedEventArgs e)
        {
            var load = UserManager == null ? new GeneralManagers().ShowDialog() : new GeneralManagers(UserManager).ShowDialog();
            if (!load.HasValue || !load.Value) return;
            CheckLogin(UserManager);
            await NavigateToPage(new ManagerPage(UserManager), new ManagerEmails(UserManager));
        }

        private async void ScheduleBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new ScheduleCalendar());
        private async void OptionsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new GameOptions());
        private async void StandingsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new LeagueStandings());
        private async void DraftBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new DraftScreen());
        
        private async void FreeAgencyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UserManager?.Team != null)
                await NavigateToPage(new FreeAgency(UserManager.Team));
            else if (UserManager?.Team == null)
                App.ShowNotification("Error", "You must be running a team to view this screen.", NotificationType.Warning, 5);
            else
                App.ShowNotification("Error", "You must be logged into view this screen.", NotificationType.Warning, 5);
        }

        private async void RecordsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new LeagueRecords());
        private async void HistoryBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new LeagueHistory(CurrentLeague));

        private async void ContinueBtn_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            Globals.LeagueAbbreviation = Properties.Settings.Default.LastSaved;
            var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{Globals.LeagueAbbreviation}\{Globals.LeagueAbbreviation}";
            var livePath = $"{path}.ifm";
            await App.DeactivateLoading();
            if (File.Exists(livePath))
            {
                var gm = new GeneralManagers().ShowDialog();
                if (gm.HasValue && gm.Value)
                {
                    var backupPath = $"{path}.bac";
                    File.Copy(livePath, backupPath, true);

                    ExistingLeague();
                }
            }
            else
            {
                Globals.LeagueAbbreviation = null;
                Properties.Settings.Default.LastSaved = "";
                Properties.Settings.Default.Save();
                ContinueBtn.IsEnabled = false;
                App.ShowNotification("Error", "League not found.", NotificationType.Error, 5);
            }

        }

        private async void PlayersBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new PlayerSearch());
        private async void StatsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new LeagueStats());
        private async void TransactionsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new LeagueTransactions());

        private void FileBtn_Click(object sender, RoutedEventArgs e)
        {
            if (FileMenu.Visibility == Visibility.Hidden)
            {
                FileBtn.IconUnicode = Solid.ChevronUp;
                FileMenu.Visibility = Visibility.Visible;
            }
            else
            {
                FileBtn.IconUnicode = Solid.ChevronDown;
                FileMenu.Visibility = Visibility.Hidden;
            }
        }

        private void LeagueBtn_Click(object sender, RoutedEventArgs e)
        {
            LeagueSubMenu.Visibility = LeagueSubMenu.Visibility == Visibility.Hidden 
                ? Visibility.Visible 
                : Visibility.Hidden;
        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!FileBtn.IsMouseOver)
            {
                FileBtn.IconUnicode = Solid.ChevronDown;
                if (FileMenu.Visibility == Visibility.Visible)
                    FileMenu.Visibility = Visibility.Hidden;
            }

            if (LeagueBtn.IsMouseOver) return;
            if (LeagueSubMenu.Visibility == Visibility.Visible)
                LeagueSubMenu.Visibility = Visibility.Hidden;
        }

        private void PreviousBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MainFrame.CanGoBack)
                MainFrame.GoBack();
        }

        private void ForwardBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MainFrame.CanGoForward)
                MainFrame.GoForward();
        }

        private async void TeamHomeQuick_Click(object sender, RoutedEventArgs e)
        {
            var team = Database.GetTeamById(UserManager.TeamId);
            await NavigateToPage(new TeamPage(team), new TeamHome());
        }

        private async void TeamRosterQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamRoster());
        private async void StrategyQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamStrategy(UserManager.Team));
        private async void DepthChartQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamDepthChart());
        private async void TeamStatsQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamStats(UserManager.Team));
        private async void EmailsQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new ManagerPage(UserManager), new ManagerEmails(UserManager));
        private async void ContractsQuick_Click(object sender, RoutedEventArgs e)
        {
            if (UserManager.Team == null) await NavigateToPage(new ManagerPage(UserManager));
            else await NavigateToPage(new ManagerPage(UserManager), new ManagerContracts(UserManager.Team));
        }

        private void DraftPicksQuick_Click(object sender, RoutedEventArgs e)
        {
            var picks = new TeamDraftPicks(UserManager.Team) { Owner = this };
            picks.ShowDialog();
        }

        private async void HistoryQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamHistory(UserManager.Team));
        private async void TeamInfoQuick_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamPage(UserManager.Team), new TeamInfo());

        private async void GameDayQuick_Click(object sender, RoutedEventArgs e)
        {
            DateTime date;
            List<Game> games = null;

            for (var i = 0; i < 7; i++)
            {
                date = CurrentLeague.CurDate.AddDays(i);
                games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.GameDate == date).ToList();
                if (games.Any())
                    break;
            }

            if (games != null && games.Any())
                await NavigateToPage(new GameDay(games));
            else
                App.ShowNotification("No Games To Display", "Not navigating...", NotificationType.Information, 5);
        }

        private async void ScheduleQuick_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            var games = Database.GetGames(CurrentLeague.CurrentSeason).ToList();
            var teams = Database.GetTeams().ToList();

            if (games.Any())
                await NavigateToPage(new GameSchedule(games, teams));
            else
                await NavigateToPage(new ScheduleCalendar());
        }

        private async void PollQuick_Click(object sender, RoutedEventArgs e)
        {
            var teams = Database.GetTeams().ToList();
            await NavigateToPage(new LeaguePowerPoll(teams));
        }

        private async void CreatePlayerBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new CreatePlayer());

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {
            var create = new CreateNewLeague().ShowDialog();
            if (!create.HasValue || !create.Value) return;
            var gm = new CreateGm().ShowDialog();
            if (!gm.HasValue || !gm.Value) return;
            ExistingLeague();
        }

        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var open = new OpenLeague();
                open.ShowDialog();
                if (!open.Successful) return;
                
                Database = Repo.Database;

                MainFrame.Navigate(null);
                var gm = new GeneralManagers().ShowDialog();
                if (!gm.HasValue || !gm.Value) return;
                ClearHistory(MainFrame);
                ExistingLeague();
            }
            catch (Exception x)
            {
                App.ShowNotification("Warning", "Something went wrong...", NotificationType.Warning, 5);
                Logger.LogException(x);
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\{CurrentLeague.Abbreviation}";
            var livePath = $"{path}.ifm";
            var backupPath = $"{path}.bac";
            var liveFileInfo = new FileInfo(livePath);
            var backupFileInfo = new FileInfo(backupPath);
            if (liveFileInfo.LastWriteTime != backupFileInfo.LastWriteTime)
            {
                void DoNotSave()
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.WaitForFullGCComplete();
                    Repo.Disconnect();
                    File.Copy(backupPath, livePath, true);
                    Globals.LeagueAbbreviation = null;
                    DraftScreen.cancelSave = true;
                    NewLeagueScreen();
                };

                void Save()
                {
                    SaveBtn_Click(sender, e);
                    Repo.Disconnect();
                    Globals.LeagueAbbreviation = null;
                    DraftScreen.cancelSave = true;
                    NewLeagueScreen();
                };

                App.ShowMessageBox("Save Game", "Would you like to save first?", NotificationType.Notification, "NotificationArea", "Yes", Save, "No", DoNotSave);
            }
            else
            {
                Repo.Disconnect();
                Globals.LeagueAbbreviation = null;
                DraftScreen.cancelSave = true;
                NewLeagueScreen();
            }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentLeague.Abbreviation.IsNullOrEmpty())
            {
                App.ShowNotification("Error", "You must have a league open to save.", NotificationType.Warning, 5);
                return;
            }

            var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\{CurrentLeague.Abbreviation}";
            var livePath = $"{path}.ifm";
            var backupPath = $"{path}.bac";
            
            Repo.Disconnect();
            File.Copy(livePath, backupPath, true);
            Repo.Reconnect(Globals.LeagueFilePath);
            Database = Repo.Database;

            Properties.Settings.Default.LastSaved = CurrentLeague.Abbreviation;
            Properties.Settings.Default.Save();
            App.ShowNotification("Saved", "League File Saved.", NotificationType.Success, 5);
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            Repo.Disconnect();
            Close();
        }

        private void Window_Closing(object sender, CancelEventArgs c)
        {
            var e = new RoutedEventArgs();

            c.Cancel = true;
            
            void DoNotExit()
            {
                c.Cancel = true;
            }

            void CheckSave()
            {
                if (Globals.LeagueAbbreviation != null)
                {
                    var path = $"{Globals.LeagueDirectory}{CurrentLeague.Abbreviation}";
                    var livePath = $"{path}.ifm";
                    var backupPath = $"{path}.bac";
                    var liveFileInfo = new FileInfo(livePath);

                    void Save()
                    {
                        SaveBtn_Click(sender, e);
                        Environment.Exit(0);
                    }

                    void DoNotSave()
                    {
                        if (!liveFileInfo.Exists) return;
                        try
                        {
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            Repo.Disconnect();
                            File.Copy(backupPath, livePath, true);
                            Environment.Exit(0);
                        }
                        catch (Exception x)
                        {
                            Logger.LogException(x);
                        }
                    }

                    App.ShowMessageBox("Save Game", "Would you like to save first?", NotificationType.Notification, "NotificationArea", "Yes", Save, "No", DoNotSave);
                }
                else Environment.Exit(0);
            }

            App.ShowMessageBox("Quit Game", "Are you sure you want to exit?", NotificationType.Notification, "NotificationArea", "Yes", CheckSave, "No", DoNotExit);
        }

        private static string _ftpUserName;
        private static string _ftpPassword;

        // upload league file
        private async Task UploadLeagueFile(string filePath, string fileName)
        {
            await App.ActivateLoading();

            var uri = CurrentLeague.FtpServer;
            var uriPath = $"{CurrentLeague.FtpRemotePath}{fileName}";
            _ftpUserName = CurrentLeague.FtpUsername;
            _ftpPassword = Converters.EncryptDecrypt(CurrentLeague.FtpPassword, 256);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            try
            {
                await Task.Run(() => Repo.Disconnect()).ConfigureAwait(false);

                using (var ftp = new FtpClient(uri, new NetworkCredential(_ftpUserName, _ftpPassword)))
                {
                    Logger.LogInformation($"Connecting to ftp: {uriPath}");
                    ftp.Connect();
                    Logger.LogInformation($"Connected to ftp. Creating directory if not exists: {CurrentLeague.FtpRemotePath}");
                    ftp.CreateDirectory(CurrentLeague.FtpRemotePath);
                    await Task.Run(() => ftp.UploadFile(filePath, uriPath, progress: (x) => App.LoadPercentage = x.Progress));
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                UploadFailed = true;
            }
            finally
            {
                Database = Repo.Database;

                await App.DeactivateLoading();

                if (UploadFailed)
                    App.ShowNotification("Upload Failed", "Unable to connect.\nPlease ensure your FTP info is correct.", NotificationType.Error, 5);
                else
                    App.ShowNotification("Upload Succesful", "League file has been uploaded.", NotificationType.Success, 5);

            }
        }

        private async Task UploadTeamFile(string filePath, string fileName)
        {
            var uri = CurrentLeague.FtpServer;
            var uriPath = $"{CurrentLeague.FtpRemotePath}Exports/{fileName}";
            _ftpUserName = CurrentLeague.FtpUsername;
            _ftpPassword = Converters.EncryptDecrypt(CurrentLeague.FtpPassword, 256);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            try
            {
                using (var ftp = new FtpClient(uri, new NetworkCredential(_ftpUserName, _ftpPassword)))
                {
                    ftp.Connect();
                    ftp.CreateDirectory($"{CurrentLeague.FtpRemotePath}Exports/");
                    await Task.Run(() => ftp.UploadFile(filePath, uriPath));
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                UploadFailed = true;
                throw x;
            }
        }

        // download all team files from server...
        private static async Task DownloadFiles()
        {
            await App.ActivateLoading();
            try
            {
                var uri = CurrentLeague.FtpServer;
                var uriPath = $"{CurrentLeague.FtpRemotePath}Exports/";

                _ftpUserName = CurrentLeague.FtpUsername;
                _ftpPassword = CurrentLeague.FtpPassword != null
                    ? Converters.EncryptDecrypt(CurrentLeague.FtpPassword, 256)
                    : "";

                var dirPath =
                    $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\Exports\";
                Directory.CreateDirectory(dirPath);

                using (var ftp = new FtpClient(uri, new NetworkCredential(_ftpUserName, _ftpPassword)))
                {
                    ftp.Connect(); ;
                    for (var i = 1; i <= CurrentLeague.LeagueTeams; i++)
                    {
                        var filePath = $"{dirPath}{i}.team";
                        try
                        {
                            await Task.Run(() => ftp.DownloadFile(filePath, $"{uriPath}{i}.team",
                                progress: (x) => App.LoadPercentage = x.Progress));
                        }
                        catch (Exception x)
                        {
                            Logger.LogException(x);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
            finally
            {
                await App.DeactivateLoading();

            }
        }

        public void CheckLogin(GeneralManager user)
        {
            if (user != null)
            {
                EmailsCount = Database.GetEmails(user.Id).Count();
            }

            NotifyPropertyChanged(nameof(CommissionerButtonsEnabled));
            NotifyPropertyChanged(nameof(TeamButtonsEnabled));
            MainFrame.Refresh();
        }

        private void ConfigBtn_Click(object sender, RoutedEventArgs e) =>MainFrame.Navigate(new LeagueSettings());

        private async void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\";
            var filePath = $"{databasePath}{CurrentLeague.Abbreviation}.ifm";
            var currentPath = new FileInfo(filePath);
            var backupPath = new FileInfo($"{databasePath}{CurrentLeague.Abbreviation}.bac");

            if (currentPath.LastWriteTime == backupPath.LastWriteTime)
            {
                UploadFailed = false;
                await UploadLeagueFile(filePath, new FileInfo(filePath).Name);
            }
            else
                App.ShowNotification("Cannot Upload", "Please save your game first.", NotificationType.Warning, 5);
        }

        private async void ImportBtn_Click(object sender, RoutedEventArgs e) => await ImportFiles();
        private async void ExportTeamQuick_Click(object sender, RoutedEventArgs e) => await ExportTeamFile();
        private async void AdvOneDay_Click(object sender, RoutedEventArgs e) => await AdvanceOneDay();
        private async void AdvDays_Click(object sender, RoutedEventArgs e) => await AdvanceMultipleDays();
        private async void AdvNext_Click(object sender, RoutedEventArgs e) => await AdvanceToEvent();

        private void CheckStatusBtn_Click(object sender, RoutedEventArgs e)
        {
            var import = new ImportStatus();
            import.ShowDialog();
        }
        
        private async Task ImportFiles()
        {
            await App.ActivateLoading();
            try
            {
                ImportTeamsFailed = false;
                var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\Exports\";
                if (CurrentLeague.UseFTP)
                    await DownloadFiles();
                var dir = new DirectoryInfo(databasePath);
                var files = dir.GetFiles();
                foreach (var file in files)
                {
                    try
                    {
                        Database.ImportTeam(Convert.ToInt32(Path.GetFileNameWithoutExtension(file.Name)), CurrentLeague.Abbreviation);
                    }
                    catch (Exception x)
                    {
                        Logger.LogException(x);
                    }
                }

            }
            catch (Exception x)
            {
                Logger.LogException(x);
                ImportTeamsFailed = true;
            }
            finally
            {
                await App.DeactivateLoading();

                if (ImportTeamsFailed)
                    App.ShowNotification("Error", "Connection attempt failed.", NotificationType.Error, 5);
                else
                {
                    ExistingLeague();
                    var import = new ImportStatus();
                    import.ShowDialog();
                }
            }
        }

        private async Task ExportTeamFile()
        {
            await App.ActivateLoading();

            UploadTeamFailed = false;
            var team = UserManager.Team;
            await Database.Vacuum().ConfigureAwait(false);

            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\Exports\";
            var filePath = $"{databasePath}{team.Id}.team";
            var fileInfo = new FileInfo(filePath);
            var file = fileInfo.Name;
            try
            {
                Database.ExportTeam(team.Id, CurrentLeague.Abbreviation);
                if (CurrentLeague.UseFTP)
                    await UploadTeamFile(filePath, file);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                UploadTeamFailed = true;
            }
            finally
            {
                await App.DeactivateLoading();

                if (UploadTeamFailed)
                    App.ShowNotification("Error", "Team file upload failed.", NotificationType.Error, 5);
                else
                    App.ShowNotification("Success", "Team file upload successfully.", NotificationType.Success, 5);
            }
        }
        
        private async Task SimCompleted()
        {
            if (!_takeToDraft) 
                return;

            await NavigateToPage(new DraftScreen());
            App.ShowNotification("Error", "Please complete the draft first.", NotificationType.Error, 5);
            _takeToDraft = false;
        }

        private async Task AdvanceOneDay()
        {
            await App.ActivateLoading();

            try
            {
                AdvanceDay();
                Dispatcher.Invoke(() =>
                {
                    MainFrame.Refresh();
                    if (MainFrame.Content.ToString() == "IndoorFootballManager.Controls.ScheduleCalendar")
                        MainFrame.Navigate(new ScheduleCalendar());
                });
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "The day could not be advanced.\nThere may be an issue.\nSee IFM_Log.txt in your Documents folder.", NotificationType.Error, 5);
            }
            finally
            {
                await SimCompleted();

                await App.DeactivateLoading();
            }
        }
        
        private async void AdvanceDay()
        {
            _hasAdvanced = true;
            _takeToDraft = false;

            var endSeason = false;

            var leagueTeams = Database.GetTeams().ToList();
            var AllPlayers = Database.GetPlayers().ToList();

            if (!CurrentLeague.IsOffSeason) // in-season
            {
                InSeasonDay.DeleteNonActiveContractOffers();
                InSeasonDay.SignFreeAgents();
                // check for a Sunday in May, June, July and August (when applicable) and during the in-season
                if (CurrentLeague.CurDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    //int month = NewLeague.CurDate.Month;
                    if (CurrentLeague.CurDate >= CurrentLeague.InSeasonDate && CurrentLeague.CurDate < CurrentLeague.SeasonEndDate)
                    {
                        // have AI check rosters for validation
                        InSeasonDay.FillCpuRosters();
                        // attempt to sign pending free agents for cpu teams
                        InSeasonDay.CpuSignPending();
                        // auto train cpu teams
                        InSeasonDay.CpuTrainPlayers();

                        // reset every player on each team for training
                        var trained = AllPlayers.Where(p => p.HasTrained).ToList();
                        foreach (var p in trained)
                            p.HasTrained = false;
                        Database.UpdateAllPlayers(trained.AsEnumerable());
                    }
                }

                var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.GameDate == CurrentLeague.CurDate && !g.HasPlayed).ToList();
                foreach (var game in games)
                {
                    try
                    {
                        await game.PlayGame();
                        await game.SaveGameStats(game.Playoffs);
                    }
                    catch (Exception x)
                    {
                        Logger.LogException(x);
                        _hasAdvanced = false;
                        return;
                    }
                }

                LeagueData.CheckForRecords(games);

                var injuredPlayers = AllPlayers.Where(p => p.IsInjured).Select(p =>
                {
                    p.InjuryDuration -= 1;
                    if (p.InjuryDuration != 0) return p;
                    p.IsInjured = false;
                    p.InjuryTxt = "";
                    return p;
                }).ToList();

                Database.UpdateAllPlayers(injuredPlayers.AsEnumerable());

                if (CurrentLeague.IsPlayoffs)
                {
                    var playoffGames = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.Playoffs).ToList();
                    if (playoffGames.All(g => g.HasPlayed))
                    {
                        if (playoffGames.Last().Conference != "Championship")
                            InSeasonDay.AdvancePlayoffs(true);
                        else
                        {
                            // send email letting everyone know the season ended
                            var managers = Database.GetManagers().Where(gm => gm.TeamId > 0 && !gm.ReceivedSeasonEnd).ToList();
                            foreach (var manager in managers)
                                SendEmail.SeasonEnd(manager);
                        }
                    }
                }

                InSeasonDay.CheckForPlayoffsStart(CurrentLeague.IsPlayoffs);
                
                if (CurrentLeague.CurDate == CurrentLeague.SeasonEndDate)
                {
                    var activePlayers = AllPlayers.Where(p => p.TeamId > -1).ToList();
                    OffSeasonDay.UpdateExperience(activePlayers);
                    var unsignedPlayers = AllPlayers.Where(p => p.ExtensionYears == 0 && p.ContractYears == 1 && p.TeamId > 0 && !p.IsFranchised).ToList();
                    OffSeasonDay.EndContracts(unsignedPlayers);
                    var contractPlayers = AllPlayers.Where(p => p.ExtensionYears == 0 && p.ContractYears > 1 && p.TeamId > 0 && !p.IsFranchised).ToList();
                    OffSeasonDay.RolloverContracts(contractPlayers);
                    var signedPlayers = AllPlayers.Where(p => p.ExtensionYears > 0 && p.TeamId > 0).ToList();
                    OffSeasonDay.ExtendPlayers(signedPlayers);
                    var notRetired = AllPlayers.Where(p => p.TeamId > -1).ToList();
                    if (CurrentLeague.UseRetirement)
                        OffSeasonDay.RetirePlayers(notRetired);
                    OffSeasonDay.DeclinePlayers(notRetired);

                    OffSeasonDay.SaveStandings(CurrentLeague.Conferences, CurrentLeague.Divisions);

                    OffSeasonDay.ClearTransactions();
                    await Database.Vacuum().ConfigureAwait(false);

                    var managers = Database.GetManagers().ToList();
                    OffSeasonDay.ClearEmails(managers);

                    CurrentLeague.IsPlayoffs = false;
                    CurrentLeague.CurrentSeason += 1;
                    OffSeasonDay.CreateNewScheduleAndDates(CurrentLeague);
                    CurrentLeague.IsOffSeason = true;
                    endSeason = true;
                }
                
                Database.SaveLeague(CurrentLeague);
                NotifyChange("EmailsCount");
            }
            else // off-season
            {
                // check if draft day
                if (CurrentLeague.CurDate == CurrentLeague.DraftDate)
                {

                    var pick1 = Database.GetDraft(CurrentLeague.CurrentSeason).FirstOrDefault(d => d.Pick == 1 && d.Round == 1);
                    if (pick1 != null)
                    {
                        if (Database.GetDraft(CurrentLeague.CurrentSeason).Last().Player == null)
                        {
                            _takeToDraft = true;
                            _hasAdvanced = false;
                        }
                    }
                    else
                    {
                        _takeToDraft = true;
                        _hasAdvanced = false;
                    }
                }

                if (CurrentLeague.CurDate >= CurrentLeague.NewSeasonDate && CurrentLeague.CurDate != CurrentLeague.DraftDate)
                {
                    var aiTeams = leagueTeams.Where(t => t.IsCpu).ToList();
                    var aiManagers = Database.GetManagers().Where(g => g.AssistSignings);
                    aiTeams.AddRange(aiManagers.Select(gm => gm.Team));
                    foreach (var t in aiTeams)
                        t.OfferFreeAgents(Database);

                    OffSeasonDay.DeleteNonActiveContractOffers();
                    OffSeasonDay.SignFreeAgents();
                }

                // check for training camp dates
                CurrentLeague.IsTrainingCamp = false;
                var campStart = CurrentLeague.TrainingCampDate;
                var campEnd = CurrentLeague.TrainingCampDate.AddDays(6);
                if (CurrentLeague.CurDate >= campStart && CurrentLeague.CurDate <= campEnd)
                {
                    CurrentLeague.IsTrainingCamp = true;
                    var allPlayers = AllPlayers.Select(p => { p.HasTrained = false; return p; });
                    Database.UpdateAllPlayers(allPlayers);
                }

            }

            if (endSeason)
                CurrentLeague.CurDate = CurrentLeague.NewSeasonDate.AddDays(-2);

            if (!_hasAdvanced) 
                return;
        
            var newDay = CurrentLeague.CurDate.AddDays(1);
            CurrentLeague.CurDate = newDay;
            var teams = Database.GetTeams().Select(t => { t.CurDate = newDay; return t; }).ToList();
            Database.UpdateAllTeams(teams.AsEnumerable());

            Dispatcher.Invoke(() =>
            {
                ChangeFormatBtn.Visibility = CurrentLeague.CurDate == CurrentLeague.NewSeasonDate 
                    ? Visibility.Visible 
                    : Visibility.Collapsed;

                if (CurrentLeague.CurDate >= CurrentLeague.NewSeasonDate.AddDays(1) && CurrentLeague.CurDate != CurrentLeague.DraftDate && CurrentLeague.IsOffSeason)
                    FreeAgencyBtn.Visibility = Visibility.Visible;

                if (CurrentLeague.CurDate == CurrentLeague.DraftDate)
                {
                    var managers = Database.GetManagers().Where(gm => gm.TeamId > 0 && !gm.ReceivedDraftEmail).ToList();
                    foreach (var gm in managers)
                        SendEmail.DraftStart(gm);

                    DraftBtn.Visibility = Visibility.Visible;
                    FreeAgencyBtn.Visibility = Visibility.Collapsed;
                }
                else
                    DraftBtn.Visibility = Visibility.Collapsed;

                if (CurrentLeague.CurDate == CurrentLeague.TrainingCampDate)
                {
                    var managers = Database.GetManagers().Where(gm => gm.TeamId > 0 && !gm.ReceivedCampEmail).ToList();
                    foreach (var gm in managers)
                        SendEmail.CampStart(gm);
                }

                if (CurrentLeague.CurDate == CurrentLeague.InSeasonDate)
                {
                    CurrentLeague.IsOffSeason = false;
                    FreeAgencyBtn.Visibility = Visibility.Collapsed;

                    var managers = Database.GetManagers().Where(gm => gm.TeamId > 0 && !gm.ReceivedSeasonStart).ToList();
                    foreach (var gm in managers)
                        SendEmail.RegularSeasonStart(gm);
                }

                Database.SaveLeague(CurrentLeague);
                NotifyChange("DateText");
                NotifyChange("EmailsCount");
            });
        
        }

        private async Task AdvanceMultipleDays()
        {
            await App.ActivateLoading();
            try
            {
                await Dispatcher.Invoke(async () =>
                {
                    var days = new SimToDate().ShowDialog();
                    if (days.HasValue && days.Value)
                    {
                        while (CurrentLeague.CurDate != ToDate)
                        {
                            await Task.Run(AdvanceDay);
                        }

                        MainFrame.Refresh();
                        if (MainFrame.Content.ToString() == "IndoorFootballManager.Controls.ScheduleCalendar")
                            MainFrame.Navigate(new ScheduleCalendar());
                    }
                });
            }
            catch(Exception x)
            {
                Logger.LogException(x);
            }
            finally
            {
                await SimCompleted();

                await App.DeactivateLoading();
            }
        }

        private async Task AdvanceToEvent()
        {
            await App.ActivateLoading();
            try
            {
                var nextEvent = new DateTime();
                var gameDates = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => !g.HasPlayed).Select(g => g.GameDate).ToList();
                var allDates = gameDates.Concat(CurrentLeague.ImportantDates).ToList();

                while (CurrentLeague.CurDate != nextEvent)
                {
                    if (!_hasAdvanced)
                        break;

                    if (CurrentLeague.CurDate != CurrentLeague.SeasonEndDate)
                    {
                        nextEvent = allDates.Where(l => l.Date > CurrentLeague.CurDate).OrderBy(l => l.Date).First();
                        await Task.Run(AdvanceDay);
                    }
                    else
                    {
                        await Task.Run(AdvanceDay);
                    }
                }

                Dispatcher.Invoke(() =>
                {
                    MainFrame.Refresh();
                    if (MainFrame.Content.ToString() == "IndoorFootballManager.Controls.ScheduleCalendar")
                        MainFrame.Navigate(new ScheduleCalendar());
                });
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
            finally
            {
                await SimCompleted();

                await App.DeactivateLoading();
            }
        }

        public void ClearHistory(Frame frame)
        {
            if (!frame.CanGoBack && !frame.CanGoForward)
            {
                return;
            }
            var entry = frame.RemoveBackEntry();
            while (entry != null)
            {
                entry = frame.RemoveBackEntry();
            }

            GC.Collect();
            frame.Navigate(new PageFunction<string> { RemoveFromJournal = true });
        }

        private async void TradeBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TradeConsole());

        private async void HomeWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.T && Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftShift))
            {
                await NavigateToPage(new TestScreen());
            }
        }

        private async void ChangeFormatBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new ChangeFormat());
    }
}

﻿using IndoorFootballManager.Models;
using System;
using System.Windows;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for ResignPlayer.xaml
    /// </summary>
    public partial class ResignPlayer : Window
    {
        public ResignPlayer()
        {
            InitializeComponent();
        }

        public ResignPlayer(Player p)
        {
            InitializeComponent();
            Player = p;
        }

        private League CurrentLeague => IFM.CurrentLeague;
        private Player Player { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(Player.Team.PrimaryColor);
            YearsSelect.Maximum = CurrentLeague.MaximumYears;
            if (Player.WillNotResign)
            {
                OfferConBtn.IsEnabled = false;
                ReqOfferBtn.IsEnabled = false;
                SalaryTxt.IsEnabled = false;
                YearsSelect.IsEnabled = false;
            }
        }

        private void OfferConBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Player != null)
            {
                if (SalaryTxt.Text.IsNullOrEmpty() || YearsSelect.Text.IsNullOrEmpty())
                {
                    if (SalaryTxt.Text.IsNullOrEmpty())
                    {
                        SalaryTxt.Text = Player.GetNegotiatedSalary.ToString("C0");
                    }

                    if (YearsSelect.Text.IsNullOrEmpty())
                    {
                        YearsSelect.Text = Player.DesiredYears.ToString("N0");
                    }

                    App.ShowNotification("Warning", "Please enter both the salary and years of the contract extension.", Notification.Wpf.NotificationType.Warning, 5);
                }
                else
                {
                    var offeredSalary = Convert.ToDouble(SalaryTxt.Text.Replace("$", "").Replace(",", "").Replace(".", ""));
                    if (YearsSelect.Value < 1 || YearsSelect.Value > CurrentLeague.MaximumYears)
                    {
                        if (YearsSelect.Value < 1)
                            App.ShowNotification("Error", "You must offer at least 1 year to the player.",Notification.Wpf.NotificationType.Error, 5);
                        else if (YearsSelect.Value > CurrentLeague.MaximumYears)
                            App.ShowNotification("Error", $"You may not offer more than {CurrentLeague.MaximumYears} years to the player.", Notification.Wpf.NotificationType.Error, 5);
                    }
                    else if (offeredSalary < CurrentLeague.MinimumSalary || offeredSalary > CurrentLeague.MaximumSalary)
                    {
                        if (offeredSalary < CurrentLeague.MinimumSalary)
                            App.ShowNotification("Error", "Your offer may not be less than the league's minimum salary.", Notification.Wpf.NotificationType.Error, 5);
                        else if (offeredSalary > CurrentLeague.MaximumSalary)
                            App.ShowNotification("Error", "Your offer may not be greater than the league's maximum salary.", Notification.Wpf.NotificationType.Error, 5);
                    }
                    else
                    {
                        if (Player.SignAttempts == 7)
                        {
                            Player.WillNotResign = true;
                            OfferConBtn.IsEnabled = false;
                            SalaryTxt.IsEnabled = false;
                            YearsSelect.IsEnabled = false;
                            ReqOfferBtn.IsEnabled = false;
                            App.ShowNotification("Refusal to Sign", "I cannot sign with a team that can't get close to my request.\nI'll be testing Free Agency.", Notification.Wpf.NotificationType.Information, 5);
                        }

                        Player.SignAttempts += 1;
                        Player.NegotiateContract(Convert.ToInt32(SalaryTxt.Text.Replace(",","").Replace("$", "")), Convert.ToInt32(YearsSelect.Text));
                        Repo.Database.SavePlayer(Player);

                        if (Player.ExtensionSalary == Convert.ToInt32(offeredSalary) && Player.ExtensionYears == Convert.ToInt32(YearsSelect.Text))
                        {
                            SalaryTxt.Clear(); SalaryTxt.IsEnabled = false;
                            YearsSelect.Value = 0; YearsSelect.IsEnabled = false;
                            OfferConBtn.IsEnabled = false; ReqOfferBtn.IsEnabled = false;
                            DialogResult = true;
                            this.Close();
                        }
                    }
                }
            }

            Repo.Database.SavePlayer(Player);
        }

        private void ReqOfferBtn_Click(object sender, RoutedEventArgs e)
        {
            ReqOfferBtn.IsEnabled = false;
            var requestedSalary = Player.GetNegotiatedSalary.ToString("C0");
            var requestedYears = Player.DesiredYears.ToString("N0");
            SalaryTxt.Text = requestedSalary;
            YearsSelect.Text = requestedYears;
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

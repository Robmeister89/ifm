﻿using IndoorFootballManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for RosterCount.xaml
    /// </summary>
    public partial class RosterCount : Window
    {
        public RosterCount()
        {
            InitializeComponent();
        }

        public RosterCount(Team t)
        {
            InitializeComponent();
            Team = t;
        }

        private Team Team { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if(Team != null)
            {
                BkColor.Color = (Color)ColorConverter.ConvertFromString(Team.SecondaryColor);
                var positionCounts = new List<PositionCount>();
                for(var i = 0; i < 8; i++)
                {
                    var pos = Positions[i];
                    var pc = new PositionCount()
                    {
                        Position = pos,
                        Count = Team.ActiveRoster.Count(p => p.Pos == pos),
                        Minimum = (int)Enum.Parse(typeof(MinPositions),pos)
                    };
                    positionCounts.Add(pc);
                }
                TeamRosterCount.ItemsSource = positionCounts;
            }
        }

        private enum MinPositions
        {
            QB = 2,
            RB = 3,
            WR = 4,
            OL = 4,
            DL = 4,
            LB = 3,
            DB = 4,
            K = 1
        }

        private List<string> Positions
        {
            get
            {
                var positions = new List<string>()
                {
                    "QB", "RB", "WR", "OL", "DL", "LB", "DB", "K"
                };
                return positions;
            }
        }

        private class PositionCount
        {
            public string Position { get; set; }
            public int Count { get; set; }
            public int Minimum { get; set; }
        }

        private void TeamRosterCount_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var item = e.Row;
            var posCount = item.Item as PositionCount;
            if(posCount.Count < posCount.Minimum)
            {
                e.Row.Background = new SolidColorBrush(Color.FromRgb(225, 0, 0));
            }

        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

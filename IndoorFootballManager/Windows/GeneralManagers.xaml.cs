﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for GeneralManagers.xaml
    /// </summary>
    public partial class GeneralManagers : Window
    {
        public GeneralManagers()
        {
            InitializeComponent();
        }

        public GeneralManagers(GeneralManager userManager)
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
            UserManager = userManager;
        }

        private static League CurrentLeague => IFM.CurrentLeague;
        private static SQLiteConnection Database => Repo.Database;
        public static GeneralManager UserManager { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CreateGM.IsEnabled = false;
            LoginBtn.IsEnabled = false;
            DeleteGM.IsEnabled = false;
            EditBtn.IsEnabled = false;
            var managers = Database.GetManagers().OrderBy(g => g.LastName);
            ManagersList.ItemsSource = managers;
            ManagersList.DisplayMemberPath = "FullName";

            if (UserManager == null) 
                return;
            
            if (UserManager.IsCommissioner)
            {
                CreateGM.IsEnabled = true;
                if(Database.GetManagers().Count() > 1)
                    DeleteGM.IsEnabled = true;
            }
            else
                CreateGM.IsEnabled = false;
        }

        private void ChooseGM_Click(object sender, RoutedEventArgs e)
        {
            if (ManagersList.SelectedItem is GeneralManager gm)
            {
                var decryptedPassword = Converters.EncryptDecrypt(gm.Password, 256);
                if (PasswordTxt.Text == decryptedPassword || PasswordTxt.Text == CurrentLeague.SystemPassword)
                {
                    IFM.UserManager = gm;
                    DialogResult = true;
                    Close();
                }
                else
                    App.ShowNotification("Error", "Password incorrect.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e) => Close();

        private void DeleteGM_Click(object sender, RoutedEventArgs e)
        {
            if (ManagersList.SelectedItem is GeneralManager selectedManager && ManagersList.Items.Count > 1)
            {
                var selectedId = selectedManager.Id;
                var ifmId = IFM.UserManager.Id;

                // this isn't working.. manager can still be deleted if its the active one...
                if (selectedId == ifmId)
                    App.ShowNotification("Error", "You cannot delete the active GM.", Notification.Wpf.NotificationType.Error, 5);
                else
                {
                    Database.Delete<GeneralManager>(selectedManager.Id);
                    ManagersList.ItemsSource = Database.GetManagers().OrderBy(g => g.LastName);
                    if (ManagersList.Items.Count == 1)
                        DeleteGM.IsEnabled = false;
                }
            }
            else
                App.ShowNotification("Error", "You cannot delete this Manager.", Notification.Wpf.NotificationType.Error, 5);
        }

        private void ManagersList_MouseDoubleClick(object sender, MouseButtonEventArgs e) => ChooseGM_Click(sender, e);

        private void ManagersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ManagersList.SelectedItem == null) 
                return;

            UserManager = ManagersList.SelectedItem as GeneralManager;
            LoginBtn.IsEnabled = true;

            if (IFM.UserManager == null || !IFM.UserManager.IsCommissioner) 
                return;

            EditBtn.IsEnabled = true;
            switch (ManagersList.Items.Count > 1)
            {
                case true when UserManager != null && UserManager.Id == IFM.UserManager.Id:
                    DeleteGM.IsEnabled = false;
                    break;
                case true when UserManager != null && UserManager.Id != IFM.UserManager.Id:
                    DeleteGM.IsEnabled = true;
                    break;
            }
        }

        private void CreateGM_Click(object sender, RoutedEventArgs e)
        {
            Hide();

            var create = new CreateGm().ShowDialog();
            if (create.HasValue && create.Value)
            {
                var managers = Database.GetManagers().ToList();
                ManagersList.ItemsSource = managers.OrderBy(g => g.LastName);
                ManagersList.DisplayMemberPath = "FullName";
                if (managers.Count > 1)
                    DeleteGM.IsEnabled = true;
            }

            ShowDialog();
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            var edit = new EditGm(UserManager).ShowDialog();
            if (edit.HasValue && edit.Value)
            {
                ShowDialog();
                ManagersList.ItemsSource = Database.GetManagers();
                ManagersList.DisplayMemberPath = "FullName";
            }
            else
                ShowDialog();
        }

        private void PasswordTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) 
                ChooseGM_Click(sender, e);
        }

    }
}

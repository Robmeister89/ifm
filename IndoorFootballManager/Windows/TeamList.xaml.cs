﻿using IndoorFootballManager.DataAccess;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for TeamList.xaml
    /// </summary>
    public partial class TeamList : Window
    {
        public TeamList()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private League CurrentLeague => IFM.CurrentLeague;
        
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TeamsListBox.ItemsSource = Repo.Database.GetTeams().OrderBy(t => t.TeamName);
            TeamsListBox.DisplayMemberPath = "TeamName";
        }

        private void SelectBtn_Click(object sender, RoutedEventArgs e)
        {
            var team = TeamsListBox.SelectedItem as Team;
            IFM.TeamListSelect = team;
            DialogResult = true;
            this.Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TeamList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(TeamsListBox.SelectedItem != null)
            {
                SelectBtn_Click(sender, e);
            }
        }

    }
}

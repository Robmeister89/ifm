﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for ImportStatus.xaml
    /// </summary>
    public partial class ImportStatus : Window
    {
        public ImportStatus()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private static League CurrentLeague => IFM.CurrentLeague;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (var i = 0; i < 3; i++)
            {
                var col = new ColumnDefinition { Width = new GridLength(i == 0 ? 125 : 100, GridUnitType.Star) };
                TeamStatusesGrid.ColumnDefinitions.Add(col);
            }

            var row0 = new RowDefinition { Height = GridLength.Auto };
            TeamStatusesGrid.RowDefinitions.Add(row0);

            var alternate = true;
            var teams = Repo.Database.GetTeams().OrderBy(t => t.City).ToList();

            var headTxt1 = new TextBlock
            {
                Background = Brushes.LightGray,
                Text = "Team Name",
                TextAlignment = TextAlignment.Center,
                FontSize = 12,
                Foreground = Brushes.Black,
                FontWeight = FontWeights.SemiBold
            };
            Grid.SetRow(headTxt1, 0);
            Grid.SetColumn(headTxt1, 0);
            TeamStatusesGrid.Children.Add(headTxt1);

            var headTxt2 = new TextBlock
            {
                Background = Brushes.LightGray,
                Text = "Export Status",
                TextAlignment = TextAlignment.Center,
                FontSize = 12,
                Foreground = Brushes.Black,
                FontWeight = FontWeights.SemiBold
            };
            Grid.SetRow(headTxt2, 0);
            Grid.SetColumn(headTxt2, 1);
            TeamStatusesGrid.Children.Add(headTxt2);

            var headTxt3 = new TextBlock
            {
                Background = Brushes.LightGray,
                Text = "Export Date/Time",
                TextAlignment = TextAlignment.Center,
                FontSize = 12,
                Foreground = Brushes.Black,
                FontWeight = FontWeights.SemiBold
            };
            Grid.SetRow(headTxt3, 0);
            Grid.SetColumn(headTxt3, 2);
            TeamStatusesGrid.Children.Add(headTxt3);

            for (var i = 0; i < teams.Count; i++)
            {
                alternate = !alternate;

                var team = teams[i];
                var row = new RowDefinition { Height = GridLength.Auto };
                TeamStatusesGrid.RowDefinitions.Add(row);

                var text = new TextBlock
                {
                    Background = alternate ? Brushes.LightGray : Brushes.GhostWhite,
                    Text = team.TeamName,
                    TextAlignment = TextAlignment.Center,
                    FontSize = 12,
                    Foreground = Brushes.Black,
                };

                Grid.SetRow(text, i + 1);
                Grid.SetColumn(text, 0);
                TeamStatusesGrid.Children.Add(text);

                var text2 = new TextBlock
                {
                    Background = alternate ? Brushes.LightGray : Brushes.GhostWhite,
                    Text = team.ExportStatus,
                    TextAlignment = TextAlignment.Center,
                    FontSize = 12,
                    Foreground = Brushes.Black,
                };

                Grid.SetRow(text2, i + 1);
                Grid.SetColumn(text2, 1);
                TeamStatusesGrid.Children.Add(text2);

                var text3 = new TextBlock
                {
                    Background = alternate ? Brushes.LightGray : Brushes.GhostWhite,
                    Text = team.ExportTime,
                    TextAlignment = TextAlignment.Center,
                    FontSize = 12,
                    Foreground = Brushes.Black,
                };

                Grid.SetRow(text3, i + 1);
                Grid.SetColumn(text3, 2);
                TeamStatusesGrid.Children.Add(text3);
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

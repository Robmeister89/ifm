﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using System.ComponentModel;
using System.Xml;
using ookii = Ookii.Dialogs.Wpf;
using SQLite;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using Notification.Wpf;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for CreateNewLeague.xaml
    /// </summary>
    public partial class CreateNewLeague : Window
    {
        private static readonly NotificationManager _notificationManager = new NotificationManager();
        public CreateNewLeague()
        {
            InitializeComponent(); 
            SettingsGrid.Visibility = Visibility.Visible;
            TeamsGrid.Visibility = Visibility.Hidden;
            CustomGrid.Visibility = Visibility.Hidden;
            PrevBtn.IsEnabled = false;

        }

        private readonly ookii.ProgressDialog ProgressDialog = new ookii.ProgressDialog()
        {
            WindowTitle = "Generate League",
            Text = "Generating the league you've designed...",
            ShowTimeRemaining = false,
            ShowCancelButton = false,
            ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar,
            MinimizeBox = false,

        };

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            StartSeasonTxt.Text = DateTime.Now.Year.ToString();
            for (var i = 0; i < 19; i++)
            {
                var row0 = new RowDefinition();
                row0.Height = new GridLength(30);
                SetupGrid.RowDefinitions.Add(row0);
            }
        }

        private void PrevBtn_Click(object sender, RoutedEventArgs e)
        {
            if (TeamsGrid.Visibility == Visibility.Visible)
            {
                TeamsGrid.Visibility = Visibility.Hidden;
                SettingsGrid.Visibility = Visibility.Visible;
                PrevBtn.IsEnabled = false;
            }
            else if (CustomGrid.Visibility == Visibility.Visible)
            {
                CustomGrid.Visibility = Visibility.Hidden;
                TeamsGrid.Visibility = Visibility.Visible;
                NextBtn.IsEnabled = true;
            }
        }

        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            if(SettingsGrid.Visibility == Visibility.Visible)
            {
                SettingsGrid.Visibility = Visibility.Hidden;
                TeamsGrid.Visibility = Visibility.Visible;
                PrevBtn.IsEnabled = true;
                NextBtn.IsEnabled = true;
            }
            else if(TeamsGrid.Visibility == Visibility.Visible)
            {
                TeamsGrid.Visibility = Visibility.Hidden;
                CustomGrid.Visibility = Visibility.Visible;
                NextBtn.IsEnabled = false;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            void yesCancel()
            {
                DialogResult = false;
                Close();
            }

            void noCancel() { return; }

            App.ShowMessageBox("Cancel Setup", "Are you sure you wish to cancel?", NotificationType.Notification, "CreateLeagueArea", "Yes", yesCancel, "No", noCancel);
        }
        
        private void FinishBtn_Click(object sender, RoutedEventArgs e)
        {
            Teams_ = new List<Team>();
            LeagueName = LeagueNameTxt.Text;
            LeagueAbbr = AbbreviationTxt.Text;
            Season = Convert.ToInt32(StartSeasonTxt.Text);

            if (CustomRosters && RosterFilePath == null)
                ShowNotification("Error", "Please choose a roster file if you're going to use custom players.", NotificationType.Error, 5);
            else
            {
                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{LeagueAbbr}";
                var doesExist = Directory.Exists(path);

                if (doesExist)
                {
                    ShowNotification("Error",
                        Globals.LeagueAbbreviation != Properties.Settings.Default.LastSaved
                            ? "Please delete the existing league to use this abbreviation."
                            : "You cannot overwrite the most recently saved league.\nPlease use a different Abbreviation.",
                        NotificationType.Error, 5);

                    return;
                }

                if (Conferences == 1) { SetTeams(); }
                else { SetTeams(); SetTeams2(); }
                
                var playoffTeams = Convert.ToInt32(PlayoffTeamsCbox.Text);
                if (playoffTeams > Teams_.Count)
                {
                    ShowNotification("Error", "You cannot have more playoff teams than teams.", NotificationType.Error, 5);
                    return;
                }

                NewLeague = new League
                {
                    LeagueName = LeagueName,
                    Abbreviation = LeagueAbbr,
                    Season = Season,
                    Conferences = Conferences,
                    Divisions = Divisions,
                    CurrentSeason = Season,
                    LeagueTeams = Teams_.Count,
                    SalaryCap = Convert.ToInt32(SalaryCapTxt.Text.Replace(",", "").Replace("$", "").Replace(".", "")),
                    MinimumSalary = Convert.ToInt32(MinSalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", "")),
                    MaximumSalary = Convert.ToInt32(MaxSalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", "")),
                    MaximumYears = Convert.ToInt32(MaxContractYearsCbox.Text.Substring(0, 1)),
                    MaxDraftSalary = Convert.ToInt32(Convert.ToDouble(MaxSalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", "")) * 0.64652165),
                    PrimaryColor = PrimaryColor.SelectedColorText,
                    SecondaryColor = SecondaryColor.SelectedColorText,
                    DraftRounds = Convert.ToInt32(DraftRoundsCbox.Text.Substring(0, 1)),
                    MaxRoster = Convert.ToInt32(RosterSizeCbox.Text),
                    ReserveRoster = Convert.ToInt32(ReserveSizeCbox.Text),
                    PlayoffTeams = playoffTeams,
                    UseFTP = (bool)UseFtpChk.IsChecked,
                    UseInjuries = (bool)UseInjuriesChk.IsChecked,
                    PenaltyMod = 4,
                    IsInauguralDraft = true,
                    IsOffSeason = true,
                    IsPlayoffs = false,
                    EndPlayoffs = false,
                };

                SetConferencesAndDivisions(NewLeague);

                Opacity = 0.01;
                ProgressDialog.DoWork += SaveLeague;
                ProgressDialog.RunWorkerCompleted += PostLeagueCreation;
                ProgressDialog.ShowDialog();
            }
        }

        private async void PostLeagueCreation(object sender, RunWorkerCompletedEventArgs e)
        {
            await App.DeactivateLoading();
            DialogResult = true;
            Close();
        }

        private bool CustomRosters { get; set; } = false;
        private int Conferences { get; set; }
        private int Divisions { get; set; }
        private int Teams { get; set; }

        private Label col0_label;
        private Label col1_label;
        private Label col2_label;
        private Label id_label;
        private TextBox conf_tbox;
        private TextBox conf2_tbox;
        private TextBox div_tbox;
        private ComboBox team_cbox;
        private int teamid;
        private int div;

        private void AddConferences(int confs)
        {
            SetupGrid.ColumnDefinitions.Clear();
            var col0 = new ColumnDefinition() { Width = new GridLength(40) }; var col1 = new ColumnDefinition() { Width = new GridLength(140) };
            var col2 = new ColumnDefinition() { Width = new GridLength(140) }; var col3 = new ColumnDefinition() { Width = new GridLength(40) };
            var col4 = new ColumnDefinition() { Width = new GridLength(140) }; var col5 = new ColumnDefinition() { Width = new GridLength(140) };
            var col6 = new ColumnDefinition() { Width = new GridLength(40) }; var col7 = new ColumnDefinition() { Width = new GridLength(140) };
            var col8 = new ColumnDefinition() { Width = new GridLength(140) }; var col9 = new ColumnDefinition() { Width = new GridLength(40) };
            var col10 = new ColumnDefinition() { Width = new GridLength(140) }; var col11 = new ColumnDefinition() { Width = new GridLength(140) };

            SetupGrid.ColumnDefinitions.Add(col0); SetupGrid.ColumnDefinitions.Add(col1); SetupGrid.ColumnDefinitions.Add(col2);
            SetupGrid.ColumnDefinitions.Add(col3); SetupGrid.ColumnDefinitions.Add(col4); SetupGrid.ColumnDefinitions.Add(col5);
            SetupGrid.ColumnDefinitions.Add(col6); SetupGrid.ColumnDefinitions.Add(col7); SetupGrid.ColumnDefinitions.Add(col8);
            SetupGrid.ColumnDefinitions.Add(col9); SetupGrid.ColumnDefinitions.Add(col10); SetupGrid.ColumnDefinitions.Add(col11);

            if (confs == 1)
            {
                conf_tbox = new TextBox() { Name = "conf1", Text = "Conference 1", Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(conf_tbox, 0); Grid.SetColumn(conf_tbox, 0); Grid.SetColumnSpan(conf_tbox, 3);
                SetupGrid.Children.Add(conf_tbox);
            }
            else
            {
                conf_tbox = new TextBox() { Name = "conf1", Text = "Conference 1", Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(conf_tbox, 0); Grid.SetColumn(conf_tbox, 0); Grid.SetColumnSpan(conf_tbox, 3);

                conf2_tbox = new TextBox() { Name = "conf2", Text = "Conference 2", Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(conf2_tbox, 10); Grid.SetColumn(conf2_tbox, 0); Grid.SetColumnSpan(conf2_tbox, 3);

                SetupGrid.Children.Add(conf_tbox); SetupGrid.Children.Add(conf2_tbox);
            }
            allTeams = new List<int>();
            for (var i = 0; i < 86; i++)
                allTeams.Add(i);
        }

        private void AddDivisions(int divs)
        {
            int max_tcols;
            if (divs == 1) { max_tcols = 0; }
            else if (divs == 2) { max_tcols = 3; }
            else if (divs == 3) { max_tcols = 6; }
            else { max_tcols = 9; }

            for (var i = 0; i <= max_tcols; i++)
            {
                if (i == 1)
                    i = 3;
                if (i == 4)
                    i = 6;
                if (i == 7)
                    i = 9;
                div_tbox = new TextBox() { Name = "div" + div.ToString(), Text = "Division " + div.ToString(), Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(div_tbox, 1); Grid.SetColumn(div_tbox, i); Grid.SetColumnSpan(div_tbox, 3);
                SetupGrid.Children.Add(div_tbox);
                div++;
            }
        }

        private void AddDivisions2(int divs)
        {
            int max_tcols;
            if (divs == 1) { max_tcols = 0; }
            else if (divs == 2) { max_tcols = 3; }
            else if (divs == 3) { max_tcols = 6; }
            else { max_tcols = 9; }

            for (var i = 0; i <= max_tcols; i++)
            {
                if (i == 1)
                    i = 3;
                if (i == 4)
                    i = 6;
                if (i == 7)
                    i = 9;
                div_tbox = new TextBox() { Name = "div" + div.ToString(), Text = "Division " + div.ToString(), Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(div_tbox, 1); Grid.SetColumn(div_tbox, i); Grid.SetColumnSpan(div_tbox, 3);
                SetupGrid.Children.Add(div_tbox);
                div++;
            }
            for (var i = 0; i <= max_tcols; i++)
            {
                if (i == 1)
                    i = 3;
                if (i == 4)
                    i = 6;
                if (i == 7)
                    i = 9;
                div_tbox = new TextBox() { Name = "div" + div.ToString(), Text = "Division " + div.ToString(), Margin = new Thickness(10, 10, 0, 0) };
                Grid.SetRow(div_tbox, 11); Grid.SetColumn(div_tbox, i); Grid.SetColumnSpan(div_tbox, 3);
                SetupGrid.Children.Add(div_tbox);
                div++;
            }
        }

        List<int> allTeams;

        private void AddTeams(int divs, int teams)
        {
            var r = new Random();
            for (var i = 0; i < divs * 3; i++)
            {
                col0_label = new Label() { Content = "ID", FontWeight = FontWeights.Bold };
                Grid.SetRow(col0_label, 2); Grid.SetColumn(col0_label, i);
                col0_label.HorizontalContentAlignment = HorizontalAlignment.Center; col0_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col0_label);

                col1_label = new Label() { Content = "City", FontWeight = FontWeights.Bold };
                Grid.SetRow(col1_label, 2); Grid.SetColumn(col1_label, i + 1);
                col1_label.HorizontalContentAlignment = HorizontalAlignment.Center; col1_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col1_label);

                col2_label = new Label() { Content = "Mascot", FontWeight = FontWeights.Bold };
                Grid.SetRow(col2_label, 2); Grid.SetColumn(col2_label, i + 2);
                col2_label.HorizontalContentAlignment = HorizontalAlignment.Center; col2_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col2_label);

                for (var j = 3; j < teams + 3; j++)
                {
                    id_label = new Label() { Content = teamid, Margin = new Thickness(10, 2, 0, 2) };
                    Grid.SetRow(id_label, j); Grid.SetColumn(id_label, i);

                    var randIndex = r.Next(0, allTeams.Count() - 1);
                    var randTeam = allTeams[randIndex];
                    allTeams.Remove(randTeam);
                    team_cbox = new ComboBox()
                    {
                        ItemsSource = AllTeams,
                        Name = "team" + teamid.ToString(),
                        SelectedIndex = randTeam,
                        Margin = new Thickness(10, 10, 0, 0),
                        Tag = team_cbox,
                        DisplayMemberPath = "TeamName",
                    };
                    Grid.SetRow(team_cbox, j); Grid.SetColumn(team_cbox, i + 1); Grid.SetColumnSpan(team_cbox, 2);

                    SetupGrid.Children.Add(team_cbox);
                    SetupGrid.Children.Add(id_label);
                    teamid++;
                }
                i += 2;
            }
        }

        private void AddTeams2(int divs, int teams)
        {
            var r = new Random();

            for (var i = 0; i < divs * 3; i++)
            {
                col0_label = new Label() { Content = "ID", FontWeight = FontWeights.Bold };
                Grid.SetRow(col0_label, 12); Grid.SetColumn(col0_label, i);
                col0_label.HorizontalContentAlignment = HorizontalAlignment.Center; col0_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col0_label);

                col1_label = new Label() { Content = "City", FontWeight = FontWeights.Bold };
                Grid.SetRow(col1_label, 12); Grid.SetColumn(col1_label, i + 1);
                col1_label.HorizontalContentAlignment = HorizontalAlignment.Center; col1_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col1_label);

                col2_label = new Label() { Content = "Mascot", FontWeight = FontWeights.Bold };
                Grid.SetRow(col2_label, 12); Grid.SetColumn(col2_label, i + 2);
                col2_label.HorizontalContentAlignment = HorizontalAlignment.Center; col2_label.VerticalContentAlignment = VerticalAlignment.Bottom;
                SetupGrid.Children.Add(col2_label);

                for (var j = 13; j < teams + 13; j++)
                {
                    id_label = new Label() { Content = teamid, Margin = new Thickness(10, 2, 0, 2) };
                    Grid.SetRow(id_label, j); Grid.SetColumn(id_label, i);

                    // find a way for force east/west....
                    // find a way to get the first team in the list -- could i use something like if (teams % i == 0) ??
                    // then get a team within a specific distance of that team??

                    var randIndex = r.Next(0, allTeams.Count() - 1);
                    var randTeam = allTeams[randIndex];
                    allTeams.Remove(randTeam);
                    team_cbox = new ComboBox()
                    {
                        ItemsSource = AllTeams,
                        Name = "team" + teamid.ToString(),
                        SelectedIndex = randTeam,
                        Margin = new Thickness(10, 10, 0, 0),
                        Tag = team_cbox,
                        DisplayMemberPath = "TeamName",
                    };
                    allTeams.RemoveAt(randIndex);
                    Grid.SetRow(team_cbox, j); Grid.SetColumn(team_cbox, i + 1); Grid.SetColumnSpan(team_cbox, 2);

                    SetupGrid.Children.Add(team_cbox);
                    SetupGrid.Children.Add(id_label);
                    teamid++;
                }
                i += 2;
            }
        }

        private List<Team> AllTeams
        {
            get
            {
                var teams = new List<Team>();
                var xml = new XmlDocument();
                xml.Load(@"Resources/TeamsWLat.xml");
                var teamNodes = xml.SelectNodes("Teams/Team");
                var id = 0;
                foreach (XmlNode item in teamNodes) 
                {
                    var _city = item.Attributes["City"].Value;
                    var _mascot = item.Attributes["Mascot"].Value;
                    var _abbr = item.Attributes["Abbreviation"].Value;
                    var _prim = item.Attributes["PrimaryColor"].Value;
                    var _sec = item.Attributes["SecondaryColor"].Value;
                    //double _lat = Convert.ToDouble(item.Attributes["Latitude"].Value);
                    //double _long = Convert.ToDouble(item.Attributes["Longitude"].Value);

                    if (_city != "")
                    {
                        teams.Add(
                            new Team()
                            {
                                Id = id++,
                                City = _city,
                                Mascot = _mascot,
                                Abbreviation = _abbr,
                                PrimaryColor = _prim,
                                SecondaryColor = _sec,
                                TeamName = _city + " " + _mascot,
                                //Location = new Coordinate(_lat, _long),
                            });
                    }
                }

                var sortedTeams = new List<Team>(teams.OrderBy(t => t.City));

                /*
                List<Team> teams = new List<Team>();
                XmlDocument xml = new XmlDocument();
                xml.Load(@"Resources/Teams.xml");
                XmlNodeList eastNodes = xml.SelectNodes("Teams/East/Team");
                XmlNodeList westNodes = xml.SelectNodes("Teams/West/Team");

                foreach (XmlNode item in eastNodes)
                {
                    //Get the value of XML fields here
                    string _city = item.Attributes["City"].Value;
                    string _mascot = item.Attributes["Mascot"].Value;
                    string _abbr = item.Attributes["Abbreviation"].Value;
                    string _prim = item.Attributes["PrimaryColor"].Value;
                    string _sec = item.Attributes["SecondaryColor"].Value;

                    if (_city != "")
                    {
                        teams.Add(
                            new Team()
                            {
                                City = _city,
                                Mascot = _mascot,
                                Abbreviation = _abbr,
                                PrimaryColor = _prim,
                                SecondaryColor = _sec,
                                TeamName = _city + " " + _mascot,
                            });
                    }
                }
                foreach (XmlNode item in westNodes)
                {
                    //Get the value of XML fields here
                    string _city = item.Attributes["City"].Value;
                    string _mascot = item.Attributes["Mascot"].Value;
                    string _abbr = item.Attributes["Abbreviation"].Value;
                    string _prim = item.Attributes["PrimaryColor"].Value;
                    string _sec = item.Attributes["SecondaryColor"].Value;

                    if (_city != "")
                    {
                        teams.Add(
                            new Team()
                            {
                                City = _city,
                                Mascot = _mascot,
                                Abbreviation = _abbr,
                                PrimaryColor = _prim,
                                SecondaryColor = _sec,
                                TeamName = _city + " " + _mascot,
                            });
                    }
                }
                List<Team> sortedTeams = new List<Team>(teams.OrderBy(t => t.City));
                */



                return sortedTeams;
            }
        }

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FormatCbox.SelectedIndex != -1)
                {
                    teamid = 1; div = 1;
                    SetupGrid.Children.Clear();
                    var format = FormatCbox.Text.Split('-');
                    var confs = format[0].Substring(0, 1);
                    var divs = format[1].Substring(1, 1);
                    var teams = format[2].Substring(1, 1);
                    Conferences = Convert.ToInt32(confs);
                    Divisions = Convert.ToInt32(divs);
                    Teams = Convert.ToInt32(teams);

                    FinishBtn.IsEnabled = true;
                    AddConferences(Conferences);
                    if (Conferences == 1)
                    {
                        AddDivisions(Divisions);
                        AddTeams(Divisions, Teams);
                    }
                    if (Conferences == 2)
                    {
                        AddDivisions2(Divisions);
                        AddTeams(Divisions, Teams);
                        AddTeams2(Divisions, Teams);
                    }
                }
                else
                    ShowNotification("Warning", "Please select a league format to generate.", NotificationType.Warning, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                ShowNotification("Error", "Something went wrong...\nCheck Documents folder for log file.", NotificationType.Error, 10);
            }
        }

        private List<Team> Teams_;
        private string LeagueName;
        private string LeagueAbbr;
        private int Season;
        private readonly Generation generation = new Generation();
        private League NewLeague;
        private Label IdLabel;
        private TextBox Conference;
        private TextBox Division;
        private SQLiteConnection db;

        private async void SaveLeague(object sender, DoWorkEventArgs e)
        {
            await App.ActivateLoading();
            db = Repo.Create(LeagueAbbr);

            Globals.LeagueAbbreviation = NewLeague.Abbreviation;
            SetDraftDate(NewLeague);
            db.SaveLeague(NewLeague);
            IFM.CurrentLeague = NewLeague;
            var picks = new List<DraftPick>();
            var strats = new List<Strategy>();
            var charts = new List<DepthChart>();
            var rounds = NewLeague.MaxRoster + NewLeague.ReserveRoster;
            var rounds_ = NewLeague.DraftRounds;
            foreach (var t in Teams_)
            {
                t.IsCpu = true;
                t.Wins = 0;
                t.Losses = 0;
                t.Logo = Converters.SaveFromFile($@"Images/Logos/{t.Mascot}.png");
                t.OffPhilosophy = 0;
                t.CurDate = NewLeague.CurDate;
                picks.AddRange(t.InauguralDraftPicks(rounds));
                picks.AddRange(t.DraftPicks(rounds_));
                strats.Add(BaseStrategy(t.Id));
                charts.Add(new DepthChart() { TeamId = t.Id });
            }
            db.InsertAll(Teams_.AsEnumerable());
            db.InsertAll(strats.AsEnumerable());
            db.InsertAll(charts.AsEnumerable());
            db.InsertAll(picks.AsEnumerable());
            Draft.CreateInauguralDraft();
            if (CustomRosters)
                ImportPlayers(RosterFilePath);
            else
            {
                players = new List<Player>();
                players.AddRange(generation.GeneratePlayers(NewLeague.LeagueTeams));
            }
            db.InsertAll(players.AsEnumerable());
            generation.Players.Clear();
            players.Clear();
            var format = $"{Conferences}-{Divisions}-{Teams}";
            var mod = (Season % 2).ToString();
            var schedule = ParseSchedule($@"Resources/Schedules/{format}-schedule-{mod}.csv");
            SetGameDates(schedule.AsEnumerable());

            var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{LeagueAbbr}\{LeagueAbbr}";
            var livePath = $"{path}.ifm";
            var backupPath = $"{path}.bac";
            File.Copy(livePath, backupPath, true);
        }

        private List<Player> players;

        public void ImportPlayers(string filePath)
        {
            var id = 1;
            using (var sr = new StreamReader(filePath))
            {
                players = new List<Player>();
                var header = sr.ReadLine();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var row = line.Split(',');
                    var p = new Player()
                    {
                        Id = id++,
                        Jersey = Convert.ToInt32(row[1]),
                        FirstName = row[2],
                        LastName = row[3],
                        TeamId = Convert.ToInt32(row[4]),
                        Pos = row[5],
                        DOB = Convert.ToDateTime(row[6]),
                        Exp = Convert.ToInt32(row[7]),
                        Accuracy = Convert.ToInt32(row[8]),
                        ArmStrength = Convert.ToInt32(row[9]),
                        Carry = Convert.ToInt32(row[10]),
                        Speed = Convert.ToInt32(row[11]),
                        Strength = Convert.ToInt32(row[12]),
                        Catching = Convert.ToInt32(row[13]),
                        Block = Convert.ToInt32(row[14]),
                        Blitz = Convert.ToInt32(row[15]),
                        Coverage = Convert.ToInt32(row[16]),
                        Tackle = Convert.ToInt32(row[17]),
                        Intelligence = Convert.ToInt32(row[18]),
                        KickStrength = Convert.ToInt32(row[19]),
                        KickAccuracy = Convert.ToInt32(row[20]),
                        Attitude = Convert.ToInt32(row[21]),
                        Greed = Convert.ToInt32(row[22]),
                        Loyalty = Convert.ToInt32(row[23]),
                        Injury = Convert.ToInt32(row[24]),
                        Leadership = Convert.ToInt32(row[25]),
                        Passion = Convert.ToInt32(row[26]),
                        WorkEthic = Convert.ToInt32(row[27]),
                        College = row[30],
                    };

                    if (p.TeamId > 0)
                    {
                        p.ContractYears = Convert.ToInt32(row[28]);
                        p.Salary = Convert.ToInt32(row[29]);
                    }
                    else
                    {
                        p.TeamId = 0;
                        p.ContractYears = 0;
                        p.Salary = 0;
                    }
                    p.Overall = p.GetOverall;
                    players.Add(p);
                }
            }
        }

        private List<Game> ParseSchedule(string filePath)
        {
            var text = File.ReadAllLines(filePath);
            var fields = new string[text.Count(), 3];
            var r = 0;

            for (var l = 0; l < text.Count(); l++)
            {
                var text_ = text[l].Split(',');
                fields[l, r] = text_[0];
                r++;
                fields[l, r] = text_[1];
                r = 0;
            }

            var games = new List<Game>();
            Game g;
            for (var k = 1; k < text.Count(); k++)
            {
                g = new Game()
                {
                    Season = Season,
                    HomeTeam = Convert.ToInt32(fields[k, 0]),
                    AwayTeam = Convert.ToInt32(fields[k, 1]),
                };
                games.Add(g);
            }
            return games;
        }

        private void SetDraftDate(League league)
        {
            var day1 = new DateTime(Season, 3, 1);
            for (var i = 0; i < 7; i++)
            {
                if (day1.AddDays(i).DayOfWeek == 0)
                {
                    day1 = day1.AddDays(i);
                    break;
                }
            }
            league.DraftDate = day1;
            league.TrainingCampDate = day1.AddDays(21);
            league.CurDate = day1;
        }

        private DateTime gameDate;
        private void SetGameDates(IEnumerable<Game> games)
        {
            var day1 = new DateTime(Season, 4, 1);
            for (var i = 0; i < 7; i++)
            {
                if (day1.AddDays(i).DayOfWeek == 0)
                {
                    day1 = day1.AddDays(i);
                    break;
                }
            }
            NewLeague.InSeasonDate = day1.AddDays(14);
            gameDate = day1.AddDays(21);
            var gamesCount = games.Count() / (NewLeague.LeagueTeams / 2);
            var gameIndex = 0;
            var seasonEnd = new DateTime();
            for (var i = 0; i < gamesCount; i++)
            {
                for (var j = 0; j < NewLeague.LeagueTeams / 2; j++)
                {
                    var g = games.ElementAt(gameIndex);
                    g.GameDate = gameDate;
                    gameIndex++;
                }
                gameDate = gameDate.AddDays(7);
                if (i == gamesCount - 1)
                    seasonEnd = gameDate.AddDays(30);
            }
            db.InsertAll(games);
            NewLeague.SeasonEndDate = seasonEnd;
            db.SaveLeague(NewLeague);
        }


        int TeamId;

        private void SetTeams()
        {
            TeamId = 1;
            Conference = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => Grid.GetRow(x) == 0 && Grid.GetColumn(x) == 0);
            for (var i = 1; i <= (Teams * Divisions); i++)
            {
                IdLabel = SetupGrid.Children.OfType<Label>().FirstOrDefault(x => x.Content.ToString() == i.ToString());
                var team = SetupGrid.Children.OfType<ComboBox>().FirstOrDefault(x => x.Name == $"team{i}").SelectedItem as Team;

                var id_col = Grid.GetColumn(IdLabel);
                Division = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => Grid.GetRow(x) == 1 && Grid.GetColumn(x) == id_col);
                var team_ = new Team(TeamId, team.City, team.Abbreviation, team.Mascot, Conference.Text, Division.Text, Season) { PrimaryColor = team.PrimaryColor, SecondaryColor = team.SecondaryColor };
                Teams_.Add(team_);

                TeamId++;
            }

        }

        private void SetTeams2()
        {

            Conference = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => Grid.GetRow(x) == 10 && Grid.GetColumn(x) == 0);
            for (var i = TeamId; i <= (Teams * Divisions * 2); i++)
            {
                IdLabel = SetupGrid.Children.OfType<Label>().FirstOrDefault(x => x.Content.ToString() == i.ToString());
                var team = SetupGrid.Children.OfType<ComboBox>().FirstOrDefault(x => x.Name == $"team{i}").SelectedItem as Team;

                var id_col = Grid.GetColumn(IdLabel);
                Division = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => Grid.GetRow(x) == 11 && Grid.GetColumn(x) == id_col);
                var team_ = new Team(TeamId, team.City, team.Abbreviation, team.Mascot, Conference.Text, Division.Text, Season) { PrimaryColor = team.PrimaryColor, SecondaryColor = team.SecondaryColor };
                Teams_.Add(team_);

                TeamId++;
            }

        }

        private void SetConferencesAndDivisions(League League)
        {
            if (Conferences == 2)
            {
                League.Conference1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "conf1").Text;
                League.Conference2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "conf2").Text;
            }
            else
            {
                League.Conference1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "conf1").Text;
            }

            switch (Divisions)
            {
                case 1:
                    if (Conferences == 1)
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                    }
                    else
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                    }
                    break;
                case 2:
                    if (Conferences == 1)
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                    }
                    else
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                        League.Division3 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div3").Text;
                        League.Division4 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div4").Text;
                    }
                    break;
                case 3:
                    if (Conferences == 1)
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                        League.Division3 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div3").Text;
                    }
                    else
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                        League.Division3 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div3").Text;
                        League.Division4 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div4").Text;
                        League.Division5 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div5").Text;
                        League.Division6 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div6").Text;
                    }
                    break;
                case 4:
                    if (Conferences == 1)
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                        League.Division3 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div3").Text;
                        League.Division4 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div4").Text;
                    }
                    else
                    {
                        League.Division1 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div1").Text;
                        League.Division2 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div2").Text;
                        League.Division3 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div3").Text;
                        League.Division4 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div4").Text;
                        League.Division5 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div5").Text;
                        League.Division6 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div6").Text;
                        League.Division7 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div7").Text;
                        League.Division8 = SetupGrid.Children.OfType<TextBox>().FirstOrDefault(x => x.Name == "div8").Text;
                    }
                    break;
            }
        }

        public Strategy BaseStrategy(int id)
        {
            var strat = new Strategy()
            {
                Id = id,
                O1st10 = 60,
                O1stShort = 40,
                O1stLong = 60,
                O2ndShort = 40,
                O2ndLong = 65,
                O3rdShort = 20,
                O3rdLong = 80,
                O4thShort = 0,
                O4thLong = 100,
                OGoalline = 40,
                D1st10 = 60,
                D1stShort = 40,
                D1stLong = 60,
                D2ndShort = 40,
                D2ndLong = 65,
                D3rdShort = 20,
                D3rdLong = 80,
                D4thShort = 0,
                D4thLong = 95,
                DGoalline = 50
            };
            return strat;
        }

        private string RosterFilePath { get; set; }

        private void RosterBtn_Click(object sender, RoutedEventArgs e)
        {
            var fd = new ookii.VistaOpenFileDialog();
            var ofd = fd.ShowDialog();
            if (ofd.HasValue && ofd.Value)
            {
                RosterFilePath = fd.FileName;
                RosterPath.Text = fd.FileName;
            }

        }

        private void CustomRostersChk_Checked(object sender, RoutedEventArgs e)
        {
            CustomRosters = true;
        }

        private void CustomRostersChk_Unchecked(object sender, RoutedEventArgs e)
        {
            CustomRosters = false;
        }

        private void AbbreviationTxt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !char.IsLetterOrDigit(e.Text, e.Text.Length - 1);
        }

        private void AbbreviationTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var abbr = AbbreviationTxt.Text;
                AbbreviationTxt.Text = abbr.Substring(0, abbr.Length - 1);
                AbbreviationTxt.SelectionStart = abbr.Length;
            }
        }

        public static void ShowNotification(string title, string msg, NotificationType type, double time)
        {
            var content = new NotificationContent
            {
                Title = title,
                Message = msg,
                Type = type
            };
            _notificationManager.Show(content, areaName: "CreateLeagueArea", expirationTime: TimeSpan.FromSeconds(time));
        }

    }
}

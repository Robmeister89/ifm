﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using SQLite;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for CreateGM.xaml
    /// </summary>
    public partial class CreateGm : Window
    {
        public CreateGm()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private static SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;
        public static GeneralManager NewGm { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            AgeSelect.SelectedDate = CurrentLeague.CurDate.AddYears(-30);
            if (!Database.GetManagers().Any())
                CancelBtn.IsEnabled = false;

            var teams = Database.GetTeams().Where(t => t.Manager == null).OrderBy(t => t.City).ToList();
            TeamSelect.ItemsSource = teams;
            TeamSelect.DisplayMemberPath = "City";
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            var team = TeamSelect.SelectedItem as Team;
            var commish = (bool)CommishAccess.IsChecked;

            var id = Database.Table<GeneralManager>().Count() != 0 ? Database.Table<GeneralManager>().Last().Id : 0;
            id += 1;

            if (!Database.GetManagers().Any() && !commish)
                App.ShowNotification("Error", "Your first GM must be a commissioner.", Notification.Wpf.NotificationType.Error, 5);
            else
            {
                if (team != null)
                {
                    NewGm = new GeneralManager(id, FNameTxt.Text, LNameTxt.Text, (DateTime)AgeSelect.SelectedDate, Convert.ToInt32(ExpSelect.Text), team.Id, commish);
                    team.IsCpu = false;
                    Database.SaveTeam(team);
                }
                else
                    NewGm = new GeneralManager(id, FNameTxt.Text, LNameTxt.Text, (DateTime)AgeSelect.SelectedDate, Convert.ToInt32(ExpSelect.Text), 0, commish);

                NewGm.Password = Converters.EncryptDecrypt(PasswordTxt.Text, 256);
                Database.SaveManager(NewGm);
                IFM.UserManager = NewGm;

                SendEmail.NewManager(NewGm);
                if (CurrentLeague.CurDate == CurrentLeague.DraftDate)
                    SendEmail.DraftStart(NewGm);
                IFM.NotifyChange("EmailsCount");
                DialogResult = true;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }



    }
}

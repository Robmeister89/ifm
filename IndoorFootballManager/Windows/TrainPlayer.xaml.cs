﻿using System.Windows;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for TrainPlayer.xaml
    /// </summary>
    public partial class TrainPlayer : Window
    {
        public TrainPlayer()
        {
            InitializeComponent();
        }

        public TrainPlayer(Player p)
        {
            InitializeComponent();
            Player = p;
        }

        private Player Player { get; set; }
        private League CurrentLeague => IFM.CurrentLeague;

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            int rating = 0, prev = 0;
            var attribute = RatingSelect.Text;
            switch (attribute)
            {
                case "Pass Accuracy":
                    prev = Player.Accuracy;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Accuracy = Player.TrainingCamp(Player.Accuracy, Player.WorkEthic);
                    else
                        Player.Accuracy = Player.WeeklyTraining(Player.Accuracy, Player.WorkEthic);
                    rating = Player.Accuracy;
                    break;
                case "Pass Strength":
                    prev = Player.ArmStrength;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.ArmStrength = Player.TrainingCamp(Player.ArmStrength, Player.WorkEthic);
                    else
                        Player.ArmStrength = Player.WeeklyTraining(Player.ArmStrength, Player.WorkEthic);
                    rating = Player.ArmStrength;
                    break;
                case "Carry":
                    prev = Player.Carry;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Carry = Player.TrainingCamp(Player.Carry, Player.WorkEthic);
                    else
                        Player.Carry = Player.WeeklyTraining(Player.Carry, Player.WorkEthic);
                    rating = Player.Carry;
                    break;
                case "Speed":
                    prev = Player.Speed;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Speed = Player.TrainingCamp(Player.Speed, Player.WorkEthic);
                    else
                        Player.Speed = Player.WeeklyTraining(Player.Speed, Player.WorkEthic);
                    rating = Player.Speed;
                    break;
                case "Catching":
                    prev = Player.Catching;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Catching = Player.TrainingCamp(Player.Catching, Player.WorkEthic);
                    else
                        Player.Catching = Player.WeeklyTraining(Player.Catching, Player.WorkEthic);
                    rating = Player.Catching;
                    break;
                case "Blocking":
                    prev = Player.Block;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Block = Player.TrainingCamp(Player.Block, Player.WorkEthic);
                    else
                        Player.Block = Player.WeeklyTraining(Player.Block, Player.WorkEthic);
                    rating = Player.Block;
                    break;
                case "Blitzing":
                    prev = Player.Blitz;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Blitz = Player.TrainingCamp(Player.Blitz, Player.WorkEthic);
                    else
                        Player.Blitz = Player.WeeklyTraining(Player.Blitz, Player.WorkEthic);
                    rating = Player.Blitz;
                    break;
                case "Kick Accuracy":
                    prev = Player.KickAccuracy;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.KickAccuracy = Player.TrainingCamp(Player.KickAccuracy, Player.WorkEthic);
                    else
                        Player.KickAccuracy = Player.WeeklyTraining(Player.KickAccuracy, Player.WorkEthic);
                    rating = Player.KickAccuracy;
                    break;
                case "Kick Strength":
                    prev = Player.KickStrength;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.KickStrength = Player.TrainingCamp(Player.KickStrength, Player.WorkEthic);
                    else
                        Player.KickStrength = Player.WeeklyTraining(Player.KickStrength, Player.WorkEthic);
                    rating = Player.KickStrength;
                    break;
                case "Coverage":
                    prev = Player.Coverage;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Coverage = Player.TrainingCamp(Player.Coverage, Player.WorkEthic);
                    else
                        Player.Coverage = Player.WeeklyTraining(Player.Coverage, Player.WorkEthic);
                    rating = Player.Coverage;
                    break;
                case "Tackling":
                    prev = Player.Tackle;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Tackle = Player.TrainingCamp(Player.Tackle, Player.WorkEthic);
                    else
                        Player.Tackle = Player.WeeklyTraining(Player.Tackle, Player.WorkEthic);
                    rating = Player.Tackle;
                    break;
                case "Strength":
                    prev = Player.Strength;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Strength = Player.TrainingCamp(Player.Strength, Player.WorkEthic);
                    else
                        Player.Strength = Player.WeeklyTraining(Player.Strength, Player.WorkEthic);
                    rating = Player.Strength;
                    break;
                case "Intelligence":
                    prev = Player.Intelligence;
                    if (CurrentLeague.IsTrainingCamp)
                        Player.Intelligence = Player.TrainingCamp(Player.Intelligence, Player.WorkEthic);
                    else
                        Player.Intelligence = Player.WeeklyTraining(Player.Intelligence, Player.WorkEthic);
                    rating = Player.Intelligence;
                    break;
            }
            Player.Overall = Player.GetOverall;
            Player.HasTrained = true;
            var result = string.Empty;
            if (rating > prev)
            {
                var playerHistory = Player.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add($"{attribute} improved from {prev} to {rating} (+{rating - prev}) on {CurrentLeague.CurDate.ToLongDateString()}.");
                Player.PlayerHistoryJson = playerHistory.SerializeToJson();
                result = $"{attribute} has increased from {prev} to {rating}.";
            }
            else
                result = $"{attribute} has remained the same at {prev}.";

            Repo.Database.SavePlayer(Player);
            
            SendEmail.IndividualTraining(Player.Team, Player, result);
            IFM.NotifyChange("EmailsCount");

            App.ShowNotification("Player Trained", $"{Player.NameWithPosition} has been trained.", Notification.Wpf.NotificationType.Information, 5);

            DialogResult = true;
            this.Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Services;
using IndoorFootballManager.Models;
using SQLite;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : Window
    {
        public GameView()
        {
            InitializeComponent();
        }

        public GameView(Game g)
        {
            InitializeComponent();
            _game = g;
            DragBar.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(League.PrimaryColor));
        }

        private readonly Game _game;
        private static SQLiteConnection Database => Repo.Database;
        private League League => IFM.CurrentLeague;
        private Team _home;
        private Team _away;
        private List<PlayerGameStats> _gameStats;
        private List<PlayerGameStats> _homeStats;
        private List<PlayerGameStats> _awayStats;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (_game == null) return;
            _home = Database.GetTeamById(_game.HomeTeam);
            _away = Database.GetTeamById(_game.AwayTeam);

            _gameStats = _game.StatsJson.DeserializeToList<PlayerGameStats>();
            _homeStats = _gameStats.Where(g => g.TeamId == _home.Id).ToList();
            _awayStats = _gameStats.Where(g => g.TeamId == _away.Id).ToList();

            HeaderTxt.Text = _game.Display;
            DisplayGame(_game);
            LoadIndividualStats();
        }

        private void DisplayGame(Game game)
        {
            var scores = game.ScoringPlays.Split('\n');
            scores[0] = "TEAM|SCORE|TIME|PLAY|" + _away.Abbreviation + "|" + _home.Abbreviation;
            for (var i = 0; i < 6; i++)
            {
                var col = new ColumnDefinition { Width = new GridLength(43) };
                switch (i)
                {
                    case 2:
                        col.Width = new GridLength(58);
                        break;
                    case 3:
                        col.Width = new GridLength(350);
                        break;
                }

                ScoringGrid.ColumnDefinitions.Add(col);
            }
            
            var alt1 = true;
            for (var i = 0; i < scores.Length; i++)
            {
                var cols = scores[i].Split('|');
                var row = new RowDefinition { Height = GridLength.Auto };
                ScoringGrid.RowDefinitions.Add(row);

                for(var j = 0; j < cols.Length; j++)
                {
                    var text = new TextBlock
                    {
                        Background = alt1 ? Brushes.LightGray : Brushes.GhostWhite,
                        Text = cols[j],
                        TextAlignment = TextAlignment.Center,
                        FontSize = 12,
                        Foreground = Brushes.Black,
                    };
                    if (j == 0)
                        text.FontWeight = FontWeights.DemiBold;
                    Grid.SetRow(text, i);
                    Grid.SetColumn(text, j);
                    ScoringGrid.Children.Add(text);
                }
                alt1 = !alt1;
            }

            var txt = game.PBP.Split('\n');
            for (var i = 0; i < 5; i++)
            {
                var col = new ColumnDefinition { Width = GridLength.Auto };
                if (i == 4)
                    col.Width = new GridLength(0.5, GridUnitType.Star);
                PbpGrid.ColumnDefinitions.Add(col);
            }
            var alt = false;
            for (var i = 0; i < txt.Length; i++)
            {
                var cols = txt[i].Split('|');

                var row = new RowDefinition { Height = GridLength.Auto };
                PbpGrid.RowDefinitions.Add(row);

                if (cols.Length == 2)
                {
                    var newCol1 = cols[0] + " - " + cols[1];
                    cols = newCol1.Split('|');
                }
                for (var j = 0; j < cols.Length; j++)
                {
                    var text = new TextBlock
                    {
                        Background = alt ? Brushes.LightGray : Brushes.GhostWhite,
                        Text = cols[j],
                        TextAlignment = TextAlignment.Left,
                        FontSize = 16,
                        Foreground = Brushes.Black,
                        //Margin = new Thickness(5,0,0,0),
                    };
                    Grid.SetRow(text, i);
                    Grid.SetColumn(text, j);
                    if (cols.Length < 2)
                        Grid.SetColumnSpan(text, 5);
                    PbpGrid.Children.Add(text);
                }
                alt = !alt;
            }

            HomeAbbr.Text = _home.Abbreviation;
            HomeAbbr1.Text = _home.Abbreviation;
            Home1st.Text = game.Home1st.ToString();
            Home2nd.Text = game.Home2nd.ToString();
            Home3rd.Text = game.Home3rd.ToString();
            Home4th.Text = game.Home4th.ToString();
            HomeOT.Text = game.HomeOT.ToString();
            HomeFin.Text = game.HomeFinal.ToString();
            HomeYards.Text = game.HomeTotalYds.ToString();
            HomeRushYards.Text = game.HomeRushYds.ToString();
            HomeRushAtt.Text = game.HomeRushAtt.ToString();
            HomeYpc.Text = ((double)game.HomeRushYds / (double)game.HomeRushAtt).ToString("N1");
            HomePassYards.Text = game.HomePassYds.ToString();
            HomeCompAtt.Text = game.HomePassComp.ToString() + " / " + game.HomePassAtt.ToString();
            HomeTdInt.Text = game.HomePassTd.ToString() + " - " + game.HomeInt.ToString();
            HomeReturns.Text = _homeStats.Sum(p => p.KickReturns).ToString() + " - " + _homeStats.Sum(p => p.KickRetYds).ToString();
            HomeYpr.Text = ((double)_homeStats.Sum(p => p.KickRetYds) / (double)_homeStats.Sum(p => p.KickReturns)).ToString("N1");
            HomeXP.Text = _homeStats.Sum(p => p.XPMade).ToString() + " / " + _homeStats.Sum(p => p.XPAtt).ToString() +
                " (" + ((double)_homeStats.Sum(p => p.XPMade) / (double)_homeStats.Sum(p => p.XPAtt)).ToString("P1") + ")";
            HomeFG.Text = _homeStats.Sum(p => p.FGMade).ToString() + " / " + _homeStats.Sum(p => p.FGAtt).ToString() +
                " (" + ((double)_homeStats.Sum(p => p.FGMade) / (double)_homeStats.Sum(p => p.FGAtt)).ToString("P1") + ")";
            HomePenalties.Text = game.HomePenalties.ToString();
            HomePenYards.Text = game.HomePenaltyYds.ToString();
            Home3rdEff.Text = game.Home3rdDownAtt == 0 ? "0/0 (0.0%)" : game.Home3rdDownComp.ToString() + "/" + game.Home3rdDownAtt.ToString() + " (" + ((double)game.Home3rdDownComp / (double)game.Home3rdDownAtt).ToString("P1") + ")";
            Home4thEff.Text = game.Home4thDownAtt == 0 ? "0/0 (0.0%)" : game.Home4thDownComp.ToString() + "/" + game.Home4thDownAtt.ToString() + " (" + ((double)game.Home4thDownComp / (double)game.Home4thDownAtt).ToString("P1") + ")";
            HomeTOs.Text = (game.HomeFumblesLost + game.HomeInt).ToString();
            HomeTOP.Text = TimeSpan.FromSeconds(game.HomeTOP).ToString(@"mm\:ss");

            AwayAbbr.Text = _away.Abbreviation;
            AwayAbbr1.Text = _away.Abbreviation;
            Away1st.Text = game.Away1st.ToString();
            Away2nd.Text = game.Away2nd.ToString();
            Away3rd.Text = game.Away3rd.ToString();
            Away4th.Text = game.Away4th.ToString();
            AwayOT.Text = game.AwayOT.ToString();
            AwayFin.Text = game.AwayFinal.ToString();
            AwayYards.Text = game.AwayTotalYds.ToString();
            AwayRushYards.Text = game.AwayRushYds.ToString();
            AwayRushAtt.Text = game.AwayRushAtt.ToString();
            AwayYpc.Text = ((double)game.AwayRushYds / (double)game.AwayRushAtt).ToString("N1");
            AwayPassYards.Text = game.AwayPassYds.ToString();
            AwayCompAtt.Text = game.AwayPassComp.ToString() + " / " + game.AwayPassAtt.ToString();
            AwayTdInt.Text = game.AwayPassTd.ToString() + " - " + game.AwayInt.ToString();
            AwayReturns.Text = _awayStats.Sum(p => p.KickReturns).ToString() + " - " + _awayStats.Sum(p => p.KickRetYds).ToString();
            AwayYpr.Text = ((double)_awayStats.Sum(p => p.KickRetYds) / (double)_awayStats.Sum(p => p.KickReturns)).ToString("N1");
            AwayXP.Text = _awayStats.Sum(p => p.XPMade).ToString() + " / " + _awayStats.Sum(p => p.XPAtt).ToString() +
                " (" + ((double)_awayStats.Sum(p => p.XPMade) / (double)_awayStats.Sum(p => p.XPAtt)).ToString("P1") + ")";
            AwayFG.Text = _awayStats.Sum(p => p.FGMade).ToString() + " / " + _awayStats.Sum(p => p.FGAtt).ToString() +
                " (" + ((double)_awayStats.Sum(p => p.FGMade) / (double)_awayStats.Sum(p => p.FGAtt)).ToString("P1") + ")";
            AwayPenalties.Text = game.AwayPenalties.ToString();
            AwayPenYards.Text = game.AwayPenaltyYds.ToString();
            Away3rdEff.Text = game.Away3rdDownAtt == 0 ? "0/0 (0.0%)" : game.Away3rdDownComp.ToString() + "/" + game.Away3rdDownAtt.ToString() + " (" + ((double)game.Away3rdDownComp / (double)game.Away3rdDownAtt).ToString("P1") + ")";
            Away4thEff.Text = game.Away4thDownAtt == 0 ? "0/0 (0.0%)" : game.Away4thDownComp.ToString() + "/" + game.Away4thDownAtt.ToString() + " (" + ((double)game.Away4thDownComp / (double)game.Away4thDownAtt).ToString("P1") + ")";
            AwayTOs.Text = (game.AwayFumblesLost + game.AwayInt).ToString();
            AwayTOP.Text = TimeSpan.FromSeconds(game.AwayTOP).ToString(@"mm\:ss");
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void LoadIndividualStats()
        {
            HmPassTxt.Text = _home.Abbreviation + " PASSING";
            HmRushTxt.Text = _home.Abbreviation + " RUSHING";
            HmRecTxt.Text = _home.Abbreviation + " RECEIVING";
            HmOLTxt.Text = _home.Abbreviation + " BLOCKING";
            HmRetTxt.Text = _home.Abbreviation + " RETURNS";
            HmDefTxt.Text = _home.Abbreviation + " DEFENSE";
            HmKickingTxt.Text = _home.Abbreviation + " KICKING";
            HomePassingDatagrid.ItemsSource = _homeStats.Where(p => p.PassAtt > 0).OrderByDescending(p => p.PassYds);
            HomePassingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeRushingDatagrid.ItemsSource = _homeStats.Where(p => p.Carries > 0).OrderByDescending(p => p.RushYds);
            HomeRushingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeReceivingDatagrid.ItemsSource = _homeStats.Where(p => p.Receptions > 0).OrderByDescending(p => p.RecYds);
            HomeReceivingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeOLineDatagrid.ItemsSource = _homeStats.Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0).OrderByDescending(p => p.PancakeBlocks);
            HomeOLineDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeReturnsDatagrid.ItemsSource = _homeStats.Where(p => p.KickReturns > 0).OrderByDescending(p => p.KickRetYds);
            HomeReturnsDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeDefenseDatagrid.ItemsSource = _homeStats.Where(p => p.Tackles > 0 || p.Sacks > 0 | p.Ints > 0).OrderByDescending(p => p.Tackles);
            HomeDefenseDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));
            HomeKickingDatagrid.ItemsSource = _homeStats.Where(p => p.XPAtt > 0 || p.FGAtt > 0).OrderByDescending(p => p.Points);
            HomeKickingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_home.PrimaryColor)));

            AwPassTxt.Text = _away.Abbreviation + " PASSING";
            AwRushTxt.Text = _away.Abbreviation + " RUSHING";
            AwRecTxt.Text = _away.Abbreviation + " RECEIVING";
            AwOLTxt.Text = _away.Abbreviation + " BLOCKING";
            AwRetTxt.Text = _away.Abbreviation + " RETURNS";
            AwDefTxt.Text = _away.Abbreviation + " DEFENSE";
            AwKickingTxt.Text = _away.Abbreviation + " KICKING";
            AwayPassingDatagrid.ItemsSource = _awayStats.Where(p => p.PassAtt > 0).OrderByDescending(p => p.PassYds);
            AwayPassingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayRushingDatagrid.ItemsSource = _awayStats.Where(p => p.Carries > 0).OrderByDescending(p => p.RushYds);
            AwayRushingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayReceivingDatagrid.ItemsSource = _awayStats.Where(p => p.Receptions > 0).OrderByDescending(p => p.RecYds);
            AwayReceivingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayOLineDatagrid.ItemsSource = _awayStats.Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0).OrderByDescending(p => p.PancakeBlocks);
            AwayOLineDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayReturnsDatagrid.ItemsSource = _awayStats.Where(p => p.KickReturns > 0).OrderByDescending(p => p.KickRetYds);
            AwayReturnsDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayDefenseDatagrid.ItemsSource = _awayStats.Where(p => p.Tackles > 0 || p.Sacks > 0 | p.Ints > 0).OrderByDescending(p => p.Tackles);
            AwayDefenseDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
            AwayKickingDatagrid.ItemsSource = _awayStats.Where(p => p.XPAtt > 0 || p.FGAtt > 0).OrderByDescending(p => p.Points);
            AwayKickingDatagrid.Background = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_away.PrimaryColor)));
        }
    }
}

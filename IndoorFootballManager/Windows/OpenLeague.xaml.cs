﻿using IndoorFootballManager.DataAccess;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.Models;
using System.Net;
using IndoorFootballManager.Services;
using FluentFTP;
using SQLite;

namespace IndoorFootballManager.Windows
{
    public partial class OpenLeague : Window
    {
        public OpenLeague()
        {
            InitializeComponent();
            if (_newLeague != null)
                BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(_newLeague.PrimaryColor);
            Successful = false;
        }

        public bool Successful;
        private League _newLeague;
        private string _leagueName;

        private void ExistingLeagues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SQLiteConnection db = null;
            try
            {
                if (ExistingLeagues.SelectedItem == null)
                {
                    SelectedLbl.Text = "Selected: N/A";
                    StageLbl.Text = "Current Date: N/A";
                    UpdateLbl.Text = "Last Update (IRL): N/A";
                    return;
                }

                _leagueName = ExistingLeagues.SelectedItem.ToString();
                db = Repo.Open(_leagueName);
                var league = Repo.GetLeague(db, 1);
                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{_leagueName}\{_leagueName}.ifm";
                SelectedLbl.Text = $"Selected: {league.LeagueName}";
                StageLbl.Text = $"Current Date: {league.CurDate.ToShortDateString()}";
                UpdateLbl.Text = $"Last Update (IRL): {File.GetLastWriteTime(path)}";

                if (ExistingLeagues.SelectedItem.ToString() == Globals.LeagueAbbreviation || ExistingLeagues.SelectedItem.ToString() == Properties.Settings.Default.LastSaved.ToString())
                    DeleteBtn.IsEnabled = false;
                else
                    DeleteBtn.IsEnabled = true;
                LoadLeagueBtn.IsEnabled = true;
            }
            catch (Exception x)
            {
                Logger.LogException(x);

                SelectedLbl.Text = "Selected: N/A";
                StageLbl.Text = "Current Date: N/A";
                UpdateLbl.Text = "Last Update (IRL): N/A";
            }
            finally
            {
                db?.Dispose();
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            _newLeague = IFM.CurrentLeague;
            DeleteBtn.IsEnabled = false;
            LoadLeagueBtn.IsEnabled = false;
            try
            {
                var path = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}/Indoor Football Manager/Leagues/";
                foreach (var f in Directory.GetDirectories(path))
                {
                    var dir = new DirectoryInfo(f).Name;
                    ExistingLeagues.Items.Add(dir);
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
            if (Properties.Settings.Default.LastSaved == "")
            {
                ContinueLbl.Text = "Continue: N/A";
                ContinueBtn.IsEnabled = false;
            }
            else
            {
                ContinueBtn.ToolTip = "Continue: " + Properties.Settings.Default.LastSaved;
                ContinueLbl.Text = "Continue: " + Properties.Settings.Default.LastSaved;
            }
        }

        private async Task LoadLeague(string name)
        {
            Opacity = 0.001;
            IsEnabled = false;
            await App.ActivateLoading();
            _leagueName = name;
            if (DownloadChk.IsChecked != null && (bool)DownloadChk.IsChecked)
            {
                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{_leagueName}\{_leagueName}";
                var livePath = $"{path}.ifm";
                var fileInfo = new FileInfo(livePath);
                var file = fileInfo.Name;
                var disconnected = false;

                try
                {
                    Repo.Disconnect();
                    disconnected = true;
                    await DownloadFile(livePath, file).ConfigureAwait(false);

                    Globals.LeagueAbbreviation = _leagueName;
                    Successful = true;

                    await App.DeactivateLoading();

                    Dispatcher.Invoke(Close);
                }
                catch (Exception x)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Opacity = 1.0;
                        IsEnabled = true;
                        Logger.LogException(x);

                        if (disconnected && Globals.LeagueAbbreviation == _leagueName)
                            Repo.Reconnect(livePath);

                        App.ShowNotification("Error", "See the log file in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
                    });

                    await App.DeactivateLoading();
                }
            }
            else
            {
                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{_leagueName}\{_leagueName}";
                var livePath = $"{path}.ifm";
                var backupPath = $"{path}.bac";
                File.Copy(livePath, backupPath, true);

                Globals.LeagueAbbreviation = _leagueName;
                Successful = true;

                await App.DeactivateLoading();

                Dispatcher.Invoke(Close);
            }
        }

        private async void LoadLeagueBtn_Click(object sender, RoutedEventArgs e)
        {
            await LoadLeague(ExistingLeagues.SelectedItem.ToString());
        }

        private async void ContinueBtn_Click(object sender, RoutedEventArgs e)
        {
            await LoadLeague(Properties.Settings.Default.LastSaved);
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            _leagueName = ExistingLeagues.SelectedItem.ToString();
            void DeleteLeague()
            {
                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{_leagueName}\";
                var doesExist = Directory.Exists(path);

                if (doesExist)
                {
                    var filename = $"{path}{_leagueName}.ifm";
                    var fileInfo = new FileInfo(filename);
                    if (fileInfo.Exists)
                        ClearFolder(path);
                }
                App.ShowNotification("League Deleted", $"{_leagueName} has been deleted.", Notification.Wpf.NotificationType.Information, 5);

                ExistingLeagues.Items.Clear();
                Grid_Loaded(sender, e);
            }
            void Cancel() { return; }
            App.ShowMessageBox("Delete League", $"Are you sure you want to delete {_leagueName}?", Notification.Wpf.NotificationType.Notification, "OpenLeagueArea", "Yes", DeleteLeague, "No", Cancel);
        }

        private static void ClearFolder(string path)
        {
            var info = new DirectoryInfo(path);
            foreach (var file in info.GetFiles())
                file.Delete();
            foreach (var dir in info.GetDirectories())
            {
                foreach (var file in dir.GetFiles())
                    file.Delete();
                dir.Delete(true);
            }
            Directory.Delete(path);
        }

        private static async Task DownloadFile(string filePath, string fileName)
        {
            var dbToOpen = Repo.Open(fileName.Substring(0, fileName.Length - 4));
            var league = dbToOpen.GetLeague(1);
            dbToOpen.Close();

            var uri = league.FtpServer;
            var uriPath = $"{league.FtpRemotePath}{fileName}";
            var username = league.FtpUsername;
            var password = Converters.EncryptDecrypt(league.FtpPassword, 256);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            try
            {
                using (var ftp = new FtpClient(uri, new NetworkCredential(username, password)))
                {
                    ftp.Connect();
                    var status = await Task.Run(() => ftp.DownloadFile(filePath, uriPath, progress: (x) => App.LoadPercentage = x.Progress));
                    if (status == FtpStatus.Failed)
                    {
                        throw new Exception("Failed to download with status Failed.");
                    }
                }
                App.ShowNotification("Success", "League file updated to the latest.", Notification.Wpf.NotificationType.Success, 5);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        private void ExistingLeagues_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ExistingLeagues.SelectedItem != null)
                LoadLeagueBtn_Click(sender, e);
        }

        private async void LoadUrlBtn_Click(object sender, RoutedEventArgs e)
        {
            Opacity = 0.001;
            IsEnabled = false;
            await App.ActivateLoading();
            var successful = false;
            var urlTxt = UrlTxt.Text;
            var url = urlTxt.Split('/');
            var league = url[url.Count() - 1].Split('.');
            _leagueName = league[0];
            var filePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{_leagueName}\";
            Directory.CreateDirectory(filePath);
            var file = $"{filePath}{_leagueName}.ifm";
            var backupPath = file.Replace(".ifm", ".bac");
            var disconnected = false;
            try
            {
                await Task.Run(() => Repo.Disconnect()).ConfigureAwait(false);
                disconnected = true;

                using (var client = new WebClient())
                {
                    await Task.Run(() => client.DownloadFile(urlTxt, file));
                }

                Globals.LeagueAbbreviation = _leagueName;
                successful = true;
            }
            catch (WebException x)
            {
                Opacity = 1.0;
                IsEnabled = true;
                Logger.LogException(x);

                if (disconnected && Globals.LeagueAbbreviation == _leagueName)
                    Repo.Reconnect(file);

                App.ShowNotification("Error", "Unable to download league file.\nPlease check the URL.",
                    Notification.Wpf.NotificationType.Error, 5);
            }
            catch (Exception x)
            {
                Opacity = 1.0;
                IsEnabled = true;
                Logger.LogException(x);

                if (disconnected && Globals.LeagueAbbreviation == _leagueName)
                    Repo.Reconnect(file);

                App.ShowNotification("Warning", "Something went wrong.", Notification.Wpf.NotificationType.Warning, 5);
            }
            finally
            {
                await App.DeactivateLoading();

                if (successful)
                {
                    Successful = true;
                    Dispatcher.Invoke(Close);
                }
            }
        }

        private void UrlTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoadUrlBtn_Click(sender, e);
            }
        }
    }
}

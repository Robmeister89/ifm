﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for PlayerFilter.xaml
    /// </summary>
    public partial class PlayerFilter : Window
    {
        public PlayerFilter()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            expMin.Value = ExpRange[0]; expMax.Value = ExpRange[1]; 
            armMin.Value = ArmRange[0]; armMax.Value = ArmRange[1]; 
            accMin.Value = AccRange[0]; accMax.Value = AccRange[1]; 
            strMin.Value = StrRange[0]; strMax.Value = StrRange[1]; 
            carMin.Value = CarRange[0]; carMax.Value = CarRange[1]; 
            spdMin.Value = SpdRange[0]; spdMax.Value = SpdRange[1]; 
            catMin.Value = CatRange[0]; catMax.Value = CatRange[1]; 
            blkMin.Value = BlkRange[0]; blkMax.Value = BlkRange[1]; 
            blzMin.Value = BlzRange[0]; blzMax.Value = BlzRange[1]; 
            covMin.Value = CovRange[0]; covMax.Value = CovRange[1]; 
            tacMin.Value = TacRange[0]; tacMax.Value = TacRange[1]; 
            intMin.Value = IntRange[0]; intMax.Value = IntRange[1]; 
            kstMin.Value = KstRange[0]; kstMax.Value = KstRange[1]; 
            kacMin.Value = KacRange[0]; kacMax.Value = KacRange[1]; 
            attMin.Value = AttRange[0]; attMax.Value = AttRange[1]; 
            grdMin.Value = GrdRange[0]; grdMax.Value = GrdRange[1]; 
            loyMin.Value = LoyRange[0]; loyMax.Value = LoyRange[1]; 
            injMin.Value = InjRange[0]; injMax.Value = InjRange[1]; 
            ldsMin.Value = LdsRange[0]; ldsMax.Value = LdsRange[1]; 
            pasMin.Value = PasRange[0]; pasMax.Value = PasRange[1]; 
            wrkMin.Value = WrkRange[0]; wrkMax.Value = WrkRange[1]; 
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //public static int[] ageRange { get; set; }
        public static int[] ExpRange { get; set; }
        public static int[] AccRange { get; set; }
        public static int[] ArmRange { get; set; }
        public static int[] StrRange { get; set; }
        public static int[] CarRange { get; set; }
        public static int[] SpdRange { get; set; }
        public static int[] CatRange { get; set; }
        public static int[] BlkRange { get; set; }
        public static int[] BlzRange { get; set; }
        public static int[] CovRange { get; set; }
        public static int[] TacRange { get; set; }
        public static int[] IntRange { get; set; }
        public static int[] KstRange { get; set; }
        public static int[] KacRange { get; set; }
        public static int[] AttRange { get; set; }
        public static int[] GrdRange { get; set; }
        public static int[] LoyRange { get; set; }
        public static int[] InjRange { get; set; }
        public static int[] LdsRange { get; set; }
        public static int[] PasRange { get; set; }
        public static int[] WrkRange { get; set; }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            ExpRange[0] = (int)expMin.Value; ExpRange[1] = (int)expMax.Value; 
            ArmRange[0] = (int)armMin.Value; ArmRange[1] = (int)armMax.Value; 
            AccRange[0] = (int)accMin.Value; AccRange[1] = (int)accMax.Value; 
            StrRange[0] = (int)strMin.Value; StrRange[1] = (int)strMax.Value; 
            CarRange[0] = (int)carMin.Value; CarRange[1] = (int)carMax.Value; 
            SpdRange[0] = (int)spdMin.Value; SpdRange[1] = (int)spdMax.Value; 
            CatRange[0] = (int)catMin.Value; CatRange[1] = (int)catMax.Value; 
            BlkRange[0] = (int)blkMin.Value; BlkRange[1] = (int)blkMax.Value; 
            BlzRange[0] = (int)blzMin.Value; BlzRange[1] = (int)blzMax.Value; 
            CovRange[0] = (int)covMin.Value; CovRange[1] = (int)covMax.Value; 
            TacRange[0] = (int)tacMin.Value; TacRange[1] = (int)tacMax.Value; 
            IntRange[0] = (int)intMin.Value; IntRange[1] = (int)intMax.Value; 
            KstRange[0] = (int)kstMin.Value; KstRange[1] = (int)kstMax.Value; 
            KacRange[0] = (int)kacMin.Value; KacRange[1] = (int)kacMax.Value; 
            AttRange[0] = (int)attMin.Value; AttRange[1] = (int)attMax.Value; 
            GrdRange[0] = (int)grdMin.Value; GrdRange[1] = (int)grdMax.Value; 
            LoyRange[0] = (int)loyMin.Value; LoyRange[1] = (int)loyMax.Value; 
            InjRange[0] = (int)injMin.Value; InjRange[1] = (int)injMax.Value; 
            LdsRange[0] = (int)ldsMin.Value; LdsRange[1] = (int)ldsMax.Value; 
            PasRange[0] = (int)pasMin.Value; PasRange[1] = (int)pasMax.Value; 
            WrkRange[0] = (int)wrkMin.Value; WrkRange[1] = (int)wrkMax.Value; 

            this.DialogResult = true;
            this.Close();
        }
    }
}

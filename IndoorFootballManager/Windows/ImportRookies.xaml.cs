﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using ookii = Ookii.Dialogs.Wpf;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for ImportRookies.xaml
    /// </summary>
    public partial class ImportRookies : Window
    {
        private readonly ookii.ProgressDialog _prog = new ookii.ProgressDialog()
        {
            WindowTitle = "Importing Draft Class",
            Text = "Importing players to league file...",
            ShowTimeRemaining = false,
            ShowCancelButton = false,
            ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
        };

        public ImportRookies()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private League CurrentLeague => IFM.CurrentLeague;

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private string _filePath;

        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new ookii.VistaOpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                _filePath = ofd.FileName;
                FileTxt.Text = _filePath;
            }
        }

        private void ImportBtn_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)OverwriteChk.IsChecked)
                _overwrite = true;

            _prog.DoWork += ImportStart;
            _prog.RunWorkerCompleted += ImportEnd;
            _prog.ShowDialog();
        }

        private bool _imported = false;
        private bool _overwrite = false;

        private void ImportStart(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_filePath != null)
                {
                    if (_overwrite)
                    {
                        foreach (var p in Repo.Database.GetPlayersByTeamId(-1))
                        {
                            Repo.Database.Delete<Player>(p.Id);
                        }
                    }
                    PlayerData.ImportRookies(_filePath);
                    _imported = !_imported;
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
        }

        private void ImportEnd(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_imported)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
                App.ShowNotification("Error", "Import failed.", Notification.Wpf.NotificationType.Error, 5);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

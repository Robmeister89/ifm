﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for ChangeDraft.xaml
    /// </summary>
    public partial class ChangeDraft : Window
    {
        public ChangeDraft()
        {
            InitializeComponent();
        }

        public ChangeDraft(List<Team> teams)
        {
            InitializeComponent();
            this.Owner = GameWindow;
            Teams = teams;
            BkColor.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private League CurrentLeague => IFM.CurrentLeague;
        public List<Team> Teams { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var draftOrder = Repo.Database.GetPicks()
                .Where(d => d.Season == CurrentLeague.CurrentSeason && d.Round == 1).OrderBy(d => d.TeamId).ToList();
            for(var j = 0; j < 2; j++)
            {
                var col = new ColumnDefinition { Width = new GridLength(200) };
                TeamGrid.ColumnDefinitions.Add(col);
            }
            var i = 0;
            var picks = draftOrder.OrderBy(d => d.Pick).Select(d => d.Pick).ToList();
            foreach(var t in Teams)
            {
                var row = new RowDefinition { Height = new GridLength(30) };
                TeamGrid.RowDefinitions.Add(row);
                var text = new TextBlock
                {
                    Text = t.DraftText,
                    Foreground = Brushes.White, 
                    HorizontalAlignment = HorizontalAlignment.Center, 
                    TextAlignment = TextAlignment.Center 
                };
                Grid.SetRow(text, i);
                Grid.SetColumn(text, 0);
                TeamGrid.Children.Add(text);

                var draftNo = new ComboBox
                {
                    Name = $"Team_{t.Id}",
                    ItemsSource = picks,
                    Text = draftOrder.FirstOrDefault(d => d.TeamId == t.Id)?.Pick.ToString(),
                    Margin = new Thickness(50,5,50,5),
                };
                Grid.SetRow(draftNo, i);
                Grid.SetColumn(draftNo, 1);
                TeamGrid.Children.Add(draftNo);
                i++;
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        public static Dictionary<int, int> NewOrder;
        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            var exit = true;
            var picks = new List<string>();
            NewOrder = new Dictionary<int, int>();
            foreach(var t in Teams)
            {
                var cbox = TeamGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == $"Team_{t.Id}");
                if (!picks.Contains(cbox.Text))
                {
                    picks.Add(cbox.Text);
                    NewOrder.Add(t.Id, Convert.ToInt32(cbox.Text));
                }
                else
                {
                    exit = false;
                    break;
                }
            }
            if (exit)
                DialogResult = true;
            else
                App.ShowNotification("Error", "You have two or more of the same picks.\nPlease fix the order and re-submit.", Notification.Wpf.NotificationType.Error, 5);
        }
    }
}

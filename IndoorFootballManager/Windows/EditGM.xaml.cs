﻿using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for EditGM.xaml
    /// </summary>
    public partial class EditGm : Window
    {
        public EditGm()
        {
            InitializeComponent();
        }

        public EditGm(GeneralManager gm)
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
            Gm = gm;
        }

        private GeneralManager Gm { get; set; }
        private static SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;
        private Team CurTeam { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var teams = Database.GetTeams().Where(t => t.Manager == null).OrderBy(t => t.City).ToList();
            if (Gm.Team != null)
            {
                CurTeam = Gm.Team;
                teams.Add(CurTeam);
            }
            teams.OrderBy(t => t.City);
            TeamSelect.ItemsSource = teams;
            TeamSelect.DisplayMemberPath = "City";

            FNameTxt.Text = Gm.FirstName;
            LNameTxt.Text = Gm.LastName;
            AgeSelect.SelectedDate = Gm.DOB;
            ExpSelect.Text = Gm.Exp.ToString();
            if(Gm.TeamId > 0)
                TeamSelect.Text = Gm.Team.City;
            CommishAccess.IsChecked = Gm.IsCommissioner;
            PasswordTxt.Text = Converters.EncryptDecrypt(Gm.Password, 256);
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            Gm.FirstName = FNameTxt.Text;
            Gm.LastName = LNameTxt.Text;
            Gm.DOB = (DateTime)AgeSelect.SelectedDate;
            Gm.Exp = Convert.ToInt32(ExpSelect.Text);
            var team = TeamSelect.SelectedItem as Team;
            Gm.TeamId = team != null ? Database.GetTeamByName(team.TeamName).Id : 0;
            Gm.Password = Converters.EncryptDecrypt(PasswordTxt.Text, 256);
            Gm.IsCommissioner = (bool)CommishAccess.IsChecked;
            Database.SaveManager(Gm);

            if (CurTeam != null)
            {
                if (Gm.TeamId != CurTeam.Id)
                {
                    CurTeam.IsCpu = true;
                    Database.SaveTeam(CurTeam);
                }
            }

            if (Gm.TeamId != 0 && Gm.Team.IsCpu)
            {
                team.IsCpu = false;
                Database.SaveTeam(team);
            }

            DialogResult = true;
            Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

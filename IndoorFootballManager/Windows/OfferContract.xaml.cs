﻿using System;
using System.Windows;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for OfferContract.xaml
    /// </summary>
    public partial class OfferContract : Window
    {
        public OfferContract()
        {
            InitializeComponent();
        }

        public OfferContract(Team t, Player p)
        {
            InitializeComponent();
            Team = t;
            Player = p;
        }

        private static League CurrentLeague => IFM.CurrentLeague;
        private Team Team { get; }
        private Player Player { get; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(Team.PrimaryColor);
            YearsSelect.Maximum = CurrentLeague.MaximumYears;
            YearsSelect.Value = YearsSelect.Minimum;
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OfferConBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SalaryTxt.Text != "")
            {
                var salary = Convert.ToInt32(SalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", ""));
                var years = Convert.ToInt32(YearsSelect.Text);
                PlayerData.OfferContract(Team, Player, years, salary);
                this.DialogResult = true;
                this.Close();
            }
            else
                App.ShowNotification("Error", "Please enter in a valid offer.", Notification.Wpf.NotificationType.Error, 5);
        }

        private void ReqOfferBtn_Click(object sender, RoutedEventArgs e)
        {
            ReqOfferBtn.IsEnabled = false;
            var requestedSalary = Player.GetNegotiatedSalary.ToString("C0");
            var requestedYears = Player.DesiredYears.ToString("N0");
            SalaryTxt.Text = requestedSalary;
            YearsSelect.Text = requestedYears;
        }
    }
}

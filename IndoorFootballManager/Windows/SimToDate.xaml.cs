﻿using System;
using System.Windows;
using System.Windows.Media;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for SimToDate.xaml
    /// </summary>
    public partial class SimToDate : Window
    {
        public SimToDate()
        {
            InitializeComponent();
            BackgroundClr.Color = (Color)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
            CurDate = CurrentLeague.CurDate;
            ToDatePicker.SelectedDate = CurDate;
            ToDatePicker.DisplayDate = CurDate;

        }

        private League CurrentLeague => IFM.CurrentLeague;
        private DateTime CurDate { get; set; }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            IFM.ToDate = (DateTime)ToDatePicker.SelectedDate;
            this.DialogResult = true;
            Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

    }
}

﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for TeamDraftPicks.xaml
    /// </summary>
    public partial class TeamDraftPicks : Window
    {
        public TeamDraftPicks()
        {
            InitializeComponent();
        }

        public TeamDraftPicks(Team team)
        {
            InitializeComponent();
            _team = team;
        }

        private readonly Team _team;
        private League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TeamPicks.ItemsSource = Repo.Database.GetPicksByTeam(_team.Id).OrderBy(d => d.Season).ThenBy(d => d.Round);
            BkColor.Color = (Color)ColorConverter.ConvertFromString(_team.SecondaryColor);
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

﻿using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Windows
{
    /// <summary>
    /// Interaction logic for NewEmail.xaml
    /// </summary>
    public partial class NewEmail : Window
    {
        public NewEmail()
        {
            InitializeComponent();
        }

        public NewEmail(GeneralManager sender)
        {
            InitializeComponent();
            Sender = sender;
        }

        public NewEmail(GeneralManager sender, GeneralManager receiver)
        {
            InitializeComponent();
            Sender = sender;
            Receiver = receiver;
        }

        private static League CurrentLeague => IFM.CurrentLeague;
        public GeneralManager Sender { get; set; }
        private Team Team { get; set; }
        public GeneralManager Receiver { get; set; }
        public string Subject { get; set; } = "";
        public string Message { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var managers = Repo.Database.GetManagers().OrderBy(g => g.FullName).ToList();
            ManagersList.ItemsSource = managers;
            ManagersList.DisplayMemberPath = "DisplayName";
            if (Receiver != null)
                ManagersList.Text = Receiver.DisplayName;
            Team = Repo.Database.GetTeamById(Sender.TeamId);
            MainGrid.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor));
            DragBar.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));
            HeaderTxt.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor));
            SubjectTxt.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));
            SendToTxt.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));
            SubjectBox.Text = Subject;
        }

        private void DragBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            if(SubjectBox.Text != "")
            {
                Receiver = ManagersList.SelectedItem as GeneralManager;
                Subject = SubjectBox.Text;
                Message = new TextRange(EmailTxt.Document.ContentStart, EmailTxt.Document.ContentEnd).Text;
                DialogResult = true;
            }
            else
            {
                App.ShowNotification("Warning", "Please enter a subject.", Notification.Wpf.NotificationType.Warning, 5);
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

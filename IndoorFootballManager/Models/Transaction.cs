﻿using System;
using SQLite;

namespace IndoorFootballManager.Models
{
    [Serializable]
    public class Transaction
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Indexed]
        public int TeamId { get; set; }
        public int Season { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        
        public Transaction() { }
        
        public Transaction(int t, int s, DateTime d, string x)
        {
            TeamId = t;
            Season = s;
            Date = d;
            Text = x;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Services;
using SQLite;
using Newtonsoft.Json;

namespace IndoorFootballManager.Models
{
    [Serializable]
    public class Player
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Indexed]
        public int Jersey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public int Exp { get; set; }
        public string Pos { get; set; }
        public int TeamId { get; set; }
        public int Accuracy { get; set; }
        public int ArmStrength { get; set; }
        public int Strength { get; set; }
        public int Carry { get; set; }
        public int Speed { get; set; }
        public int Catching { get; set; }
        public int Block { get; set; }
        public int Blitz { get; set; }
        public int Coverage { get; set; }
        public int Tackle { get; set; }
        public int Intelligence { get; set; }
        public int KickStrength { get; set; }
        public int KickAccuracy { get; set; }
        public int Overall { get; set; }
        public int ContractYears { get; set; }
        public int Salary { get; set; }
        public string College { get; set; }
        public int Attitude { get; set; }
        public int Greed { get; set; }
        public int Loyalty { get; set; }
        public int Injury { get; set; }
        public int Leadership { get; set; }
        public int Passion { get; set; }
        public int WorkEthic { get; set; }
        public int DraftRound { get; set; }
        public int DraftPick { get; set; }
        public int DraftYear { get; set; }
        public int Potential { get; set; }
        public int Traded { get; set; }
        public int Season { get; set; }
        public bool IsFranchised { get; set; }
        public int ExtensionSalary { get; set; }
        public int ExtensionYears { get; set; }
        public bool WillNotResign { get; set; }
        public int SignAttempts { get; set; }
        public bool HasTrained { get; set; }
        public bool InjuredReserve { get; set; }
        public bool RosterReserve { get; set; }
        public bool IsInactive { get; set; }
        public bool IsInjured { get; set; }
        public string InjuryTxt { get; set; }
        public int InjuryDuration { get; set; }
        public string PlayerHistoryJson { get; set; }
        [Obsolete]
        public byte[] PlayerHistory { get; set; }

        [Indexed]
        public string Name => $"{FirstName} {LastName}";

        public string City
        {
            get
            {
                switch (TeamId)
                {
                    case -2:
                        return "Retired";
                    case -1:
                        return "Draft Class";
                    case 0:
                        return "Free Agent";
                    default:
                        return Team.City;
                }
            }
        }

        [Indexed]
        public int PositionSort
        {
            get
            {
                int pos;
                switch (Pos)
                {
                    default:
                        pos = 0;
                        break;
                    case "RB":
                        pos = 1;
                        break;
                    case "WR":
                        pos = 2;
                        break;
                    case "OL":
                        pos = 3;
                        break;
                    case "DL":
                        pos = 4;
                        break;
                    case "LB":
                        pos = 5;
                        break;
                    case "DB":
                        pos = 6;
                        break;
                    case "K":
                        pos = 7;
                        break;
                }
                return pos;
            }
        }

        public string Contract => TeamId < 0
            ? "Not Under Contract"
            : ContractYears == 1
                ? $"1 year / {Salary:C0}"
                : $"{ContractYears} years / {Salary:C0}";

        public string Extension => ExtensionYears < 1
            ? "No Extension"
            : ExtensionYears == 1
                ? $"1 year / {ExtensionSalary:C0}"
                : $"{ExtensionYears} years / {ExtensionSalary:C0}";

        public SolidColorBrush TeamPrimaryColor => Team == null
            ? new SolidColorBrush(Colors.Black)
            : new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));

        public SolidColorBrush TeamSecondaryColor => Team == null
            ? new SolidColorBrush(Colors.GhostWhite)
            : new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor));

        public Player() => Overall = GetOverall;
        
        public Player(int id, int jersey, string first, string last, DateTime age, int exp, string pos, int teamId, int acc, int arm, int str, int car, int spd, int cat, int blk, int blz, int cov, int tck, int intel, int kpw, int kac, int yc, string college, int potential)
        {
            Id = id;
            Jersey = jersey;
            FirstName = first;
            LastName = last;
            DOB = age;
            Exp = exp;
            Pos = pos;
            TeamId = teamId;
            Accuracy = acc;
            ArmStrength = arm;
            Strength = str;
            Carry = car;
            Speed = spd;
            Catching = cat;
            Block = blk;
            Blitz = blz;
            Coverage = cov;
            Intelligence = intel;
            Tackle = tck;
            KickStrength = kpw;
            KickAccuracy = kac;
            Overall = GetOverall;
            if (TeamId <= 0)
            {
                ContractYears = 0;
                Salary = 0;
            }
            else
            {
                ContractYears = yc;
                Salary = GetNegotiatedSalary;
            }
            College = college;
            Potential = potential;
            IsFranchised = false;
            WillNotResign = false;
            SignAttempts = 0;
            HasTrained = false;
            InjuredReserve = false;
            RosterReserve = false;
            IsInactive = false;
            IsInjured = false;
            Traded = 0;
            Attitude = Random.Next(0, 4) == 0 ? Random.Next(0, 60) : Random.Next(60, 101);
            Greed = Random.Next(0, 101);
            Loyalty = Greed > 50 ? Random.Next(0, 51) : Random.Next(50, 101);
            Passion = Random.Next(0, 101);
            Injury = Random.Next(0, 3) == 0 ? Random.Next(0, 70) : Random.Next(70, 101);
            Leadership = Random.Next(0, 101);
            WorkEthic = Random.Next(0, 101);
            DraftRound = 0; DraftPick = 0; DraftYear = 0;
        }

        public int Age => CalculateAge(DOB, CurrentLeague.CurDate);

        public int CalculateAge(DateTime birthDate, DateTime now)
        {
            var age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age;
        }

        [Ignore]
        public int Fatigue { get; set; }

        private static SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;

        //RETURNS THE PLAYER'S OVERALL
        public int GetOverall
        {
            get
            {
                switch (Pos)
                {
                    case "QB":
                        Overall = Convert.ToInt32((((double)Accuracy + ArmStrength + Intelligence) / 3 * 0.7) + (((double)Speed + Carry) / 2 * 0.2) + (Strength * 0.1) + 12);
                        break;
                    case "RB":
                        Overall = Convert.ToInt32((((double)Carry + Speed) / 2 * 0.6) + (Catching * 0.2) + (((double)Strength + Block + Intelligence) / 2 * 0.2) + 10);
                        break;
                    case "WR":
                        Overall = Convert.ToInt32((((double)Catching + Speed) / 2 * 0.6) + (Carry * 0.2) + (((double)Strength + Block + Intelligence) / 2 * 0.2) + 10);
                        break;
                    case "OL":
                        Overall = Convert.ToInt32((((double)Strength + Block) / 2 * 0.8) + (Intelligence * 0.1) + (Speed * 0.1) + 7);
                        break;
                    case "DL":
                        Overall = Convert.ToInt32((((double)Strength + Blitz) / 2 * 0.65) + (Tackle * 0.25) + (((double)Speed + Intelligence) / 2 * 0.1) + 8);
                        break;
                    case "LB":
                        Overall = Convert.ToInt32((((double)Blitz + Tackle) / 2 * 0.65) + (((double)Speed + Coverage) / 2 * 0.2) + (((double)Intelligence + Strength) / 2 * 0.1) + (((double)Carry + Catching) / 2 * 0.05) + 10);
                        break;
                    case "DB":
                        Overall = Convert.ToInt32((((double)Speed + Coverage) / 2 * 0.65) + (((double)Tackle + Intelligence) / 2 * 0.25) + (((double)Strength + Catching + Carry) / 2 * 0.1) + 11.5);
                        break;
                    default: // k
                        Overall = Convert.ToInt32((((double)KickStrength + KickAccuracy) / 2 * 0.8) + (Intelligence * 0.15) + (((double)Strength + Speed) / 2 * 0.05) + 7);
                        break;
                }

                return Overall > 100 ? 100 : Overall;
            }
        }

        public int ReturnRating => Convert.ToInt32((Speed * 1.25) + (Carry * 1.15) + (Intelligence * 1.05));

        internal Team Team => Database?.GetTeamById(TeamId);

        public ImageSource TeamLogo => Team != null ? Converters.LoadImage(Team.Logo) : null;

        //RETURN A TEAM NAME, WHETHER THAT BE A FREE AGENT, DRAFTEE, RETIRED OR SPECIFIC TEAM
        public string TeamName
        {
            get
            {
                switch (TeamId)
                {
                    case -1:
                        return "Draft Class";
                    case 0:
                        return "Free Agent";
                    case -2:
                        return "Retired";
                    default:
                        return Team?.TeamName;
                }
            }
        }

        public int JerseyNumber
        {
            get
            {
                if (TeamId <= 0) return JerseyNumbers[new Random().Next(JerseyNumbers.Count)];
                var currentJerseyNumbers = Team.Players.Select(p => p.Jersey).ToList();
                if (!currentJerseyNumbers.Contains(Jersey)) return Jersey;
                var numbers = JerseyNumbers.Except(currentJerseyNumbers).ToList();
                return numbers[new Random().Next(numbers.Count)];
            }
        }

        private List<int> JerseyNumbers
        {
            get
            {
                var numbers = new List<int>();
                switch (Pos)
                {
                    case "QB":
                        for (var i = 1; i < 20; i++)
                            numbers.Add(i);
                        break;
                    case "RB":
                    case "DB":
                        for (var i = 1; i < 50; i++)
                            numbers.Add(i);
                        break;
                    case "WR":
                        for (var i = 1; i < 90; i++)
                        {
                            if (i == 20)
                                i = 80;
                            numbers.Add(i);
                        }
                        break;
                    case "OL":
                        for (var i = 50; i < 80; i++)
                            numbers.Add(i);
                        break;
                    case "DL":
                        for (var i = 50; i < 100; i++)
                        {
                            if (i == 60)
                                i = 70;
                            if (i == 80)
                                i = 90;
                            numbers.Add(i);
                        }
                        break;
                    case "LB":
                        for (var i = 1; i < 100; i++)
                        {
                            if (i == 60)
                                i = 90;
                            numbers.Add(i);
                        }
                        break;
                    default: // K
                        for (var i = 1; i < 20; i++)
                            numbers.Add(i);
                        break;
                }
                return numbers;
            }
        }

        //JUST A METHOD TO GET A RANDOM NUMBER
        private static readonly Random Random = new Random();
        private static double RandomDouble(double minValue, double maxValue)
        {
            var next = Random.NextDouble();
            return minValue + next * (maxValue - minValue);
        }

        // should this be based on overall or exp??? currently it is exp and only young or vet
        public int GetFreeAgentSalary => Exp > 4
            ? Convert.ToInt32(CurrentLeague.MinimumSalary * 1.5)
            : CurrentLeague.MinimumSalary;
        
        [JsonIgnore]
        public int GetNegotiatedSalary
        {
            get
            {
                var ageX = Math.Pow(Age, 1.13279106) + Age;
                var ovrX = Math.Pow(Overall, 1.33279106) + ageX;
                var modX = CurrentLeague.SalaryCap * (CurrentLeague.MaximumSalary / CurrentLeague.MinimumSalary) / (ageX + ovrX);
                double greedX;
                if (Pos == "K")
                {
                    greedX = Greed > 70 ? RandomDouble(0.5, 1) : RandomDouble(0.85, 1.35);
                }
                else
                {
                    greedX = Greed > 70 ? RandomDouble(1.85, 2.35) : RandomDouble(1.5, 2);
                }

                var salary = Overall * greedX * modX / ovrX * 1.357;
                var salaryX = salary * (ovrX * 0.08) * (greedX * 0.01737);
                if (salaryX <= CurrentLeague.MinimumSalary)
                    salaryX = CurrentLeague.MinimumSalary;
                else if (salaryX >= CurrentLeague.MaximumSalary)
                    salaryX = CurrentLeague.MaximumSalary;
                return Convert.ToInt32(salaryX);
            }
        }

        [JsonIgnore]
        public int DesiredYears
        {
            get
            {
                var years = 1;

                if (Age < 25)
                    years += Random.Next(1, CurrentLeague.MaximumYears);
                else if (Age < 31)
                    years += Random.Next(0, 3);
                else
                    years += Random.Next(0, 2);

                if (Loyalty > 70)
                    years += new Random().Next(0, 2);

                if (years > CurrentLeague.MaximumYears)
                    years = CurrentLeague.MaximumYears;

                return years;
            }
        }

        [JsonIgnore]
        public string DraftText => TeamId == -1 
            ? "TBD" 
            : DraftRound < 1 
                ? "Undrafted" 
                : $"Round {DraftRound}, Pick {DraftPick} ({DraftYear})";

        [JsonIgnore] public string ExpText => $"{Exp} year(s)";
        [JsonIgnore] public string AgeText => $"{DOB.ToShortDateString()} ({Age} years old)";
        [JsonIgnore] public string ContractText => ContractYears == 0 ? "Not Under Contract" : $"{Salary:C0} / {ContractYears} year(s)";
        [JsonIgnore] public string ExtensionText => ContractYears == 0 ? "Not Applicable" : ExtensionYears == 0 ? "No Extension Signed" : $"{ExtensionSalary:C0} / {ExtensionYears} year(s)";
        [JsonIgnore] public string InjuryText => InjuryTxt.ToTitleCase();
        [JsonIgnore] public string InjuryDurationText => $"{InjuryDuration} Day(s)";
        [JsonIgnore] public string InjuryAndDurationText => $"{InjuryTxt.ToTitleCase()} - {InjuryDurationText}";

        public string PositionText
        {
            get
            {
                var keys = new Dictionary<string, string>
                {
                    { "QB", "Quarterback" },
                    { "RB", "Running Back" },
                    { "WR", "Wide Receiver" },
                    { "OL", "Offensive Line" },
                    { "DL", "Defensive Line" },
                    { "LB", "Linebacker" },
                    { "DB", "Defensive Back" },
                    { "K", "Kicker" }
                };
                return keys[Pos];
            }
        }

        public void NegotiateContract(int offer, int years)
        {
            var signed = false;
            if (Age < 24)
            {
                if (offer >= GetNegotiatedSalary && years >= DesiredYears - 1)
                {
                    signed = true;
                    ExtensionSalary = offer; ExtensionYears = years;
                    SignAttempts = 0;
                    App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.",Notification.Wpf.NotificationType.Information, 5);
                }
                else if (offer >= GetNegotiatedSalary * .8 && years >= DesiredYears - 1)
                {
                    if (years < DesiredYears - 1 && offer >= GetNegotiatedSalary * .8)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.",Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .8 && years >= DesiredYears - 1)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer >= GetNegotiatedSalary * .7 && years >= DesiredYears)
                {
                    if (years < DesiredYears && offer >= GetNegotiatedSalary * .7)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .7 && years >= DesiredYears)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer < GetNegotiatedSalary * .69 && years < DesiredYears - 1)
                {
                    if (offer < GetNegotiatedSalary * .69)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                }
                else if (offer < GetNegotiatedSalary * .69 && years >= DesiredYears - 1)
                    App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else if (years < DesiredYears && offer >= GetNegotiatedSalary * .69)
                    App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else
                    App.ShowNotification("Extension Update", $"You're way off.\nOffer {Name} something better.", Notification.Wpf.NotificationType.Warning, 5);
            }
            if (Age > 24 && Age < 31)
            {
                if (offer >= GetNegotiatedSalary && years >= DesiredYears - 1)
                {
                    signed = true;
                    ExtensionSalary = offer; ExtensionYears = years;
                    SignAttempts = 0;
                    App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                }
                else if (offer >= GetNegotiatedSalary * .85 && years >= DesiredYears - 1)
                {
                    if (years < DesiredYears - 1 && offer >= GetNegotiatedSalary * .85)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .85 && years >= DesiredYears - 1)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer >= GetNegotiatedSalary * .75 && years >= DesiredYears)
                {
                    if (years < DesiredYears && offer >= GetNegotiatedSalary * .75)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .75 && years >= DesiredYears)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer < GetNegotiatedSalary * .74 && years < DesiredYears - 1)
                {
                    if (offer < GetNegotiatedSalary * .74)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                }
                else if (offer < GetNegotiatedSalary * .74 && years >= DesiredYears - 1)
                    App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else if (years < DesiredYears && offer >= GetNegotiatedSalary * .74)
                    App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else
                    App.ShowNotification("Extension Update", $"You're way off.\nOffer {Name} something better.", Notification.Wpf.NotificationType.Warning, 5);
            }
            if (Age > 30)
            {
                if (offer >= GetNegotiatedSalary && years >= DesiredYears - 1)
                {
                    signed = true;
                    ExtensionSalary = offer; ExtensionYears = years;
                    SignAttempts = 0;
                    App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                }
                else if (offer >= GetNegotiatedSalary * .9 && years >= DesiredYears - 1)
                {
                    if (years < DesiredYears - 1 && offer >= GetNegotiatedSalary * .9)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .9 && years >= DesiredYears - 1)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer >= GetNegotiatedSalary * .8 && years >= DesiredYears)
                {
                    if (years < DesiredYears && offer >= GetNegotiatedSalary * .8)
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else if (offer < GetNegotiatedSalary * .8 && years >= DesiredYears)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                    {
                        signed = true;
                        ExtensionSalary = offer; ExtensionYears = years;
                        SignAttempts = 0;
                        App.ShowNotification("Extension Update", $"{Name} has been signed to an extension.", Notification.Wpf.NotificationType.Information, 5);
                    }
                }
                else if (offer < GetNegotiatedSalary * .79 && years < DesiredYears - 1)
                {
                    if (offer < GetNegotiatedSalary * .79)
                        App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                    else
                        App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                }
                else if (offer < GetNegotiatedSalary * .79 && years >= DesiredYears - 1)
                    App.ShowNotification("Extension Update", $"{Name} would like more money on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else if (years < DesiredYears && offer >= GetNegotiatedSalary * .79)
                    App.ShowNotification("Extension Update", $"{Name} would like more years on the deal.", Notification.Wpf.NotificationType.Warning, 5);
                else
                    App.ShowNotification("Extension Update", $"You're way off.\nOffer {Name} something better.", Notification.Wpf.NotificationType.Warning, 5);
            }

            if (signed)
            {
                List<string> playerHistory;
                if (PlayerHistoryJson == null)
                    playerHistory = new List<string>();
                else
                    playerHistory = PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add($"Signed an extension with {TeamName} on {CurrentLeague.CurDate.ToShortDateString()} ({offer:C0}) / {years} years)");
                PlayerHistoryJson = playerHistory.SerializeToJson();
                
                var transax = new Transaction(Team.Id,
                                    CurrentLeague.CurrentSeason,
                                    CurrentLeague.CurDate,
                                    $"{NameWithPosition} signed an extension with {TeamName} on {CurrentLeague.CurDate.ToShortDateString()} for {years} year(s) and {offer:C0}/year.");

                Database.SaveTransaction(transax);
            }

            Database.SavePlayer(this);
        }

        public string NameWithPosition => $"{Pos} {Name}";
        public string NameWithOverall => $"{NameWithPosition} - {Overall}";
        public string ShortName => $"{FirstName.Substring(0, 1).Trim()}. {LastName}";
        public string ShortNameWithPosition => $"{Pos} {ShortName}";
        public string NameWithPosAndJersey => $"{Pos} #{Jersey} {Name}";

        public int EvaluateOffer(ContractOffer offer)
        {
            var eval = 0;
            var years = DesiredYears;
            var salary = GetNegotiatedSalary;
            if (Loyalty > 70)
            {
                if (offer.Years >= years || offer.Years >= years + 1 || offer.Years >= years - 1) eval += new Random().Next(20, 30);
                else eval += new Random().Next(5, 15);
            }
            else
            {
                if (offer.Years >= years || offer.Years >= years + 1 || offer.Years >= years + 2) eval += new Random().Next(20, 30);
                else eval += new Random().Next(5, 15);
            }
            if(Greed > 70)
            {
                if (offer.Salary >= salary * 1.2) eval += new Random().Next(20, 30);
                else eval += new Random().Next(5, 15);
            }
            else
            {
                if (offer.Salary >= salary) eval += new Random().Next(20, 30);
                else eval += new Random().Next(5, 15);
            }

            return eval;
        }

        public void UseFranchiseTag()
        {
            var player = this;
            var team = player.Team;
            var playerSalaries = Database.GetPlayers().Where(p => p.Pos == Pos).OrderBy(p => p.Overall).Select(p => p.Salary).ToList();
            var average = (int)playerSalaries.Take(5).Average();
            var franchiseTagSalary = average < player.Salary * 1.2 ? Convert.ToInt32(Salary * 1.2) : average;

            player.ExtensionSalary = franchiseTagSalary;
            player.ExtensionYears = 1;
            player.IsFranchised = true;
            var playerHistory = player.PlayerHistoryJson.DeserializeToList<string>();
            playerHistory.Add("Franchised by " + team.TeamName + " on " + CurrentLeague.CurDate.ToShortDateString());
            player.PlayerHistoryJson = playerHistory.SerializeToJson();
            team.FranchiseTagUsed = true;
            
            Database.SavePlayer(player);

            var transaction = new Transaction(player.TeamId,
                                CurrentLeague.CurrentSeason,
                                CurrentLeague.CurDate,
                                $"{team.TeamName} franchise tagged {player.NameWithPosition} on {CurrentLeague.CurDate.ToShortDateString()}.");
            Database.SaveTransaction(transaction);
                
            Database.SaveTeam(team);
            SendEmail.PlayerFranchised(team, player);
            IFM.NotifyChange("EmailsCount");
        }
                
        //TRAINS THE PLAYERS FOR OFFSEASON TRAINING CAMP
        public int TrainingCamp(int rating, int workEthic)
        {
            var hitNum = ((100 - workEthic + rating) / 10) + (Potential / 60);
            var hits = 0;
            for(var i = 0; i < 5; i++)
            {
                var num = new Random().Next(1, 21);
                if (num >= hitNum)
                    hits += 1;
            }
            var addPoints = hits;
            for(var i = 0; i < hits; i++)
            {
                var num = new Random().Next(1, 21);
                if (num > 15)
                    addPoints += 1;
            }
            var newRating = Convert.ToDouble(rating);
            while (addPoints > 0)
            {
                if (newRating > 89)
                    newRating += .34;
                else if (newRating > 79)
                    newRating += .67;
                else
                    newRating += 1;
                addPoints -= 1;
            }

            return newRating >= 100 ? 100 : Convert.ToInt32(newRating);
        }

        //TRAINS THE PLAYER FOR WEEKLY TRAINING
        public int WeeklyTraining(int rating, int workEthic)
        {
            var hitNum = ((100 - workEthic + rating) / 10) - (Potential / 60);
            var hits = 0;
            for (var i = 0; i < 3; i++)
            {
                var num = new Random().Next(1, 21);
                if (num >= hitNum)
                    hits += 1;
            }
            var addPoints = hits;
            for (var i = 0; i < hits - 1; i++)
            {
                var num = new Random().Next(1, 21);
                if (num > 15)
                    addPoints += 1;
            }
            var newRating = Convert.ToDouble(rating);
            while (addPoints > 0)
            {
                if (newRating > 89)
                    newRating += .34;
                else if (newRating > 79)
                    newRating += .67;
                else
                    newRating += 1;
                addPoints -= 1;
            }

            return newRating >= 100 ? 100 : Convert.ToInt32(newRating);
        }

        public bool CheckDecline => AgeEval > GlobalRandom.NextDouble * 1.039;
        public bool CheckRetirement => AgeEval > GlobalRandom.NextDouble * 1.739;

        // will use this for player decline and retirement cases
        public double AgeEval
        {
            get
            {
                var ageX = Math.Pow(Age, 1.15279106) + Age;
                var ovrX = Overall * 0.4745;
                var workX = WorkEthic * 0.0219;
                var mod = 0.0059632;
                if (Age >= 30)
                    mod = 0.0102982;
                var eval = ageX - ovrX - workX;
                if (Pos == "QB" || Pos == "K")
                    return (eval * mod) - 0.007912;
                return eval * mod;
            }
        }

        // used to check if a player will be re-signed or not as well as player evaluation in the off-season to sign players
        public double Evaluation
        {
            get
            {
                var ageMod = Age * Overall * 0.0001;
                var expMod = Exp * Overall * 0.0001;
                var potMod = Potential * Overall * 0.0001;
                double keyRating1;
                double keyRating2;
                double keyRating3;

                switch (Pos)
                {
                    case "QB":
                        keyRating1 = Accuracy * Overall * 0.0001;
                        keyRating2 = ArmStrength * Overall * 0.0001;
                        keyRating3 = Intelligence * Overall * 0.0001;
                        break;
                    case "RB":
                        keyRating1 = Speed * Overall * 0.0001;
                        keyRating2 = Carry * Overall * 0.0001;
                        keyRating3 = Strength * Overall * 0.0001;
                        break;
                    case "WR":
                        keyRating1 = Speed * Overall * 0.0001;
                        keyRating2 = Catching * Overall * 0.0001;
                        keyRating3 = Strength * Overall * 0.0001;
                        break;
                    case "OL":
                        keyRating1 = Block * Overall * 0.0001;
                        keyRating2 = Strength * Overall * 0.0001;
                        keyRating3 = Intelligence * Overall * 0.0001;
                        break;
                    case "DL":
                        keyRating1 = Blitz * Overall * 0.0001;
                        keyRating2 = Strength * Overall * 0.0001;
                        keyRating3 = Tackle * Overall * 0.0001;
                        break;
                    case "LB":
                        keyRating1 = Speed * Overall * 0.0001;
                        keyRating2 = Blitz * Overall * 0.0001;
                        keyRating3 = Coverage * Overall * 0.0001;
                        break;
                    case "DB":
                        keyRating1 = Coverage * Overall * 0.0001;
                        keyRating2 = Speed * Overall * 0.0001;
                        keyRating3 = Intelligence * Overall * 0.0001;
                        break;
                    default: // kicker
                        keyRating1 = KickAccuracy * Overall * 0.0001;
                        keyRating2 = KickStrength * Overall * 0.0001;
                        keyRating3 = Intelligence * Overall * 0.0001;
                        break;
                }
                var ageExpMod = ageMod + expMod * 1.697265;
                var potModifier = potMod * 0.74496;
                var ratingsMod = (keyRating1 + keyRating2 + keyRating3) / 3 * 1.032275;
                return ratingsMod + potModifier - ageExpMod;
            }
        }

        public string TrainingAttribute
        {
            get
            {
                List<string> attributes;
                switch (Pos)
                {
                    case "QB":
                        attributes = new List<string> { "Strength", "Accuracy", "ArmStrength", "Carry", "Speed", "Intelligence" };
                        break;
                    case "RB":
                        attributes = new List<string> { "Strength", "Carry", "Speed", "Catching", "Block", "Intelligence" };
                        break;
                    case "WR":
                        attributes = new List<string> { "Strength", "Carry", "Speed", "Catching", "Block", "Intelligence" };
                        break;
                    case "OL":
                        attributes = new List<string> { "Strength", "Speed", "Block", "Intelligence" };
                        break;
                    case "DL":
                        attributes = new List<string> { "Strength", "Speed", "Blitz", "Tackle", "Intelligence" };
                        break;
                    case "LB":
                        attributes = new List<string> { "Strength", "Speed", "Blitz", "Coverage", "Tackle", "Intelligence" };
                        break;
                    case "DB":
                        attributes = new List<string> { "Strength", "Carry", "Speed", "Coverage", "Tackle", "Intelligence" };
                        break;
                    default: // K
                        attributes = new List<string> { "Strength", "Speed", "Intelligence", "KickStrength", "KickAccuracy" };
                        break;
                }

                var i = Random.Next(attributes.Count);
                return attributes[i];

                // this can be edited to make a 'smarter' way of picking what attribute to train by using the player work ethic and the rating value...
                // for now I will just pick a random one, no need to take the extra step and use a dictionary...
                /* 
                var attributeValues = new Dictionary<string, int>();
                foreach (var a in attributes)
                    attributeValues.Add(a, GetAttributeValue(a));

                int i = r.Next(attributeValues.Count);
                return attributeValues.Keys.ElementAt(i);
                
                */
            }
        }

        public int GetAttributeValue(string s) => (int)GetType().GetProperty(s)?.GetValue(this);

    }
}
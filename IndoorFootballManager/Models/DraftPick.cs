﻿using SQLite;
using IndoorFootballManager.DataAccess;

namespace IndoorFootballManager.Models
{
    public class DraftPick
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int TeamId { get; set; }
        public int CurrentTeamId { get; set; }
        public int Season { get; set; }
        public int Round { get; set; }
        public int Pick { get; set; }

        public DraftPick() { }

        public DraftPick(int teamId, int round, int pick, int season)
        {
            TeamId = teamId;
            CurrentTeamId = teamId;
            Season = season;
            Round = round;
            Pick = pick;
        }

        public static League CurrentLeague => IFM.CurrentLeague;

        private Team Team => Repo.Database.GetTeamById(CurrentTeamId);

        public string Abbreviation
        {
            get
            {
                var team = Repo.Database.GetTeamById(TeamId);
                var abbr = CurrentTeamId != 0
                    ? CurrentTeamId == TeamId
                        ? Team.Abbreviation
                        : $"(From {team.Abbreviation})"
                    : "";
                return abbr;
            }
        }

        public double PickValue
        {
            get
            {
                double num;
                switch (Round)
                {
                    case 1:
                        num = 100.0;
                        break;
                    case 2:
                        num = 80.0;
                        break;
                    case 3:
                        num = 60.0;
                        break;
                    case 4:
                        num = 50.0;
                        break;
                    case 5:
                        num = 40.0;
                        break;
                    case 6:
                        num = 30.0;
                        break;
                    case 7:
                        num = 20.0;
                        break;
                    case 8:
                        num = 10.0;
                        break;
                    default:
                        num = 5.0;
                        break;
                }
                return num;
            }
        }

    }
}

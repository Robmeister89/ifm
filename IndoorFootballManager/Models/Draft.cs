﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using IndoorFootballManager.DataAccess;

namespace IndoorFootballManager.Models
{
    public class Draft
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int Season { get; set; }
        public int Round { get; set; }
        public int Pick { get; set; }
        public int DraftPickId { get; set; }
        public int PlayerId { get; set; }
        public string Player { get; set; }
        public string Salary { get; set; }
        public int OrigTeamId { get; set; }
        public int CurTeamId { get; set; }
        
        public Draft() { }

        public Draft(int id, int t, int p)
        {
            Id = id;
            CurTeamId = t;
            PlayerId = p;
        }

        public override string ToString() => $"{Id},{CurTeamId},{PlayerId}";

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        internal Team Team => Database.GetTeamById(CurTeamId);

        public string CurrentTeam => Team.TeamName;

        internal static void CreateDraft()
        {
            var i = 1;
            var draft = new List<Draft>();
            var sortedTeams = Database.GetTeams().OrderBy(t => t.LastSeasonWins).ThenBy(t => t.LastSeasonSOS).Select(t => t.Id).ToList();
            var teams = Database.GetTeams().ToList();
            var picks = Database.GetPicks().Where(d => d.Season == CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ToList();
            foreach (var d in picks)
            {
                var cur = teams.FirstOrDefault(t => t.Id == d.CurrentTeamId);
                var newDraft = new Draft
                {
                    Season = CurrentLeague.CurrentSeason,
                    Round = d.Round,
                    Pick = sortedTeams.IndexOf(cur.Id) + 1,
                    DraftPickId = d.Id,
                    OrigTeamId = d.TeamId,
                    CurTeamId = d.CurrentTeamId,
                };
                draft.Add(newDraft);
                d.Pick = newDraft.Pick;
                i++;
                if (i == sortedTeams.Count() + 1)
                    i = 1;
            }
            _ = Database.InsertAll(draft.AsEnumerable());
            _ = Database.UpdateAll(picks.AsEnumerable());
        }

        private static List<int> _sortedTeams;

        internal static void CreateInauguralDraft()
        {
            var i = 1;
            var drafts = new List<Draft>();
            _sortedTeams = new List<int>();
            _sortedTeams.AddRange(Database.GetTeams().OrderBy(item => new Random().Next()).Select(t => t.Id).ToList());
            var picks = Database.GetPicks().Where(d => d.Season == CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ToList();
            var teams = Database.GetTeams().ToList();
            foreach (var d in picks)
            {
                var cur = teams.FirstOrDefault(t => t.Id == d.CurrentTeamId);
                var newDraft = new Draft
                {
                    Season = CurrentLeague.CurrentSeason,
                    Round = d.Round,
                    Pick = _sortedTeams.IndexOf(cur.Id) + 1,
                    DraftPickId = d.Id,
                    OrigTeamId = d.TeamId,
                    CurTeamId = d.CurrentTeamId
                };
                drafts.Add(newDraft);
                d.Pick = i;
                i++;
                if (i != _sortedTeams.Count() + 1) continue;
                i = 1;
                _sortedTeams = ReversedList(_sortedTeams);
            }
            _ = Database.InsertAll(drafts.AsEnumerable());
            _ = Database.UpdateAll(picks.AsEnumerable());
        }

        public static void UpdateDraft(Dictionary<int, int> newOrder)
        {
            var picks = Database.GetDraft(CurrentLeague.CurrentSeason).ToList();
            for (var i = 1; i <= newOrder.Count; i++)
            {
                var id = newOrder[i];
                _ = newOrder.TryGetValue(id, out var newPick);
                var updatedPicks = picks.Where(d => d.OrigTeamId == id).Select(d =>
                {
                    d.Pick = newPick;
                    return d;
                });
                Database.UpdateAllDrafts(updatedPicks);
                var draftPicks = Database.GetPicksByTeam(id).Select(d =>
                {
                    d.Pick = newPick;
                    return d;
                });
                _ = Database.UpdateAll(draftPicks);
            }
        }

        public static void UpdateInauguralDraft(Dictionary<int, int> newOrder)
        {
            var picks = Database.GetDraft(CurrentLeague.CurrentSeason).ToList();
            for(var i = 1; i <= newOrder.Count; i++)
            {
                var id = newOrder[i];
                _ = newOrder.TryGetValue(id, out var newPick);
                var updatedPicks = picks.Where(d => d.OrigTeamId == id).Select(d =>
                {
                    if (d.Round % 2 == 1) d.Pick = newPick;
                    else d.Pick = newOrder.Count() - (newPick - 1);
                    return d;
                });
                Database.UpdateAllDrafts(updatedPicks);
                var draftPicks = Database.GetPicksByTeam(id).Select(d =>
                {
                    d.Pick = newPick;
                    return d;
                });
                _ = Database.UpdateAll(draftPicks);
            }
        }

        private static List<int> ReversedList(List<int> list)
        {
            list.Reverse();
            return list;
        }


    }
}

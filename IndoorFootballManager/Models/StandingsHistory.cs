﻿using SQLite;

namespace IndoorFootballManager.Models
{
    public class StandingsHistory
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int Season { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int PointsFor { get; set; }
        public int PointsAgainst { get; set; }
        public int DivWins { get; set; }
        public int DivLosses { get; set; }
        public int ConfWins { get; set; }
        public int ConfLosses { get; set; }
        public int HomeWins { get; set; }
        public int HomeLosses { get; set; }
        public int AwayWins { get; set; }
        public int AwayLosses { get; set; }
        public int Streak { get; set; }
        public string Conference { get; set; }
        public string Division { get; set; }

        public double WinPct => Wins == 0 ? 0.000 : Wins / (double)(Wins + Losses);
        public double DivPct => DivWins == 0 ? 0.000 : DivWins / (double)(DivWins + DivLosses);
        public double ConfPct => ConfWins == 0 ? 0.000 : ConfWins / (double)(ConfWins + ConfLosses);
        public string TeamRecord => $"{Wins}-{Losses}";
        public string HomeRecord => $"{HomeWins}-{HomeLosses}";
        public string AwayRecord => $"{AwayWins}-{AwayLosses}";
        public string DivRecord => $"{DivWins}-{DivLosses}";
        public string ConfRecord => $"{ConfWins}-{ConfLosses}";
        public string StreakTxt => Streak == 0 ? "-" : Streak > 0 ? $"W{Streak}" : $"L{Streak * -1}";



    }
}

﻿namespace IndoorFootballManager.Models
{
    public class Injury
    {
        public int MinTime { get; set; }
        public int MaxTime { get; set; }
        public bool CareerEnding { get; set; }
        public string Diagnosis { get; set; }
        public int Frequency { get; set; }

        public Injury() { }

        public Injury(int min, int max, bool ending, string txt, int freq)
        {
            MinTime = min;
            MaxTime = max;
            CareerEnding = ending;
            Diagnosis = txt;
            Frequency = freq;
        }

    }
}

﻿using System;
using SQLite;

namespace IndoorFootballManager.Models
{
    public class Email
    {


        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int GmId { get; set; }
        public int SenderId { get; set; }
        public int TeamId { get; set; }
        public int PlayerId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime SendDate { get; set; }
        public bool CanReply { get; set; }
        public bool Read { get; set; }
        


    }
}

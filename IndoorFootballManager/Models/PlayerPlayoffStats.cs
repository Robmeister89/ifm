﻿using System;
using SQLite;
using IndoorFootballManager.Services;
using Newtonsoft.Json;

namespace IndoorFootballManager.Models
{
    [Serializable]
    public class PlayerPlayoffStats
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int Season { get; set; }
        public int PlayerId { get; set; }
        public string Team { get; set; }
        public bool Started { get; set; }
        public int PassAtt { get; set; }
        public int PassCmp { get; set; }
        public int PassYds { get; set; }
        public int PassTds { get; set; }
        public int PassInts { get; set; }
        public int Pass20 { get; set; }
        public int PassFirst { get; set; }
        public int PassLong { get; set; }
        public int Carries { get; set; }
        public int RushYds { get; set; }
        public int RushTds { get; set; }
        public int Rush20 { get; set; }
        public int RushFirst { get; set; }
        public int RushLong { get; set; }
        public int Receptions { get; set; }
        public int RecYds { get; set; }
        public int RecTds { get; set; }
        public int Rec20 { get; set; }
        public int RecFirst { get; set; }
        public int RecLong { get; set; }
        public int Drops { get; set; }
        public int Fumbles { get; set; }
        public int FumblesLost { get; set; }
        public int Tackles { get; set; }
        public int Sacks { get; set; }
        public int ForFumb { get; set; }
        public int FumbRec { get; set; }
        public int PassBreakups { get; set; }
        public int Ints { get; set; }
        public int IntYds { get; set; }
        public int Safeties { get; set; }
        public int DefTds { get; set; }
        public int FGAtt { get; set; }
        public int FGMade { get; set; }
        public int FGLong { get; set; }
        public int XPAtt { get; set; }
        public int XPMade { get; set; }
        public int Rouges { get; set; }
        public int KickReturns { get; set; }
        public int KickRetYds { get; set; }
        public int KickReturnTds { get; set; }
        public int KickReturnLong { get; set; }
        public int PancakeBlocks { get; set; }
        public int SacksAllowed { get; set; }
        public string StatsJson { get; set; }
        [Obsolete] public byte[] StatsArray { get; set; }

        public PlayerPlayoffStats() { }

        [Ignore] public string PlayerName { get; set; }
        [Ignore] public int TeamId { get; set; }
        [JsonIgnore] public int Points => (PassTds * 6) + (RushTds * 6) + (RecTds * 6) + (KickReturnTds * 6) + XPMade + (FGMade * 3);
        [JsonIgnore] public int AllPurposeYards => RushYds + RecYds + KickRetYds;

        [JsonIgnore]
        public double QBRating
        {
            get
            {
                var a = (((PassCmp / (double)PassAtt * 100) - 30) / 20.0).RatingValue();
                var b = (PassTds / (double)PassAtt * 100 / (20 / 3.0)).RatingValue();
                var c = ((9.5 - (PassInts / (double)PassAtt) * 100) / 4.0).RatingValue();
                var d = ((PassYds / (double)PassAtt - 3) / 4.0).RatingValue();
                return (a + b + c + d) / 0.06;
            }
        }

        [JsonIgnore]
        public double CompPct => PassAtt > 0
            ? (double)PassCmp / PassAtt
            : 0.0;

        [JsonIgnore]
        public double PassYPA => PassAtt > 0
            ? (double)PassYds / PassAtt
            : 0.0;

        [JsonIgnore]
        public double PassYPC => PassCmp > 0
            ? (double)PassYds / PassCmp
            : 0.0;

        [JsonIgnore]
        public double PassTdPct => PassCmp > 0
            ? (double)PassTds / PassAtt
            : 0.0;

        [JsonIgnore]
        public double PassIntPct => PassCmp > 0
            ? (double)PassInts / PassAtt
            : 0.0;

        [JsonIgnore]
        public double Pass1stPct => PassFirst > 0
            ? (double)PassFirst / PassAtt
            : 0.0;

        [JsonIgnore]
        public double RushAvg => Carries > 0
            ? (double)RushYds / Carries
            : 0.0;

        [JsonIgnore]
        public double RushTdPct => Carries > 0
            ? (double)RushTds / Carries
            : 0.0;

        [JsonIgnore]
        public double Rush1stPct => Carries > 0
            ? (double)RushFirst / Carries
            : 0.0;

        [JsonIgnore]
        public double RushFumblePct => Carries > 0
            ? (double)Fumbles / Carries
            : 0.0;

        [JsonIgnore]
        public double RushFumLostPct => Carries > 0
            ? (double)FumblesLost / Carries
            : 0.0;

        [JsonIgnore]
        public double RecAvg => Receptions > 0
            ? (double)RecYds / Receptions
            : 0.0;

        [JsonIgnore]
        public double RecTdPct => Receptions > 0
            ? (double)RecTds / Receptions
            : 0.0;

        [JsonIgnore]
        public double Rec1stPct => Receptions > 0
            ? (double)RecFirst / Receptions
            : 0.0;

        [JsonIgnore]
        public double RecFumblePct => Receptions > 0
            ? (double)Fumbles / Receptions
            : 0.0;

        [JsonIgnore]
        public double RecFumLostPct => Receptions > 0
            ? (double)FumblesLost / Receptions
            : 0.0;

        [JsonIgnore]
        public double ReturnAvg => KickReturns > 0
            ? (double)KickRetYds / KickReturns
            : 0.0;

        [JsonIgnore]
        public double RetTdPct => KickReturns > 0
            ? (double)KickReturnTds / KickReturns
            : 0.0;

        [JsonIgnore]
        public double XpPct => XPAtt > 0
            ? (double)XPMade / XPAtt
            : 0.0;

        [JsonIgnore]
        public double FgPct => FGAtt > 0
            ? (double)FGMade / FGAtt
            : 0.0;


    }
}


﻿using System;
using System.Collections.Generic;
using SQLite;
using IndoorFootballManager.DataAccess;
using System.Linq;

namespace IndoorFootballManager.Models
{
    public class League
    {        
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public string LeagueName { get; set; }
        public string Abbreviation { get; set; }
        public int CurrentSeason { get; set; }
        public int Conferences { get; set; }
        public int Divisions { get; set; }
        public int LeagueTeams { get; set; }
        public int DraftRounds { get; set; }
        public int PlayoffTeams { get; set; }
        public int SalaryCap { get; set; }
        public int MinimumSalary { get; set; }
        public int MaximumSalary { get; set; }
        public int MaximumYears { get; set; }
        public int MaxDraftSalary { get; set; }
        public int Season { get; set; }
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }
        public bool IsInauguralDraft { get; set; }
        public bool IsOffSeason { get; set; }
        public bool IsPlayoffs { get; set; }
        public bool IsTrainingCamp { get; set; }
        public bool EndPlayoffs { get; set; }
        public int MaxRoster { get; set; }
        public int ReserveRoster { get; set; }
        public bool UseFTP { get; set; }
        public string FtpServer { get; set; }
        public string FtpWebRoot { get; set; }
        public string FtpRemotePath { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public int PenaltyMod { get; set; }
        public bool UseInjuries { get; set; }
        public bool UseRetirement { get; set; }
        public DateTime CurDate { get; set; }
        public DateTime DraftDate { get; set; }
        public DateTime InSeasonDate { get; set; }
        public DateTime SeasonEndDate { get; set; }
        public DateTime TrainingCampDate { get; set; }
        public DateTime NewSeasonDate { get; set; }
        public string Conference1 { get; set; }
        public string Conference2 { get; set; }
        public string Division1 { get; set; }
        public string Division2 { get; set; }
        public string Division3 { get; set; }
        public string Division4 { get; set; }
        public string Division5 { get; set; }
        public string Division6 { get; set; }
        public string Division7 { get; set; }
        public string Division8 { get; set; }

        public League() { }

        public League(int id, string leagueName, string leagueAbbr, int season, int conferences, int divisions, int leagueTeams)
        {
            Id = id;
            LeagueName = leagueName;
            Abbreviation = leagueAbbr;
            Season = season;
            Conferences = conferences;
            Divisions = divisions;
            LeagueTeams = leagueTeams;
        }

        [Ignore]
        public List<string> DivisionsList
        {
            get
            {
                var divisions = new List<string>();
                if (Division1 != null)
                    divisions.Add(Division1);
                if (Division2 != null)
                    divisions.Add(Division2);
                if (Division3 != null)
                    divisions.Add(Division3);
                if (Division4 != null)
                    divisions.Add(Division4);
                if (Division5 != null)
                    divisions.Add(Division5);
                if (Division6 != null)
                    divisions.Add(Division6);
                if (Division7 != null)
                    divisions.Add(Division7);
                if (Division8 != null)
                    divisions.Add(Division8);

                return divisions;
            }
        }

        public List<string> GetConferenceDivisions(string conf)
        {
            var teams = Repo.Database.GetTeams().Where(t => t.Conference == conf).ToList();
            var divisions = teams.Select(t => t.Division).Distinct().ToList();
            return divisions;
        }

        public List<DateTime> ImportantDates
        {
            get
            {
                var importantDates = new List<DateTime>()
                {
                    NewSeasonDate,
                    DraftDate,
                    TrainingCampDate,
                    InSeasonDate,
                    SeasonEndDate,
                };
                return importantDates;
            }
        }

        [Ignore] public string SystemPassword => "Giants8911!!";
        [Ignore] public int RegularSeasonDaysRemaining => (SeasonEndDate - CurDate).Days;
    }
}

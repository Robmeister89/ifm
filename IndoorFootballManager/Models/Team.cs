﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Media;
using SQLite;
using IndoorFootballManager.Services;
using IndoorFootballManager.DataAccess;
using Geolocation;
using System.Web.UI;

namespace IndoorFootballManager.Models
{
    public class Team
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Indexed]
        public string City { get; set; }
        public string Mascot { get; set; }
        public byte[] Logo { get; set; }
        public string TeamName { get; set; }
        public string Abbreviation { get; set; }
        public string Conference { get; set; }
        public string Division { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int PointsFor { get; set; }
        public int PointsAgainst { get; set; }
        public int DivWins { get; set; }
        public int DivLosses { get; set; }
        public int ConfWins { get; set; }
        public int ConfLosses { get; set; }
        public int HomeWins { get; set; }
        public int HomeLosses { get; set; }
        public int AwayWins { get; set; }
        public int AwayLosses { get; set; }
        public int Streak { get; set; }
        public int LastSeasonFinish { get; set; }
        public int LastSeasonWins { get; set; }
        public int LastSeasonLosses { get; set; }
        public double LastSeasonSOS { get; set; }
        public int DraftRounds { get; set; }
        public int Season { get; set; }
        public bool FranchiseTagUsed { get; set; }
        public bool IsCpu { get; set; }
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }
        public bool UseDefaultDepthChart { get; set; } = true;
        public int OffPhilosophy { get; set; }
        public DateTime CurDate { get; set; }
        public string ExportStatus { get; set; }
        public string ExportTime { get; set; }
        [Obsolete] public byte[] DepthChart { get; set; }
        public string GameVersion { get; set; }

        [Ignore]
        public int Longitude { get; set; } // may not be necessary...
        [Ignore]
        public int Latitude { get; set; } // may not be necessary...
        [Ignore]
        public Coordinate Location { get; set; }

        [Ignore]
        public int Round { get; set; }

        private List<DraftPick> _draftPicks;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        public GeneralManager Manager => Database?.GetManagerByTeamId(Id);

        public Team() { }

        public Team(int teamId, string city, string abbr, string mascot, string conference, string division, int season)
        {
            Id = teamId;
            City = city;
            Abbreviation = abbr;
            Mascot = mascot;
            TeamName = City + " " + Mascot;
            Conference = conference;
            Division = division;
            Season = season;
            FranchiseTagUsed = false;
            IsCpu = true;
            GameVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public ImageSource TeamLogo => Converters.LoadImage(Logo);
        public double WinPct => Wins == 0 ? 0.000 : Wins / (double)(Wins + Losses);
        public double DivPct => DivWins == 0 ? 0.000 : DivWins / (double)(DivWins + DivLosses);
        public double ConfPct => ConfWins == 0 ? 0.000 : ConfWins / (double)(ConfWins + ConfLosses);
        public string TeamRecord => $"{Wins}-{Losses}";
        public string HomeRecord => $"{HomeWins}-{HomeLosses}";
        public string AwayRecord => $"{AwayWins}-{AwayLosses}";
        public string DivRecord => $"{DivWins}-{DivLosses}";
        public string ConfRecord => $"{ConfWins}-{ConfLosses}";
        public string StreakTxt => Streak == 0 ? "-" : Streak > 0 ? $"W{Streak}" : $"L{Streak * -1}";
        public string DraftText => $"{TeamName} ({LastSeasonWins}-{LastSeasonLosses}, {LastSeasonSOS:N3})";

        public string VsOpponent(Team team2)
        {
            var games = Database.GetTvTGames(CurrentLeague.CurrentSeason, this, team2).ToList();
            var count = games.Count;
            var wins = games.Sum(game => game.HomeTeam == Id
                ? game.HomeFinal > game.AwayFinal
                    ? 1
                    : 0
                : game.AwayFinal > game.HomeFinal
                    ? 1
                    : 0);

            var losses = count - wins;
            return $"{wins}-{losses}";
        }

        internal Enum OffensivePhilosophy
        {
            get => (Enums.OffensivePhilosophies)Enum.ToObject(typeof(Enums.OffensivePhilosophies), OffPhilosophy);
            set { }
        }

        internal string OffensivePhilosophyStr => OffensivePhilosophy.ToString("f");

        public void SetDepthChart()
        {
            try
            {
                if (IsCpu)
                    this.CpuSetDepthChart();
                else
                {
                    var dc = Database.GetDepthChart(Id);
                    var props = dc.GetType().GetProperties()
                        .Where(prop =>
                            prop.PropertyType ==
                            typeof(string)); // the new json depth chart properties are string values; so only check those...
                    if (props.Any(prop => prop.GetValue(dc) == null))
                    {
                        this.CpuSetDepthChart();
                    }

                    this.UpdateDepthChart();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
        }

        public List<DraftPick> InauguralDraftPicks(int rounds)
        {
            //CREATE A LIST OF DRAFT PICKS FOR EACH TEAM
            _draftPicks = new List<DraftPick>();
            if (Id <= 0) return _draftPicks;
            for (Round = 1; Round <= rounds; Round++)
            {
                _draftPicks.Add(new DraftPick(Id, Round, 0, Season));
            }
            return _draftPicks;
        }

        public List<DraftPick> DraftPicks(int rounds)
        {
            _draftPicks = new List<DraftPick>();

            var num1 = Season + 1;
            var num2 = Season + 4;
            for (var i = num1; i < num2; i++)
            {
                for (Round = 1; Round <= rounds; Round++)
                {
                    _draftPicks.Add(new DraftPick(Id, Round, 0, i));
                }
            }
            return _draftPicks;
        }

        public List<DraftPick> NewDraftPicks
        {
            get
            {
                //CREATE A LIST OF DRAFT PICKS FOR EACH TEAM
                _draftPicks = new List<DraftPick>();
                var season = CurrentLeague.CurrentSeason + 4;
                if (Id <= 0) return _draftPicks;
                for (Round = 1; Round <= CurrentLeague.DraftRounds; Round++)
                {
                    _draftPicks.Add(new DraftPick(Id, Round, 0, season));
                }
                return _draftPicks;
            }
        }

        [Ignore] 
        public IEnumerable<Player> Players => Database?.GetPlayersByTeamId(Id);

        [Ignore]
        public IEnumerable<Player> ActiveRoster => Players?.Where(p => !p.RosterReserve && !p.InjuredReserve);
        [Ignore]
        public IEnumerable<Player> ReservePlayers => Players?.Where(p => p.RosterReserve);
        [Ignore]
        public IEnumerable<Player> InjuredReserved => Players?.Where(p => p.InjuredReserve);

        public Player EmergencyQB => ActiveRoster?.OrderByDescending(p => p.Accuracy)
            .ThenByDescending(p => p.ArmStrength).ThenByDescending(p => p.Intelligence)
            .FirstOrDefault(p => p.Pos != "QB");

        public Player EmergencyKicker => ActiveRoster?.OrderByDescending(p => p.KickStrength)
            .ThenByDescending(p => p.KickAccuracy).ThenByDescending(p => p.Intelligence)
            .FirstOrDefault(p => p.Pos != "K");
        
        private void CheckSalaryCap()
        {
            if (!(CapRoom < 0)) 
                return;

            var over = (int)CapRoom * -1;
            if (ReservePlayers.Any())
            {
                if (ReservePlayers.Any(p => p.Salary >= over))
                {
                    var player = ReservePlayers.FirstOrDefault(p => p.Salary >= (CapRoom * -1));
                    PlayerData.ReleasePlayer(player);
                }
                else
                {
                    while (CapRoom > 0)
                    {
                        if (ReservePlayers.Any())
                            PlayerData.ReleasePlayer(ReservePlayers.OrderBy(p => p.Overall).First());
                        else
                            break;
                        if (CapRoom > 0)
                            break;
                    }
                }
            }
            var playerList = Players.Where(p => p.Salary >= over).OrderBy(p => p.Overall).ToList();
            if (CapRoom < 0 && playerList.Any())
            {
                for (var i = 0; i < playerList.Count(); i++)
                {
                    var player = playerList[i];
                    var posCount = TeamData.MinPositions.First(m => m.Key == player.Pos).Value;
                    if (Players.Count(p => p.Pos == player.Pos) > posCount)
                    {
                        PlayerData.ReleasePlayer(player);
                    }
                    if (CapRoom > 0)
                        break;
                }
            }

            if (!(CapRoom < 0)) 
                return;
            var newList = Players.OrderBy(p => p.Overall).ToList();
            for (var i = 0; i < newList.Count(); i++)
            {
                var player = newList[i];
                var posCount = TeamData.MinPositions.First(m => m.Key == player.Pos).Value;
                if (Players.Count(p => p.Pos == player.Pos) > posCount)
                {
                    PlayerData.ReleasePlayer(player);
                }
                if (CapRoom > 0)
                    break;
            }
        }

        public void FillRoster()
        {
            var maxRoster = CurrentLeague.MaxRoster;
            var resRoster = CurrentLeague.ReserveRoster;

            //CheckSalaryCap();

            var daysLeft = CurrentLeague.SeasonEndDate.AddDays(-60).Subtract(CurrentLeague.CurDate).TotalDays;
            foreach(var p in ActiveRoster.Where(p => p.IsInjured))
            {
                if (p.InjuryDuration >= daysLeft)
                {
                    p.InjuredReserve = true;
                    p.RosterReserve = false;
                    Repo.Database.SavePlayer(p);
                }
            }

            if (ActiveRoster.Count() != maxRoster && ReservePlayers.Count() != resRoster)
            {
                var freeAgents = Repo.Database.GetPlayersByTeamId(0).OrderByDescending(p => p.Evaluation).ToList();
                // get to minimums... check reserves before signing free agents
                foreach (var positionCount in TeamData.MinPositions)
                {
                    var position = positionCount.Key;
                    var count = positionCount.Value;
                    var rosterCount = ActiveRoster.Count(p => p.Pos == position);
                    if (rosterCount >= count) continue;
                    {
                        var freeAgentList = freeAgents.Where(p => p.Pos == position).ToList();
                        var player = new Player();
                        while (player != null)
                        {
                            player = ReservePlayers.OrderByDescending(p => p.Overall).FirstOrDefault(p => p.Pos == position);
                            player?.ActivatePlayer();
                            rosterCount = ActiveRoster.Count(p => p.Pos == position);
                            if (rosterCount == count)
                                break;
                        }

                        for (var i = rosterCount; i < count; i++)
                        {
                            if (position != "K")
                            {
                                var take = freeAgentList.Count() < 8 ? freeAgentList.Count() - 1 : 8;
                                PlayerData.CpuSignInSeason(this, freeAgentList.ElementAt(new Random().Next(0, take)), false);
                            }
                            else
                            {
                                PlayerData.CpuSignInSeason(this, freeAgentList.First(), false);
                            }
                        }
                    }
                }
            }

            // do something with overflow of players.. move to reserved or cut... (injuries adding later)
            if (ActiveRoster.Count() > maxRoster)
            {
                var activeRoster = ActiveRoster.OrderBy(pl => pl.Evaluation).ToList();
                // move players to reserve...
                for (var i = 0; i < activeRoster.Count(); i++)
                {
                    var p = activeRoster.ElementAt(i);
                    var pos = p.Pos;
                    var count = activeRoster.Count(pl => pl.Pos == pos);
                    var min = TeamData.MinPositions.First(po => po.Key == pos).Value;
                    if (count > min)
                    {
                        PlayerData.AssignToReserves(p);
                        activeRoster.Remove(p);
                        //Console.WriteLine(p.ShortNameWithPosition + " moved to reserves on " + TeamName);
                        if (activeRoster.Count() == maxRoster)
                            break;
                    }
                    else
                        continue;
                }

                //int over = Players.Count() - 33;
                // cut down to 33...
                if (ReservePlayers.Count() > resRoster)
                {
                    var reserveRoster = ReservePlayers.OrderBy(pl => pl.Evaluation).ToList();
                    while (reserveRoster.Count() > resRoster)
                    {
                        PlayerData.ReleasePlayer(reserveRoster.First());
                        reserveRoster.RemoveAt(0);
                        if (!reserveRoster.Any())
                            break;
                    }
                }
            }

            if (ActiveRoster.Count() < maxRoster && !ReservePlayers.Any())
            {
                var freeAgents = Database.GetPlayersByTeamId(0).ToList();
                while(ActiveRoster.Count() < maxRoster)
                {
                    var pl = freeAgents.First(p => p.Pos != "K");
                    PlayerData.CpuSignInSeason(this, pl, false);
                    freeAgents.Remove(pl);
                }
            }

            // TODO: add in checks for under 28 and under 5 on reserves and sign free agents
            if(ActiveRoster.Count() < maxRoster && ReservePlayers.Count() == resRoster)
            {
                var freeAgents = Database.GetPlayersByTeamId(0).ToList();
                var reserveRoster = ReservePlayers.OrderByDescending(pl => pl.Evaluation).ToList();
                var count = maxRoster - ActiveRoster.Count();
                for(var i = 0; i < count; i++)
                {
                    if(reserveRoster.Count() >= i)
                    {
                        PlayerData.ActivatePlayer(reserveRoster.First());
                        reserveRoster.RemoveAt(0);
                    }
                    else
                    {
                        var pl = freeAgents.First(p => p.Pos != "K");
                        PlayerData.CpuSignInSeason(this, pl, false);
                        freeAgents.Remove(pl);
                    }
                }
            }

            if(ActiveRoster.Count() == maxRoster && ReservePlayers.Count() < resRoster)
            {
                var freeAgents = Database.GetPlayersByTeamId(0).ToList();
                while(ReservePlayers.Count() < resRoster)
                {
                    var pl = freeAgents.Where(p => p.Pos != "K").OrderByDescending(p => p.Evaluation).First();
                    PlayerData.CpuSignInSeason(this, pl, true);
                    freeAgents.Remove(pl);
                }
            }

            if(ActiveRoster.Count() == maxRoster && ReservePlayers.Count() > resRoster)
            {
                var reserveRoster = ReservePlayers.OrderBy(pl => pl.Evaluation).ToList();
                while (reserveRoster.Count() > resRoster)
                {
                    PlayerData.ReleasePlayer(reserveRoster.First());
                    reserveRoster.RemoveAt(0);
                    if (!reserveRoster.Any())
                        break;
                }
            }

            CheckSalaryCap();
        }

        internal void OfferFreeAgents(SQLiteConnection db)
        {
            var maxRoster = CurrentLeague.MaxRoster;
            var resRoster = CurrentLeague.ReserveRoster;
            var total = maxRoster + resRoster;

            if (Players.Count() >= total)
                return;

            var needs = new List<string>();
            for (var i = 0; i < TeamData.MinPositions.Count(); i++)
            {
                var pos = TeamData.MinPositions.ElementAt(i);
                var players = Players.Count(p => p.Pos == pos.Key);
                var count = pos.Value;
                if (players < count)
                    needs.Add(pos.Key);
            }

            if (!needs.Any())
                return;

            var offers = Repo.GetContractOffers(db).Where(c => c.Active && c.TeamId == Id).ToList();
            if (offers.Any())
            {
                var positions = new HashSet<string>();
                foreach (var p in offers.Select(offer => Repo.GetPlayerById(db, offer.PlayerId)))
                {
                    _ = positions.Add(p.Pos);
                }
                _ = needs.RemoveAll(positions.Contains);
            }

            var freeAgents = Repo.GetPlayersByTeamId(db, 0).OrderByDescending(p => p.Evaluation).ToList();
            foreach (var p in from need in needs
                              let max = freeAgents.Count(pl => pl.Pos == need)
                              select max > 10
                                ? freeAgents.Where(pl => pl.Pos == need).ElementAt(new Random().Next(0, 10))
                                : freeAgents.Where(pl => pl.Pos == need).ElementAt(new Random().Next(0, max - 1)))
            {
                PlayerData.CpuOfferOffSeasonContract(this, p);
            }
        }



        public double PassProf
        {
            get
            {
                var wrs = WRs.ToList();
                return OffensivePhilosophy.Equals(Enums.OffensivePhilosophies.ThreeWRs)
                    ? (wrs[0].Overall + wrs[1].Overall + wrs[2].Overall) / 3
                    : (wrs[0].Overall + wrs[1].Overall) / 2;
            }
        }

        public double RushProf
        {
            get
            {
                var rbs = RBs.ToList();
                return OffensivePhilosophy.Equals(Enums.OffensivePhilosophies.ThreeRBs)
                    ? (rbs[0].Overall + rbs[1].Overall + rbs[2].Overall) / 3
                    : (rbs[0].Overall + rbs[1].Overall) / 2;
            }
        }

        public double CompositeOlBlock => (double)(OLine[0].Block + OLine[1].Block + OLine[2].Block) / 3;

        public double PassDef
        {
            get
            {
                var dbs = DBs.ToList();
                var lb = LBs[0];
                return (double)(dbs[0].Coverage + dbs[1].Coverage + dbs[2].Coverage + lb.Coverage) / 4;
            }
        }

        public double RushDef
        {
            get
            {
                var dl = DLine.ToList();
                var lb = LBs[1];
                return (double)(dl[0].Blitz + dl[1].Blitz + dl[2].Blitz + lb.Blitz) / 4;
            }
        }

        public double CompositeIntelligence => ((QBs[0].Intelligence * 1.5)
                                                + RBs[0].Intelligence
                                                + WRs.Take(3).Average(p => p.Intelligence)
                                                + OLine.Take(3).Average(p => p.Intelligence)
                                                + DLine.Take(3).Average(p => p.Intelligence)
                                                + LBs.Take(2).Average(p => p.Intelligence)
                                                + DBs.Take(3).Average(p => p.Intelligence)) / 10;

        // create this injury section to fit our Team class!
        //public void checkForInjury()
        //{
        //    playersInjured = new ArrayList<>();
        //    playersRecovered = new ArrayList<>();
        //    if (league.isHardMode())
        //    {
        //        checkInjuryPosition(teamQBs, 1);
        //        checkInjuryPosition(teamRBs, 2);
        //        checkInjuryPosition(teamWRs, 3);
        //        checkInjuryPosition(teamOLs, 5);
        //        checkInjuryPosition(teamKs, 1);
        //        checkInjuryPosition(teamSs, 1);
        //        checkInjuryPosition(teamCBs, 3);
        //        checkInjuryPosition(teamF7s, 1);
        //    }
        //}

        //private void checkInjuryPosition(ArrayList<? extends Player> players, int numStarters)
        //{
        //    int numInjured = 0;

        //    foreach (Player p in players)
        //    {
        //        if (p.injury != null)
        //        {
        //            p.injury.advanceGame();
        //            numInjured++;
        //            if (p.injury == null)
        //            {
        //                playersRecovered.add(p);
        //                playersInjuredAll.remove(p);
        //            }
        //        }
        //    }

        //    // Only injure if there are people left to injure
        //    if (numInjured < numStarters)
        //    {
        //        for (int i = 0; i < numStarters; ++i)
        //        {
        //            Player p = players.get(i);
        //            if (Math.random() < Math.pow(1 - (double)p.ratDur / 100, 3) && numInjured < numStarters)
        //            {
        //                // injury!
        //                p.injury = new Injury(p);
        //                playersInjured.add(p);
        //                playersInjuredAll.add(p);
        //                numInjured++;
        //            }
        //        }
        //    }

        //    if (numInjured > 0) Collections.sort(players, new PlayerComparator());
        //}

        internal List<Player> QBs => ActiveRoster.Where(p => p.Pos == "QB").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> RBs => ActiveRoster.Where(p => p.Pos == "RB").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> WRs => ActiveRoster.Where(p => p.Pos == "WR").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> OLine => ActiveRoster.Where(p => p.Pos == "OL").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> DLine => ActiveRoster.Where(p => p.Pos == "DL").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> LBs => ActiveRoster.Where(p => p.Pos == "LB").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> DBs => ActiveRoster.Where(p => p.Pos == "DB").OrderByDescending(p => p.Overall).ToList();
        internal List<Player> Ks => ActiveRoster.Where(p => p.Pos == "K").OrderByDescending(p => p.Overall).ToList();

        [Ignore]
        internal List<Player> Returners
        {
            get
            {
                var returners = new List<Player>();
                returners.AddRange(RBs);
                returners.AddRange(WRs);
                returners.AddRange(DBs);
                return returners.OrderByDescending(p => p.ReturnRating).ToList();
            }
        }

        public double TeamSalaries => Players.Select(p => p.Salary).Sum();

        public double CapRoom => CurrentLeague.SalaryCap - TeamSalaries;

        public class Need
        {
            public string Position { get; set; }
            public double Rating { get; set; }
            public double AvgAge { get; set; }
            public double AvgOvr { get; set; }
            public int Count { get; set; }
            public int Min { get; set; }
            public double FinalRating { get; set; }
            private double PctMin => Count == 0 ? 0.0 : Count / (double)Min;
            private double X => Count == 0 ? 0.0 : Count * AvgOvr / (Count * AvgAge);
            private double FinalRate => X == 0.0 ? 0.0 : PctMin / X;
        }

        private List<Need> _teamNeeds;
        public void SetTeamNeed()
        {
            _teamNeeds = new List<Need>();
            foreach (var position in TeamData.MinPositions)
            {
                var pos = position.Key;
                var players = Players.Count(p => p.Pos == pos);
                var count = position.Value;
                if (players < count)
                    _teamNeeds.Add(new Need { Position = pos, Count = count - players });
            }
        }

        public List<string> TeamNeeds
        {
            get
            {
                var needs = new List<string>();
                SetTeamNeed();
                foreach(var n in _teamNeeds)
                {
                    for (var i = 0; i < n.Count; i++)
                        needs.Add(n.Position);
                }
                return needs;
            }
        }

        public string GetTeamNeed
        {
            get
            {
                SetTeamNeed();
                var sorted = _teamNeeds.OrderBy(n => n.Count).ToList();
                return sorted.Any() ? sorted.First().Position : string.Empty;
            }
        }

        public double GetTeamNeedRating
        {
            get
            {
                SetTeamNeed();
                var sorted = _teamNeeds.OrderBy(n => n.FinalRating).ToList();
                return sorted.Any() ? sorted.First().FinalRating : 0.0;
            }
        }

        public int PlayoffWins
        {
            get
            {
                var games = Database.GetGames(CurrentLeague.CurrentSeason)
                    .Where(g => g.Playoffs && (g.HomeTeam == Id || g.AwayTeam == Id))
                    .OrderBy(g => g.GameDate).ToList();

                if (!games.Any())
                    return 0;

                return games.Sum(g => Id == g.HomeTeam
                    ? g.HomeFinal > g.AwayFinal
                        ? 1
                        : 0
                    : g.AwayFinal > g.HomeFinal
                        ? 1
                        : 0);
            }
        }


        public int PlayoffLosses
        {
            get
            {
                var games = Database.GetGames(CurrentLeague.CurrentSeason)
                    .Where(g => g.Playoffs && (g.HomeTeam == Id || g.AwayTeam == Id))
                    .OrderBy(g => g.GameDate).ToList();

                if (!games.Any())
                    return 0;

                return games.Sum(g => Id == g.HomeTeam
                    ? g.HomeFinal < g.AwayFinal
                        ? 1
                        : 0
                    : g.AwayFinal < g.HomeFinal
                        ? 1
                        : 0);
            }
        }

        public string PlayoffRecord => $"{PlayoffWins}-{PlayoffLosses}";

        [Ignore]
        public double SOS
        {
            get
            {
                var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.HomeTeam == Id || g.AwayTeam == Id).ToList();
                var homeOpp = games.Where(g => g.AwayTeam != Id).Select(g => g.AwayTeam).ToList();
                var awayOpp = games.Where(g => g.HomeTeam != Id).Select(g => g.HomeTeam).ToList();
                var opponents = homeOpp.Select(id => Database.GetTeamById(id)).ToList();
                opponents.AddRange(awayOpp.Select(id => Database.GetTeamById(id)));
                var or = opponents.Average(t => t.WinPct);
                var oor = new List<double>();
                foreach (var t in opponents)
                {
                    var games2 = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.HomeTeam == t.Id || g.AwayTeam == t.Id).ToList();
                    var homeOpp2 = games2.Where(g => g.AwayTeam != t.Id).Select(g => g.AwayTeam).ToList();
                    var awayOpp2 = games2.Where(g => g.HomeTeam != t.Id).Select(g => g.HomeTeam).ToList();
                    var opponents2 = homeOpp2.Select(tm => Database.GetTeamById(tm)).ToList();
                    opponents2.AddRange(awayOpp2.Select(tm => Database.GetTeamById(tm)));
                    var or2 = opponents.Average(tm => tm.WinPct);
                    oor.Add(or2);
                }

                return Math.Round(((or * 2) + oor.Average()) / 3, 3);
            }
        }

        public int PollRank
        {
            get
            {
                var poll = Database.GetTeams().OrderByDescending(t => t.PowerRanking).ToList();
                return poll.IndexOf(this) + 1;
            }
        }

        public double PowerRanking => (Wins * 0.35)
                                      - (Losses * 0.35)
                                      + (PointsFor * 0.0005)
                                      - (PointsAgainst * 0.0005)
                                      - (HomeLosses * 0.25)
                                      + (AwayWins * 0.25)
                                      + (DivWins * 0.25)
                                      - (DivLosses * 0.25)
                                      + (ConfWins * 0.25)
                                      - (ConfLosses * 0.25)
                                      + (Streak * 0.15);












    }

}

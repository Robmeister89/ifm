﻿using SQLite;
using System;

namespace IndoorFootballManager.Models
{
    public class Coach
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birth { get; set; }
        public decimal LifetimeEarnings { get; set; }
        public bool IsEmployed { get; set; }
    }
}

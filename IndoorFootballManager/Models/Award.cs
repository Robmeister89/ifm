﻿using System.Collections.Generic;
using System.Linq;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Services;
using SQLite;


namespace IndoorFootballManager.Models
{
    public class Award
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Indexed]
        public string Name { get; set; }
        public int Season { get; set; }
        public int PlayerId { get; set; }
        public string Stats { get; set; }
        public double Points { get; set; }

        public Award() { }

        public Award(string n, int se, int id)
        {
            Name = n;
            Season = se;
            PlayerId = id;
        }

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        public double GetPoints(int season, int id)
        {
            var p = Database.GetPlayerById(id);
            switch (p.Pos)
            {
                default:
                    return GetQb(SeasonStats(p, season));
                case "RB":
                    return GetRb(SeasonStats(p, season));
                case "WR":
                    return GetWr(SeasonStats(p, season));
                case "OL":
                    return GetOl(SeasonStats(p, season));
                case "DL":
                case "LB":
                case "DB":
                    return GetDefense(SeasonStats(p, season));
                case "K":
                    return GetK(SeasonStats(p, season));
            }

        }

        private double GetQb(PlayerStats p)
        {
            return p != null ? (p.PassTds * 4) + (p.PassYds * 0.04) + (p.PassInts * -2) + (p.RushYds * 0.05) + (p.RushTds * 6) + (p.FumblesLost * -2) : 0.0;
        }

        private double GetRb(PlayerStats p)
        {
            return p != null ? (p.RushYds * 0.05) + (p.RushTds * 6) + (p.Receptions * 0.05) + (p.RecYds * 0.025) + (p.RecTds * 3) + (p.FumblesLost * -2) : 0.0;
        }

        private double GetWr(PlayerStats p)
        {
            return p != null ? (p.RushYds * 0.025) + (p.RushTds * 3) + (p.Receptions * 0.1) + (p.RecYds * 0.075) + (p.RecTds * 6) + (p.FumblesLost * -2) : 0.0;
        }

        private double GetOl(PlayerStats p)
        {
            return p != null ? (p.PancakeBlocks * 1.75) + (p.SacksAllowed * -1) : 0.0;
        }

        private double GetDefense(PlayerStats p)
        {
            return p != null ? (p.Tackles * 3) + (p.Sacks * 6) + (p.Ints * 6) + (p.ForFumb * 4) + (p.Safeties * 5) + (p.FumbRec * 4) + (p.DefTds * 10) : 0.0;
        }

        private double GetK(PlayerStats p)
        {
            return p != null ? ((p.FGMade * 3.25) * p.FgPct) + ((p.XPMade * 1.15) * p.XpPct) : 0.0;
        }

        private PlayerStats SeasonStats(Player player, int season)
        {
            var seasonStats = Database.GetAllPlayerStats(season).First();
            var seasonStatsNew = new List<PlayerStats>();
            if (seasonStats != null)
                seasonStatsNew = seasonStats.StatsJson.DeserializeToList<PlayerStats>();
            var seasonTotal = seasonStatsNew.GroupBy(g => g.PlayerId).Select(p => new PlayerStats
            {
                Season = p.First().Season,
                Team = p.First().Team,
                PlayerId = p.First().PlayerId,
                PassAtt = p.Sum(c => c.PassAtt),
                PassCmp = p.Sum(c => c.PassCmp),
                PassYds = p.Sum(c => c.PassYds),
                PassTds = p.Sum(c => c.PassTds),
                PassInts = p.Sum(c => c.PassInts),
                Pass20 = p.Sum(c => c.Pass20),
                PassFirst = p.Sum(c => c.PassFirst),
                PassLong = p.Max(c => c.PassLong),
                Carries = p.Sum(c => c.Carries),
                RushYds = p.Sum(c => c.RushYds),
                RushTds = p.Sum(c => c.RushTds),
                Rush20 = p.Sum(c => c.Rush20),
                RushFirst = p.Sum(c => c.RushFirst),
                RushLong = p.Max(c => c.RushLong),
                Receptions = p.Sum(c => c.Receptions),
                RecYds = p.Sum(c => c.RecYds),
                RecTds = p.Sum(c => c.RecTds),
                Rec20 = p.Sum(c => c.Rec20),
                RecFirst = p.Sum(c => c.RecFirst),
                RecLong = p.Max(c => c.RecLong),
                Drops = p.Sum(c => c.Drops),
                Fumbles = p.Sum(c => c.Fumbles),
                FumblesLost = p.Sum(c => c.FumblesLost),
                Tackles = p.Sum(c => c.Tackles),
                Sacks = p.Sum(c => c.Sacks),
                ForFumb = p.Sum(c => c.ForFumb),
                FumbRec = p.Sum(c => c.FumbRec),
                Ints = p.Sum(c => c.Ints),
                IntYds = p.Sum(c => c.IntYds),
                Safeties = p.Sum(c => c.Safeties),
                DefTds = p.Sum(c => c.DefTds),
                FGAtt = p.Sum(c => c.FGAtt),
                FGMade = p.Sum(c => c.FGMade),
                FGLong = p.Max(c => c.FGLong),
                XPAtt = p.Sum(c => c.XPAtt),
                XPMade = p.Sum(c => c.XPMade),
                Rouges = p.Sum(c => c.Rouges),
                KickReturns = p.Sum(c => c.KickReturns),
                KickRetYds = p.Sum(c => c.KickRetYds),
                KickReturnTds = p.Sum(c => c.KickReturnTds),
                KickReturnLong = p.Max(c => c.KickReturnLong),
                PancakeBlocks = p.Sum(c => c.PancakeBlocks),
                SacksAllowed = p.Sum(c => c.SacksAllowed),
            }).ToList();

            var stats = seasonTotal.Where(p => p.PlayerId == player.Id).ToList();
            return stats.Any() ? stats.First() : null;
        }
    }
}

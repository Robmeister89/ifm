﻿using System.Collections.Generic;

namespace IndoorFootballManager.Models
{
    public class Penalty
    {
        public bool Presnap { get; set; }
        public string Team { get; set; }
        public bool LossOfDown { get; set; }
        public string PlayType { get; set; }
        public bool AutoFirst { get; set; }
        public int Yards { get; set; }
        public string Text { get; set; }
        public int Frequency { get; set; }

        public Penalty(bool preSnap, string team, bool lossOfDown, string playType, bool autoFirst, int yards, string text, int frequency)
        {
            Presnap = preSnap;
            Team = team;
            LossOfDown = lossOfDown;
            PlayType = playType;
            AutoFirst = autoFirst;
            Yards = yards;
            Text = text;
            Frequency = frequency;
        }

        public string[] PosGroup => Team == "Offense"
            ? QbPenalties.Contains(Text)
                ? new[] { "QB" }
                : WrRbPenalties.Contains(Text)
                    ? new[] { "RB", "WR" }
                    : Text == "Ineligible receiver down field"
                        ? new[] { "OL" }
                        : new[] { "RB", "WR", "OL" }
            : DlPenalties.Contains(Text)
                ? new[] { "DL" }
                : LbDbPenalties.Contains(Text)
                    ? new[] { "LB", "DB" }
                    : new[] { "DL", "LB", "DB" };

        private static List<string> QbPenalties => new List<string>
        {
            "Delay of game",
            "Intentional grounding",
        };

        private static List<string> WrRbPenalties => new List<string>
        {
            "Illegal motion",
            "Illegal formation",
            "Illegal touching forward pass",
            "Offensive pass interference",
        };

        private static List<string> DlPenalties => new List<string>
        {
            "Pyramiding",
            "Encroachment",
        };

        private static List<string> LbDbPenalties => new List<string>
        {
            "Defensive pass interference",
            "Illegal kick rush",
        };
    }
}

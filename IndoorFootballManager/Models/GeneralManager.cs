﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using IndoorFootballManager.DataAccess;
using SQLite;

namespace IndoorFootballManager.Models
{
    public class GeneralManager
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Indexed]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public int Exp { get; set; }
        public int TeamId { get; set; }
        public bool IsCommissioner { get; set; } = false;
        public string Password { get; set; }
        public bool FillRoster { get; set; } = false;
        public bool AssistSignings { get; set; } = false;
        public bool AssistExtensions { get; set; } = false;

        public string FullName => $"{FirstName} {LastName}";
        private string ShortName => $"{FirstName[0]}. {LastName}";

        public string DisplayName => Team != null
            ? IsCommissioner
                ? $"{ShortName} ({Team.TeamName}) *"
                : $"{ShortName} ({Team.TeamName})"
            : IsCommissioner
                ? $"{ShortName} (Unemployed) *"
                : $"{ShortName} (Unemployed)";

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        public Team Team => Database.GetTeamById(TeamId);

        public GeneralManager() { }

        public GeneralManager(int id, string first, string last, DateTime age, int exp, int teamId, bool commish)
        {
            Id = id;
            FirstName = first;
            LastName = last;
            DOB = age;
            Exp = exp;
            TeamId = teamId;
            IsCommissioner = commish;
        }

        public int Age => CalculateAge(DOB, CurrentLeague.CurDate);
        private static int CalculateAge(DateTime birthDate, DateTime now)
        {
            var age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age;
        }

        private IEnumerable<Email> Emails => Database.GetEmails(Id).ToList();
        public bool ReceivedDraftEmail => Emails.Any(e => e.Subject.Contains("Draft"));
        public bool ReceivedCampEmail => Emails.Any(e => e.Subject.Contains("Camp"));
        public bool ReceivedSeasonStart => Emails.Any(e => e.Subject.Contains("Regular Season"));
        public bool ReceivedSeasonEnd => Emails.Any(e => e.Subject.Contains("End of Season"));
    }


}

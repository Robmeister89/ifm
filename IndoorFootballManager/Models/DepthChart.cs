﻿using System;
using SQLite;

namespace IndoorFootballManager.Models
{
    public class DepthChart
    {
        [PrimaryKey]
        public int TeamId { get; set; }
        [Indexed]
        public string Quarterback { get; set; }
        public string Runningback { get; set; }
        public string WideReceiver { get; set; }
        public string OffensiveLine { get; set; }
        public string DefensiveLine { get; set; }
        public string Linebacker { get; set; }
        public string DefensiveBack { get; set; }
        public string Kicker { get; set; }
        public string KickReturner { get; set; }
        [Obsolete, Ignore] public byte[] QB { get; set; }
        [Obsolete, Ignore] public byte[] RB { get; set; }
        [Obsolete, Ignore] public byte[] WR { get; set; }
        [Obsolete, Ignore] public byte[] OL { get; set; }
        [Obsolete, Ignore] public byte[] DL { get; set; }
        [Obsolete, Ignore] public byte[] LB { get; set; }
        [Obsolete, Ignore] public byte[] DB { get; set; }
        [Obsolete, Ignore] public byte[] K { get; set; }
        [Obsolete, Ignore] public byte[] KR { get; set; }
        
        public DepthChart() { }
    }
}

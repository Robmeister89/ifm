﻿using System;
using SQLite;
using IndoorFootballManager.DataAccess;

namespace IndoorFootballManager.Models
{
    [Serializable]
    public class ContractOffer
    {
        [PrimaryKey]
        public int Id { get; set; }
        [Indexed]
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        public int Years { get; set; }
        public int Salary { get; set; }
        public bool Active { get; set; }

        public Team Team => Repo.Database.GetTeamById(TeamId);
        public Player Player => Repo.Database.GetPlayerById(PlayerId);
        public int Evaluation => Player.EvaluateOffer(this);

        public override string ToString() => Player.ShortNameWithPosition + " (" + Salary.ToString("C0") + " / " + Years.ToString() + " years)";


    }
}

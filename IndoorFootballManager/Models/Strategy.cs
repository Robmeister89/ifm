﻿using SQLite;

namespace IndoorFootballManager.Models
{
    public class Strategy
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Indexed]
        public int O1st10 { get; set; }
        public int O1stShort { get; set; }
        public int O1stLong { get; set; }
        public int O2ndShort { get; set; }
        public int O2ndLong { get; set; }
        public int O3rdShort { get; set; }
        public int O3rdLong { get; set; }
        public int O4thShort { get; set; }
        public int O4thLong { get; set; }
        public int OGoalline { get; set; }
        public int D1st10 { get; set; }
        public int D1stShort { get; set; }
        public int D1stLong { get; set; }
        public int D2ndShort { get; set; }
        public int D2ndLong { get; set; }
        public int D3rdShort { get; set; }
        public int D3rdLong { get; set; }
        public int D4thShort { get; set; }
        public int D4thLong { get; set; }
        public int DGoalline { get; set; }


    }
}

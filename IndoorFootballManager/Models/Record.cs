﻿using System;
using SQLite;

namespace IndoorFootballManager.Models
{
    public class Record
    {
        [PrimaryKey]
        public int Id { get; set; }
        [Indexed]
        public int Type { get; set; } // 0 = game, 1 = season, 2 = career
        public string RecordName { get; set; }
        public string PlayerName { get; set; }
        public string TeamName { get; set; }
        public string OpponentName { get; set; }
        public int RecordSeason { get; set; }
        public DateTime RecordDate { get; set; }
        public string RecordValue { get; set; }

        public Record() { }

        public Record(int id, int t, string n, string p, string tm, string opp, int s, DateTime d, string v)
        {
            Id = id;
            Type = t;
            RecordName = n;
            PlayerName = p;
            TeamName = tm;
            OpponentName = opp;
            RecordSeason = s;
            RecordDate = d;
            RecordValue = v;
        }


    }
}

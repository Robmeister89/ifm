﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IndoorFootballManager.UIControls
{
    /// <summary>
    /// Interaction logic for IconLabel.xaml
    /// </summary>
    public partial class IconLabelButton : Button
    {
        public IconLabelButton()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconFontFamilyProperty =
            DependencyProperty.Register(nameof(IconFontFamily), typeof(FontFamily), typeof(IconLabelButton), new PropertyMetadata(new FontFamily("Arial")));

        public FontFamily IconFontFamily
        {
            get => (FontFamily)GetValue(IconFontFamilyProperty);
            set => SetValue(IconFontFamilyProperty, value);
        }

        public static readonly DependencyProperty IconUnicodeProperty =
            DependencyProperty.Register(nameof(IconUnicode), typeof(string), typeof(IconLabelButton), new PropertyMetadata(string.Empty));

        public string IconUnicode
        {
            get => (string)GetValue(IconUnicodeProperty);
            set => SetValue(IconUnicodeProperty, value);
        }

        public static readonly DependencyProperty IconFontSizeProperty =
            DependencyProperty.Register(nameof(IconFontSize), typeof(double), typeof(IconLabelButton),
                new PropertyMetadata(SystemFonts.MessageFontSize));

        [TypeConverter(typeof(FontSizeConverter))]
        public double IconFontSize
        {
            get => (double)GetValue(IconFontSizeProperty);
            set => SetValue(IconFontSizeProperty, value);
        }

        public static readonly DependencyProperty LabelTextProperty =
            DependencyProperty.Register(nameof(LabelText), typeof(string), typeof(IconLabelButton), new PropertyMetadata(string.Empty));

        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        public static readonly DependencyProperty LabelFontSizeProperty =
            DependencyProperty.Register(nameof(LabelFontSize), typeof(double), typeof(IconLabelButton),
                new PropertyMetadata(SystemFonts.MessageFontSize));

        [TypeConverter(typeof(FontSizeConverter))]
        public double LabelFontSize
        {
            get => (double)GetValue(LabelFontSizeProperty);
            set => SetValue(LabelFontSizeProperty, value);
        }
    }
}

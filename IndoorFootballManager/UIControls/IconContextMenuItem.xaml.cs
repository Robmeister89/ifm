﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IndoorFootballManager.UIControls
{
    /// <summary>
    /// Interaction logic for IconContextMenuItem.xaml
    /// </summary>
    public partial class IconContextMenuItem : MenuItem
    {
        public IconContextMenuItem()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconFontFamilyProperty =
            DependencyProperty.Register(nameof(IconFontFamily), typeof(FontFamily), typeof(IconContextMenuItem), new PropertyMetadata(new FontFamily("Arial")));

        public FontFamily IconFontFamily
        {
            get => (FontFamily)GetValue(IconFontFamilyProperty);
            set => SetValue(IconFontFamilyProperty, value);
        }

        public static readonly DependencyProperty IconUnicodeProperty =
            DependencyProperty.Register(nameof(IconUnicode), typeof(string), typeof(IconContextMenuItem), new PropertyMetadata(string.Empty));

        public string IconUnicode
        {
            get => (string)GetValue(IconUnicodeProperty);
            set => SetValue(IconUnicodeProperty, value);
        }

        public static readonly DependencyProperty HeaderTextProperty =
            DependencyProperty.Register(nameof(HeaderText), typeof(string), typeof(IconContextMenuItem), new PropertyMetadata(string.Empty));

        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

    }
}

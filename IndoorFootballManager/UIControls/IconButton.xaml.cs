﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IndoorFootballManager.UIControls
{
    /// <summary>
    /// Interaction logic for IconButton.xaml
    /// </summary>
    public partial class IconButton : Button
    {
        public IconButton()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconFontFamilyProperty =
            DependencyProperty.Register(nameof(IconFontFamily), typeof(FontFamily), typeof(IconButton), new PropertyMetadata(new FontFamily("Arial")));

        public FontFamily IconFontFamily
        {
            get => (FontFamily)GetValue(IconFontFamilyProperty);
            set => SetValue(IconFontFamilyProperty, value);
        }

        public static readonly DependencyProperty IconUnicodeProperty =
            DependencyProperty.Register(nameof(IconUnicode), typeof(string), typeof(IconButton), new PropertyMetadata(string.Empty));

        public string IconUnicode
        {
            get => (string)GetValue(IconUnicodeProperty);
            set => SetValue(IconUnicodeProperty, value);
        }

        public static readonly DependencyProperty IsEmailButtonProperty =
            DependencyProperty.Register(nameof(IsEmailButton), typeof(bool), typeof(IconButton),
                new PropertyMetadata(false));

        public bool IsEmailButton
        {
            get => (bool)GetValue(IsEmailButtonProperty);
            set => SetValue(IsEmailButtonProperty, value);
        }

        public static readonly DependencyProperty EmailsCountProperty =
            DependencyProperty.Register(nameof(EmailsCount), typeof(int), typeof(IconButton), new UIPropertyMetadata(0, OnEmailsCountChanged));

        public int EmailsCount
        {
            get => (int)GetValue(EmailsCountProperty);
            set => SetValue(EmailsCountProperty, value);
        }

        public static readonly DependencyProperty EmailsVisibilityProperty =
            DependencyProperty.Register(nameof(EmailsVisibility), typeof(Visibility), typeof(IconButton),
                new UIPropertyMetadata(Visibility.Collapsed));

        public Visibility EmailsVisibility
        {
            get => (Visibility)GetValue(EmailsVisibilityProperty);
            set => SetValue(EmailsVisibilityProperty, value);
        }

        private static void OnEmailsCountChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var iconButton = (IconButton)sender;
            if (e.Property == EmailsCountProperty)
            {
                iconButton.EmailsVisibility = iconButton.IsEmailButton && iconButton.EmailsCount > 0
                    ? Visibility.Visible
                    : Visibility.Collapsed;
            }
        }

    }
}

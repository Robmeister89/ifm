﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IndoorFootballManager.UIControls
{
    /// <summary>
    /// Interaction logic for IconMenuItem.xaml
    /// </summary>
    public partial class IconMenuItem : MenuItem
    {
        public IconMenuItem()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconFontFamilyProperty =
            DependencyProperty.Register(nameof(IconFontFamily), typeof(FontFamily), typeof(IconMenuItem), new PropertyMetadata(new FontFamily("Arial")));

        public FontFamily IconFontFamily
        {
            get => (FontFamily)GetValue(IconFontFamilyProperty);
            set => SetValue(IconFontFamilyProperty, value);
        }

        public static readonly DependencyProperty IconUnicodeProperty =
            DependencyProperty.Register(nameof(IconUnicode), typeof(string), typeof(IconMenuItem), new PropertyMetadata(string.Empty));

        public string IconUnicode
        {
            get => (string)GetValue(IconUnicodeProperty);
            set => SetValue(IconUnicodeProperty, value);
        }

        public static readonly DependencyProperty LabelTextProperty =
            DependencyProperty.Register(nameof(LabelText), typeof(string), typeof(IconMenuItem), new PropertyMetadata(string.Empty));

        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }
    }
}

﻿namespace IndoorFootballManager
{
    internal class Enums
    {
        public enum Positions
        {
            QB = 0,
            RBDB = 1,
            RBLB = 2,
            WRDB = 3,
            WRLB = 4,
            OLDL = 5,
            OS = 6,
            DS = 7,
            K = 8
        }
        
        public enum OffensivePhilosophies
        {
            ThreeWRs = 0,
            TwoRBs = 1,
            ThreeRBs = 2
        }
            
        public enum InjurySeverity
        {
            Probable = 0,
            Questionable = 1,
            Doubtful = 2,
            Out = 3,
            Suspended = 4
        }

        public enum Stages
        {
             FreeAgency,
             Draft,
             TrainingCamp,
             PreSeason,
             RegularSeason,
             PostSeason,
             LeagueMeetings
        }


    }

    
    
}

﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeagueStandings.xaml
    /// </summary>
    public partial class LeagueStandings : UserControl
    {
        public LeagueStandings()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            CreateGrid(CurrentLeague.Conferences, CurrentLeague.Divisions);
            HeaderTxt.Text = CurrentLeague.LeagueName + " Standings";
        }

        private void CreateGrid(int conf, int divs)
        {
            DataGrid dg;
            if(conf == 1)
            {
                var col = new ColumnDefinition() { Width = new GridLength(1270) };
                Standings.ColumnDefinitions.Add(col);

                for (var i = 0; i < divs; i++)
                {
                    var row = new RowDefinition() { Height = GridLength.Auto, };
                    Standings.RowDefinitions.Add(row);

                    dg = StandingsGrid(CurrentLeague.DivisionsList[i]);
                    dg.ItemsSource = Database.GetTeamsForDivisionStandings(CurrentLeague.Conference1, CurrentLeague.DivisionsList[i]);
                    Grid.SetRow(dg, i);
                    Grid.SetColumn(dg, 0);
                    dg.VerticalAlignment = VerticalAlignment.Center;
                    Standings.Children.Add(dg);
                }
            }
            else
            {
                for(var i = 0; i < 2; i++)
                {
                    var col = new ColumnDefinition() { Width = new GridLength(635) };
                    Standings.ColumnDefinitions.Add(col);
                    var col1 = new ColumnDefinition() { Width = new GridLength(635) };
                    Conferences.ColumnDefinitions.Add(col1);

                    var header = new TextBlock()
                    {
                        Foreground = Brushes.GhostWhite,
                        FontSize = 22,
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Text = i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2,
                    };
                    Grid.SetRow(header, 0);
                    Grid.SetColumn(header, i);
                    Conferences.Children.Add(header);

                    for (var j = 0; j < divs; j++)
                    {
                        var row = new RowDefinition() { Height = GridLength.Auto, };
                        Standings.RowDefinitions.Add(row);

                        var id = j + i;
                        if (i == 1)
                            id = j + divs;
                        dg = StandingsGrid(CurrentLeague.DivisionsList[id]);
                        dg.ItemsSource = Database.GetTeamsForDivisionStandings(i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2, CurrentLeague.DivisionsList[id]);
                        Grid.SetRow(dg, j + 1);
                        Grid.SetColumn(dg, i);
                        Standings.Children.Add(dg);
                    }
                }
            }
        }

        private DataGrid StandingsGrid(string divName)
        {
            var dg = new DataGrid()
            {
                Margin = new Thickness(20,0,20,20),
                Background = Brushes.Black,
                RowBackground = Brushes.Transparent,
                CanUserReorderColumns = false,
                CanUserResizeColumns = false,
                CanUserResizeRows = false,
                CanUserSortColumns = false,
                IsReadOnly = true,
                SelectionMode = DataGridSelectionMode.Extended,
                HeadersVisibility = DataGridHeadersVisibility.Column,
                GridLinesVisibility = DataGridGridLinesVisibility.None,
                AutoGenerateColumns = false,
                Width = 595
            };
            dg.MouseDoubleClick += new MouseButtonEventHandler(TeamDoubleClick);
            var teams = new DataGridTextColumn()
            {
                Header = divName,
                Binding = new Binding("TeamName"),
                Width = new DataGridLength(150),
                FontSize = 10,
            };
            var wins = new DataGridTextColumn()
            {
                Header = "W",
                Binding = new Binding("Wins"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var losses = new DataGridTextColumn()
            {
                Header = "L",
                Binding = new Binding("Losses"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pct = new DataGridTextColumn()
            {
                Header = "Pct",
                Binding = new Binding("WinPct") { StringFormat = "N3" },
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pf = new DataGridTextColumn()
            {
                Header = "PF",
                Binding = new Binding("PointsFor"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pa = new DataGridTextColumn()
            {
                Header = "PA",
                Binding = new Binding("PointsAgainst"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var home = new DataGridTextColumn()
            {
                Header = "Home",
                Binding = new Binding("HomeRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var away = new DataGridTextColumn()
            {
                Header = "Away",
                Binding = new Binding("AwayRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var div = new DataGridTextColumn()
            {
                Header = "Div",
                Binding = new Binding("DivRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var conf = new DataGridTextColumn()
            {
                Header = "Conf",
                Binding = new Binding("ConfRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var strk = new DataGridTextColumn()
            {
                Header = "Stk",
                Binding = new Binding("StreakTxt"),
                Width = new DataGridLength(45, DataGridLengthUnitType.Star),
                FontSize = 10,
            };

            dg.Columns.Add(teams); dg.Columns.Add(wins); dg.Columns.Add(losses); dg.Columns.Add(pct);
            dg.Columns.Add(pf); dg.Columns.Add(pa);
            dg.Columns.Add(home); dg.Columns.Add(away);
            dg.Columns.Add(div); dg.Columns.Add(conf);
            dg.Columns.Add(strk);
            foreach (var col in dg.Columns)
                col.HeaderStyle = App.Current.Resources["DGHeaderStyle"] as Style;

            return dg;
        }


        private void TeamDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((sender as DataGrid).SelectedItem is Team team)
                GameWindow.MainFrame.Navigate(new TeamPage(team));
        }






    }
}

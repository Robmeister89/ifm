﻿using IndoorFootballManager.Models;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using sql = System.Data.SQLite;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Services;
using IndoorFootballManager.Windows;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamHome.xaml
    /// </summary>
    public partial class TeamHome : UserControl
    {
        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private readonly Team _team;

        public TeamHome()
        {
            InitializeComponent();
            _team = TeamPage.Team;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            ScheduleBox.ItemsSource = Repo.Database.GetGamesByTeamId(CurrentLeague.CurrentSeason, _team.Id);
            ScheduleBox.DisplayMemberPath = "Display";

            Standings.ItemsSource = Repo.Database.GetTeamsForDivisionStandings(_team.Conference, _team.Division);
            DivHeader.Header = _team.Division + " Standings";
            
            InjuryList.ItemsSource = _team.Players.Where(p => p.IsInjured);

            TransactionList.ItemsSource = Repo.Database.GetTeamTransactions(_team.Id, _team.Season).OrderByDescending(t => t.Date).ThenByDescending(t => t.Id);
        }

        private void ScheduleBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var game = (sender as ListBox).SelectedItem as Game;
            if(game.HasPlayed)
            {
                var view = new GameView(game);
                view.ShowDialog();
            }
        }


        private DataGrid StandingsGrid(string divName)
        {
            var dg = new DataGrid()
            {
                Margin = new Thickness(20, 20, 20, 0),
                //Background = Brushes.Black,
                RowBackground = Brushes.Transparent,
                CanUserReorderColumns = false,
                CanUserResizeColumns = false,
                CanUserResizeRows = false,
                CanUserSortColumns = false,
                IsReadOnly = true,
                SelectionMode = DataGridSelectionMode.Extended,
                HeadersVisibility = DataGridHeadersVisibility.Column,
                GridLinesVisibility = DataGridGridLinesVisibility.None,
                AutoGenerateColumns = false,
            };
            dg.MouseDoubleClick += new MouseButtonEventHandler(TeamDoubleClick);
            var teams = new DataGridTextColumn()
            {
                Header = divName,
                Binding = new Binding("TeamName"),
                Width = 150,
                FontSize = 10,
            };
            var wins = new DataGridTextColumn()
            {
                Header = "W",
                Binding = new Binding("Wins"),
                Width = new DataGridLength(30),
                FontSize = 10,
            };
            var losses = new DataGridTextColumn()
            {
                Header = "L",
                Binding = new Binding("Losses"),
                Width = new DataGridLength(30),
                FontSize = 10,
            };
            var pct = new DataGridTextColumn()
            {
                Header = "Pct",
                Binding = new Binding("WinPct") { StringFormat = "N3" },
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pf = new DataGridTextColumn()
            {
                Header = "PF",
                Binding = new Binding("PointsFor"),
                Width = new DataGridLength(35),
                FontSize = 10,
            };
            var pa = new DataGridTextColumn()
            {
                Header = "PA",
                Binding = new Binding("PointsAgainst"),
                Width = new DataGridLength(35),
                FontSize = 10,
            };
            var home = new DataGridTextColumn()
            {
                Header = "Home",
                Binding = new Binding("HomeRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var away = new DataGridTextColumn()
            {
                Header = "Away",
                Binding = new Binding("AwayRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var div = new DataGridTextColumn()
            {
                Header = "Div",
                Binding = new Binding("DivRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var conf = new DataGridTextColumn()
            {
                Header = "Conf",
                Binding = new Binding("ConfRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var strk = new DataGridTextColumn()
            {
                Header = "Stk",
                Binding = new Binding("StreakTxt"),
                Width = new DataGridLength(35, DataGridLengthUnitType.Star),
                FontSize = 10,
            };

            dg.Columns.Add(teams); dg.Columns.Add(wins); dg.Columns.Add(losses); dg.Columns.Add(pct);
            dg.Columns.Add(pf); dg.Columns.Add(pa);
            dg.Columns.Add(home); dg.Columns.Add(away);
            dg.Columns.Add(div); dg.Columns.Add(conf);
            dg.Columns.Add(strk);

            foreach (var col in dg.Columns)
                col.HeaderStyle = App.Current.Resources["DGHeaderStyle"] as Style;

            return dg;
        }

        private void TeamDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var team = (sender as DataGrid).SelectedItem as Team;
            if (team != null)
            {
                GameWindow.MainFrame.Navigate(new TeamPage(team));
            }
        }

        private void InjuryList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
    }
}

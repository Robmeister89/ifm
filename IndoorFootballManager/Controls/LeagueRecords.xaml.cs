﻿using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using ookii = Ookii.Dialogs.Wpf;
using System.ComponentModel;
using Notification.Wpf;
using SQLite;
using System.Linq;

namespace IndoorFootballManager.Controls
{
    public partial class LeagueRecords : UserControl
    {
        public LeagueRecords()
        {
            InitializeComponent();
        }

        private static SQLiteConnection Database => Repo.Database;
        private ookii.ProgressDialog _prog;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            CheckForRecords();
        }

        private void SetRecords(object sender, DoWorkEventArgs e)
        {
            LeagueData.SetRecords();
        }

        private void LoadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _prog.Dispose();
            GameRecordsGrid.ItemsSource = Database.GetGameRecords();
            App.ShowNotification("Success", "Records have been generated!", NotificationType.Success, 5);
        }

        private void CheckForRecords()
        {
            var tableExists = Database.ExecuteScalar<bool>("SELECT COUNT(*) FROM sqlite_master WHERE name='Record'");
            if (tableExists)
            {
                GameRecordsGrid.ItemsSource = Database.GetGameRecords();
            }
            else
            {
                _prog = new ookii.ProgressDialog()
                {
                    WindowTitle = "Loading Records",
                    Text = "Loading records for the very first time...",
                    ShowTimeRemaining = false,
                    ShowCancelButton = false,
                    ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar,
                    MinimizeBox = false,
                };

                _prog.DoWork += SetRecords;
                _prog.RunWorkerCompleted += LoadCompleted;
                _prog.ShowDialog();
            }

        }

    }
}

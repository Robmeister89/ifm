﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TradeConsole.xaml
    /// </summary>
    public partial class TradeConsole : UserControl
    {
        public TradeConsole()
        {
            InitializeComponent();
            Team1Cbox.SelectedIndex = 0;
            Team2Cbox.SelectedIndex = 1;
        }

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private Team _team1;
        private Team _team2;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var teamList = Database.GetTeams().OrderBy(t => t.TeamName).ToList();
            Team1Cbox.ItemsSource = teamList;
            Team2Cbox.ItemsSource = teamList;
            _team1 = Team1Cbox.SelectedItem as Team;
            _team2 = Team2Cbox.SelectedItem as Team;

            if (_team1 != null)
            {
                SetTeam1Labels(_team1);
                Team1Players = Database.GetPlayersByTeamId(_team1.Id).OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToList();
                Team1PlayersGrid.ItemsSource = Team1Players;
                Team1Picks = Database.GetPicksByTeam(_team1.Id).OrderBy(d => d.Season).ThenBy(d => d.Round).ToList();
                Team1PicksGrid.ItemsSource = Team1Picks;
            }

            if (_team2 != null)
            {
                SetTeam2Labels(_team2);
                Team2Players = Database.GetPlayersByTeamId(_team2.Id).OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToList();
                Team2PlayersGrid.ItemsSource = Team2Players;
                Team2Picks = Database.GetPicksByTeam(_team2.Id).OrderBy(d => d.Season).ThenBy(d => d.Round).ToList();
                Team2PicksGrid.ItemsSource = Team2Picks;
            }
        }

        private void SetTeam1Labels(Team team)
        {
            TeamName1.Text = _team1.TeamName + " (" + (_team1.Players.Count()).ToString() + " players)";
            TeamSalaries1.Text = _team1.TeamSalaries.ToString("C0");
            TeamCapSpace1.Text = (CurrentLeague.SalaryCap - _team1.TeamSalaries).ToString("C0");
            TeamSelected1.Text = "$0";
        }

        private void SetTeam2Labels(Team team)
        {
            TeamName2.Text = _team2.TeamName + " (" + (_team2.Players.Count()).ToString() + " players)";
            TeamSalaries2.Text = _team2.TeamSalaries.ToString("C0");
            TeamCapSpace2.Text = (CurrentLeague.SalaryCap - _team2.TeamSalaries).ToString("C0");
            TeamSelected2.Text = "$0";
        }

        private List<Player> Team1Players { get; set; } = new List<Player>();
        private List<Player> Team1Traded { get; set; } = new List<Player>();
        private List<DraftPick> Team1Picks { get; set; } = new List<DraftPick>();
        private List<DraftPick> Team1PicksTraded { get; set; } = new List<DraftPick>();
        private List<Player> Team2Players { get; set; } = new List<Player>();
        private List<Player> Team2Traded { get; set; } = new List<Player>();
        private List<DraftPick> Team2Picks { get; set; } = new List<DraftPick>();
        private List<DraftPick> Team2PicksTraded { get; set; } = new List<DraftPick>();

        private void Team1Cbox_DropDownClosed(object sender, EventArgs e)
        {
            _team1 = (sender as ComboBox).SelectedItem as Team;
            SetTeam1Labels(_team1);
            Team1Players = Database.GetPlayersByTeamId(_team1.Id).OrderBy(p => p.PositionSort)
                .ThenByDescending(p => p.Overall).ToList();
            Team1Traded.Clear();
            Team1PlayersGrid.ItemsSource = Team1Players;
            Team1PlayersTraded.ItemsSource = null;
            Team1Picks = Database.GetPicksByTeam(_team1.Id).OrderBy(d => d.Season).ThenBy(d => d.Round).ToList();
            Team1PicksTraded.Clear();
            Team1PicksGrid.ItemsSource = Team1Picks;
            Team1PicksTradedGrid.ItemsSource = null;
        }

        private void Team1Move_Click(object sender, RoutedEventArgs e)
        {
            if(Team1PlayersGrid.SelectedItem is Player player)
            {
                Team1Players.Remove(player);
                Team1Traded.Add(player);
                Team1PlayersGrid.ItemsSource = Team1Players.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
                Team1PlayersTraded.ItemsSource = Team1Traded.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
            }
            var tradeSalaries = 0;
            foreach(Player p in Team1PlayersTraded.Items)
            {
                tradeSalaries += p.Salary;
            }
            TeamSelected1.Text = tradeSalaries.ToString("C0");
        }

        private void Team1MoveBack_Click(object sender, RoutedEventArgs e)
        {
            if(Team1PlayersTraded.SelectedItem is Player player)
            {
                Team1Traded.Remove(player);
                Team1Players.Add(player);
                Team1PlayersGrid.ItemsSource = Team1Players.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
                Team1PlayersTraded.ItemsSource = Team1Traded.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
            }
            var tradeSalaries = 0;
            foreach (Player p in Team1PlayersTraded.Items)
            {
                tradeSalaries += p.Salary;
            }
            TeamSelected1.Text = tradeSalaries.ToString("C0");

        }

        private void Team1PlayersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team1Move_Click(sender, e);
        }

        private void Team1PlayersTraded_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team1MoveBack_Click(sender, e);
        }

        private void Team1MovePick_Click(object sender, RoutedEventArgs e)
        {
            if(Team1PicksGrid.SelectedItem is DraftPick pick)
            {
                Team1Picks.Remove(pick);
                Team1PicksTraded.Add(pick);
                Team1PicksGrid.ItemsSource = Team1Picks.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
                Team1PicksTradedGrid.ItemsSource = Team1PicksTraded.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
            }
        }

        private void Team1MovePickBack_Click(object sender, RoutedEventArgs e)
        {
            if (Team1PicksTradedGrid.SelectedItem is DraftPick pick)
            {
                Team1PicksTraded.Remove(pick);
                Team1Picks.Add(pick);
                Team1PicksGrid.ItemsSource = Team1Picks.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
                Team1PicksTradedGrid.ItemsSource = Team1PicksTraded.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
            }
        }

        private void Team1PicksGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team1MovePick_Click(sender, e);
        }

        private void Team1PicksTradedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team1MovePickBack_Click(sender, e);
        }

        private void Team2Cbox_DropDownClosed(object sender, EventArgs e)
        {
            _team2 = (sender as ComboBox).SelectedItem as Team;
            SetTeam2Labels(_team2);
            Team2Players = Database.GetPlayersByTeamId(_team2.Id).OrderBy(p => p.PositionSort)
                .ThenByDescending(p => p.Overall).ToList();
            Team2Traded.Clear();
            Team2PlayersGrid.ItemsSource = Team2Players;
            Team2PlayersTraded.ItemsSource = null;
            Team2Picks = Database.GetPicksByTeam(_team2.Id).OrderBy(d => d.Season).ThenBy(d => d.Round).ToList();
            Team2PicksTraded.Clear();
            Team2PicksGrid.ItemsSource = Team2Picks;
            Team2PicksTradedGrid.ItemsSource = null;
        }

        private void Team2Move_Click(object sender, RoutedEventArgs e)
        {
            if (Team2PlayersGrid.SelectedItem is Player player)
            {
                Team2Players.Remove(player);
                Team2Traded.Add(player);
                Team2PlayersGrid.ItemsSource = Team2Players.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
                Team2PlayersTraded.ItemsSource = Team2Traded.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
            }
            var tradeSalaries = 0;
            foreach (Player p in Team2PlayersTraded.Items)
            {
                tradeSalaries += p.Salary;
            }
            TeamSelected2.Text = tradeSalaries.ToString("C0");
        }

        private void Team2MoveBack_Click(object sender, RoutedEventArgs e)
        {
            if (Team2PlayersTraded.SelectedItem is Player player)
            {
                Team2Traded.Remove(player);
                Team2Players.Add(player);
                Team2PlayersGrid.ItemsSource = Team2Players.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
                Team2PlayersTraded.ItemsSource = Team2Traded.OrderBy(p => p.PositionSort)
                    .ThenByDescending(p => p.Overall).ToArray();
            }
            var tradeSalaries = 0;
            foreach (Player p in Team2PlayersTraded.Items)
            {
                tradeSalaries += p.Salary;
            }
            TeamSelected2.Text = tradeSalaries.ToString("C0");

        }

        private void Team2PlayersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team2Move_Click(sender, e);
        }

        private void Team2PlayersTraded_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team2MoveBack_Click(sender, e);
        }

        private void Team2MovePick_Click(object sender, RoutedEventArgs e)
        {
            if (Team2PicksGrid.SelectedItem is DraftPick pick)
            {
                Team2Picks.Remove(pick);
                Team2PicksTraded.Add(pick);
                Team2PicksGrid.ItemsSource = Team2Picks.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
                Team2PicksTradedGrid.ItemsSource = Team2PicksTraded.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
            }
        }

        private void Team2MovePickBack_Click(object sender, RoutedEventArgs e)
        {
            if (Team2PicksTradedGrid.SelectedItem is DraftPick pick)
            {
                Team2PicksTraded.Remove(pick);
                Team2Picks.Add(pick);
                Team2PicksGrid.ItemsSource = Team2Picks.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
                Team2PicksTradedGrid.ItemsSource = Team2PicksTraded.OrderBy(d => d.Season)
                    .ThenBy(d => d.Round).ToArray();
            }
        }

        private void Team2PicksGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team2MovePick_Click(sender, e);
        }

        private void Team2PicksTradedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
                Team2MovePickBack_Click(sender, e);
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            var t1Cs = int.Parse(ConvDollarString(TeamCapSpace1.Text));
            var t1S = int.Parse(ConvDollarString(TeamSelected1.Text));
            var t2Cs = int.Parse(ConvDollarString(TeamCapSpace2.Text));
            var t2S = int.Parse(ConvDollarString(TeamSelected2.Text));
            if ((t1Cs + t2S) < 0 || (t2Cs + t1S) < 0)
            {
                void Cancel() { return; }
                App.ShowMessageBox("Warning", "This trade puts at least one team over the cap.\nDo you wish to continue?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", ProcessTrade, "No", Cancel);
            }
            else
                ProcessTrade();
        }

        private void ProcessTrade()
        {
            foreach(Player p in Team1PlayersTraded.Items)
            {
                SwapPlayer(p, _team2);
            }
            foreach(DraftPick p in Team1PicksTradedGrid.Items)
            {
                SwapPick(p, _team2);
            }
            foreach(Player p in Team2PlayersTraded.Items)
            {
                SwapPlayer(p, _team1);
            }
            foreach(DraftPick p in Team2PicksTradedGrid.Items)
            {
                SwapPick(p, _team1);
            }

            // updated grids
            Team1PlayersGrid.ItemsSource = Database.GetPlayersByTeamId(_team1.Id).OrderBy(p => p.PositionSort)
                .ThenByDescending(p => p.Overall);
            Team1PlayersTraded.ItemsSource = null;
            Team1PicksGrid.ItemsSource = Database.GetPicksByTeam(_team1.Id).OrderBy(d => d.Season)
                .ThenBy(d => d.Round);
            Team1PicksTradedGrid.ItemsSource = null;

            Team2PlayersGrid.ItemsSource = Database.GetPlayersByTeamId(_team2.Id).OrderBy(p => p.PositionSort)
                .ThenByDescending(p => p.Overall);
            Team2PlayersTraded.ItemsSource = null;
            Team2PicksGrid.ItemsSource = Database.GetPicksByTeam(_team2.Id).OrderBy(d => d.Season)
                .ThenBy(d => d.Round);
            Team2PicksTradedGrid.ItemsSource = null;

            SetTeam1Labels(_team1);
            SetTeam2Labels(_team2);
            Team1Players.Clear();
            Team1Traded.Clear();
            Team1Picks.Clear();
            Team1PicksTraded.Clear();
            Team2Players.Clear();
            Team2Traded.Clear();
            Team2Picks.Clear();
            Team2PicksTraded.Clear();
            App.ShowNotification("Success", "Trade has been processed.", Notification.Wpf.NotificationType.Success, 5);
        }

        private void SwapPlayer(Player p, Team newTeam)
        {
            p.TeamId = newTeam.Id;
            Database.SavePlayer(p);
        }

        private void SwapPick(DraftPick p, Team newTeam)
        {
            p.CurrentTeamId = newTeam.Id;
            Database.SaveDraftPick(p);
            var d = Database.GetDraftByPickId(p.Id);
            if (d != null)
            {
                d.CurTeamId = newTeam.Id;
                Database.SaveDraft(d);
            }
        }

        private string ConvDollarString(string str)
        {
            return str.Replace("(", "-").Replace(")", "").Replace(",", "").Replace("$", "");
        }

        private void SendEmail(GeneralManager team1Gm, GeneralManager team2Gm) 
        {
            // do something...
        }

    }
}

﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SQLite;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for ManagerHome.xaml
    /// </summary>
    public partial class ManagerPage : UserControl, INotifyPropertyChanged
    {

        private GeneralManager UserManager { get; }
        private static SQLiteConnection Database => Repo.Database;

        public ManagerPage()
        {
            InitializeComponent();
        }

        public ManagerPage(GeneralManager userManager)
        {
            InitializeComponent();
            UserManager = userManager;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool HasTeam => UserManager != null && UserManager.TeamId > 0;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (UserManager.TeamId > 0)
            {
                var team = Database.GetTeamById(UserManager.TeamId);
                Overlay.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(team.PrimaryColor));
                TeamLogo.Source = Converters.LoadImage(team.Logo);
                NotifyPropertyChanged(nameof(HasTeam));
            }
        }

        private void EmailsBtn_Click(object sender, RoutedEventArgs e)
        {
            GmFrame.Navigate(new ManagerEmails(UserManager));
        }

        private void ContractsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UserManager.Team != null)
                GmFrame.Navigate(new ManagerContracts(UserManager.Team));
            else
                App.ShowNotification("Warning", "You must run a team to view this page.", Notification.Wpf.NotificationType.Warning, 3);
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            GmFrame.Navigate(new ManagerInfo(UserManager));
        }
    }
}

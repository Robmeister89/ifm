﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Windows;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for ManagerEmails.xaml
    /// </summary>
    public partial class ManagerEmails : UserControl
    {
        public ManagerEmails()
        {
            InitializeComponent();
        }

        public ManagerEmails(GeneralManager manager)
        {
            InitializeComponent();
            UserManager = manager;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private GeneralManager UserManager { get; }
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var emails = Database.GetEmails(UserManager.Id).OrderByDescending(em => em.SendDate).ThenByDescending(em => em.Id).ToList();
            EmailList.ItemsSource = emails;
            EmailList.DisplayMemberPath = "Subject";
            if (!emails.Any())
                DeleteAllBtn.IsEnabled = false;
        }

        private void EmailList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ListBox).SelectedItem is Email email)
            {
                DeleteBtn.IsEnabled = true;
                EmailViewer.Text = email.Message;
                if(email.CanReply)
                    ReplyBtn.IsEnabled = true;
            }
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!(EmailList.SelectedItem is Email email)) 
                return;

            Database.DeleteEmail(email.Id);
            GameWindow.MainFrame.Refresh();
            GameWindow.EmailsCount = Database.GetEmails(UserManager.Id).Count();

        }

        private void ComposeBtn_Click(object sender, RoutedEventArgs e)
        {
            var compose = new NewEmail(UserManager);
            var open = compose.ShowDialog();
            if (open.HasValue && open.Value)
            {
                SendEmail.ManagerToManager(compose.Sender, compose.Receiver, compose.Subject, compose.Message);
                IFM.NotifyChange("EmailsCount");
            }
        }

        private void ReplyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!(EmailList.SelectedItem is Email email))
            {
                return;
            }

            var compose = new NewEmail(UserManager, Database.GetManagerById(email.SenderId));

            var doc = new FlowDocument();
            var p = new Paragraph(new Run("\n\n------------------\nSent on: " + email.SendDate.ToShortDateString() + "\n\n" + email.Message));
            doc.Blocks.Add(p);
            compose.EmailTxt.Document = doc;
            compose.Subject = "Re: " + email.Subject;
            var open = compose.ShowDialog();
            if (open.HasValue && open.Value)
            {
                SendEmail.ManagerToManager(compose.Sender, compose.Receiver, compose.Subject, compose.Message);
                IFM.NotifyChange("EmailsCount");
            }
        }

        private void DeleteAllBtn_Click(object sender, RoutedEventArgs e)
        {
            var emails = Database.GetEmails(UserManager.Id).ToList();
            emails.ForEach(em =>
            {
                Database.DeleteEmail(em.Id);
            });
            EmailList.ItemsSource = null;
            DeleteAllBtn.IsEnabled = false;
            GameWindow.EmailsCount = 0;
        }
    }
}

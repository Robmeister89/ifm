﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeagueStats.xaml
    /// </summary>
    public partial class LeagueStats : UserControl
    {
        public LeagueStats()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            HeaderTxt.Text = CurrentLeague.LeagueName + " Season Stats";

            var leagueStatistics = Database.GetAllPlayerStats(CurrentLeague.CurrentSeason).FirstOrDefault()?.StatsJson?.DeserializeToList<PlayerStats>();
            if (leagueStatistics == null) 
                return;

            PassingDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.PassYds).Where(p => p.PassAtt > 0);
            RushingDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.RushYds).Where(p => p.Carries > 0);
            ReceivingDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.RecYds).Where(p => p.Receptions > 0);
            BlockingDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.PancakeBlocks).Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0);
            ReturnsDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.KickRetYds).Where(p => p.KickReturns > 0);
            DefenseDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.Tackles).Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0);
            KickingDatagrid.ItemsSource = leagueStatistics.OrderByDescending(g => g.Points).Where(p => p.XPAtt > 0 || p.FGAtt > 0);
        }

        private void Datagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var id = ((sender as DataGrid).SelectedItem as PlayerStats).PlayerId;
            var player = Database.GetPlayerById(id);
            GameWindow.MainFrame.Navigate(new PlayerCard(player));
        }

        private void PlayoffStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.Navigate(new LeaguePlayoffStats());
        }
    }
}

﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.Services;
using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for GameGrid.xaml
    /// </summary>
    public partial class GameGrid : UserControl, INotifyPropertyChanged
    {
        public GameGrid()
        {
            InitializeComponent();
        }

        public GameGrid(Game g)
        {
            InitializeComponent();
            Game = g;
            if (g.Season != CurrentLeague.CurrentSeason)
                RecapBtn.Visibility = Visibility.Collapsed;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static GeneralManager Manager => IFM.UserManager;
    
        private Game _game;
        public Game Game
        {
            get => _game;
            set
            {
                _game = value;
                OnPropertyChanged(nameof(Game));
            }
        }   

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (Game.HasPlayed)
            {
                RecapBtn.Click += (s, r) => ViewRecap(Game);
            }
            else
            {
                if (CurrentLeague.CurDate == Game.GameDate)
                {
                    RecapBtn.Content = "Sim Game";
                    RecapBtn.Click += SimClick;
                }
                else
                {
                    RecapBtn.IsEnabled = false;
                }

                if (!Manager.IsCommissioner)
                    RecapBtn.IsEnabled = false;
            }
        }

        private async void SimClick(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();

            try
            {
                await Game.PlayGame();
                await Game.SaveGameStats(Game.Playoffs);
            }
            finally
            {
                var gameList = Database.GetGames(Game.GameDate.Year).Where(g => g.GameDate == Game.GameDate).ToList();
                _ = GameWindow.MainFrame.Navigate(new GameDay(gameList));

                await App.DeactivateLoading();
            }
        }
        
        private static void ViewRecap(Game g)
        {
            var view = new GameView(g);
            _ = view.ShowDialog();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for EditPlayer.xaml
    /// </summary>
    public partial class EditPlayer : UserControl
    {
        public EditPlayer()
        {
            InitializeComponent();
        }

        public EditPlayer(Player p)
        {
            InitializeComponent();
            Player = p;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private Player Player { get; set; }
        private static League CurrentLeague => IFM.CurrentLeague;
        private List<Injury> _injuries;
        private bool _isRetired = false;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            _injuries = LoadedInjuries;
            CollegeSelect.ItemsSource = File.ReadAllLines("Resources/Colleges.txt");
            PositionSelect.ItemsSource = Positions;
            DeleteExt.IsEnabled = false;
            AssignTeam.IsEnabled = false;
            
            FirstNameTxt.Text = Player.FirstName;
            LastNameTxt.Text = Player.LastName;
            PositionSelect.Text = Player.Pos;
            JerseySelect.Value = Player.Jersey;
            AgeSelect.SelectedDate = Player.DOB;
            ExpSelect.Value = Player.Exp;
            CollegeSelect.Text = Player.College;
            ConYearsSelect.Value = Player.ContractYears;
            SalaryTxt.Text = Player.Salary.ToString();
            PassATxt.Value = Player.Accuracy;
            PassSTxt.Value = Player.ArmStrength;
            StrengthTxt.Value = Player.Strength;
            CarryTxt.Value = Player.Carry;
            SpeedTxt.Value = Player.Speed;
            CatchTxt.Value = Player.Catching;
            BlockTxt.Value = Player.Block;
            BlitzTxt.Value = Player.Blitz;
            PassDTxt.Value = Player.Coverage;
            TackleTxt.Value = Player.Tackle;
            IntelTxt.Value = Player.Intelligence;
            KickSTxt.Value = Player.KickStrength;
            KickATxt.Value = Player.KickAccuracy;
            AttitudeTxt.Value = Player.Attitude;
            GreedTxt.Value = Player.Greed;
            LoyaltyTxt.Value = Player.Loyalty;
            PassionTxt.Value = Player.Passion;
            InjuryTxt.Value = Player.Injury;
            LeadershipTxt.Value = Player.Leadership;
            WorkEthicTxt.Value = Player.WorkEthic;

            switch (Player.TeamId)
            {
                case 0:
                    AssignTeam.IsEnabled = true;
                    TeamSelect.ItemsSource = Repo.Database.GetTeams().OrderBy(t => t.City);
                    break;
                case -2:
                    RetiredPlayer.IsChecked = true;
                    _isRetired = true;
                    break;
                default:
                    if (Player.TeamId > 0)
                    {
                        if (Player.ExtensionYears != 0 || Player.IsFranchised)
                            DeleteExt.IsEnabled = true;
                    }

                    break;
            }

        }

        private static IEnumerable<string> Positions =>
            new List<string>
            {
                "QB", "RB", "WR", "OL", "DL", "LB", "DB", "K"
            };

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            SavePlayer(Player);
        }

        private void SavePlayer(Player p)
        {
            p.FirstName = FirstNameTxt.Text;
            p.LastName = LastNameTxt.Text;
            p.Pos = PositionSelect.Text;
            p.Jersey = Convert.ToInt32(JerseySelect.Text);
            p.DOB = (DateTime)AgeSelect.SelectedDate;
            p.Exp = Convert.ToInt32(ExpSelect.Text);
            p.College = CollegeSelect.Text;
            var conYears = Convert.ToInt32(ConYearsSelect.Value);
            if (conYears != p.ContractYears)
                p.ContractYears = conYears;
            p.Salary = Convert.ToInt32(SalaryTxt.Text);
            p.Accuracy = Convert.ToInt32(PassATxt.Value);
            p.ArmStrength = Convert.ToInt32(PassSTxt.Value);
            p.Strength = Convert.ToInt32(StrengthTxt.Value);
            p.Carry = Convert.ToInt32(CarryTxt.Value);
            p.Speed = Convert.ToInt32(SpeedTxt.Value);
            p.Catching = Convert.ToInt32(CatchTxt.Value);
            p.Block = Convert.ToInt32(BlockTxt.Value);
            p.Blitz = Convert.ToInt32(BlitzTxt.Value);
            p.Coverage = Convert.ToInt32(PassDTxt.Value);
            p.Tackle = Convert.ToInt32(TackleTxt.Value);
            p.Intelligence = Convert.ToInt32(IntelTxt.Value);
            p.KickStrength = Convert.ToInt32(KickSTxt.Value);
            p.KickAccuracy = Convert.ToInt32(KickATxt.Value);
            p.Attitude = Convert.ToInt32(AttitudeTxt.Value);
            p.Greed = Convert.ToInt32(GreedTxt.Value);
            p.Loyalty = Convert.ToInt32(LoyaltyTxt.Value);
            p.Passion = Convert.ToInt32(PassionTxt.Value);
            p.Injury = Convert.ToInt32(InjuryTxt.Value);
            p.Leadership = Convert.ToInt32(LeadershipTxt.Value);
            p.WorkEthic = Convert.ToInt32(WorkEthicTxt.Value);

            if ((bool)DeleteExt.IsChecked)
            {
                p.ExtensionSalary = 0;
                p.ExtensionYears = 0;
            }

            if ((bool)InjuryChk.IsChecked)
            {
                if (p.IsInjured)
                {
                    p.IsInjured = false;
                    p.InjuryTxt = "";
                    p.InjuryDuration = 0;
                }
                else
                {
                    var severity = new Random().Next(0, 5);
                    var frequencyInjuries = _injuries.Where(i => i.Frequency > severity).ToList();
                    var randInjury = frequencyInjuries[new Random().Next(0, frequencyInjuries.Count() - 1)];
                    p.IsInjured = true;
                    p.InjuryTxt = randInjury.Diagnosis;
                    p.InjuryDuration = new Random().Next(randInjury.MinTime, randInjury.MaxTime);
                }
            }

            if ((bool)AssignTeam.IsChecked)
            {
                var team = TeamSelect.SelectedItem as Team;
                PlayerData.CpuSignInSeason(team, p, false);
            }

            if (_isRetired && !(bool)RetiredPlayer.IsChecked)
                p.TeamId = 0;
            else if (!_isRetired && (bool)RetiredPlayer.IsChecked)
            {
                var transaction = new Transaction(Player.TeamId,
                                    CurrentLeague.CurrentSeason,
                                    CurrentLeague.CurDate,
                                    $"{p.NameWithPosition} has retired on {CurrentLeague.CurDate.ToShortDateString()}.");

                Repo.Database.SaveTransaction(transaction);

                p.ContractYears = 0;
                p.Salary = 0;
                p.ExtensionSalary = 0;
                p.ExtensionYears = 0;
                if (p.IsFranchised)
                {
                    p.IsFranchised = false;
                    p.Team.FranchiseTagUsed = false;
                    Repo.Database.SaveTeam(p.Team);
                }
                p.TeamId = -2;
                var playerHistory = p.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add("Retired on " + CurrentLeague.CurDate.ToShortDateString());
                p.PlayerHistoryJson = playerHistory.SerializeToJson();
            }

            p.Overall = p.GetOverall;

            Repo.Database.SavePlayer(p);
            GameWindow.MainFrame.GoBack();
        }

        private static List<Injury> LoadedInjuries
        {
            get
            {
                var injuries = new List<Injury>();
                var text = File.ReadAllLines(@"Resources/Injuries.csv");
                var x = 1;
                while (x < text.Length)
                {
                    var col = text[x].Split(',');
                    var injury = new Injury()
                    {
                        MinTime = Convert.ToInt32(col[0]),
                        MaxTime = Convert.ToInt32(col[1]),
                        CareerEnding = Convert.ToInt32(col[2]) != 0,
                        Diagnosis = col[3],
                        Frequency = Convert.ToInt32(col[4]),
                    };
                    injuries.Add(injury);
                    x++;
                }
                return injuries;
            }
        }
    }
}

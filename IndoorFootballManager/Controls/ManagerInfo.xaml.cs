﻿using System;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for ManagerInfo.xaml
    /// </summary>
    public partial class ManagerInfo : UserControl
    {
        public ManagerInfo()
        {
            InitializeComponent();
        }

        public ManagerInfo(GeneralManager manager)
        {
            InitializeComponent();
            UserManager = manager;
        }

        private GeneralManager UserManager { get; }
        private IFM GameWindow => Window.GetWindow(this) as IFM;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            FNameTxt.Text = UserManager.FirstName;
            LNameTxt.Text = UserManager.LastName;
            AgeSelect.SelectedDate = UserManager.DOB;
            PasswordTxt.Text = Converters.EncryptDecrypt(UserManager.Password, 256);
            AssistSigning.IsChecked = UserManager.AssistExtensions;
            FillRoster.IsChecked = UserManager.FillRoster;
            SignFreeAgents.IsChecked = UserManager.AssistSignings;
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            UserManager.FirstName = FNameTxt.Text;
            UserManager.LastName = LNameTxt.Text;
            UserManager.DOB = (DateTime)AgeSelect.SelectedDate;
            UserManager.Password = Converters.EncryptDecrypt(PasswordTxt.Text, 256);
            UserManager.AssistSignings = (bool)SignFreeAgents.IsChecked ? true : false;
            UserManager.FillRoster = (bool)FillRoster.IsChecked ? true : false;
            UserManager.AssistExtensions = (bool)AssistSigning.IsChecked ? true : false;
            Repo.Database.SaveManager(UserManager);
            App.ShowNotification("Success", "Manager updated.", Notification.Wpf.NotificationType.Success, 5);
            GameWindow.MainFrame.GoBack();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

    }
}

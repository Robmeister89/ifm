﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using IndoorFootballManager.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ookii = Ookii.Dialogs.Wpf;
using System.IO;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for DraftScreen.xaml
    /// </summary>
    public partial class DraftScreen : UserControl
    {
        private ookii.ProgressDialog _prog = new ookii.ProgressDialog()
        {
            WindowTitle = "Setting Up Draft",
            Text = "Getting your draft ready...",
            ShowTimeRemaining = false,
            ShowCancelButton = false,
            ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
        };

        public DraftScreen()
        {
            InitializeComponent();
        }

        private readonly Generation _generation = new Generation();
        private static SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;
        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private List<Player> UndraftedPlayers { get; set; }
        private IEnumerable<Draft> DraftPicks { get; set; }
        private List<Player> DraftedPlayers { get; set; }
        private List<int> PicksMade { get; set; }

        private double _lastDraftSalary;

        private sealed class TeamNeed
        {
            public int TeamId { get; set; }
            public List<string> Needs { get; set; }
        }

        private List<TeamNeed> _teamNeeds;
        
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            ExportDraftBtn.IsEnabled = false;
            ImportDraftBtn.IsEnabled = false;
            ChangeOrderBtn.IsEnabled = false;
            SelectPlayerBtn.IsEnabled = false;
            CpuSuggestBtn.IsEnabled = false;
            CpuPickBtn.IsEnabled = false;
            CpuFinishBtn.IsEnabled = false;
            UntilHumanBtn.IsEnabled = false;
            RosterCount.IsEnabled = false;

            DraftedPlayers = new List<Player>();
            PicksMade = new List<int>();
            _teamNeeds = new List<TeamNeed>();
            var teams = Database.GetTeams().ToList();
            foreach (var t in teams)
            {
                _teamNeeds.Add(new TeamNeed
                {
                    TeamId = t.Id,
                    Needs = t.TeamNeeds,
                });
            }
            _lastDraftSalary = Database.GetLastDraftSalary(CurrentLeague.CurrentSeason);
            var picks = CurrentLeague.DraftRounds * CurrentLeague.LeagueTeams;
            if (CurrentLeague.IsInauguralDraft)
                picks = CurrentLeague.MaxRoster * CurrentLeague.LeagueTeams;
            var rMod = picks * 0.00035;
            _xMod = 0.927 + rMod > 0.9965 ? 0.9965 : rMod + 0.927;

            DraftPicks = Database.GetDraft(CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ThenBy(d => d.Pick).ToList();
            if(!DraftPicks.Any())
            {
                _prog.DoWork += CreateDraft;
                _prog.RunWorkerCompleted += PostCreateDraft;
                _prog.ShowDialog();
            }
            else
            {
                DraftDatagrid.ItemsSource = DraftPicks;
                if((DraftDatagrid.Items[DraftDatagrid.Items.Count - 1] as Draft).Player == null)
                {
                    if (CurrentLeague.IsInauguralDraft)
                    {
                        UndraftedPlayers = Database.GetPlayers().Where(p => p.TeamId == 0).OrderByDescending(p => p.Overall).ToList();
                        RookiesDatagrid.ItemsSource = UndraftedPlayers;
                    }
                    else
                    {
                        UndraftedPlayers = Database.GetPlayersByTeamId(-1).OrderByDescending(p => p.Overall).ToList();
                        RookiesDatagrid.ItemsSource = UndraftedPlayers;
                    }
                    RookiesDatagrid.SelectedIndex = 0;
                    SelectPlayerBtn.IsEnabled = true;
                    CpuSuggestBtn.IsEnabled = true;
                    CpuPickBtn.IsEnabled = true;
                    CpuFinishBtn.IsEnabled = true;
                    UntilHumanBtn.IsEnabled = true;
                    RosterCount.IsEnabled = true;
                }
                if ((DraftDatagrid.Items[0] as Draft).Player == null)
                {
                    ExportDraftBtn.IsEnabled = true;
                    ImportDraftBtn.IsEnabled = true;
                    ChangeOrderBtn.IsEnabled = true;
                }
            }
            SelectAllBtn_Click(sender, e);
        }

        private void CreateDraft(object sender, DoWorkEventArgs e)
        {
            Draft.CreateDraft();
        }

        private void PostCreateDraft(object sender, RunWorkerCompletedEventArgs e)
        {
            UndraftedPlayers = Database.GetPlayersByTeamId(-1).OrderByDescending(p => p.Overall).ToList();
            RookiesDatagrid.ItemsSource = UndraftedPlayers;
            DraftDatagrid.ItemsSource = Database.GetDraft(CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ThenBy(d => d.Pick);
            ExportDraftBtn.IsEnabled = true;
            ImportDraftBtn.IsEnabled = true;
            ChangeOrderBtn.IsEnabled = true;
            SelectPlayerBtn.IsEnabled = true;
            CpuSuggestBtn.IsEnabled = true;
            CpuPickBtn.IsEnabled = true;
            CpuFinishBtn.IsEnabled = true;
            UntilHumanBtn.IsEnabled = true;
            RosterCount.IsEnabled = true;
            
            _prog.DoWork -= CreateDraft;
            _prog.RunWorkerCompleted -= PostCreateDraft;
        }

        private void SelectAllBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var c in CheckBoxes)
                c.IsChecked = true;
        }

        private void UncheckAllBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var c in CheckBoxes)
                c.IsChecked = false;
        }

        private List<CheckBox> _checkBoxes;
        private List<CheckBox> CheckBoxes
        {
            get
            {
                _checkBoxes = new List<CheckBox>()
                {
                    QBChk, RBChk, WRChk, OLChk, DLChk, LBChk, DBChk, KChk,
                };
                return _checkBoxes;
            }
        }

        public void ImportDraft(object sender, DoWorkEventArgs e)
        {
            try
            {
                // set this in an open file dialog or set it automatically and set a warning prior to import
                var filePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\" +
                               $@"{CurrentLeague.Abbreviation}\Output\{CurrentLeague.Abbreviation}_{CurrentLeague.CurrentSeason}Draft.csv";
                var broken = false;
                DraftPicks = Database.GetDraft(CurrentLeague.CurrentSeason);
                var playerIds = new List<int>();
                using (var sr = new StreamReader(filePath))
                {
                    string line;
                    var i = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var newLine = line.Split(',');
                        var p = DraftDatagrid.Items[i] as Draft;
                        var d = DraftPicks.First(dp => dp.Id == Convert.ToInt32(line[0]));
                        if (p.Round == d.Round && p.Pick == d.Pick)
                            broken = false;
                        else
                            broken = true;

                        if (newLine[2] == "0")
                            broken = true;

                        if (broken)
                        {
                            _prog.RunWorkerCompleted -= CpuAutoDraftComplete;
                            break;
                        }

                        playerIds.Add(Convert.ToInt32(line[2]));
                        i++;
                    }
                }

                if (broken) 
                    return;

                var j = 0;
                foreach (Draft d in DraftDatagrid.Items)
                {
                    CurrentPick = d;
                    var p = Database.GetPlayerById(playerIds[j]);
                    SaveDraftPlayer(p, d.CurrentTeam, d.CurTeamId, d);
                    j++;
                }
            }
            catch
            {
                App.ShowNotification("Error", "Please make sure the draft file exists before importing. Check your league folder for instructions.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private double _xMod;
        // save drafted players new values
        private void SaveDraftPlayer(Player player, string teamName, int teamId, Draft d)
        {
            player.TeamId = teamId;
            d.Player = player.NameWithPosition;
            d.PlayerId = player.Id;
            player.DraftPick = CurrentPick.Pick;
            player.DraftRound = CurrentPick.Round;
            player.DraftYear = CurrentLeague.CurrentSeason;
            if (d.Round <= 2)
                player.ContractYears = 3;
            else if (d.Round <= 4)
                player.ContractYears = 2;
            else
                player.ContractYears = 1;

            if (_lastDraftSalary == 0.0)
                player.Salary = Convert.ToInt32(CurrentLeague.MaxDraftSalary * _xMod);
            else
            {
                _lastDraftSalary *= _xMod;
                player.Salary = Convert.ToInt32(_lastDraftSalary);
            }
            if (player.Salary < CurrentLeague.MinimumSalary)
                player.Salary = Convert.ToInt32(CurrentLeague.MinimumSalary);
            d.Salary = player.Salary.ToString("C0");

            var playerHistory = new List<string> { $"Drafted by {teamName} - Round {d.Round}, Pick {d.Pick} ({d.Season})." };
            player.PlayerHistoryJson = playerHistory.SerializeToJson();
            PicksMade.Add(d.DraftPickId);
            DraftedPlayers.Add(player);
            UndraftedPlayers.Remove(player);
        }

        private void FinishDraft(object sender, DoWorkEventArgs e)
        {
            Database.UpdateAllDrafts(DraftDatagrid.Items);
            Database.UpdateAllPlayers(DraftedPlayers.AsEnumerable());

            _savePlayers = false;
            if(Database.GetDraft(CurrentLeague.CurrentSeason).Last().Player != null)
            {
                if (CurrentLeague.IsInauguralDraft)
                {
                    CurrentLeague.IsInauguralDraft = false;
                    Database.SaveLeague(CurrentLeague);
                }
                Database.DeleteAllPicks(PicksMade);
                var rookies = Database.GetPlayersByTeamId(-1);
                foreach (var p in rookies)
                {
                    p.TeamId = 0;
                    p.DraftPick = 0;
                    p.DraftRound = 0;
                    p.DraftYear = 0;
                }
                Database.UpdateAllPlayers(rookies.AsEnumerable());
                var newRookies = _generation.GenerateRookies(CurrentLeague.LeagueTeams);
                Database.InsertAllPlayers(newRookies.AsEnumerable());

                var teams = Database.GetTeams().ToList();
                foreach (var picks in teams.Select(t => t.NewDraftPicks.AsEnumerable()))
                {
                    Database.InsertAllPicks(picks);
                }
            }
        }

        private void PostFinishDraft(object sender, RunWorkerCompletedEventArgs e)
        {
            DraftDatagrid.ScrollIntoView(DraftDatagrid.Items[DraftDatagrid.Items.Count - 1]);
            RookiesDatagrid.ItemsSource = null;
            App.ShowNotification(string.Empty, "Draft is Completed.", Notification.Wpf.NotificationType.Information, 5);
        }

        // handles all after-draft stuff... assigns rookies a '0' teamid (free agent), generates the next list of rookies, and assigns a new list of draft picks to teams
        private void AfterDraft()
        {
            _prog = new ookii.ProgressDialog()
            {
                WindowTitle = "Finalizing Draft",
                Text = "Finishing up the draft process...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
            };
            _prog.DoWork += FinishDraft;
            _prog.RunWorkerCompleted += PostFinishDraft;
            _prog.ShowDialog();
        }

        private Draft CurrentPick { get; set; }

        private void SelectPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Draft d in DraftDatagrid.Items)
                {
                    if (d.Player == null)
                    {
                        CurrentPick = d;

                        var team = Database.GetTeamByName(CurrentPick.CurrentTeam);
                        var player = RookiesDatagrid.SelectedItem as Player;
                        if(player != null)
                        {
                            SaveDraftPlayer(player, team.TeamName, team.Id, CurrentPick);

                            DraftDatagrid.Items.Refresh();
                            RookiesDatagrid.Items.Refresh();
                            RookiesDatagrid.ScrollIntoView(CurrentPick);
                            App.ShowNotification("Player Drafted", $"{team.TeamName} has selected {player.NameWithPosition}.", Notification.Wpf.NotificationType.Information, 5);
                        }
                        if (ChangeOrderBtn.IsEnabled)
                        {
                            ExportDraftBtn.IsEnabled = false;
                            ImportDraftBtn.IsEnabled = false;
                            ChangeOrderBtn.IsEnabled = false;
                        }

                        UpdatePlayerList(sender, e);

                        break;
                    }
                }
                if((DraftDatagrid.Items[DraftDatagrid.Items.Count - 1] as Draft).Player != null)
                    AfterDraft();
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Please refer the the Log file saved in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void CpuSuggestBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Draft d in DraftDatagrid.Items)
                {
                    if (d.Player == null)
                    {
                        CurrentPick = d;
                        var team = Database.GetTeamById(d.CurTeamId);
                        Player player;

                        var need = _teamNeeds.First(t => t.TeamId == team.Id);
                        if (need.Needs.Any())
                        {
                            if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round < 5)
                                player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                            else if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round >= 5)
                            {
                                player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                            }
                            else
                            {
                                if (CurrentLeague.IsInauguralDraft && d.Round < 20 && need.Needs[0] == "K")
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && need.Needs[0] == "K")
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                else
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                            }
                        }
                        else
                        {
                            if (CurrentLeague.IsInauguralDraft && d.Round < 20 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                            else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                            else
                                player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                        }

                        // checks to make sure player is not null and assigned player his new values (teamid, draftpick, draftround, etc.)
                        if (player != null)
                        {
                            void SavePlayer()
                            {
                                SaveDraftPlayer(player, team.TeamName, team.Id, d);
                                if (ChangeOrderBtn.IsEnabled)
                                {
                                    ExportDraftBtn.IsEnabled = false;
                                    ImportDraftBtn.IsEnabled = false;
                                    ChangeOrderBtn.IsEnabled = false;
                                }
                            }

                            void Cancel() { return; }

                            App.ShowMessageBox("Draft Player", $"Do you wish to draft {player.NameWithPosition}?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", SavePlayer, "No", Cancel);
                        }

                        if ((DraftDatagrid.Items[DraftDatagrid.Items.Count - 1] as Draft).Player != null)
                            AfterDraft();
                        else
                            DraftDatagrid.ScrollIntoView(CurrentPick);

                        break;
                    }
                }

                DraftDatagrid.Items.Refresh();
                UpdatePlayerList(sender, e);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Please refer the the Log file saved in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void CpuPickBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Draft d in DraftDatagrid.Items)
                {
                    if (d.Player == null)
                    {
                        CurrentPick = d;
                        var team = Database.GetTeamById(d.CurTeamId);
                        Player player;

                        var need = _teamNeeds.First(t => t.TeamId == team.Id);
                        if (need.Needs.Any())
                        {
                            if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round < 5)
                                player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                            else if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round >= 5)
                            {
                                player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                            }
                            else
                            {
                                if (CurrentLeague.IsInauguralDraft && d.Round < 20 && need.Needs[0] == "K")
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && need.Needs[0] == "K")
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                else
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                            }
                        }
                        else
                        {
                            if (CurrentLeague.IsInauguralDraft && d.Round < 20 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                            else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                            else
                                player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                        }

                        // checks to make sure player is not null and assigned player his new values (teamid, draftpick, draftround, etc.)
                        if (player != null)
                        {
                            SaveDraftPlayer(player, team.TeamName, team.Id, d);
                            if (ChangeOrderBtn.IsEnabled)
                            {
                                ExportDraftBtn.IsEnabled = false;
                                ImportDraftBtn.IsEnabled = false;
                                ChangeOrderBtn.IsEnabled = false;
                            }
                        }

                        if ((DraftDatagrid.Items[DraftDatagrid.Items.Count - 1] as Draft).Player != null)
                            AfterDraft();
                        else
                            DraftDatagrid.ScrollIntoView(CurrentPick);
                        break;
                    }
                }

                DraftDatagrid.Items.Refresh();
                UpdatePlayerList(sender, e);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Please refer the the Log file saved in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void CpuFinishBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ChangeOrderBtn.IsEnabled)
                ChangeOrderBtn.IsEnabled = false;

            _prog = new ookii.ProgressDialog()
            {
                WindowTitle = "CPU Auto Draft",
                Text = "CPU is auto drafting...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
            };
            _prog.DoWork += CpuAutoDraft;
            _prog.RunWorkerCompleted += CpuAutoDraftComplete;
            _prog.ShowDialog();
        }


        private void CpuAutoDraft(object sender, DoWorkEventArgs e)
        {
            try
            {
                var breakout = false;
                while (!breakout)
                {
                    if (breakout)
                        break;

                    foreach (Draft d in DraftDatagrid.Items)
                    {
                        if (d.Player == null)
                        {
                            CurrentPick = d;
                            var team = Database.GetTeamById(d.CurTeamId);
                            Player player;

                            var need = _teamNeeds.First(t => t.TeamId == team.Id);
                            if (need.Needs.Any())
                            {
                                if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round < 5)
                                    player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                                else if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round >= 5)
                                {
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                    need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                                }
                                else
                                {
                                    if (CurrentLeague.IsInauguralDraft && d.Round < 20 && need.Needs[0] == "K")
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                    else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && need.Needs[0] == "K")
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                    else
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                    need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                                }
                            }
                            else
                            {
                                if (CurrentLeague.IsInauguralDraft && d.Round < 20 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                    player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                                else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                    player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                                else
                                    player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                            }

                            SaveDraftPlayer(player, team.TeamName, team.Id, d);
                            if ((DraftDatagrid.Items[DraftDatagrid.Items.Count - 1] as Draft).Player != null)
                                breakout = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Please refer the the Log file saved in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void CpuAutoDraftComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            AfterDraft();
        }

        private void UntilHumanBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ChangeOrderBtn.IsEnabled)
            {
                ExportDraftBtn.IsEnabled = false;
                ImportDraftBtn.IsEnabled = false;
                ChangeOrderBtn.IsEnabled = false;
            }

            _prog = new ookii.ProgressDialog()
            {
                WindowTitle = "CPU Auto Draft",
                Text = "CPU is auto drafting...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
            };
            _prog.DoWork += UntilHuman;
            _prog.RunWorkerCompleted += UntilHumanFinish;
            _prog.ShowDialog();
        }

        private void UntilHumanFinish(object sender, RunWorkerCompletedEventArgs e)
        {
            DraftDatagrid.Items.Refresh();
            UpdatePlayerList(sender, null);
            _prog.Dispose();
        }

        private int _currentIndex;

        private void UntilHuman(object sender, DoWorkEventArgs e)
        {
            try
            {
                Team team = null;
                var cpu = true;
                do
                {
                    foreach (Draft d in DraftDatagrid.Items)
                    {
                        if (d.Player == null)
                        {
                            CurrentPick = d;
                            team = Database.GetTeamById(d.CurTeamId);
                            cpu = team.IsCpu;
                            if (!cpu)
                            {
                                _currentIndex = DraftDatagrid.Items.IndexOf(d);
                                break;
                            }

                            Player player;

                            var need = _teamNeeds.First(t => t.TeamId == team.Id);
                            if (need.Needs.Any())
                            {
                                if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round < 5)
                                    player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                                else if (need.Needs.Count() == 1 && need.Needs[0] == "K" && d.Round >= 5)
                                {
                                    player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                    need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                                }
                                else
                                {
                                    if (CurrentLeague.IsInauguralDraft && d.Round < 20 && need.Needs[0] == "K")
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                    else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && need.Needs[0] == "K")
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos) && pl.Pos != "K");
                                    else
                                        player = UndraftedPlayers.First(pl => need.Needs.Contains(pl.Pos));
                                    need.Needs.RemoveAt(need.Needs.IndexOf(player.Pos));
                                }
                            }
                            else
                            {
                                if (CurrentLeague.IsInauguralDraft && d.Round < 20 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                    player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                                else if (!CurrentLeague.IsInauguralDraft && d.Round < 5 && UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First().Pos == "K")
                                    player = UndraftedPlayers.Where(pl => pl.Pos != "K").OrderByDescending(pl => pl.Evaluation).First();
                                else
                                    player = UndraftedPlayers.OrderByDescending(pl => pl.Evaluation).First();
                            }

                            SaveDraftPlayer(player, team.TeamName, team.Id, d);

                            Dispatcher.Invoke(() =>
                            {
                                if (!ChangeOrderBtn.IsEnabled) return;
                                ExportDraftBtn.IsEnabled = false;
                                ImportDraftBtn.IsEnabled = false;
                                ChangeOrderBtn.IsEnabled = false;
                            });
                        }
                    }
                } while (cpu);
                Dispatcher.Invoke(() => DraftDatagrid.ScrollIntoView(DraftDatagrid.Items[_currentIndex]));
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Please refer the the Log file saved in your Documents folder.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private void UpdatePlayerList(object sender, RoutedEventArgs e)
        {
            var positions = CheckBoxes.Where(c => (bool)c.IsChecked).Select(c => c.Content.ToString().TrimStart()).ToList();
            if (positions.Count() == CheckBoxes.Count(c => (bool)c.IsChecked))
            {
                if(UndraftedPlayers != null)
                    RookiesDatagrid.ItemsSource = UndraftedPlayers.Where(p => positions.Contains(p.Pos)).OrderByDescending(p => p.Overall);
            }
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdatePlayerList(sender, e);
        }

        private Player _player;
        private void RookiesDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _player = (sender as DataGrid).SelectedItem as Player;
            if (_player != null)
                GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(_player));
        }

        private void RosterCount_Click(object sender, RoutedEventArgs e)
        {
            var team = (from Draft d in DraftDatagrid.Items where d.Player == null select Database.GetTeamByName(d.CurrentTeam)).FirstOrDefault();
            var rosterCount = new RosterCount(team);
            rosterCount.ShowDialog();
        }

        private bool _savePlayers = true;
        public static bool cancelSave = false;
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue && _savePlayers)
            {
                if (!cancelSave)
                {
                    Database.UpdateAllPlayers(DraftedPlayers);
                    Database.UpdateAllDrafts(DraftDatagrid.Items);
                    Database.DeleteAllPicks(PicksMade);
                }
            }
        }

        private void ChangeOrderBtn_Click(object sender, RoutedEventArgs e)
        {
            var teams = Database.GetTeams().OrderBy(t => t.TeamName).ToList();
            var change = new ChangeDraft(teams).ShowDialog();
            if(change.HasValue && change.Value)
            {
                //List<int> NewOrder = ChangeDraft.DraftOrder.ToList();
                var newOrder = ChangeDraft.NewOrder;
                if (CurrentLeague.IsInauguralDraft)
                    Draft.UpdateInauguralDraft(newOrder);
                else
                    Draft.UpdateDraft(newOrder);

                DraftPicks = Database.GetDraft(CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ThenBy(d => d.Pick);
                DraftDatagrid.ItemsSource = DraftPicks;
                GameWindow.MainFrame.Refresh();
            }
        }

        private void ExportDraftBtn_Click(object sender, RoutedEventArgs e)
        {
            LeagueData.ExportDraft(Database.GetDraft(CurrentLeague.CurrentSeason).OrderBy(d => d.Round).ThenBy(d => d.Pick));
            PlayerData.ExportPlayers(
                CurrentLeague.IsInauguralDraft ? Database.GetPlayersByTeamId(0) : Database.GetPlayersByTeamId(-1),
                $"{CurrentLeague.CurrentSeason}DraftPool");

            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\";
            var filePath = $"{databasePath}Import Draft Instructions.txt";
            if (File.Exists(filePath))
                File.Delete(filePath);

            using (var sw = new StreamWriter(filePath))
            {
                sw.WriteLine("1. Do not change the name of the draft file or move it to another directory.");
                sw.WriteLine("2. If you want to change the order, please do it in-game and then re-export the draft files.");
                sw.WriteLine("3. Only change the non-existent third column with the player id.");
                sw.WriteLine("4a. If you opened the file in Excel, just add the player ids to Column C.");
                sw.WriteLine("4b. If you opened the file in another text editor, please be sure you include a comma [,] after the team id (second value) prior to entering the player id.");
                sw.WriteLine("5. Once all teams have a player id in the third column, you may go back into the game and click on the Import Draft button.");
            }

            App.ShowNotification("Success", "Draft files have been exported.", Notification.Wpf.NotificationType.Success, 5);
        }

        private void ImportDraftBtn_Click(object sender, RoutedEventArgs e)
        {
            _prog = new ookii.ProgressDialog()
            {
                WindowTitle = "Importing Draft",
                Text = "Draft file is being imported...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar
            };

            _prog.DoWork += ImportDraft;
            _prog.RunWorkerCompleted += CpuAutoDraftComplete;
            _prog.ShowDialog();
        }

    }
}

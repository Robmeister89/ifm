﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using SQLite;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeagueHistory.xaml
    /// </summary>
    public partial class LeagueHistory : UserControl
    {
        public LeagueHistory()
        {
            InitializeComponent();
        }

        public LeagueHistory(League league)
        {
            InitializeComponent();
            _currentLeague = league;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private readonly League _currentLeague;
        private int _year;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var seasons = new List<int>();
            for(var i = _currentLeague.Season; i <= _currentLeague.CurrentSeason; i++)
            {
                seasons.Add(i);
            }
            YearSelect.ItemsSource = seasons;

            HeaderTxt.Text = _currentLeague.LeagueName + " Historical Information";
            
            _year = Convert.ToInt32(YearSelect.SelectedItem);
            var draft = Repo.Database.GetDraft(_year);
            DraftGrid.ItemsSource = draft.OrderBy(d => d.Round).ThenBy(d => d.Pick);
            Standings.Children.Clear();
            CreateGrid(_year);
            PlayoffGrid.Children.Clear();
            CreatePlayoffGrid();
            LoadStats();
        }


        private void YearSelect_DropDownClosed(object sender, EventArgs e)
        {
            _year = Convert.ToInt32(YearSelect.SelectedItem);
            // set draft grid
            var draft = Repo.Database.GetDraft(_year);
            DraftGrid.ItemsSource = draft.OrderBy(d => d.Round).ThenBy(d => d.Pick);
            // create standings grids
            Standings.Children.Clear();
            CreateGrid(_year);
            PlayoffGrid.Children.Clear();
            CreatePlayoffGrid();
        }

        private void DraftGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var pick = (sender as DataGrid).SelectedItem as Draft;
            var season = pick.Season;
            var round = pick.Round;
            var draftPick = pick.Pick;

            var player = Database.GetPlayers().FirstOrDefault(p => p.DraftYear == season && p.DraftRound == round && p.DraftPick == draftPick);
            if (player != null)
                GameWindow.MainFrame.Navigate(new PlayerCard(player));
        }

        private void LoadStats()
        {
            var careerTotal = Career.GroupBy(g => g.PlayerId).Select(p => new PlayerStats
            {
                Team = p.GroupBy(g => g.Season).Count().ToString(),
                PlayerId = p.First().PlayerId,
                PlayerName = p.First().PlayerName,
                PassAtt = p.Sum(c => c.PassAtt),
                PassCmp = p.Sum(c => c.PassCmp),
                PassYds = p.Sum(c => c.PassYds),
                PassTds = p.Sum(c => c.PassTds),
                PassInts = p.Sum(c => c.PassInts),
                Pass20 = p.Sum(c => c.Pass20),
                PassFirst = p.Sum(c => c.PassFirst),
                PassLong = p.Max(c => c.PassLong),
                Carries = p.Sum(c => c.Carries),
                RushYds = p.Sum(c => c.RushYds),
                RushTds = p.Sum(c => c.RushTds),
                Rush20 = p.Sum(c => c.Rush20),
                RushFirst = p.Sum(c => c.RushFirst),
                RushLong = p.Max(c => c.RushLong),
                Receptions = p.Sum(c => c.Receptions),
                RecYds = p.Sum(c => c.RecYds),
                RecTds = p.Sum(c => c.RecTds),
                Rec20 = p.Sum(c => c.Rec20),
                RecFirst = p.Sum(c => c.RecFirst),
                RecLong = p.Max(c => c.RecLong),
                Drops = p.Sum(c => c.Drops),
                Fumbles = p.Sum(c => c.Fumbles),
                FumblesLost = p.Sum(c => c.FumblesLost),
                Tackles = p.Sum(c => c.Tackles),
                Sacks = p.Sum(c => c.Sacks),
                ForFumb = p.Sum(c => c.ForFumb),
                FumbRec = p.Sum(c => c.FumbRec),
                Ints = p.Sum(c => c.Ints),
                IntYds = p.Sum(c => c.IntYds),
                Safeties = p.Sum(c => c.Safeties),
                DefTds = p.Sum(c => c.DefTds),
                FGAtt = p.Sum(c => c.FGAtt),
                FGMade = p.Sum(c => c.FGMade),
                FGLong = p.Max(c => c.FGLong),
                XPAtt = p.Sum(c => c.XPAtt),
                XPMade = p.Sum(c => c.XPMade),
                Rouges = p.Sum(c => c.Rouges),
                KickReturns = p.Sum(c => c.KickReturns),
                KickRetYds = p.Sum(c => c.KickRetYds),
                KickReturnTds = p.Sum(c => c.KickReturnTds),
                KickReturnLong = p.Max(c => c.KickReturnLong),
                PancakeBlocks = p.Sum(c => c.PancakeBlocks),
                SacksAllowed = p.Sum(c => c.SacksAllowed),
            }).ToList();
            PassingDatagrid.ItemsSource = careerTotal.Where(g => g.PassAtt > 0).OrderByDescending(p => p.PassYds);
            RushingDatagrid.ItemsSource = careerTotal.Where(g => g.Carries > 0).OrderByDescending(p => p.RushYds);
            ReceivingDatagrid.ItemsSource = careerTotal.Where(g => g.Receptions > 0).OrderByDescending(p => p.RecYds);
            BlockingDatagrid.ItemsSource = careerTotal.Where(g => g.SacksAllowed > 0 || g.PancakeBlocks > 0).OrderByDescending(p => p.PancakeBlocks);
            ReturnsDatagrid.ItemsSource = careerTotal.Where(g => g.KickReturns > 0).OrderByDescending(p => p.KickRetYds);
            DefenseDatagrid.ItemsSource = careerTotal.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0).OrderByDescending(p => p.Tackles);
            KickingDatagrid.ItemsSource = careerTotal.Where(g => g.XPAtt > 0 || g.FGAtt > 0).OrderByDescending(p => p.Points);
        }

        private IEnumerable<PlayerStats> Career => Database.GetAllPlayerCareerStats()
            .SelectMany(s => s.StatsJson.DeserializeToList<PlayerStats>()).ToList();

        private void Datagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var id = ((sender as DataGrid).SelectedItem as PlayerStats).PlayerId;
            var player = Database.GetPlayerById(id);
            if(player != null)
                GameWindow.MainFrame.Navigate(new PlayerCard(player));
        }

        private void CreateGrid(int year)
        {
            var standings = Database.GetHistoricalStandings(year).Distinct().Select(t => new { t.Conference, t.Division }).ToList();
            if (!standings.Any()) 
                return;

            var conferences = new List<string>();
            var divisions = new List<string>();
            foreach (var s in standings)
            {
                if (!conferences.Contains(s.Conference))
                    conferences.Add(s.Conference);
                if (!divisions.Contains(s.Division))
                    divisions.Add(s.Division);
            }

            var conf = conferences.Count();
            var divs = divisions.Count();

            DataGrid dg;

            if (conf == 1)
            {
                var header = new TextBlock()
                {
                    Foreground = Brushes.GhostWhite,
                    FontSize = 22,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Text = _currentLeague.Conference1,
                };
                Grid.SetRow(header, 0); 
                Grid.SetColumn(header, 0);
                Conferences.Children.Add(header);

                var col = new ColumnDefinition() { Width = new GridLength(1370) };
                Standings.ColumnDefinitions.Add(col);

                for (var i = 0; i < divs; i++)
                {
                    var row = new RowDefinition() { Height = GridLength.Auto, };
                    Standings.RowDefinitions.Add(row);

                    dg = StandingsGrid(divisions[i]);
                    dg.ItemsSource = Database.GetHistoricalDivisionStandings(year, divisions[i]);
                    Grid.SetRow(dg, i + 1);
                    Grid.SetColumn(dg, 0);
                    Standings.Children.Add(dg);
                }
            }
            else
            {
                var id = 0;
                for (var i = 0; i < 2; i++)
                {
                    var col = new ColumnDefinition() { Width = new GridLength(635) };
                    Standings.ColumnDefinitions.Add(col);
                    var col1 = new ColumnDefinition() { Width = new GridLength(635) };
                    Conferences.ColumnDefinitions.Add(col1);

                    var header = new TextBlock()
                    {
                        Foreground = Brushes.GhostWhite,
                        FontSize = 22,
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Text = i == 0 ? _currentLeague.Conference1 : _currentLeague.Conference2,
                    };
                    Grid.SetRow(header, 0);
                    Grid.SetColumn(header, i);
                    Conferences.Children.Add(header);

                    for (var j = 0; j < (divs / 2); j++)
                    {
                        var row = new RowDefinition() { Height = GridLength.Auto, };
                        Standings.RowDefinitions.Add(row);

                        dg = StandingsGrid(divisions[id]);
                        dg.ItemsSource = Database.GetHistoricalDivisionStandings(year, divisions[id++]);
                        Grid.SetRow(dg, j + 1);
                        Grid.SetColumn(dg, i);
                        Standings.Children.Add(dg);
                    }
                }
            }
            //db.Disconnect();
        }

        private DataGrid StandingsGrid(string divName)
        {
            var dg = new DataGrid()
            {
                Margin = new Thickness(20, 0, 20, 20),
                Background = Brushes.Black,
                RowBackground = Brushes.Transparent,
                CanUserReorderColumns = false,
                CanUserResizeColumns = false,
                CanUserResizeRows = false,
                CanUserSortColumns = false,
                IsReadOnly = true,
                SelectionMode = DataGridSelectionMode.Extended,
                HeadersVisibility = DataGridHeadersVisibility.Column,
                GridLinesVisibility = DataGridGridLinesVisibility.None,
                AutoGenerateColumns = false,
                Width = 595
            };
            
            var teams = new DataGridTextColumn()
            {
                Header = divName,
                Binding = new Binding("TeamName"),
                Width = new DataGridLength(150),
                FontSize = 10,
            };
            var wins = new DataGridTextColumn()
            {
                Header = "W",
                Binding = new Binding("Wins"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var losses = new DataGridTextColumn()
            {
                Header = "L",
                Binding = new Binding("Losses"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pct = new DataGridTextColumn()
            {
                Header = "Pct",
                Binding = new Binding("WinPct") { StringFormat = "N3" },
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pf = new DataGridTextColumn()
            {
                Header = "PF",
                Binding = new Binding("PointsFor"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var pa = new DataGridTextColumn()
            {
                Header = "PA",
                Binding = new Binding("PointsAgainst"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var home = new DataGridTextColumn()
            {
                Header = "Home",
                Binding = new Binding("HomeRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var away = new DataGridTextColumn()
            {
                Header = "Away",
                Binding = new Binding("AwayRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var div = new DataGridTextColumn()
            {
                Header = "Div",
                Binding = new Binding("DivRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var conf = new DataGridTextColumn()
            {
                Header = "Conf",
                Binding = new Binding("ConfRecord"),
                Width = new DataGridLength(45),
                FontSize = 10,
            };
            var strk = new DataGridTextColumn()
            {
                Header = "Stk",
                Binding = new Binding("StreakTxt"),
                Width = new DataGridLength(45, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            dg.Columns.Add(teams); dg.Columns.Add(wins); dg.Columns.Add(losses); dg.Columns.Add(pct);
            dg.Columns.Add(pf); dg.Columns.Add(pa);
            dg.Columns.Add(home); dg.Columns.Add(away);
            dg.Columns.Add(div); dg.Columns.Add(conf);
            dg.Columns.Add(strk);

            foreach (var col in dg.Columns)
                col.HeaderStyle = App.Current.Resources["DGHeaderStyle"] as Style;

            return dg;
        }

        private void CreatePlayoffGrid()
        {
            PlayoffGrid.Children.Clear();
            PlayoffGrid.ColumnDefinitions.Clear();
            PlayoffGrid.RowDefinitions.Clear();
            var games = Database.GetGames(_year).Where(g => g.Playoffs).ToList();
            
            var col = new ColumnDefinition() { Width = new GridLength(635) };
            PlayoffGrid.ColumnDefinitions.Add(col);
            var col1 = new ColumnDefinition() { Width = new GridLength(635) };
            PlayoffGrid.ColumnDefinitions.Add(col1);
            for (var j = 0; j < (games.Count() / 2) + 1; j++)
            {
                var row = new RowDefinition() { Height = new GridLength(175), };
                PlayoffGrid.RowDefinitions.Add(row);
            }

            var r = 0;
            var c = 0;
            foreach (var g in games.OrderBy(game => game.GameDate))
            {
                var grid = new Grid();
                grid.Children.Add(new GameGrid(g) { VerticalAlignment = VerticalAlignment.Center, Width = 500 });
                if(g.Conference == "Championship")
                {
                    Grid.SetRow(grid, r);
                    Grid.SetColumn(grid, 0);
                    Grid.SetColumnSpan(grid, 2);
                }
                else
                {
                    Grid.SetRow(grid, r);
                    Grid.SetColumn(grid, c++);
                }
                PlayoffGrid.Children.Add(grid);
                if (c != 2) continue;
                c = 0;
                r++;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeaguePowerPoll.xaml
    /// </summary>
    public partial class LeaguePowerPoll : UserControl
    {
        public LeaguePowerPoll()
        {
            InitializeComponent();
        }

        public LeaguePowerPoll(List<Team> t)
        {
            InitializeComponent();
            Teams = t;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private List<Team> Teams { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            HeaderTxt.Text = $"{CurrentLeague.CurrentSeason} {CurrentLeague.LeagueName} Power Rankings";
            PowerRankingsGrid.ItemsSource = Teams.OrderByDescending(t => t.PowerRanking);
        }


        private void TeamDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var team = (sender as DataGrid).SelectedItem as Team;
            if (team != null)
            {
                GameWindow.MainFrame.Navigate(new TeamPage(team));
            }
        }

    }
}

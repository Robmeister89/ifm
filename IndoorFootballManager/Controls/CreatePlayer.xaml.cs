﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for CreatePlayer.xaml
    /// </summary>
    public partial class CreatePlayer : UserControl
    {
        public CreatePlayer()
        {
            InitializeComponent();
        }

        private Player _player;
        private IFM GameWindow => Window.GetWindow(this) as IFM;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            CollegeSelect.ItemsSource = File.ReadAllLines("Resources/Colleges.txt");
            PositionSelect.ItemsSource = Positions;
        }

        private List<string> Positions
        {
            get
            {
                var positions = new List<string>()
                {
                    "QB", "RB", "WR", "OL", "DL", "LB", "DB", "K"
                };

                return positions;
            }
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            Create();

            if (_player != null)
                GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(_player));
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

        private void Create()
        {
            try
            {
                var id = Repo.Database.GetPlayers().Last().Id + 1;
                _player = new Player
                (
                    id,
                    Convert.ToInt32(JerseySelect.Value),
                    FirstNameTxt.Text,
                    LastNameTxt.Text,
                    (DateTime)AgeSelect.SelectedDate,
                    Convert.ToInt32(ExpSelect.Value),
                    PositionSelect.SelectedValue.ToString(),
                    0,
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    Convert.ToInt32(PassATxt.Value),
                    0,
                    CollegeSelect.SelectedValue.ToString(),
                    0
                );

                Repo.Database.SavePlayer(_player);
                App.ShowNotification("Player Created", $"{_player.NameWithPosition} has been created.", Notification.Wpf.NotificationType.Information, 5);
            }
            catch (Exception x)
            {
                Services.Logger.LogException(x);
                App.ShowNotification("Error", "Could not create this player.\nPlease ensure all fields are filled in.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

    }
}

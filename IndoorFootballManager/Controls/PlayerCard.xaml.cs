﻿using IndoorFootballManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Notification.Wpf;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Windows;
using IndoorFootballManager.Services;
using Notifications.Wpf.Annotations;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for PlayerCard.xaml
    /// </summary>
    public partial class PlayerCard : UserControl, INotifyPropertyChanged
    {
        public PlayerCard() {}

        public PlayerCard(Player p)
        {
            InitializeComponent();
            Player = p;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static GeneralManager UserManager => IFM.UserManager;
        private static League CurrentLeague => IFM.CurrentLeague;

        private Player _player;
        public Player Player
        {
            get => _player;
            set
            {
                _player = value;
                OnPropertyChanged(nameof(Player));
            }
        }

        private static void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            if (e.ExtraData != null)
            {
                App.NavigationParameters = e.ExtraData;
            }
        }

        private void Card_Loaded(object sender, RoutedEventArgs e)
        {
            OnPropertyChanged(nameof(Player));
            GameWindow.MainFrame.NavigationService.LoadCompleted += NavigationService_LoadCompleted;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            SetOptions();
            if (Player.TeamId > 0)
            {
                Overlay.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Player.Team.PrimaryColor));
                TeamLogo.Source = Converters.LoadImage(Player.Team.Logo);
            }
            else
            {
                TeamTxtBtn.IsEnabled = false;
                TeamTxtBtn.Foreground = new SolidColorBrush(Colors.GhostWhite);
                TeamTxtBtn.FontStyle = FontStyles.Normal;
            }

            if (!Player.IsInjured)
            {
                InjuryDiag.Visibility = Visibility.Hidden;
                InjuryDiagTxt.Visibility = Visibility.Hidden;
                InjuryDur.Visibility = Visibility.Hidden;
                InjuryDurTxt.Visibility = Visibility.Hidden;
            }

            FillRect(OverallBar, OverallBarN, Player.Overall);
            FillRect(PassaBar, PassaBarN, Player.Accuracy);
            FillRect(PasssBar, PasssBarN, Player.ArmStrength);
            FillRect(StrengthBar, StrengthBarN, Player.Strength);
            FillRect(CarryBar, CarryBarN, Player.Carry);
            FillRect(SpeedBar, SpeedBarN, Player.Speed);
            FillRect(CatchBar, CatchBarN, Player.Catching);
            FillRect(BlockBar, BlockBarN, Player.Block);
            FillRect(BlitzBar, BlitzBarN, Player.Blitz);
            FillRect(CoverBar, CoverBarN, Player.Coverage);
            FillRect(TackleBar, TackleBarN, Player.Tackle);
            FillRect(IntBar, IntBarN, Player.Intelligence);
            FillRect(KickSBar, KickSBarN, Player.KickStrength);
            FillRect(KickABar, KickABarN, Player.KickAccuracy);

            FillRect(AttitudeBar, AttitudeBarN, Player.Attitude, true);
            FillRect(GreedBar, GreedBarN, Player.Greed, true);
            FillRect(LoyaltyBar, LoyaltyBarN, Player.Loyalty, true);
            FillRect(PassionBar, PassionBarN, Player.Passion, true);
            FillRect(InjuryBar, InjuryBarN, Player.Injury, true);
            FillRect(LeadershipBar, LeadershipBarN, Player.Leadership, true);
            FillRect(WorkEthicBar, WorkEthicBarN, Player.WorkEthic, true);

            // stats
            var gameTotals = GameStats.OrderByDescending(g => g.GameDate).ToList();
            if (!gameTotals.IsNullOrEmpty())
            {
                PassingDatagrid.ItemsSource = gameTotals.Where(g => g.PassAtt > 0);
                RushingDatagrid.ItemsSource = gameTotals.Where(g => g.Carries > 0);
                ReceivingDatagrid.ItemsSource = gameTotals.Where(g => g.Receptions > 0);
                BlockingDatagrid.ItemsSource = gameTotals.Where(g => g.SacksAllowed > 0 || g.PancakeBlocks > 0);
                ReturnsDatagrid.ItemsSource = gameTotals.Where(g => g.KickReturns > 0);
                DefenseDatagrid.ItemsSource = gameTotals.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0);
                KickingDatagrid.ItemsSource = gameTotals.Where(p => p.FGAtt > 0 || p.XPAtt > 0);
            }

            // career stats
            PassingCareerDatagrid.ItemsSource = Career.Where(g => g.PassAtt > 0);
            RushingCareerDatagrid.ItemsSource = Career.Where(g => g.Carries > 0);
            ReceivingCareerDatagrid.ItemsSource = Career.Where(g => g.Receptions > 0);
            BlockingCareerDatagrid.ItemsSource = Career.Where(g => g.SacksAllowed > 0 || g.PancakeBlocks > 0);
            ReturnsCareerDatagrid.ItemsSource = Career.Where(g => g.KickReturns > 0);
            DefenseCareerDatagrid.ItemsSource = Career.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0);
            KickingCareerDatagrid.ItemsSource = Career.Where(g => g.XPAtt > 0 || g.FGAtt > 0);

            // hide empty datagrids
            foreach (var dg in SeasonGrids)
            {
                if (dg.Items.Count >= 1) continue;
                if (dg.Name.Contains("Passing"))
                    PassingSGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Rushing"))
                    RushingSGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Receiving"))
                    ReceivingSGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Block"))
                    BlockingSGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Return"))
                    ReturnsSGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Defense"))
                    DefenseSGrid.Visibility = Visibility.Collapsed;
                else
                    KickingSGrid.Visibility = Visibility.Collapsed;
            }
            foreach (var dg in GameGrids)
            {
                if (dg.Items.Count >= 1) continue;
                if (dg.Name.Contains("Passing"))
                    PassingGGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Rushing"))
                    RushingGGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Receiving"))
                    ReceivingGGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Block"))
                    BlockingGGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Return"))
                    ReturnsGGrid.Visibility = Visibility.Collapsed;
                else if (dg.Name.Contains("Defense"))
                    DefenseGGrid.Visibility = Visibility.Collapsed;
                else
                    KickingGGrid.Visibility = Visibility.Collapsed;
            }


            // history
            var history = Player.PlayerHistoryJson;
            if (history.IsNullOrEmpty()) return;
            var col = new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) };
            HistoryGrid.ColumnDefinitions.Add(col);

            var historyList = history.DeserializeToList<string>();
            var alt = true;
                
            for (var i = 0; i < historyList.Count; i++)
            {
                var row = new RowDefinition { Height = GridLength.Auto };
                HistoryGrid.RowDefinitions.Add(row);

                var text = new TextBlock
                {
                    Background = alt ? Brushes.LightGray : Brushes.GhostWhite,
                    Text = historyList[i],
                    TextAlignment = TextAlignment.Left,
                    FontSize = 20,
                    Padding = new Thickness(10,0,0,0),
                    Foreground = Brushes.Black,
                };

                Grid.SetRow(text, i);
                Grid.SetColumn(text, 0);
                HistoryGrid.Children.Add(text);

                alt = !alt;
            }
        }

        private IEnumerable<DataGrid> SeasonGrids
        {
            get
            {
                var seasonGrids = new List<DataGrid>
                {
                    PassingCareerDatagrid,
                    RushingCareerDatagrid,
                    ReceivingCareerDatagrid,
                    BlockingCareerDatagrid,
                    ReturnsCareerDatagrid,
                    DefenseCareerDatagrid,
                    KickingCareerDatagrid,
                };

                return seasonGrids.AsEnumerable();
            }
        }

        private IEnumerable<DataGrid> GameGrids
        {
            get
            {
                var gameGrids = new List<DataGrid>
                {
                    PassingDatagrid,
                    RushingDatagrid,
                    ReceivingDatagrid,
                    BlockingDatagrid,
                    ReturnsDatagrid,
                    DefenseDatagrid,
                    KickingDatagrid,
                };

                return gameGrids.AsEnumerable();
            }
        }

        private IEnumerable<PlayerGameStats> GameStats
        {
            get
            {
                var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.HasPlayed).ToList();
                var gameStats = new List<PlayerGameStats>();
                foreach(var g in games)
                {
                    if (g.StatsJson != null)
                    {
                        var stats = g.StatsJson.DeserializeToList<PlayerGameStats>();
                        gameStats.AddRange(stats.Where(p => p.PlayerId == Player.Id));
                    }
                    else
                        break;
                    
                }
                return gameStats;
            }
        }

        private IEnumerable<PlayerStats> Career
        {
            get
            {
                var careerStats = Database.GetAllPlayerCareerStats().ToList();
                var seasonStats = new List<PlayerStats>();
                foreach (var stats in careerStats.Select(s => s.StatsJson.DeserializeToList<PlayerStats>()))
                {
                    seasonStats.AddRange(stats.Where(stat => stat.PlayerId == Player.Id));
                }

                var seasons = seasonStats.GroupBy(g => g.Season).Count();
                var careerTotal = seasonStats.GroupBy(g => g.PlayerId).Select(p => new PlayerStats
                {
                    Season = seasons,
                    Team = "Career",
                    PlayerId = p.First().PlayerId,
                    PassAtt = p.Sum(c => c.PassAtt),
                    PassCmp = p.Sum(c => c.PassCmp),
                    PassYds = p.Sum(c => c.PassYds),
                    PassTds = p.Sum(c => c.PassTds),
                    PassInts = p.Sum(c => c.PassInts),
                    Pass20 = p.Sum(c => c.Pass20),
                    PassFirst = p.Sum(c => c.PassFirst),
                    PassLong = p.Max(c => c.PassLong),
                    Carries = p.Sum(c => c.Carries),
                    RushYds = p.Sum(c => c.RushYds),
                    RushTds = p.Sum(c => c.RushTds),
                    Rush20 = p.Sum(c => c.Rush20),
                    RushFirst = p.Sum(c => c.RushFirst),
                    RushLong = p.Max(c => c.RushLong),
                    Receptions = p.Sum(c => c.Receptions),
                    RecYds = p.Sum(c => c.RecYds),
                    RecTds = p.Sum(c => c.RecTds),
                    Rec20 = p.Sum(c => c.Rec20),
                    RecFirst = p.Sum(c => c.RecFirst),
                    RecLong = p.Max(c => c.RecLong),
                    Drops = p.Sum(c => c.Drops),
                    Fumbles = p.Sum(c => c.Fumbles),
                    FumblesLost = p.Sum(c => c.FumblesLost),
                    Tackles = p.Sum(c => c.Tackles),
                    Sacks = p.Sum(c => c.Sacks),
                    ForFumb = p.Sum(c => c.ForFumb),
                    FumbRec = p.Sum(c => c.FumbRec),
                    Ints = p.Sum(c => c.Ints),
                    IntYds = p.Sum(c => c.IntYds),
                    Safeties = p.Sum(c => c.Safeties),
                    DefTds = p.Sum(c => c.DefTds),
                    FGAtt = p.Sum(c => c.FGAtt),
                    FGMade = p.Sum(c => c.FGMade),
                    FGLong = p.Max(c => c.FGLong),
                    XPAtt = p.Sum(c => c.XPAtt),
                    XPMade = p.Sum(c => c.XPMade),
                    Rouges = p.Sum(c => c.Rouges),
                    KickReturns = p.Sum(c => c.KickReturns),
                    KickRetYds = p.Sum(c => c.KickRetYds),
                    KickReturnTds = p.Sum(c => c.KickReturnTds),
                    KickReturnLong = p.Max(c => c.KickReturnLong),
                    PancakeBlocks = p.Sum(c => c.PancakeBlocks),
                    SacksAllowed = p.Sum(c => c.SacksAllowed),
                }).ToList();
                seasonStats.AddRange(careerTotal);

                return seasonStats;
            }
        }

        private static void FillRect(Shape rect, TextBlock txt, int rate, bool @char = false)
        {
            txt.Text = rate.ToString();
            rect.Width = Convert.ToDouble(rate) * 2.82;
            Color start;
            Color end;
            if (@char)
            {
                start = Colors.LightBlue;
                end = Colors.GhostWhite;
                rect.Fill = new LinearGradientBrush(start, end, 0);
                return;
            }

            if (rate < 20)
            {
                start = Colors.DarkRed;
                end = Colors.Red;
            }
            else if (rate < 40)
            {
                start = Colors.OrangeRed;
                end = Colors.Orange;
            }
            else if (rate < 60)
            {
                start = Colors.Orange;
                end = Colors.Yellow;
            }
            else if (rate < 80)
            {
                start = Colors.DarkGreen;
                end = Colors.Green;
            }
            else
            {
                start = Colors.DarkBlue;
                end = Colors.Blue;
            }

            rect.Fill = new LinearGradientBrush(start, end, 0);
        }

        private void SetOptions()
        {
            SignPlayerBtn.IsEnabled = false;
            ResignPlayerBtn.IsEnabled = false;
            CutPlayerBtn.IsEnabled = false;
            TagPlayerBtn.IsEnabled = false;
            TrainPlayerBtn.IsEnabled = false;
            EditPlayerBtn.IsEnabled = false;

            if (UserManager == null) return;
            if (UserManager.IsCommissioner)
                EditPlayerBtn.IsEnabled = true;

            if (UserManager.TeamId <= 0) return;
            if (Player.TeamId == 0)
                SignPlayerBtn.IsEnabled = true;

            if (UserManager.TeamId == Player.TeamId)
            {
                if (Player.ContractYears == 1 && Player.ExtensionYears == 0)
                {
                    ResignPlayerBtn.IsEnabled = true;
                    if (!Player.Team.FranchiseTagUsed)
                        TagPlayerBtn.IsEnabled = true;
                }

                if (!Player.HasTrained)
                    TrainPlayerBtn.IsEnabled = true;
            }

            if (UserManager.TeamId != Player.TeamId && !UserManager.IsCommissioner) return;
            CutPlayerBtn.IsEnabled = true;
            switch (Player.RosterReserve)
            {
                case true when !Player.InjuredReserve:
                    AssignPlayerBtn.IsEnabled = true;
                    break;
                case false when !Player.InjuredReserve:
                    ReservePlayerBtn.IsEnabled = true;
                    break;
            }
            if (Player.IsInjured && !Player.InjuredReserve)
                IrPlayerBtn.IsEnabled = true;
        }

        private void EditPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.Navigate(new EditPlayer(Player));
        }

        private void SignPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (UserManager != null)
            {
                if (CurrentLeague.IsOffSeason)
                {
                    var offer = new OfferContract(UserManager.Team, Player).ShowDialog();
                    if (offer.HasValue && offer.Value)
                        App.ShowNotification("Contract Offered", $"An offer has been put in for {Player.NameWithPosition}.", NotificationType.Success, 5);
                }
                else
                {
                    void OfferContract()
                    {
                        PlayerData.OfferInSeasonContract(UserManager.Team, Player);
                        App.ShowNotification("Contract Offered", $"An offer has been put in for {Player.NameWithPosition}.", NotificationType.Success, 5);
                    }
                    App.ShowMessageBox("Contract Offer", "Are you sure you wish to offer a contract?", NotificationType.Notification, "NotificationArea", "Yes", OfferContract, string.Empty, null);
                }
            }
            else
                App.ShowNotification("Warning","You must be running a team for this option.", NotificationType.Error, 5);
        }

        private void ResignPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            var resign = new ResignPlayer(Player).ShowDialog();
            if (resign.HasValue && resign.Value)
                ResignPlayerBtn.IsEnabled = false;
            else
                return;
            GameWindow.MainFrame.Refresh();
        }

        private void CutPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerData.CutPlayer(Player);
            GameWindow.MainFrame.Refresh();
        }

        private void TagPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerData.FranchisePlayer(Player);
            TagPlayerBtn.IsEnabled = false;
        }

        private void TrainPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            var train = new TrainPlayer(Player).ShowDialog();
            if (train.HasValue && train.Value)
            {
                TrainPlayerBtn.IsEnabled = false;
                GameWindow.MainFrame.Refresh();
            }
            else
                return;
        }

        private void TeamTxtBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.Navigate(new TeamPage(Player.Team));
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

        private void Datagrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // Get the DataRow corresponding to the DataGridRow that is loading.
            var item = e.Row;
            if(item.GetIndex() == ((DataGrid)sender).Items.Count - 1)
            {
                e.Row.Background = new SolidColorBrush(Color.FromRgb(60, 60, 60));
            }
        }

        private void GameDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var id = ((PlayerGameStats)((DataGrid)sender)?.SelectedItem).GameId;
            var game = Database.GetGame(id);
            var view = new GameView(game);
            view.ShowDialog();
        }

        private void AssignPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            Player.ActivatePlayer();
            GameWindow.MainFrame.Refresh();
        }

        private void ReservePlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            Player.AssignToReserves();
            GameWindow.MainFrame.Refresh();
        }

        private void IrPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            Player.AssignToIr();
            GameWindow.MainFrame.Refresh();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using IndoorFootballManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using sql = System.Data.SQLite;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Annotations;
using IndoorFootballManager.Windows;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamRoster.xaml
    /// </summary>
    public partial class TeamRoster : UserControl, INotifyPropertyChanged
    {
        public TeamRoster()
        {
            InitializeComponent();
            TeamDatagrid.Columns[0].Visibility = Visibility.Hidden;
            TeamDatagrid.Columns[1].Width = 150;
            for (var i = 2; i < TeamDatagrid.Columns.Count; i++)
                TeamDatagrid.Columns[i].Width = 60;
            TeamDatagrid.Columns[TeamDatagrid.Columns.Count - 1].Width = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static Team Team => TeamPage.Team;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static GeneralManager UserManager => IFM.UserManager;
        private static Player Player { get; set; }
        public IEnumerable<Player> Players { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            SetPlayerGrid("All Players", "All Positions");
            CheckLogin(UserManager);
            if (CurrentLeague.IsOffSeason && !CurrentLeague.IsTrainingCamp)
                TrainingBtn.IsEnabled = false;
        }

        private void TeamDatagrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!(e.OriginalSource is TextBlock t)) return;
            if (!(t.DataContext is Player p)) return;
            Player = p;
            App.Player = p;
            SetContextMenu();
        }

        private void SetContextMenu()
        {
            var menu = FindResource("DatagridMenu") as ContextMenu;
            var pName = menu.Items.OfType<TextBlock>().ToArray().FirstOrDefault(m => m.Name == "PlayerNameTxt");
            var edit = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "EditPlayerBtn");
            var sign = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "SignPlayerBtn");
            var cut = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "CutPlayerBtn");
            var resign = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "ResignPlayerBtn");
            var tag = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "TagPlayerBtn");
            var train = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "TrainPlayerBtn");
            var activate = menu.Items.OfType<MenuItem>().ToArray()
                .FirstOrDefault(m => m.Name == "ActivatePlayerBtn");
            var deactivate = menu.Items.OfType<MenuItem>().ToArray()
                .FirstOrDefault(m => m.Name == "DeactivatePlayerBtn");
            var irPlayer = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "IRPlayerBtn");

            pName.Text = Player.NameWithPosition;
            edit.IsEnabled = false;
            cut.IsEnabled = false;
            train.IsEnabled = false;
            resign.IsEnabled = false;
            tag.IsEnabled = false;
            sign.IsEnabled = false;
            activate.IsEnabled = false;
            irPlayer.IsEnabled = false;
            deactivate.IsEnabled = false;

            if (UserManager.IsCommissioner)
                edit.IsEnabled = true;

            if (Player.TeamId > 0)
            {
                if (Player.TeamId == UserManager.TeamId)
                {
                    if (!Player.HasTrained && !Player.IsInjured)
                        train.IsEnabled = true;

                    if (Player.ContractYears == 1 && Player.ExtensionYears == 0)
                    {
                        resign.IsEnabled = true;
                        if (!Player.IsFranchised && !Player.Team.FranchiseTagUsed)
                            tag.IsEnabled = true;
                    }
                }

                if (Player.TeamId == UserManager.TeamId || UserManager.IsCommissioner)
                {
                    cut.IsEnabled = true;

                    if (Player.RosterReserve && !Player.InjuredReserve)
                        activate.IsEnabled = true;
                    if (Player.IsInjured && !Player.InjuredReserve)
                        irPlayer.IsEnabled = true;
                    if (!Player.RosterReserve && !Player.InjuredReserve)
                        deactivate.IsEnabled = true;
                }
            }

            if (Player.TeamId == 0)
                sign.IsEnabled = true;
        }

        private void TeamPosSortCbox_DropDownClosed(object sender, EventArgs e)
        {
            var players = (TeamRosterSortCbox.SelectedItem as ComboBoxItem)?.Content.ToString();
            var position = ((sender as ComboBox)?.SelectedItem as ComboBoxItem)?.Content.ToString();
            SetPlayerGrid(players, position);
        }

        private void TeamRosterSortCbox_DropDownClosed(object sender, EventArgs e)
        {
            var players = ((sender as ComboBox)?.SelectedItem as ComboBoxItem)?.Content.ToString();
            var position = (TeamPosSortCbox.SelectedItem as ComboBoxItem)?.Content.ToString();
            SetPlayerGrid(players, position);
        }

        private void SetPlayerGrid(string players, string position)
        {
            switch (players)
            {
                case "All Players":
                    Players = position == "All Positions"
                        ? Team.Players.OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.Players.Where(p => p.Pos == position).OrderByDescending(p => p.Overall);
                    break;
                case "Veterans":
                    Players = position == "All Positions"
                        ? Team.Players.Where(p => p.Exp > 0).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.Players.Where(p => p.Pos == position && p.Exp > 0).OrderByDescending(p => p.Overall);
                    break;
                case "Rookies":
                    Players = position == "All Positions"
                        ? Team.Players.Where(p => p.Exp == 0).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.Players.Where(p => p.Pos == position && p.Exp == 0).OrderByDescending(p => p.Overall);
                    break;
                case "Restricted Free Agents":
                    // not implemented yet
                    break;
                case "Pending Free Agents":
                    Players = position == "All Positions"
                        ? Team.Players.Where(p => p.ContractYears == 1 && p.ExtensionYears == 0).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.Players.Where(p => p.Pos == position && p.ContractYears == 1 && p.ExtensionYears == 0).OrderByDescending(p => p.Overall);
                    break;
                case "Active":
                    Players = position == "All Positions"
                        ? Team.ActiveRoster.OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.ActiveRoster.Where(p => p.Pos == position).OrderByDescending(p => p.Overall);
                    break;
                case "Reserves":
                    Players = position == "All Positions"
                        ? Team.ReservePlayers.OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.ReservePlayers.Where(p => p.Pos == position).OrderByDescending(p => p.Overall);
                    break;
                case "Injured Reserve":
                    Players = position == "All Positions"
                        ? Team.InjuredReserved.OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall)
                        : Team.InjuredReserved.Where(p => p.Pos == position).OrderByDescending(p => p.Overall);
                    break;
            }

            OnPropertyChanged(nameof(Players));
        }

        private async void TeamDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Player = (sender as DataGrid)?.SelectedItem as Player;
            if (Player == null) return;
            await App.ActivateLoading();
            GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(Player));
            await App.DeactivateLoading();
        }

        private void CheckLogin(GeneralManager user)
        {
            AdjustBtn.Visibility = Visibility.Hidden;
            if (user == null || Team == null) return;
            if ((user.TeamId == Team.Id || user.IsCommissioner) && !CurrentLeague.IsOffSeason)
                AdjustBtn.Visibility = Visibility.Visible;
        }

        private void AdjustBtn_Click(object sender, RoutedEventArgs e)
        {
            async void AutoAdjust()
            {
                await App.ActivateLoading();
                Team.FillRoster();
                GameWindow.MainFrame.Refresh();
                await App.DeactivateLoading();
            }
            void Cancel() { return; }

            App.ShowMessageBox("Auto Adjust Roster", "Are you sure you wish to auto adjust your roster?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", AutoAdjust, "No", Cancel);
        }

        private async void RosterCountBtn_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            var rosterCount = new RosterCount(Team) { Owner = GameWindow };
            await App.DeactivateLoading();
            rosterCount.ShowDialog();
        }

        private async void TrainingBtn_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            _ = (GameWindow.MainFrame.Content as TeamPage)?.TeamFrame.Navigate(new TeamTraining(Team));
            await App.DeactivateLoading();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using FluentFTP;
using System.ComponentModel;
using ookii = Ookii.Dialogs.Wpf;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeagueSettings.xaml
    /// </summary>
    public partial class LeagueSettings : UserControl
    {
        public LeagueSettings()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static SQLiteConnection Database => Repo.Database;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            NameTxt.Text = CurrentLeague.LeagueName;
            AbbrTxt.Text = CurrentLeague.Abbreviation;
            PrimClr.SelectedColor = (Color?)ColorConverter.ConvertFromString(CurrentLeague.PrimaryColor);
            SecClr.SelectedColor = (Color?)ColorConverter.ConvertFromString(CurrentLeague.SecondaryColor);
            ServerTxt.Text = CurrentLeague.FtpServer;
            WebRootTxt.Text = CurrentLeague.FtpWebRoot;
            DirectoryTxt.Text = CurrentLeague.FtpRemotePath;
            UsernameTxt.Text = CurrentLeague.FtpUsername;
            PasswordTxt.Password = CurrentLeague.FtpPassword == null ? "" : Converters.EncryptDecrypt(CurrentLeague.FtpPassword, 256);
            UseFtpChk.IsChecked = CurrentLeague.UseFTP;
            UseInjuryChk.IsChecked = CurrentLeague.UseInjuries;
            UseRetirementChk.IsChecked = CurrentLeague.UseRetirement;
            PenaltyModVal.Value = CurrentLeague.PenaltyMod;
            PlayoffTeamsCbox.Text = CurrentLeague.PlayoffTeams.ToString();
            if (!CurrentLeague.IsOffSeason)
            {
                PlayoffTeamsTxt.FontSize = 13.0;
                PlayoffTeamsTxt.IsEnabled = false;
                PlayoffTeamsTxt.FontStyle = FontStyles.Italic;
                PlayoffTeamsTxt.Foreground = Brushes.Gray;
                PlayoffTeamsTxt.Text = "Playoff Teams (Disabled During Season)";
                PlayoffTeamsCbox.IsEnabled = false;
            }
            SalaryCapTxt.Text = CurrentLeague.SalaryCap.ToString();
            MaxSalaryTxt.Text = CurrentLeague.MaximumSalary.ToString();
            MinSalaryTxt.Text = CurrentLeague.MinimumSalary.ToString();
            ContractYearsVal.Value = CurrentLeague.MaximumYears;

            var seasons = new List<int>();
            for (var i = CurrentLeague.Season; i <= CurrentLeague.CurrentSeason; i++)
            {
                seasons.Add(i);
            }
            YearSelect.ItemsSource = seasons;
            _year = seasons[0];
            if (CurrentLeague.UseFTP)
            {
                ServerTxt.IsEnabled = true;
                DirectoryTxt.IsEnabled = true;
                WebRootTxt.IsEnabled = true;
                UsernameTxt.IsEnabled = true;
                PasswordTxt.IsEnabled = true;
                TestBtn.IsEnabled = true;
            }
            else
            {
                ServerTxt.IsEnabled = false;
                DirectoryTxt.IsEnabled = false;
                WebRootTxt.IsEnabled = false;
                UsernameTxt.IsEnabled = false;
                PasswordTxt.IsEnabled = false;
                TestBtn.IsEnabled = false;
            }

        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            var oldAbbr = CurrentLeague.Abbreviation;
            CurrentLeague.LeagueName = NameTxt.Text;
            CurrentLeague.Abbreviation = AbbrTxt.Text;
            CurrentLeague.PrimaryColor = PrimClr.SelectedColorText;
            CurrentLeague.SecondaryColor = SecClr.SelectedColorText;
            CurrentLeague.FtpRemotePath = DirectoryTxt.Text;
            CurrentLeague.FtpWebRoot = WebRootTxt.Text;
            CurrentLeague.FtpUsername = UsernameTxt.Text;
            CurrentLeague.FtpPassword = Converters.EncryptDecrypt(PasswordTxt.Password, 256);
            CurrentLeague.UseFTP = (bool)UseFtpChk.IsChecked;
            CurrentLeague.UseInjuries = (bool)UseInjuryChk.IsChecked;
            CurrentLeague.UseRetirement = (bool)UseRetirementChk.IsChecked;
            CurrentLeague.PenaltyMod = (int)PenaltyModVal.Value;
            if (!CurrentLeague.IsPlayoffs)
                CurrentLeague.PlayoffTeams = Convert.ToInt32(PlayoffTeamsCbox.Text);
            CurrentLeague.SalaryCap = Convert.ToInt32(SalaryCapTxt.Text.Replace(",", "").Replace("$", "").Replace(".", ""));
            CurrentLeague.MaximumSalary = Convert.ToInt32(MaxSalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", ""));
            CurrentLeague.MinimumSalary = Convert.ToInt32(MinSalaryTxt.Text.Replace(",", "").Replace("$", "").Replace(".", ""));
            CurrentLeague.MaximumYears = (int)ContractYearsVal.Value;

            Database.SaveLeague(CurrentLeague);
            IFM.NotifyChange("LeagueNameText");

            if(oldAbbr != CurrentLeague.Abbreviation)
            {
                var oldPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{oldAbbr}";
                var oldFile = $@"{oldPath}\{oldAbbr}.ifm";

                var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}";
                var databaseFile = $@"{databasePath}\{CurrentLeague.Abbreviation}.ifm";
                
                var dir = new DirectoryInfo(databasePath);
                dir.Create();
                File.Copy(oldFile, databaseFile, true);

                Globals.LeagueAbbreviation = CurrentLeague.Abbreviation;
                ClearFolder(oldPath);
            }
            App.ShowNotification("Success", $"{CurrentLeague.Abbreviation} has been saved.", Notification.Wpf.NotificationType.Success, 5);
        }

        private void ClearFolder(string path)
        {
            var info = new DirectoryInfo(path);
            foreach (var file in info.GetFiles())
                file.Delete();
            foreach (var dir in info.GetDirectories())
            {
                foreach (var f in dir.GetFiles())
                    f.Delete();
                dir.Delete(true);
            }
            Directory.Delete(path);
        }

        private void TestBtn_Click(object sender, RoutedEventArgs e)
        {
            CurrentLeague.FtpServer = ServerTxt.Text;
            CurrentLeague.FtpRemotePath = DirectoryTxt.Text;
            CurrentLeague.FtpWebRoot = WebRootTxt.Text;
            CurrentLeague.FtpUsername = UsernameTxt.Text;
            CurrentLeague.FtpPassword = Converters.EncryptDecrypt(PasswordTxt.Password, 256);

            string[] testFile = { "This is a test upload", "League: " + CurrentLeague.LeagueName, "Team Count: " + CurrentLeague.LeagueTeams.ToString(), "Player Count: " + Database.GetPlayers().Count() };
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\Output\";
            Directory.CreateDirectory(databasePath);

            var testUpload = new FileInfo(databasePath + "TestUpload.txt");
            if (testUpload.Exists)
                testUpload.Delete();

            var file = testUpload.Name;
            var filePath = databasePath + file;
            File.WriteAllLines(filePath, testFile);
            try
            {
                UploadTestFile(filePath, file);
                App.ShowNotification("Success", "Test file uploaded successfully.", Notification.Wpf.NotificationType.Success, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                File.AppendAllText(filePath, "Test upload not successful.");
                App.ShowNotification("Error", "Please ensure your FTP info is correct.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        private static string _ftpusername;
        private static string _ftppassword;

        private void UploadTestFile(string filePath, string fileName)
        {
            var uri = CurrentLeague.FtpServer;
            var uriPath = $"{CurrentLeague.FtpRemotePath}{fileName}";
            _ftpusername = CurrentLeague.FtpUsername;
            _ftppassword = Converters.EncryptDecrypt(CurrentLeague.FtpPassword, 256);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            try
            {
                using (var ftp = new FtpClient(uri, new NetworkCredential(_ftpusername, _ftppassword)))
                {
                    ftp.Connect();
                    ftp.UploadFile(filePath, uriPath, FtpRemoteExists.Overwrite);
                }
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (GameWindow.MainFrame.CanGoBack)
                GameWindow.MainFrame.GoBack();
            else
                GameWindow.MainFrame.Navigate(null);
        }

        private int _year;

        private void GameStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            var gamesToExport = Database.GetHistoricalGameStats(_year);
            LeagueData.ExportGameStats(gamesToExport, _year);
        }

        private void SeasonStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            var stats = Database.GetAllPlayerStats(_year).First();
            if (stats != null)
                LeagueData.ExportSeasonStats(stats.StatsJson, _year);
        }

        private void YearSelect_DropDownClosed(object sender, EventArgs e)
        {
            _year = Convert.ToInt32(YearSelect.SelectedItem);
        }

        private void StandingsBtn_Click(object sender, RoutedEventArgs e)
        {
            if(_year == CurrentLeague.CurrentSeason)
            {
                var export = Database.GetTeams();
                LeagueData.ExportCurrentStandings(export);
            }
            else
            {
                var export = Database.GetHistoricalStandings(_year);
                LeagueData.ExportHistoricalStandings(export, _year);
            }
        }

        private void AbbrTxt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !char.IsLetterOrDigit(e.Text, e.Text.Length - 1);
        }

        private void AbbrTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var abbr = AbbrTxt.Text;
                AbbrTxt.Text = abbr.Substring(0, abbr.Length - 1);
                AbbrTxt.SelectionStart = abbr.Length;
            }
        }

        private void StandingsXlsxBtn_Click(object sender, RoutedEventArgs e)
        {
            if (_year == CurrentLeague.CurrentSeason)
            {
                var export = Database.GetTeamsForStandings()
                    .OrderBy(t => t.Conference)
                    .ThenBy(t => t.Division)
                    .ThenByDescending(t => t.WinPct)
                    .ThenByDescending(t => t.DivPct)
                    .ThenByDescending(t => t.ConfPct);
                LeagueData.ExportExcelStandings(export);
            }
            else
            {
                var export = Repo.Database.GetHistoricalStandings(_year)
                    .OrderBy(t => t.Conference)
                    .ThenBy(t => t.Division)
                    .ThenByDescending(t => t.WinPct)
                    .ThenByDescending(t => t.DivPct)
                    .ThenByDescending(t => t.ConfPct);
                LeagueData.ExportExcelStandings(export, _year);
            }

        }

        private void UseFtpChk_Check(object sender, RoutedEventArgs e)
        {
            if ((bool)UseFtpChk.IsChecked)
            {
                ServerTxt.IsEnabled = true;
                DirectoryTxt.IsEnabled = true;
                WebRootTxt.IsEnabled = true;
                UsernameTxt.IsEnabled = true;
                PasswordTxt.IsEnabled = true;
                TestBtn.IsEnabled = true;
            }
            else
            {
                ServerTxt.IsEnabled = false;
                DirectoryTxt.IsEnabled = false;
                WebRootTxt.IsEnabled = false;
                UsernameTxt.IsEnabled = false;
                PasswordTxt.IsEnabled = false;
                TestBtn.IsEnabled = false;
            }
        }

        private ookii.ProgressDialog _prog;

        private void ExportHTMLBtn_Click(object sender, RoutedEventArgs e)
        {
            _prog = new ookii.ProgressDialog()
            {
                WindowTitle = "Exporting HTML Reports",
                Text = "Exporting... This may take a while...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar,
                MinimizeBox = false,
            };

            _prog.DoWork += ExportHtml;
            _prog.RunWorkerCompleted += ExportHtmlCompleted;
            _prog.ShowDialog();

        }

        private void ExportHtml(object sender, DoWorkEventArgs e)
        {
            Dispatcher.Invoke(() => _prog.Text = "Exporting Index.html...");
            HtmlHelpers.ExportIndex(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting Managers.html...");
            HtmlHelpers.ExportManagersHtml(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting Teams.html...");
            HtmlHelpers.ExportTeamsHtml(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting Transactions.html...");
            HtmlHelpers.ExportTransactions(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting Standings.html...");
            HtmlHelpers.ExportStandingsHtml(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting Games.html...");
            HtmlHelpers.ExportGamesList(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Exporting All Games...");
            var games = Database.GetGames(CurrentLeague.CurrentSeason).ToList();
            games.ForEach(x => { if (x.HasPlayed) x.ExportHtml(); });
            Dispatcher.Invoke(() => _prog.Text = "Exporting Season Stats...");
            HtmlHelpers.ExportSeasonStats(CurrentLeague);
            Dispatcher.Invoke(() => _prog.Text = "Copying styles...");
            var cssPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\html\styles.css";
            HtmlHelpers.CopyCss(cssPath);
            Dispatcher.Invoke(() => _prog.Text = "Copying scripts...");
            var jqPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\html\jquery.js";
            HtmlHelpers.CopyJquery(jqPath);
            var jsPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\html\scripts.js";
            HtmlHelpers.CopyJs(jsPath);
            var stPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\html\sorttable.js";
            HtmlHelpers.CopySortTable(stPath);
        }

        private void ExportHtmlCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            App.ShowNotification("Success", "HTML Exported!", Notification.Wpf.NotificationType.Success, 5);
        }

        private void TeamsBtn_Click(object sender, RoutedEventArgs e)
        {
            var teams = Database.GetTeams();
            LeagueData.ExportTeamsList(teams);
        }

        private void ManagersBtn_Click(object sender, RoutedEventArgs e)
        {
            var managers = Database.GetManagers();
            LeagueData.ExportManagersList(managers);
        }
    }
}

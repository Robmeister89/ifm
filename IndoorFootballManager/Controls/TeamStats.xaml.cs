﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamStats.xaml
    /// </summary>
    public partial class TeamStats : UserControl
    {
        public TeamStats()
        {
            InitializeComponent();
        }

        public TeamStats(Team tm)
        {
            InitializeComponent();
            Team = tm;
        }

        private League CurrentLeague => IFM.CurrentLeague;
        private Team Team { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var teamStats = TeamStatistics.ToList();
            PassingDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.PassYds).Where(p => p.PassAtt > 0);
            RushingDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.RushYds).Where(p => p.Carries > 0);
            ReceivingDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.RecYds).Where(p => p.Receptions > 0);
            BlockingDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.PancakeBlocks).Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0);
            ReturnsDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.KickRetYds).Where(p => p.KickReturns > 0);
            DefenseDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.Tackles).Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0);
            KickingDatagrid.ItemsSource = teamStats.OrderByDescending(g => g.Points).Where(p => p.XPAtt > 0 || p.FGAtt > 0);
        }

        private IEnumerable<PlayerStats> TeamStatistics => Repo.Database.GetAllPlayerStats(CurrentLeague.CurrentSeason)
                                                               .FirstOrDefault()?.StatsJson
                                                               .DeserializeToList<PlayerStats>()
                                                               .Where(p => p.TeamId == Team.Id) ??
                                                           Array.Empty<PlayerStats>();
    }
}

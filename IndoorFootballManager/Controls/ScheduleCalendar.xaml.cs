﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using MonthCalendar;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for Calendar.xaml
    /// </summary>
    public partial class ScheduleCalendar : UserControl
    {
        private static SQLiteConnection Database => Repo.Database;
        public static ScheduleCalendar ScheduleCal = null;

        public ScheduleCalendar()
        {
            ScheduleCal = this;
            MonthCalendarControl.Today = CurrentLeague.CurDate;
            InitializeComponent();
            var i = 1;
            var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.Season == CurrentLeague.CurrentSeason).GroupBy(g => g.GameDate).ToList();
            foreach (var g in games)
            {
                var apt = new Appointment
                {
                    AppointmentID = i++,
                    StartTime = g.First().GameDate,
                    EndTime = g.First().GameDate.AddHours(3),
                    Subject = "Game Day",
                };
                _appointments.Add(apt);
            }

            RegularSeasonEndDate = _appointments.Last().StartTime.Value.AddDays(6);

            _appointments.AddRange(LeagueDates);

            SetAppointments();
            CurrentDate.Text = $"Current Date: {CurrentLeague.CurDate.ToShortDateString()}";
            var days = (RegularSeasonEndDate - CurrentLeague.CurDate).Days;
            var daysLeft = days < 0 ? 0 : days;
            DaysLeft.Text = $"Regular Season Days Remaining: {daysLeft}";
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private DateTime RegularSeasonEndDate { get; }
        
        private IEnumerable<Appointment> LeagueDates
        {
            get
            {
                var dates = new List<Appointment>
                {
                    new Appointment
                    {
                        AppointmentID = -200,
                        StartTime = CurrentLeague.DraftDate,
                        EndTime = CurrentLeague.DraftDate.AddHours(8),
                        Subject = CurrentLeague.LeagueName + " Draft"
                    },
                    new Appointment
                    {
                        AppointmentID = -196,
                        StartTime = CurrentLeague.TrainingCampDate,
                        EndTime = CurrentLeague.TrainingCampDate.AddHours(8),
                        Subject = "Training Camp Start"
                    },
                    new Appointment
                    {
                        AppointmentID = -199,
                        StartTime = CurrentLeague.InSeasonDate,
                        EndTime = CurrentLeague.InSeasonDate.AddHours(8),
                        Subject = "Regular Season Start"
                    },
                    new Appointment
                    {
                        AppointmentID = -198,
                        StartTime = CurrentLeague.SeasonEndDate,
                        EndTime = CurrentLeague.SeasonEndDate.AddHours(8),
                        Subject = $"End of Season"
                    },
                    new Appointment
                    {
                        AppointmentID = -197,
                        StartTime = CurrentLeague.NewSeasonDate,
                        EndTime = CurrentLeague.NewSeasonDate.AddHours(8),
                        Subject = "New League Year"
                    },
                    new Appointment
                    {
                        AppointmentID = -195,
                        StartTime = RegularSeasonEndDate,
                        EndTime = RegularSeasonEndDate.AddHours(8),
                        Subject = "End of Regular Season"
                    }
                };

                return dates;
            }
        }

        private void LgCalendar_DisplayMonthChanged(MonthChangedEventArgs e)
        {
            SetAppointments();
        }

        private void LgCalendar_DayBoxDoubleClicked(NewAppointmentEventArgs e)
        {
            var date = (DateTime)e.StartDate;
            var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.GameDate.Year == date.Year && g.GameDate.Month == date.Month && g.GameDate.Day == date.Day).ToList();
            if(games.Any())
                GameWindow.MainFrame.Navigate(new GameDay(games));
        }

        private void LgCalendar_AppointmentDblClicked(int appointmentId)
        {
            if(appointmentId > 0)
            {
                //Game game = Repo.GetGame(db, Appointment_Id);
                //if(game.HasPlayed)
                //{
                //    var gv = new GameView(game);
                //    gv.ShowDialog();
                //}
                //else
                //    MessageBox.Show(Repo.GetGame(db, Appointment_Id).Display, "Game Event", MessageBoxButton.OK);
            }
            else if(appointmentId == -200 && CurrentLeague.CurDate == CurrentLeague.DraftDate)
            {
                GameWindow.MainFrame.Navigate(new DraftScreen());
            }
        }

        private readonly List<Appointment> _appointments = new List<Appointment>();

        private void SetAppointments()
        {
            LgCalendar.MonthAppointments = _appointments.FindAll(new Predicate<Appointment>((Appointment apt) => apt.StartTime != null && 
                Convert.ToDateTime(apt.StartTime).Month == this.LgCalendar.DisplayStartDate.Month && 
                Convert.ToDateTime(apt.StartTime).Year == this.LgCalendar.DisplayStartDate.Year));
        }



    }
}

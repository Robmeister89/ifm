﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using IndoorFootballManager.Windows;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for GameSchedule.xaml
    /// </summary>
    public partial class GameSchedule : UserControl
    {
        public GameSchedule()
        {
            InitializeComponent();
        }

        public GameSchedule(List<Game> games, List<Team> teams)
        {
            InitializeComponent();
            _games = games;
            _teams = teams.OrderBy(t => t.TeamName).ToList();
        }

        private readonly List<Game> _games;
        private readonly List<Team> _teams;
        private League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            ScheduleDatagrid.ItemsSource = _games.OrderBy(g => g.GameDate).ThenBy(g => g.HomeTeam);
            DateSelect.Items.Add("All Dates");
            foreach(var g in _games.Select(g => g.GameDate).Distinct())
            {
                DateSelect.Items.Add(g.ToString("MMMM dd, yyyy"));
            }
            TeamSelect.Items.Add("All Teams");
            _teams.ForEach(t =>
            {
                TeamSelect.Items.Add(t.TeamName);
            });

            DateSelect.SelectedIndex = 0;
            TeamSelect.SelectedIndex = 0;
        }

        private void DateSelect_DropDownClosed(object sender, EventArgs e)
        {
            if (DateSelect.Text == "All Dates")
                ScheduleDatagrid.ItemsSource = _games.OrderBy(g => g.GameDate).ThenBy(g => g.HomeTeam);
            else
                ScheduleDatagrid.ItemsSource = _games.Where(g => g.GameDate == Convert.ToDateTime(DateSelect.Text)).OrderBy(g => g.GameDate);
        }

        private void TeamSelect_DropDownClosed(object sender, EventArgs e)
        {
            if (TeamSelect.Text == "All Teams")
                ScheduleDatagrid.ItemsSource = _games.OrderBy(g => g.GameDate).ThenBy(g => g.HomeTeam);
            else
            {
                var team = Repo.Database.GetTeamByName(TeamSelect.Text);
                if (team != null)
                    ScheduleDatagrid.ItemsSource = _games.Where(g => g.AwayTeam == team.Id || g.HomeTeam == team.Id).OrderBy(g => g.GameDate);
            }
        }

        private void ScheduleDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var game = (sender as DataGrid).SelectedItem as Game;
            if (game != null && game.HasPlayed)
            {
                var view = new GameView(game);
                view.ShowDialog();
            }
        }
    }
}

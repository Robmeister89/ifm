﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ookii = Ookii.Dialogs.Wpf;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamInfo.xaml
    /// </summary>
    public partial class TeamInfo : UserControl
    {
        public TeamInfo()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static Team Team => TeamPage.Team;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TeamCityTxt.Text = Team.City;
            TeamNicknameTxt.Text = Team.Mascot;
            TeamAbbrvTxt.Text = Team.Abbreviation;
            PrimClr.SelectedColor = (Color?)(ColorConverter.ConvertFromString(Team.PrimaryColor));
            SecClr.SelectedColor = (Color?)(ColorConverter.ConvertFromString(Team.SecondaryColor));
            Logo.Source = Converters.LoadImage(Team.Logo);
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            Team.City = TeamCityTxt.Text;
            Team.Mascot = TeamNicknameTxt.Text;
            Team.TeamName = Team.City + " " + Team.Mascot;
            Team.Abbreviation = TeamAbbrvTxt.Text;
            Team.PrimaryColor = PrimClr.SelectedColorText;
            Team.SecondaryColor = SecClr.SelectedColorText;
            Team.Logo = Converters.SaveImage(Logo.Source);

            Repo.Database.SaveTeam(Team);
            App.ShowNotification("Team Updated", "Team info has been updated.", Notification.Wpf.NotificationType.Success, 5);
            GameWindow.MainFrame.Refresh();
        }

        private void ChangeLogoBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Title = "Choose a logo",
                Filter = "Png files (*.png) | *.png",
            };
            var result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
                Logo.Source = new ImageSourceConverter().ConvertFromString(ofd.FileName) as ImageSource;
        }
    }
}

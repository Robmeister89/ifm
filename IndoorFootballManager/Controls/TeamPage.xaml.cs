﻿using IndoorFootballManager.Models;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using IndoorFootballManager.Windows;
using IndoorFootballManager.Services;
using System.Runtime.CompilerServices;
using IndoorFootballManager.Annotations;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamPage.xaml
    /// </summary>
    public partial class TeamPage : UserControl, INotifyPropertyChanged
    {
        public TeamPage(Team team)
        {
            InitializeComponent();
            TeamFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            Team = team;
            NotifyPropertyChanges();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static GeneralManager UserManager => IFM.UserManager;
        private static double Salaries => Team?.TeamSalaries ?? 0.0;
        private static double CapRoom => Team != null ? CurrentLeague.SalaryCap - Team.TeamSalaries : 0.0;

        public static Team Team { get; set; }
        public int ActiveCount => Team?.ActiveRoster.Count() ?? 0;
        public int InactiveCount => Team?.ReservePlayers.Count() ?? 0;
        public int IrCount => Team?.InjuredReserved.Count() ?? 0;
        public string SalariesText => Salaries.ToString("C0");
        public string CapRoomText => CapRoom.ToString("C0");

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (Team == null) 
                return;

            DepthChartBtn.IsEnabled = false;
            StrategyBtn.IsEnabled = false;

            TeamNameLabel.Content = Team.TeamName;
            Overlay.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));
            TeamNameLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor));
            TeamLogo.Source = Converters.LoadImage(Team.Logo);

            if (!CurrentLeague.IsOffSeason)
            {
                if (CapRoom < 0)
                {
                    SalariesTxt.Foreground = Brushes.Red;
                    CapRoomTxt.Foreground = Brushes.Red;
                }

                if (ActiveCount > CurrentLeague.MaxRoster)
                    ActiveCountTxt.Foreground = Brushes.Red;

                if (InactiveCount > CurrentLeague.ReserveRoster)
                    InactiveCountTxt.Foreground = Brushes.Red;
            }

            if (TeamFrame.Content == null)
                await NavigateToPage(new TeamHome());

            CheckLogin(UserManager);
        }

        private async void TeamHomeBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamHome());
        private async void TeamRosterBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamRoster());
        private async void DepthChartBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamDepthChart());
        private async void TeamInfoBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamInfo());
        private async void StatsBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamStats(Team));
        private async void StrategyBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamStrategy(Team));
        private async void HistoryBtn_Click(object sender, RoutedEventArgs e) => await NavigateToPage(new TeamHistory(Team));

        private async void DraftPicksBtn_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            var picks = new TeamDraftPicks(Team) { Owner = GameWindow };
            await App.DeactivateLoading();
            picks.ShowDialog();
        }

        private void CheckLogin(GeneralManager user)
        {
            if (user == null)
                return;

            if (user.IsCommissioner)
            {
                DepthChartBtn.IsEnabled = true;
                StrategyBtn.IsEnabled = true;
            }

            if (Team == null || Team.Id != user.TeamId || user.TeamId != Team.Id)
                return;

            DepthChartBtn.IsEnabled = true;
            StrategyBtn.IsEnabled = true;

        }

        private async Task NavigateToPage(UserControl control)
        {
            await App.ActivateLoading();
            try
            {
                _ = TeamFrame.Navigate(control);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
            }
            finally
            {
                await App.DeactivateLoading();
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanges()
        {
            OnPropertyChanged(nameof(Salaries));
            OnPropertyChanged(nameof(SalariesText));
            OnPropertyChanged(nameof(CapRoom));
            OnPropertyChanged(nameof(CapRoomText));
            OnPropertyChanged(nameof(ActiveCount));
            OnPropertyChanged(nameof(InactiveCount));
            OnPropertyChanged(nameof(IrCount));
        }
    }
}

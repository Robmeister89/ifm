﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for ManagerContracts.xaml
    /// </summary>
    public partial class ManagerContracts : UserControl
    {
        public ManagerContracts()
        {
            InitializeComponent();
        }

        public ManagerContracts(Team t)
        {
            InitializeComponent();
            _team = t;
        }

        private static League CurrentLeague => IFM.CurrentLeague;
        private readonly Team _team;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (_team != null)
            {
                HeaderTxt.Text = $"{_team.TeamName} Contracts";
                HeaderTxt.Foreground = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(_team.SecondaryColor)));
                LoadContracts(_team);
            }
        }

        private void LoadContracts(Team t)
        {
            var contractValues = new List<ContractValues>();
            t.Players.ToList().ForEach(p =>
            {
                var years = p.ContractYears;
                var extYears = p.ExtensionYears;
                var contract = new ContractValues()
                {
                    PlayerName = p.NameWithPosition,
                    Year1 = p.Salary.ToString("C0"),
                    Year2 = years > 1 ? p.Salary.ToString("C0") : extYears > 0 ? p.ExtensionSalary.ToString("C0") : "",
                    Year3 = years > 2 ? p.Salary.ToString("C0") : extYears > 1 ? p.ExtensionSalary.ToString("C0") : "",
                    Year4 = years > 3 ? p.Salary.ToString("C0") : extYears > 2 ? p.ExtensionSalary.ToString("C0") : "",
                    Year5 = years > 4 ? p.Salary.ToString("C0") : extYears > 3 ? p.ExtensionSalary.ToString("C0") : "",
                    Year6 = years > 5 ? p.Salary.ToString("C0") : extYears > 4 ? p.ExtensionSalary.ToString("C0") : "",
                    Year7 = years > 6 ? p.Salary.ToString("C0") : extYears > 5 ? p.ExtensionSalary.ToString("C0") : "",
                    Year8 = years > 7 ? p.Salary.ToString("C0") : extYears > 6 ? p.ExtensionSalary.ToString("C0") : "",
                    Year9 = years > 8 ? p.Salary.ToString("C0") : extYears > 7 ? p.ExtensionSalary.ToString("C0") : "",
                    Year10 = years > 9 ? p.Salary.ToString("C0") : extYears > 8 ? p.ExtensionSalary.ToString("C0") : "",
                    Year11 = extYears > 9 ? p.ExtensionSalary.ToString("C0") : ""
                };
                contractValues.Add(contract);
            });
            var sortedValues = contractValues.OrderByDescending(p => Convert.ToInt32(p.Year1.Replace("$","").Replace(",","")));
            var season = CurrentLeague.CurrentSeason;
            var max = CurrentLeague.MaximumYears;
            int i;
            for (i = 1;  i < max + 2; i++)
            {
                var child = this.FindName($"year{i}");
                (child as DataGridTextColumn).Header = season++;
            }
            if (i != 11)
            {
                for(var j = i; j < 12; j++)
                {
                    var child = this.FindName($"year{j}");
                    (child as DataGridTextColumn).Visibility = Visibility.Collapsed;
                }
            }
            ContractsList.ItemsSource = sortedValues;
        }

        private sealed class ContractValues
        {
            public string PlayerName { get; set; }
            public string Year1 { get; set; }
            public string Year2 { get; set; }
            public string Year3 { get; set; }
            public string Year4 { get; set; }
            public string Year5 { get; set; }
            public string Year6 { get; set; }
            public string Year7 { get; set; }
            public string Year8 { get; set; }
            public string Year9 { get; set; }
            public string Year10 { get; set; }
            public string Year11 { get; set; }
        }
    }
}

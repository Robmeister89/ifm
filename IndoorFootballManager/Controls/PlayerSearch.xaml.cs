﻿using IndoorFootballManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using sql = System.Data.SQLite;
using IndoorFootballManager.Windows;
using System.Runtime.CompilerServices;
using Notifications.Wpf.Annotations;
using ookii = Ookii.Dialogs.Wpf;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for PlayerSearch.xaml
    /// </summary>
    public partial class PlayerSearch : UserControl, INotifyPropertyChanged
    {
        public PlayerSearch()
        {
            InitializeComponent();
        }

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static GeneralManager UserManager => IFM.UserManager;
        private IFM GameWindow => Window.GetWindow(this) as IFM;
        public IEnumerable<Player> Players { get; set; }
        public Player Player { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.NavigationParameters != null && App.NavigationParameters is string @string)
            {
                PlayerSearchLabel.Text = @string;
                App.NavigationParameters = null;
            }
            else
            {
                PlayerSearchLabel.Text = ActivePlayersBtn.Content.ToString();
            }

            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(PlayerSearchLabel.Text, pos);

            if (!UserManager.IsCommissioner)
            {
                ImportPlayersBtn.Visibility = Visibility.Hidden;
                ExportPlayersBtn.Visibility = Visibility.Hidden;
            }

            if (CurrentLeague.CurDate != CurrentLeague.DraftDate) return;
            var draft = Database.GetDraft(CurrentLeague.CurrentSeason).ToList();
            var firstPick = !draft.Any() ? null : draft.FirstOrDefault(d => d.Pick == 1 && d.Round == 1)?.Player;
            if (firstPick != null || CurrentLeague.IsInauguralDraft)
                ImportRookiesBtn.IsEnabled = false;
        }

        private void AllPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = AllPlayersBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(AllPlayersBtn.Content.ToString(), pos);
        }

        private void ActivePlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = ActivePlayersBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(ActivePlayersBtn.Content.ToString(), pos);
        }

        private void FreeAgentsBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = FreeAgentsBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(FreeAgentsBtn.Content.ToString(), pos);
        }

        private void RookiePlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = RookiePlayersBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(RookiePlayersBtn.Content.ToString(), pos);
        }

        private void DraftPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = DraftPlayersBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(DraftPlayersBtn.Content.ToString(), pos);
        }

        private void RetiredPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerSearchLabel.Text = RetiredPlayersBtn.Content.ToString();
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(RetiredPlayersBtn.Content.ToString(), pos);
        }

        private async void PlayersDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Player = ((DataGrid)sender)?.SelectedItem as Player;
            if (Player == null) return;
            await App.ActivateLoading();
            GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(Player), PlayerSearchLabel.Text);
            await App.DeactivateLoading();
        }

        private async void PlayersDatagrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            await Task.Delay(1);
            Player = ((DataGrid)sender)?.SelectedItem as Player;
            if (Player == null) return;
            SetContextMenu(Player);
        }

        private void SetContextMenu(Player player)
        {
            Player = player;
            App.Player = player;

            var menu = FindResource("DatagridMenu") as ContextMenu;
            var pName = menu.Items.OfType<TextBlock>().ToArray().FirstOrDefault(m => m.Name == "PlayerNameTxt");
            var edit = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "EditPlayerBtn");
            var sign = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "SignPlayerBtn");
            var cut = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "CutPlayerBtn");
            var resign = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "ResignPlayerBtn");
            var tag = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "TagPlayerBtn");
            var train = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "TrainPlayerBtn");
            var activate = menu.Items.OfType<MenuItem>().ToArray()
                .FirstOrDefault(m => m.Name == "ActivatePlayerBtn");
            var deactivate = menu.Items.OfType<MenuItem>().ToArray()
                .FirstOrDefault(m => m.Name == "DeactivatePlayerBtn");
            var irPlayer = menu.Items.OfType<MenuItem>().ToArray().FirstOrDefault(m => m.Name == "IRPlayerBtn");

            pName.Text = Player.NameWithPosition;
            edit.IsEnabled = false;
            cut.IsEnabled = false;
            train.IsEnabled = false;
            resign.IsEnabled = false;
            tag.IsEnabled = false;
            sign.IsEnabled = false;
            activate.IsEnabled = false;
            irPlayer.IsEnabled = false;
            deactivate.IsEnabled = false;

            if (UserManager.IsCommissioner)
                edit.IsEnabled = true;

            if (Player.TeamId > 0)
            {
                if (Player.TeamId == UserManager.TeamId)
                {
                    if (!Player.HasTrained && !Player.IsInjured)
                        train.IsEnabled = true;

                    if (Player.ContractYears == 1 && Player.ExtensionYears == 0)
                    {
                        resign.IsEnabled = true;
                        if (!Player.IsFranchised && !Player.Team.FranchiseTagUsed)
                            tag.IsEnabled = true;
                    }
                }

                if (Player.TeamId == UserManager.TeamId || UserManager.IsCommissioner)
                {
                    cut.IsEnabled = true;

                    if (Player.RosterReserve && !Player.InjuredReserve)
                        activate.IsEnabled = true;
                    if (Player.IsInjured && !Player.InjuredReserve)
                        irPlayer.IsEnabled = true;
                    if (!Player.RosterReserve && !Player.InjuredReserve)
                        deactivate.IsEnabled = true;
                }
            }

            if (Player.TeamId == 0)
                sign.IsEnabled = true;
        }


        private void ExportPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Player> players = null;
            
            switch (PlayerSearchLabel.Text)
            {
                case "All Players":
                    players = Database.GetPlayers().OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
                case "Active Players":
                    players = Database.GetPlayers().Where(p => p.TeamId > -1).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
                case "Free Agents":
                    players = Database.GetPlayersByTeamId(0).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
                case "Rookies":
                    players = Database.GetPlayers().Where(p => p.TeamId >= 0 && p.Exp == 0).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
                case "Draft Class":
                    players = Database.GetPlayersByTeamId(-1).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
                case "Retired":
                    players = Database.GetPlayersByTeamId(-2).OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall);
                    break;
            }

            PlayerData.ExportPlayers(players, PlayerSearchLabel.Text);
        }

        private void ImportPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new ookii.VistaOpenFileDialog();
            if (ofd.ShowDialog() == true)
                PlayerData.ImportPlayers(ofd.FileName);
            else
                App.ShowNotification("Nothing happened...", "No players imported.", Notification.Wpf.NotificationType.Information, 5);
        }

        private void PlayersSearchCbox_DropDownClosed(object sender, EventArgs e)
        {
            if (PlayerSearchLabel == null) return;
            var position = ((ComboBoxItem)((ComboBox)sender)?.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(PlayerSearchLabel.Text, position);
        }

        private void FilterPlayersBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerFilter.ExpRange = ExpRange;
            PlayerFilter.ArmRange = ArmRange;
            PlayerFilter.AccRange = AccRange;
            PlayerFilter.StrRange = StrRange;
            PlayerFilter.CarRange = CarRange;
            PlayerFilter.SpdRange = SpdRange;
            PlayerFilter.CatRange = CatRange;
            PlayerFilter.BlkRange = BlkRange;
            PlayerFilter.BlzRange = BlzRange;
            PlayerFilter.CovRange = CovRange;
            PlayerFilter.TacRange = TacRange;
            PlayerFilter.IntRange = IntRange;
            PlayerFilter.KstRange = KstRange;
            PlayerFilter.KacRange = KacRange;
            PlayerFilter.AttRange = AttRange;
            PlayerFilter.GrdRange = GrdRange;
            PlayerFilter.LoyRange = LoyRange;
            PlayerFilter.InjRange = InjRange;
            PlayerFilter.LdsRange = LdsRange;
            PlayerFilter.PasRange = PasRange;
            PlayerFilter.WrkRange = WrkRange;

            var filters = new PlayerFilter { Owner = GameWindow }.ShowDialog();
            if (!filters.HasValue || !filters.Value) return;
            ExpRange = PlayerFilter.ExpRange;
            ArmRange = PlayerFilter.ArmRange;
            AccRange = PlayerFilter.AccRange;
            StrRange = PlayerFilter.StrRange;
            CarRange = PlayerFilter.CarRange;
            SpdRange = PlayerFilter.SpdRange;
            CatRange = PlayerFilter.CatRange;
            BlkRange = PlayerFilter.BlkRange;
            BlzRange = PlayerFilter.BlzRange;
            CovRange = PlayerFilter.CovRange;
            TacRange = PlayerFilter.TacRange;
            IntRange = PlayerFilter.IntRange;
            KstRange = PlayerFilter.KstRange;
            KacRange = PlayerFilter.KacRange;
            AttRange = PlayerFilter.AttRange;
            GrdRange = PlayerFilter.GrdRange;
            LoyRange = PlayerFilter.LoyRange;
            InjRange = PlayerFilter.InjRange;
            LdsRange = PlayerFilter.LdsRange;
            PasRange = PlayerFilter.PasRange;
            WrkRange = PlayerFilter.WrkRange;

            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(PlayerSearchLabel.Text, pos);
        }

        private void ResetFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            AgeRange = new[] { 20, 60 };
            ExpRange = new[] { 0, 25 };
            AccRange = new[] { 0, 100 };
            ArmRange = new[] { 0, 100 };
            StrRange = new[] { 0, 100 };
            CarRange = new[] { 0, 100 };
            SpdRange = new[] { 0, 100 };
            CatRange = new[] { 0, 100 };
            BlkRange = new[] { 0, 100 };
            BlzRange = new[] { 0, 100 };
            CovRange = new[] { 0, 100 };
            TacRange = new[] { 0, 100 };
            IntRange = new[] { 0, 100 };
            KstRange = new[] { 0, 100 };
            KacRange = new[] { 0, 100 };
            AttRange = new[] { 0, 100 };
            GrdRange = new[] { 0, 100 };
            LoyRange = new[] { 0, 100 };
            InjRange = new[] { 0, 100 };
            LdsRange = new[] { 0, 100 };
            PasRange = new[] { 0, 100 };
            WrkRange = new[] { 0, 100 };

            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(PlayerSearchLabel.Text, pos);

        }

        private void SetFilteredPlayers(string label, string position)
        {
            switch (label)
            {
                case "All Players":
                    Players = GetFilteredPlayers(position);
                    break;
                case "Active Players":
                    Players = GetFilteredPlayers(position).Where(p => p.TeamId > -1);
                    break;
                case "Free Agents":
                    Players = GetFilteredPlayers(position).Where(p => p.TeamId == 0);
                    break;
                case "Rookies":
                    Players = GetFilteredPlayers(position).Where(p => p.Exp == 0 && p.TeamId >= 0);
                    break;
                case "Draft Class":
                    Players = GetFilteredPlayers(position).Where(p => p.TeamId == -1);
                    break;
                case "Retired":
                    Players = GetFilteredPlayers(position).Where(p => p.TeamId == -2);
                    break;
            }

            OnPropertyChanged(nameof(Players));
        }

        private static IEnumerable<Player> GetFilteredPlayers(string pos)
        {
            var players = Database.GetFilteredPlayers(ExpRange, ArmRange, AccRange, StrRange, CarRange, SpdRange,
                CatRange, BlkRange, BlzRange, CovRange, TacRange, IntRange, KstRange, KacRange, AttRange, GrdRange,
                LoyRange, InjRange, LdsRange, PasRange, WrkRange);
            return pos == "All Positions" ? players.OrderByDescending(pl => pl.Overall) : players.Where(pl => pl.Pos == pos);
        }

        public static int[] AgeRange { get; set; } = { 20, 60 };
        public static int[] ExpRange { get; set; } = { 0, 25 };
        public static int[] AccRange { get; set; } = { 0, 100 };
        public static int[] ArmRange { get; set; } = { 0, 100 };
        public static int[] StrRange { get; set; } = { 0, 100 };
        public static int[] CarRange { get; set; } = { 0, 100 };
        public static int[] SpdRange { get; set; } = { 0, 100 };
        public static int[] CatRange { get; set; } = { 0, 100 };
        public static int[] BlkRange { get; set; } = { 0, 100 };
        public static int[] BlzRange { get; set; } = { 0, 100 };
        public static int[] CovRange { get; set; } = { 0, 100 };
        public static int[] TacRange { get; set; } = { 0, 100 };
        public static int[] IntRange { get; set; } = { 0, 100 };
        public static int[] KstRange { get; set; } = { 0, 100 };
        public static int[] KacRange { get; set; } = { 0, 100 };
        public static int[] AttRange { get; set; } = { 0, 100 };
        public static int[] GrdRange { get; set; } = { 0, 100 };
        public static int[] LoyRange { get; set; } = { 0, 100 };
        public static int[] InjRange { get; set; } = { 0, 100 };
        public static int[] LdsRange { get; set; } = { 0, 100 };
        public static int[] PasRange { get; set; } = { 0, 100 };
        public static int[] WrkRange { get; set; } = { 0, 100 };

        private void ImportRookiesBtn_Click(object sender, RoutedEventArgs e)
        {
            var import = new ImportRookies { Owner = GameWindow }.ShowDialog();
            if (!import.HasValue || !import.Value) return;
            var pos = ((ComboBoxItem)PlayersSearchCbox.SelectedItem)?.Content.ToString();
            SetFilteredPlayers(PlayerSearchLabel.Text, pos);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}

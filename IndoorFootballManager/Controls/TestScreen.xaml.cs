﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
//using System.Windows.Shapes;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TestScreen.xaml
    /// </summary>
    public partial class TestScreen : UserControl
    {
        public TestScreen()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            HomeTeamCbox.ItemsSource = Database.GetTeams().OrderBy(t => t.City);
            HomeTeamCbox.DisplayMemberPath = "TeamName";
            HomeTeamCbox.SelectedIndex = 0;
            AwayTeamCbox.ItemsSource = Database.GetTeams().OrderBy(t => t.City);
            AwayTeamCbox.DisplayMemberPath = "TeamName";
            AwayTeamCbox.SelectedIndex = 1;

            var seasons = Database.GetTransactionSeasons().ToList();
            seasons.Remove(CurrentLeague.CurrentSeason);
            List<string> stringSeasons;
            if (seasons.Any())
            {
                stringSeasons = seasons.ConvertAll(x => x.ToString());
                stringSeasons.Insert(0, "Select One");
            }
            else
            {
                stringSeasons = new List<string> { "-" };
            }

            TransactionsCbox.ItemsSource = stringSeasons;
            TransactionsCbox.SelectedIndex = 0;
        }

        private void HomeTeamCbox_DropDownClosed(object sender, EventArgs e)
        {
            var team = Database.GetTeamByName(HomeTeamCbox.Text);
            var contractValues = new List<ContractValues>();
            team.Players.ToList().ForEach(p =>
            {
                var years = p.ContractYears;
                var extYears = p.ExtensionYears;
                var contract = new ContractValues()
                {
                    PlayerName = p.NameWithPosition,
                    Year1 = p.Salary.ToString("C0"),
                    Year2 = years > 1 ? p.Salary.ToString("C0") : extYears > 0 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year3 = years > 2 ? p.Salary.ToString("C0") : extYears > 1 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year4 = years > 3 ? p.Salary.ToString("C0") : extYears > 2 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year5 = years > 4 ? p.Salary.ToString("C0") : extYears > 3 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year6 = years > 5 ? p.Salary.ToString("C0") : extYears > 4 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year7 = years > 6 ? p.Salary.ToString("C0") : extYears > 5 ? p.ExtensionSalary.ToString("C0") : "$-",
                    Year8 = extYears > 6 ? p.ExtensionSalary.ToString("C0") : "$-"
                };

                contractValues.Add(contract);
            });
            var sortedValues = contractValues.OrderByDescending(p => p.Year1);

            var season = CurrentLeague.CurrentSeason;
            
            var year1 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year1"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year2 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year2"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year3 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year3"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year4 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year4"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year5 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year5"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year6 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year6"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year7 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year7"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year8 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year8"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year9 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year9"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year10 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year10"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };
            var year11 = new DataGridTextColumn()
            {
                Header = season++.ToString(),
                Binding = new Binding("Year11"),
                Width = new DataGridLength(60, DataGridLengthUnitType.Star),
                FontSize = 10,
            };

            Contracts.Columns.Add(year1);
            Contracts.Columns.Add(year2);
            Contracts.Columns.Add(year3);
            Contracts.Columns.Add(year4);
            Contracts.Columns.Add(year5);
            Contracts.Columns.Add(year6);
            Contracts.Columns.Add(year7);
            Contracts.Columns.Add(year8);

            Contracts.ItemsSource = sortedValues;
        }

        private class ContractValues
        {
            public string PlayerName { get; set; }
            public string Year1 { get; set; }
            public string Year2 { get; set; }
            public string Year3 { get; set; }
            public string Year4 { get; set; }
            public string Year5 { get; set; }
            public string Year6 { get; set; }
            public string Year7 { get; set; }
            public string Year8 { get; set; }
            public string Year9 { get; set; }
            public string Year10 { get; set; }
            public string Year11 { get; set; }
        }

        private void TestGameBtn_Click(object sender, RoutedEventArgs e)
        {
            ExecuteGame();
        }

        private Team _home;
        private Team _away;

        private void ExecuteGame()
        {
            pbpBox.Text = "";
            _home = Database.GetTeamByName(HomeTeamCbox.Text);
            _away = Database.GetTeamByName(AwayTeamCbox.Text);

            try
            {
                var game = new Game(_home, _away);
                _ = game.PlayGame();
                DisplayGame(game);
            }
            catch
            {
                App.ShowNotification("Error", "Game didn't run.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        // for testing purposes... this eventually needs to pull from playerstats based on game id...
        // also needs to be pulled into a screen of its own, not a rich textbox.. lots more coding to do for game results!
        private void DisplayGame(Game game)
        {
            richTextBox1.ScrollToHome();
            pbpBox.Text = game.pbpLog;

            HomeAbbr.Text = _home.Abbreviation;
            Home1st.Text = game.homeQScore[0].ToString();
            Home2nd.Text = game.homeQScore[1].ToString();
            Home3rd.Text = game.homeQScore[2].ToString();
            Home4th.Text = game.homeQScore[3].ToString();
            var homeOt = 0;
            for (var i = 0; i < 6; i++)
                homeOt += game.homeQScore[i + 4];
            HomeOT.Text = homeOt.ToString();
            HomeFin.Text = game.homeScore.ToString();

            AwayAbbr.Text = _away.Abbreviation;
            Away1st.Text = game.awayQScore[0].ToString();
            Away2nd.Text = game.awayQScore[1].ToString();
            Away3rd.Text = game.awayQScore[2].ToString();
            Away4th.Text = game.awayQScore[3].ToString();
            var awayOt = 0;
            for (var i = 0; i < 6; i++)
                awayOt += game.awayQScore[i + 4];
            AwayOT.Text = awayOt.ToString();
            AwayFin.Text = game.awayScore.ToString();
        }

        private void GetSOSBtn_Click(object sender, RoutedEventArgs e)
        {
            var r = new Random().Next(0, CurrentLeague.LeagueTeams + 1);
            var team = Database.GetTeams().FirstOrDefault(t => t.Id == r);
            App.ShowNotification("Test SOS", $"{team.TeamName}:\n{team.SOS}", Notification.Wpf.NotificationType.Information, 5);
        }

        private void GetEvalBtn_Click(object sender, RoutedEventArgs e)
        {
            var players = Database.GetPlayers();
            var player = players.ToList()[new Random().Next(0, players.Count())];
            var eval = player.Evaluation;
            var rand = new Random().NextDouble();
            var sign = " NOT ";
            if (eval > rand)
                sign = " ";
            EvalTxt.Text = "Evaluation Test: " + player.NameWithOverall + " (OVR) -- Eval Rating: " + eval.ToString("N3") + " -- Random Double: " + rand.ToString("N3") + " -- Will" + sign + "sign";
        }

        private void GetSalaryBtn_Click(object sender, RoutedEventArgs e)
        {
            var players = Database.GetPlayers();
            var player = players.ToList()[new Random().Next(0, players.Count())];
            SalaryTxt.Text = "Salary Test: " + player.NameWithOverall + " (OVR) -- Salary: " + player.Salary.ToString("C0") + " -- Negotiated: " + player.GetNegotiatedSalary.ToString("C0");
        }

        private void GameRecordBtn_Click(object sender, RoutedEventArgs e)
        {
            var team1 = HomeTeamCbox.SelectedItem as Team;
            var team2 = AwayTeamCbox.SelectedItem as Team;
            App.ShowNotification("Test Team vs Team", $"{team1.TeamName}'s record vs {team2.TeamName}:\n{team1.VsOpponent(team2)}", Notification.Wpf.NotificationType.Information, 5);
        }


        private void AwardBtn_Click(object sender, RoutedEventArgs e)
        {
            var players = Database.GetPlayers().Where(p => p.TeamId > 0);
            var playerAwards = new Dictionary<Player, double>();
            var award = new Award();
            foreach (var p in players)
            {
                playerAwards.Add(p, award.GetPoints(CurrentLeague.CurrentSeason, p.Id));
            }
            var awards = playerAwards.OrderByDescending(a => a.Value).ToList().Take(100);
            for (var i = 0; i < awards.Count(); i++)
            {
                pbpBox.Text += $"{i + 1}. {awards.ElementAt(i).Key.NameWithPosition} - {Math.Round(awards.ElementAt(i).Value, 0)} votes\n";
            }
        }

        private void TeamStatusBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.Navigate(new ChangeFormat());
        }

        private void JsonBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var season = Convert.ToInt32(TransactionsCbox.SelectedItem);
                if (Database.GetTransactions(season).Any())
                {
                    Database.DeleteTransactions(season);
                    App.ShowNotification("Transactions Deleted",
                        $"You have delete the transactions for the {TransactionsCbox.SelectedItem} season.",
                        Notification.Wpf.NotificationType.Success, 5);
                }
                else
                {
                    App.ShowNotification("No Transactions Deleted",
                        $"There were no transactions to delete for the {TransactionsCbox.SelectedItem} season.",
                        Notification.Wpf.NotificationType.Warning, 5);
                }
            }
            catch
            {
                App.ShowNotification("Uh Oh",
                    $"Something went wrong! Try again...",
                    Notification.Wpf.NotificationType.Error, 5);
            }
        }


    }
}

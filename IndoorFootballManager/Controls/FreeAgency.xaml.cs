﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Windows;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for FreeAgency.xaml
    /// </summary>
    public partial class FreeAgency : UserControl
    {
        public FreeAgency()
        {
            InitializeComponent();
        }

        public FreeAgency(Team t)
        {
            InitializeComponent();
            Team = t;
        }

        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private Team Team { get; set; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            MainGrid.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.PrimaryColor));
            FreeAgents = new List<Player>();
            FreeAgents = Database.GetPlayersByTeamId(0).OrderByDescending(p => p.Overall).ToList();
            OffersList.ItemsSource = Database.GetContractOffers().Where(c => c.TeamId == Team.Id && c.Active);
            OffersList.DisplayMemberPath = "Display";

            SalariesTxt.Text = Team.TeamSalaries.ToString("C0");
            var capRoom = CurrentLeague.SalaryCap - Team.TeamSalaries;
            CapRoomTxt.Text = capRoom.ToString("C0");

            SelectAllBtn_Click(sender, e);
        }

        private List<Player> FreeAgents { get; set; }

        private void SelectAllBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var c in CheckBoxes)
                c.IsChecked = true;
        }

        private void UncheckAllBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var c in CheckBoxes)
                c.IsChecked = false;
        }

        private void UpdatePlayerList(object sender, RoutedEventArgs e)
        {
            var positions = CheckBoxes.Where(c => (bool)c.IsChecked).Select(c => { return c.Content.ToString(); }).ToList();
            FreeAgentsDatagrid.ItemsSource = FreeAgents.Where(p => positions.Contains(p.Pos)).OrderByDescending(p => p.Overall);
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            var positions = new List<string>();
            foreach (var chk in CheckBoxes)
            {
                if (chk.IsChecked == true)
                    positions.Add(chk.Content.ToString());
            }
            FreeAgentsDatagrid.ItemsSource = Database.GetPlayersByTeamId(0).Where(p => positions.Contains(p.Pos)).OrderByDescending(p => p.Overall);
        }

        private List<CheckBox> _checkBoxes;
        private List<CheckBox> CheckBoxes
        {
            get
            {
                _checkBoxes = new List<CheckBox>()
                {
                    QBChk, RBChk, WRChk, OLChk, DLChk, LBChk, DBChk, KChk,
                };
                return _checkBoxes;
            }
        }

        private Player _player;

        private void FreeAgentsDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _player = (sender as DataGrid).SelectedItem as Player;
            if (_player != null)
                GameWindow.MainFrame.NavigationService.Navigate(new PlayerCard(_player));
        }

        private void RosterCountBtn_Click(object sender, RoutedEventArgs e)
        {
            var rosterCount = new RosterCount(Team) { Owner = GameWindow };
            rosterCount.ShowDialog();
        }

        private void DraftPicksBtn_Click(object sender, RoutedEventArgs e)
        {
            var picks = new TeamDraftPicks(Team) { Owner = GameWindow };
            picks.ShowDialog();
        }
        private void MakeOfferBtn_Click(object sender, RoutedEventArgs e)
        {
            _player = FreeAgentsDatagrid.SelectedItem as Player;
            if (_player != null)
            {
                var offer = new OfferContract(Team, _player).ShowDialog();
                if (offer.HasValue && offer.Value)
                {
                    OffersList.ItemsSource = Database.GetContractOffers().Where(c => c.TeamId == Team.Id && c.Active);
                    OffersList.DisplayMemberPath = "Display";
                }
            }
            else
                App.ShowNotification("Error", "Please select a player to make an offer.", Notification.Wpf.NotificationType.Error, 5);
        }

        private void DeleteOfferBtn_Click(object sender, RoutedEventArgs e)
        {
            var offer = OffersList.SelectedItem as ContractOffer;
            if (offer != null)
            {
                void DeleteOffer()
                {
                    offer.Active = false;
                    Database.SaveContract(offer);
                    OffersList.ItemsSource = Database.GetContractOffers().Where(c => c.TeamId == Team.Id && c.Active);
                }

                void Cancel() { return; }

                App.ShowMessageBox("Delete Offer", "Are you sure you want to delete this offer?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", DeleteOffer, "No", Cancel);
            }
            else
                App.ShowNotification("Error", "Please select an offer to delete.", Notification.Wpf.NotificationType.Error, 5);
        }
    }
}

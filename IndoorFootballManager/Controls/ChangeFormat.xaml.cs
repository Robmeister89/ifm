﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for ChangeFormat.xaml
    /// </summary>
    public partial class ChangeFormat : UserControl
    {
        public ChangeFormat()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private List<Team> _currentTeams;
        private List<Team> _teams;
        private int _teamsPerDivision;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            _currentTeams = Database.GetTeams().ToList();
            ResetScreen();

            foreach(var c in FormatCbox.Items)
            {
                var tag = Convert.ToInt32(((ComboBoxItem)c).Tag);
                if (tag >= _currentTeams.Count)
                    ((ComboBoxItem)c).Visibility = Visibility.Visible;
            }
        }

        private void ResetScreen()
        {
            ListBoxes.ForEach(l => 
            {
                l.Items.Clear();
                if (l.Name != "TeamsListBox")
                    l.Visibility = Visibility.Hidden;
            });

            TextBoxes.ForEach(t => t.Visibility = Visibility.Hidden);

            var teams = _currentTeams.OrderBy(t => t.LastSeasonFinish).ToList();
            teams.ForEach(t =>
            {
                TeamsListBox.Items.Add(t);
            });

            _teams = AllTeams.ToList();
            _teamsPerDivision = 0;
        }

        private List<ListBox> ListBoxes
        {
            get
            {
                var listBoxes = new List<ListBox>()
                {
                    TeamsListBox,
                    Conf1Div1,
                    Conf1Div2,
                    Conf1Div3,
                    Conf1Div4,
                    Conf2Div1,
                    Conf2Div2,
                    Conf2Div3,
                    Conf2Div4
                };

                return listBoxes;
            }
        }

        private List<TextBox> TextBoxes
        {
            get
            {
                var textBoxes = new List<TextBox>()
                {
                    Conf1,
                    Div11,
                    Div12,
                    Div13,
                    Div14,
                    Conf2,
                    Div21,
                    Div22,
                    Div23,
                    Div24
                };
                return textBoxes;
            }
        }

        private object _dragSource = null;
        private void TeamsListBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var parent = (ListBox)sender;
            _dragSource = parent;
            var data = GetTeamData(_dragSource, e.GetPosition(parent));
            if (data != null)
                DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
        }

        private void TeamsListBox_Drop(object sender, DragEventArgs e)
        {
            if ((sender as ListBox).Items.Count < _teamsPerDivision)
            {
                var data = e.Data.GetData(typeof(Team));
                (_dragSource as ListBox).Items.Remove(data);
                var parent = (ListBox)sender;
                parent.Items.Add(data);
            }
        }

        private static Team GetTeamData(object source, Point point)
        {
            var element = (source as ListBox).InputHitTest(point) as UIElement;

            if (element != null)
            {
                var data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = (source as ListBox).ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;

                    if (element == source)
                        return null;
                }
                if (data != DependencyProperty.UnsetValue)
                    return data as Team;
            }
            return null;
        }

        private void FormatCbox_DropDownClosed(object sender, EventArgs e)
        {
            ResetScreen();
            if (!(sender is ComboBox box))
                return;

            var tag = Convert.ToInt32(((ComboBoxItem)box.SelectedItem).Tag);
            var teams = Database.GetTeams().ToList();
            var teamsCount = teams.Count();
            var teamsToAdd = tag - teamsCount;
            var r = new Random();
            
            for (var i = 0; i < teamsToAdd; i++)
            {
                var randIndex = r.Next(0, _teams.Count - 1);
                TeamsListBox.Items.Add(_teams[randIndex]);
                _teams.RemoveAt(randIndex);
            }

            var confs = Convert.ToInt32(box.Text[0].ToString());
            var divs = Convert.ToInt32(box.Text[9].ToString());
            _teamsPerDivision = Convert.ToInt32(box.Text[17].ToString());

            for (var i = 1; i <= confs; i++)
            {
                var conf = this.FindName($"Conf{i}");
                (conf as TextBox).Visibility = Visibility.Visible;

                for (var j = 1; j <= divs; j++)
                {
                    var listBox = this.FindName($"Conf{i}Div{j}");
                    (listBox as ListBox).Visibility = Visibility.Visible;

                    var textBox = this.FindName($"Div{i}{j}");
                    (textBox as TextBox).Visibility = Visibility.Visible;
                }
            }
        }

        private List<Team> AllTeams
        {
            get
            {
                var newTeams = new List<Team>();
                var xml = new XmlDocument();
                xml.Load(@"Resources/TeamsWLat.xml");
                var teamNodes = xml.SelectNodes("Teams/Team");
                var id = 0;
                foreach (XmlNode item in teamNodes)
                {
                    var city = item.Attributes["City"].Value;
                    var mascot = item.Attributes["Mascot"].Value;
                    var abbr = item.Attributes["Abbreviation"].Value;
                    var prim = item.Attributes["PrimaryColor"].Value;
                    var sec = item.Attributes["SecondaryColor"].Value;
                    //double _lat = Convert.ToDouble(item.Attributes["Latitude"].Value);
                    //double _long = Convert.ToDouble(item.Attributes["Longitude"].Value);

                    if (city != "")
                    {
                        var exists = _currentTeams.Any(t => t.City == city && t.Mascot == mascot);
                        if (!exists)
                        {
                            newTeams.Add(
                                new Team()
                                {
                                    Id = id++,
                                    City = city,
                                    Mascot = mascot,
                                    Abbreviation = abbr,
                                    PrimaryColor = prim,
                                    SecondaryColor = sec,
                                    TeamName = city + " " + mascot,
                                    //Location = new Coordinate(_lat, _long),
                                });
                        }
                    }
                }
                return newTeams;
            }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            var saveFormat = true;
            var teamsLeft = TeamsListBox.Items.Count;
            if (teamsLeft == 0)
            {
                var confs = Convert.ToInt32(FormatCbox.Text[0].ToString());
                var divs = Convert.ToInt32(FormatCbox.Text[9].ToString());
                var teams = Convert.ToInt32(((ComboBoxItem)FormatCbox.SelectedItem).Tag);
                for (var i = 1; i <= confs; i++)
                {
                    var confName = ((TextBox)this.FindName($"Conf{i}")).Text;
                    if (confName.Length > 0)
                    {
                        for (var j = 1; j <= divs; j++)
                        {
                            var divName = ((TextBox)this.FindName($"Div{i}{j}")).Text;
                            if (divName.Length > 0)
                            {
                                continue;
                            }
                            else
                            {
                                saveFormat = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        saveFormat = false;
                        break;
                    }
                }
                if (saveFormat)
                    SaveLeagueFormat(confs, divs, teams);
                else
                    App.ShowNotification("Error", "You must have conference and division names filled out.", Notification.Wpf.NotificationType.Error, 5);
            }
            else
                App.ShowNotification("Error", "You must include all teams in new format.", Notification.Wpf.NotificationType.Error, 5);

        }

        private void SaveLeagueFormat(int confs, int divs, int teams)
        {
            CurrentLeague.LeagueTeams = teams;
            CurrentLeague.Conferences = confs;
            CurrentLeague.Divisions = divs;
            var finish = 0;
            var div = 0;
            var id = _currentTeams.Count;
            for (var i = 1; i <= confs; i++)
            {
                var confName = ((TextBox)this.FindName($"Conf{i}")).Text;
                if (i == 1)
                    CurrentLeague.Conference1 = confName;
                else
                    CurrentLeague.Conference2 = confName;
                for (var j = 1; j <= divs; j++)
                {
                    var divName = ((TextBox)this.FindName($"Div{i}{j}")).Text;
                    CurrentLeague.GetType().GetProperty($"Division{++div}").SetValue(CurrentLeague, divName);
                    var divTeams = (ListBox)this.FindName($"Conf{i}Div{j}");
                    foreach (var t in divTeams.Items)
                    {
                        var team = t as Team;
                        team.Division = divName;
                        team.Conference = confName;
                        team.LastSeasonFinish = ++finish;

                        if (!_currentTeams.Contains(team))
                        {
                            team.Id = ++id;
                            team.IsCpu = true;
                            team.Season = CurrentLeague.CurrentSeason - 1; // for draft pick purposes put previous season
                            team.Wins = 0;
                            team.Losses = 0;
                            team.LastSeasonWins = 0;
                            team.LastSeasonLosses = 0;
                            team.LastSeasonSOS = new Random().NextDouble();
                            team.Logo = Converters.SaveFromFile(@"Images/Logos/" + team.Mascot + ".png");
                            team.OffPhilosophy = 0;
                            team.CurDate = CurrentLeague.CurDate;
                            var picks = team.DraftPicks(CurrentLeague.DraftRounds);
                            picks.AddRange(team.NewDraftPicks);
                            Database.InsertAllPicks(picks.AsEnumerable());
                            Database.SaveStrategy(BaseStrategy(team.Id));
                            Database.SaveDepthChart(new DepthChart() { TeamId = team.Id });
                            
                            team.Season = CurrentLeague.CurrentSeason; // correct season to be current
                        }
                        Database.SaveTeam(team);
                    }
                }
            }

            OffSeasonDay.DeleteAndCreateGames();
            Database.SaveLeague(CurrentLeague);

            GameWindow.AdvOneDay.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            GameWindow.MainFrame.Navigate(new ScheduleCalendar());
        }

        public Strategy BaseStrategy(int id)
        {
            var strat = new Strategy()
            {
                Id = id,
                O1st10 = 60,
                O1stShort = 40,
                O1stLong = 60,
                O2ndShort = 40,
                O2ndLong = 65,
                O3rdShort = 20,
                O3rdLong = 80,
                O4thShort = 0,
                O4thLong = 100,
                OGoalline = 40,
                D1st10 = 60,
                D1stShort = 40,
                D1stLong = 60,
                D2ndShort = 40,
                D2ndLong = 65,
                D3rdShort = 20,
                D3rdLong = 80,
                D4thShort = 0,
                D4thLong = 95,
                DGoalline = 50
            };
            return strat;
        }
    }
}

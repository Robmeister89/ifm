﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SQLite;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamHistory.xaml
    /// </summary>
    public partial class TeamHistory : UserControl
    {
        public TeamHistory()
        {
            InitializeComponent();
        }

        public TeamHistory(Team t)
        {
            InitializeComponent();
            Team = t;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        public static Team Team { get; set; }
        
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var seasons = new List<int>();
            for (var i = CurrentLeague.Season; i <= CurrentLeague.CurrentSeason; i++)
            {
                seasons.Add(i);
            }
            YearSelect.ItemsSource = seasons;

            var year = Convert.ToInt32(YearSelect.SelectedItem);
            var draft = Database.GetDraft(year).Where(d => d.CurTeamId == Team.Id);
            DraftGrid.ItemsSource = draft.OrderBy(d => d.Round).ThenBy(d => d.Round);
            ScheduleBox.ItemsSource = Database.GetGamesByTeamId(year, Team.Id);
            ScheduleBox.DisplayMemberPath = "Display";
        }

        private void YearSelect_DropDownClosed(object sender, EventArgs e)
        {
            var year = Convert.ToInt32(YearSelect.SelectedItem);
            var draft = Database.GetDraft(year).Where(d => d.CurTeamId == Team.Id);
            DraftGrid.ItemsSource = draft.OrderBy(d => d.Round).ThenBy(d => d.Round);
            ScheduleBox.ItemsSource = Database.GetGamesByTeamId(year, Team.Id);
        }

        private void DraftGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!((sender as DataGrid)?.SelectedItem is Draft pick))
                return;

            var season = pick.Season;
            var round = pick.Round;
            var draftPick = pick.Pick;

            var player = Database.GetPlayers().FirstOrDefault(p => p.DraftYear == season && p.DraftRound == round && p.DraftPick == draftPick);
            if (player != null)
                GameWindow.MainFrame.Navigate(new PlayerCard(player));
        }

    }
}

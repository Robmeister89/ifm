﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using Notification.Wpf;
using ookii = Ookii.Dialogs.Wpf;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for GameOptions.xaml
    /// </summary>
    public partial class GameOptions : UserControl
    {
        public GameOptions()
        {
            InitializeComponent();
            AlphaVersion.Text = $"Game Version: Alpha {Assembly.GetExecutingAssembly().GetName().Version}";
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var width = Properties.Settings.Default.Width;
            switch (width)
            {
                default:
                    ResolutionSelect.SelectedIndex = 0;
                    break;
                case "1632":
                    ResolutionSelect.SelectedIndex = 1;
                    break;
            }
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void ChangeBkgrBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new ookii.VistaOpenFileDialog()
            {
                Title = "Select A New Background",
                Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.png; *.bmp"
            };
            var result = ofd.ShowDialog();
            if (!result.HasValue || !result.Value) return;
            var custom = new DirectoryInfo(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Images\\");
            var newFile = new FileInfo(ofd.FileName);
            var filePath = custom.FullName + newFile.Name;
            File.Copy(ofd.FileName, filePath, true);
            Properties.Settings.Default.Background = filePath;
            Properties.Settings.Default.Save();
        }

        private void ResolutionSelect_DropDownClosed(object sender, EventArgs e)
        {
            var cbox = sender as ComboBox;
            var item = (cbox.SelectedItem as ComboBoxItem).Content.ToString();
            var width = item.Split('x')[0];
            var height = item.Split('x')[1];
            Properties.Settings.Default.Width = width;
            Properties.Settings.Default.Height = height;
            Properties.Settings.Default.Save();
            GameWindow.Height = Convert.ToDouble(Properties.Settings.Default.Height);
            GameWindow.Width = Convert.ToDouble(Properties.Settings.Default.Width);

            var screenWidth = SystemParameters.PrimaryScreenWidth;
            var screenHeight = SystemParameters.PrimaryScreenHeight;
            GameWindow.Left = (screenWidth / 2) - (GameWindow.Width / 2);
            GameWindow.Top = (screenHeight / 2) - (GameWindow.Height / 2);
            App.ShowNotification("Changes Saved", "This change will be reflected the next time you start IFM.", Notification.Wpf.NotificationType.Information, 5);
        }

        private void BackupBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var abbr = Globals.LeagueAbbreviation;
                if (abbr == null) 
                    return;

                var path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{abbr}\";
                var backupPath = $@"{path}Backup\";
                Directory.CreateDirectory(backupPath);

                var liveFile = $"{path}{abbr}.ifm";
                var backupFile = $"{backupPath}{abbr}.ifm";
                File.Copy(liveFile, backupFile, true);
                App.ShowNotification("Success", "League file backed up.", NotificationType.Success, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Something went wrong.\nPlease check the log file in your IFM folder.", NotificationType.Error, 5);
            }
        }

        private ookii.ProgressDialog _prog;
        private bool _updated;
        private bool _exceptionCaught;

        private void FixStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            _prog = new ookii.ProgressDialog
            {
                WindowTitle = @"Updating Statistics",
                Text = @"Updating...",
                ShowTimeRemaining = false,
                ShowCancelButton = false,
                ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar,
                MinimizeBox = false,
            };

            _prog.DoWork += UpdateStats;
            _prog.RunWorkerCompleted += SimCompleted;
            _prog.ShowDialog();
        }

        private void SimCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _prog.Dispose();
            if (_updated)
            {
                App.ShowNotification("Stats have been fixed.", "Sorry for any inconvenience.", NotificationType.Success, 5);
            }
            else if (_exceptionCaught)
            {
                App.ShowNotification("Error", "Something wrong happened.\nPlease check log file in the Indoor Football Manager folder.", NotificationType.Error, 5);
            }
            else
            {
                App.ShowNotification("Error", "You must have a league loaded to fix stats.", NotificationType.Error, 5);
            }
        }

        private void UpdateStats(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (Globals.LeagueAbbreviation == null) 
                    return;

                var allSeasonStats = Repo.Database.GetAllPlayerCareerStats();
                foreach (var seasonStats in allSeasonStats)
                {
                    var playerStats = new List<PlayerStats>();
                    var playerPlayoffStats = new List<PlayerPlayoffStats>();

                    var updatedSeasonStats = new PlayerStats { Id = seasonStats.Id, Season = seasonStats.Season };
                    var updatedPlayoffStats = new PlayerPlayoffStats
                        { Id = seasonStats.Id, Season = seasonStats.Season };
                    var games = Repo.Database.GetGames(seasonStats.Season).ToList();

                    foreach (var game in games)
                    {
                        if (game.StatsJson == null) continue;
                        var gameStats = game.StatsJson.DeserializeToList<PlayerGameStats>();

                        foreach (var stat in gameStats)
                        {
                            var team = Repo.Database.GetTeamByAbbreviation(stat.Team);
                            if (team != null)
                            {
                                stat.TeamId = team.Id;
                            }
                            else
                            {
                                throw new Exception();
                            }

                            var player = Repo.Database.GetPlayerById(stat.PlayerId);
                            if (player != null)
                            {
                                stat.PlayerName = player.Name;
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }

                        game.StatsJson = gameStats.SerializeToJson();
                        Repo.Database.SaveGame(game);

                        if (!game.Playoffs)
                        {
                            foreach (var p in gameStats)
                            {
                                var ps = playerStats.FirstOrDefault(s =>
                                    s.PlayerId == p.PlayerId && s.Team == p.Team);
                                if (ps != null)
                                {
                                    ps.Season = p.Season;
                                    ps.Team = p.Team;
                                    ps.TeamId = p.TeamId;
                                    ps.PlayerName = p.PlayerName;
                                    ps.PlayerId = p.PlayerId;
                                    ps.PassAtt += p.PassAtt;
                                    ps.PassCmp += p.PassCmp;
                                    ps.PassYds += p.PassYds;
                                    ps.PassTds += p.PassTds;
                                    ps.PassInts += p.PassInts;
                                    ps.Pass20 += p.Pass20;
                                    ps.PassFirst += p.PassFirst;
                                    if (ps.PassLong < p.PassLong)
                                    {
                                        ps.PassLong = p.PassLong;
                                    }

                                    ps.Carries += p.Carries;
                                    ps.RushYds += p.RushYds;
                                    ps.RushTds += p.RushTds;
                                    ps.Rush20 += p.Rush20;
                                    ps.RushFirst += p.RushFirst;
                                    if (ps.RushLong < p.RushLong)
                                    {
                                        ps.RushLong = p.RushLong;
                                    }

                                    ps.Receptions += p.Receptions;
                                    ps.RecYds += p.RecYds;
                                    ps.RecTds += p.RecTds;
                                    ps.Rec20 += p.Rec20;
                                    ps.RecFirst += p.RecFirst;
                                    if (ps.RecLong < p.RecLong)
                                    {
                                        ps.RecLong = p.RecLong;
                                    }

                                    ps.Drops += p.Drops;
                                    ps.Fumbles += p.Fumbles;
                                    ps.FumblesLost += p.FumblesLost;
                                    ps.Tackles += p.Tackles;
                                    ps.Sacks += p.Sacks;
                                    ps.ForFumb += p.ForFumb;
                                    ps.FumbRec += p.FumbRec;
                                    ps.Ints += p.Ints;
                                    ps.IntYds += p.IntYds;
                                    ps.Safeties += p.Safeties;
                                    ps.DefTds += p.DefTds;
                                    ps.FGAtt += p.FGAtt;
                                    ps.FGMade += p.FGMade;
                                    if (ps.FGLong < p.FGLong)
                                    {
                                        ps.FGLong = p.FGLong;
                                    }

                                    ps.XPAtt += p.XPAtt;
                                    ps.XPMade += p.XPMade;
                                    ps.Rouges += p.Rouges;
                                    ps.KickReturns += p.KickReturns;
                                    ps.KickRetYds += p.KickRetYds;
                                    ps.KickReturnTds += p.KickReturnTds;
                                    if (ps.KickReturnLong < p.KickReturnLong)
                                    {
                                        ps.KickReturnLong = p.KickReturnLong;
                                    }

                                    ps.PancakeBlocks += p.PancakeBlocks;
                                    ps.SacksAllowed += p.SacksAllowed;
                                }
                                else
                                {
                                    ps = new PlayerStats
                                    {
                                        Season = p.Season,
                                        Team = p.Team,
                                        TeamId = p.TeamId,
                                        PlayerName = p.PlayerName,
                                        PlayerId = p.PlayerId,
                                        PassAtt = p.PassAtt,
                                        PassCmp = p.PassCmp,
                                        PassYds = p.PassYds,
                                        PassTds = p.PassTds,
                                        PassInts = p.PassInts,
                                        Pass20 = p.Pass20,
                                        PassFirst = p.PassFirst,
                                        PassLong = p.PassLong,
                                        Carries = p.Carries,
                                        RushYds = p.RushYds,
                                        RushTds = p.RushTds,
                                        Rush20 = p.Rush20,
                                        RushFirst = p.RushFirst,
                                        RushLong = p.RushLong,
                                        Receptions = p.Receptions,
                                        RecYds = p.RecYds,
                                        RecTds = p.RecTds,
                                        Rec20 = p.Rec20,
                                        RecFirst = p.RecFirst,
                                        RecLong = p.RecLong,
                                        Drops = p.Drops,
                                        Fumbles = p.Fumbles,
                                        FumblesLost = p.FumblesLost,
                                        Tackles = p.Tackles,
                                        Sacks = p.Sacks,
                                        ForFumb = p.ForFumb,
                                        FumbRec = p.FumbRec,
                                        Ints = p.Ints,
                                        IntYds = p.IntYds,
                                        Safeties = p.Safeties,
                                        DefTds = p.DefTds,
                                        FGAtt = p.FGAtt,
                                        FGMade = p.FGMade,
                                        FGLong = p.FGLong,
                                        XPAtt = p.XPAtt,
                                        XPMade = p.XPMade,
                                        Rouges = p.Rouges,
                                        KickReturns = p.KickReturns,
                                        KickRetYds = p.KickRetYds,
                                        KickReturnTds = p.KickReturnTds,
                                        KickReturnLong = p.KickReturnLong,
                                        PancakeBlocks = p.PancakeBlocks,
                                        SacksAllowed = p.SacksAllowed,
                                    };
                                    playerStats.Add(ps);
                                }
                            }
                        }
                        else
                        {
                            foreach (var p in gameStats)
                            {
                                var ps = playerPlayoffStats.FirstOrDefault(s =>
                                    s.PlayerId == p.PlayerId && s.Team == p.Team);
                                if (ps != null)
                                {
                                    ps.Season = p.Season;
                                    ps.Team = p.Team;
                                    ps.TeamId = p.TeamId;
                                    ps.PlayerName = p.PlayerName;
                                    ps.PlayerId = p.PlayerId;
                                    ps.PassAtt += p.PassAtt;
                                    ps.PassCmp += p.PassCmp;
                                    ps.PassYds += p.PassYds;
                                    ps.PassTds += p.PassTds;
                                    ps.PassInts += p.PassInts;
                                    ps.Pass20 += p.Pass20;
                                    ps.PassFirst += p.PassFirst;
                                    if (ps.PassLong < p.PassLong)
                                    {
                                        ps.PassLong = p.PassLong;
                                    }

                                    ps.Carries += p.Carries;
                                    ps.RushYds += p.RushYds;
                                    ps.RushTds += p.RushTds;
                                    ps.Rush20 += p.Rush20;
                                    ps.RushFirst += p.RushFirst;
                                    if (ps.RushLong < p.RushLong)
                                    {
                                        ps.RushLong = p.RushLong;
                                    }

                                    ps.Receptions += p.Receptions;
                                    ps.RecYds += p.RecYds;
                                    ps.RecTds += p.RecTds;
                                    ps.Rec20 += p.Rec20;
                                    ps.RecFirst += p.RecFirst;
                                    if (ps.RecLong < p.RecLong)
                                    {
                                        ps.RecLong = p.RecLong;
                                    }

                                    ps.Drops += p.Drops;
                                    ps.Fumbles += p.Fumbles;
                                    ps.FumblesLost += p.FumblesLost;
                                    ps.Tackles += p.Tackles;
                                    ps.Sacks += p.Sacks;
                                    ps.ForFumb += p.ForFumb;
                                    ps.FumbRec += p.FumbRec;
                                    ps.Ints += p.Ints;
                                    ps.IntYds += p.IntYds;
                                    ps.Safeties += p.Safeties;
                                    ps.DefTds += p.DefTds;
                                    ps.FGAtt += p.FGAtt;
                                    ps.FGMade += p.FGMade;
                                    if (ps.FGLong < p.FGLong)
                                    {
                                        ps.FGLong = p.FGLong;
                                    }

                                    ps.XPAtt += p.XPAtt;
                                    ps.XPMade += p.XPMade;
                                    ps.Rouges += p.Rouges;
                                    ps.KickReturns += p.KickReturns;
                                    ps.KickRetYds += p.KickRetYds;
                                    ps.KickReturnTds += p.KickReturnTds;
                                    if (ps.KickReturnLong < p.KickReturnLong)
                                    {
                                        ps.KickReturnLong = p.KickReturnLong;
                                    }

                                    ps.PancakeBlocks += p.PancakeBlocks;
                                    ps.SacksAllowed += p.SacksAllowed;
                                }
                                else
                                {
                                    ps = new PlayerPlayoffStats
                                    {
                                        Season = p.Season,
                                        Team = p.Team,
                                        TeamId = p.TeamId,
                                        PlayerName = p.PlayerName,
                                        PlayerId = p.PlayerId,
                                        PassAtt = p.PassAtt,
                                        PassCmp = p.PassCmp,
                                        PassYds = p.PassYds,
                                        PassTds = p.PassTds,
                                        PassInts = p.PassInts,
                                        Pass20 = p.Pass20,
                                        PassFirst = p.PassFirst,
                                        PassLong = p.PassLong,
                                        Carries = p.Carries,
                                        RushYds = p.RushYds,
                                        RushTds = p.RushTds,
                                        Rush20 = p.Rush20,
                                        RushFirst = p.RushFirst,
                                        RushLong = p.RushLong,
                                        Receptions = p.Receptions,
                                        RecYds = p.RecYds,
                                        RecTds = p.RecTds,
                                        Rec20 = p.Rec20,
                                        RecFirst = p.RecFirst,
                                        RecLong = p.RecLong,
                                        Drops = p.Drops,
                                        Fumbles = p.Fumbles,
                                        FumblesLost = p.FumblesLost,
                                        Tackles = p.Tackles,
                                        Sacks = p.Sacks,
                                        ForFumb = p.ForFumb,
                                        FumbRec = p.FumbRec,
                                        Ints = p.Ints,
                                        IntYds = p.IntYds,
                                        Safeties = p.Safeties,
                                        DefTds = p.DefTds,
                                        FGAtt = p.FGAtt,
                                        FGMade = p.FGMade,
                                        FGLong = p.FGLong,
                                        XPAtt = p.XPAtt,
                                        XPMade = p.XPMade,
                                        Rouges = p.Rouges,
                                        KickReturns = p.KickReturns,
                                        KickRetYds = p.KickRetYds,
                                        KickReturnTds = p.KickReturnTds,
                                        KickReturnLong = p.KickReturnLong,
                                        PancakeBlocks = p.PancakeBlocks,
                                        SacksAllowed = p.SacksAllowed,
                                    };
                                    playerPlayoffStats.Add(ps);
                                }
                            }
                        }
                    }

                    updatedSeasonStats.StatsJson = playerStats.SerializeToJson();
                    Repo.Database.SavePlayerStats(updatedSeasonStats);

                    updatedPlayoffStats.StatsJson = playerPlayoffStats.SerializeToJson();
                    Repo.Database.SavePlayerPlayoffStats(updatedPlayoffStats);
                }

                _updated = true;
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                _exceptionCaught = true;
            }
        }
    }
}

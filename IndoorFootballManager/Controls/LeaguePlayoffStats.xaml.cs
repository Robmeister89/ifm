﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeaguePlayoffStats.xaml
    /// </summary>
    public partial class LeaguePlayoffStats : UserControl
    {
        public LeaguePlayoffStats()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //var db = Repo.Connect(NewLeague.Abbreviation);

            HeaderTxt.Text = CurrentLeague.LeagueName + " Playoff Stats";

            //IEnumerable<PlayerPlayoffStats> LeagueStats = Repo.GetAllPlayerPlayoffStats(db, NewLeague.CurrentSeason);
            if(LeagueStats != null)
            {
                PassingDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.PassYds).Where(p => p.PassAtt > 0);
                RushingDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.RushYds).Where(p => p.Carries > 0);
                ReceivingDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.RecYds).Where(p => p.Receptions > 0);
                BlockingDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.PancakeBlocks).Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0);
                ReturnsDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.KickRetYds).Where(p => p.KickReturns > 0);
                DefenseDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.Tackles).Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0);
                KickingDatagrid.ItemsSource = LeagueStats.OrderByDescending(g => g.Points).Where(p => p.XPAtt > 0 || p.FGAtt > 0);
            }
            //db.Disconnect();
        }

        private List<PlayerPlayoffStats> LeagueStats
        {
            get
            {
                var seasonStats = Database.GetAllPlayerPlayoffStats(CurrentLeague.CurrentSeason).FirstOrDefault();
                var seasonStatsNew = new List<PlayerPlayoffStats>();
                if(seasonStats != null)
                    seasonStatsNew = seasonStats.StatsJson.DeserializeToList<PlayerPlayoffStats>();
                return seasonStatsNew;
            }
        }

        private void Datagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var id = ((sender as DataGrid).SelectedItem as PlayerPlayoffStats).PlayerId;
            var player = Database.GetPlayerById(id);
            GameWindow.MainFrame.Navigate(new PlayerCard(player));
        }

        private void SeasonStatsBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.Navigate(new LeagueStats());
        }


    }
}

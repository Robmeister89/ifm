﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamTraining.xaml
    /// </summary>
    public partial class TeamTraining : UserControl
    {
        public TeamTraining()
        {
            InitializeComponent();
        }

        public TeamTraining(Team t)
        {
            InitializeComponent();
            Team = t;
            NoteTxt.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor));
            new List<TextBlock>(TrainingGrid.Children.OfType<TextBlock>())
                .ForEach(txt => txt.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Team.SecondaryColor)));
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private Team Team { get; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TeamDatagrid.Items.Clear();
            ListBoxes.ForEach(l => l.Items.Clear());
            _teamPlayers = new List<Player>();
            _teamPlayers.AddRange(Team.Players.OrderBy(p => p.PositionSort).ThenByDescending(p => p.Overall));
            for (var i = 0; i < _teamPlayers.Count(); i++)
            {
                if (!_teamPlayers[i].HasTrained && !_teamPlayers[i].IsInjured)
                    TeamDatagrid.Items.Add(_teamPlayers[i]);
            }
        }

        private List<Player> _teamPlayers;
        private List<Player> _playersTraining;
        private List<string> _trainingResults;

        private List<ListBox> ListBoxes
        {
            get
            {
                var listBoxes = new List<ListBox>()
                {
                    strBox, armBox, accBox, carBox, spdBox, catBox, intBox, blkBox, blzBox, covBox, tckBox, kpwBox, kacBox,
                };
                return listBoxes;
            }
        }

        private void TrainBtn_Click(object sender, RoutedEventArgs e)
        {
            _playersTraining = new List<Player>();
            _trainingResults = new List<string>();

            new List<Player>(accBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Pass Accuracy"));
            new List<Player>(armBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Pass Strength"));
            new List<Player>(carBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Carry"));
            new List<Player>(spdBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Speed"));
            new List<Player>(catBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Catching"));
            new List<Player>(blkBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Blocking"));
            new List<Player>(blzBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Blitzing"));
            new List<Player>(kacBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Kick Accuracy"));
            new List<Player>(kpwBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Kick Strength"));
            new List<Player>(covBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Coverage"));
            new List<Player>(tckBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Tackling"));
            new List<Player>(strBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Strength"));
            new List<Player>(intBox.Items.OfType<Player>()).ForEach(p => TrainPlayer(p, "Intelligence"));

            Repo.Database.UpdateAllPlayers(_playersTraining.AsEnumerable());
            SendEmail.TrainingResults(Team, _trainingResults);
            IFM.NotifyChange("EmailsCount");
            GameWindow.MainFrame.Refresh();
        }

        private void TrainPlayer(Player player, string attribute)
        {
            int prev = 0, rating = 0;
            switch (attribute)
            {
                case "Accuracy":
                case "Pass Accuracy":
                    prev = player.Accuracy;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Accuracy = player.TrainingCamp(player.Accuracy, player.WorkEthic);
                    else
                        player.Accuracy = player.WeeklyTraining(player.Accuracy, player.WorkEthic);
                    rating = player.Accuracy;
                    break;
                case "ArmStrength":
                case "Pass Strength":
                    prev = player.ArmStrength;
                    if (CurrentLeague.IsTrainingCamp)
                        player.ArmStrength = player.TrainingCamp(player.ArmStrength, player.WorkEthic);
                    else
                        player.ArmStrength = player.WeeklyTraining(player.ArmStrength, player.WorkEthic);
                    rating = player.ArmStrength;
                    break;
                case "Carry":
                    prev = player.Carry;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Carry = player.TrainingCamp(player.Carry, player.WorkEthic);
                    else
                        player.Carry = player.WeeklyTraining(player.Carry, player.WorkEthic);
                    rating = player.Carry;
                    break;
                case "Speed":
                    prev = player.Speed;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Speed = player.TrainingCamp(player.Speed, player.WorkEthic);
                    else
                        player.Speed = player.WeeklyTraining(player.Speed, player.WorkEthic);
                    rating = player.Speed;
                    break;
                case "Catching":
                    prev = player.Catching;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Catching = player.TrainingCamp(player.Catching, player.WorkEthic);
                    else
                        player.Catching = player.WeeklyTraining(player.Catching, player.WorkEthic);
                    rating = player.Catching;
                    break;
                case "Block":
                case "Blocking":
                    prev = player.Block;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Block = player.TrainingCamp(player.Block, player.WorkEthic);
                    else
                        player.Block = player.WeeklyTraining(player.Block, player.WorkEthic);
                    rating = player.Block;
                    break;
                case "Blitz":
                case "Blitzing":
                    prev = player.Blitz;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Blitz = player.TrainingCamp(player.Blitz, player.WorkEthic);
                    else
                        player.Blitz = player.WeeklyTraining(player.Blitz, player.WorkEthic);
                    rating = player.Blitz;
                    break;
                case "KickAccuracy":
                case "Kick Accuracy":
                    prev = player.KickAccuracy;
                    if (CurrentLeague.IsTrainingCamp)
                        player.KickAccuracy = player.TrainingCamp(player.KickAccuracy, player.WorkEthic);
                    else
                        player.KickAccuracy = player.WeeklyTraining(player.KickAccuracy, player.WorkEthic);
                    rating = player.KickAccuracy;
                    break;
                case "KickStrength":
                case "Kick Strength":
                    prev = player.KickStrength;
                    if (CurrentLeague.IsTrainingCamp)
                        player.KickStrength = player.TrainingCamp(player.KickStrength, player.WorkEthic);
                    else
                        player.KickStrength = player.WeeklyTraining(player.KickStrength, player.WorkEthic);
                    rating = player.KickStrength;
                    break;
                case "Coverage":
                    prev = player.Coverage;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Coverage = player.TrainingCamp(player.Coverage, player.WorkEthic);
                    else
                        player.Coverage = player.WeeklyTraining(player.Coverage, player.WorkEthic);
                    rating = player.Coverage;
                    break;
                case "Tackle":
                case "Tackling":
                    prev = player.Tackle;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Tackle = player.TrainingCamp(player.Tackle, player.WorkEthic);
                    else
                        player.Tackle = player.WeeklyTraining(player.Tackle, player.WorkEthic);
                    rating = player.Tackle;
                    break;
                case "Strength":
                    prev = player.Strength;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Strength = player.TrainingCamp(player.Strength, player.WorkEthic);
                    else
                        player.Strength = player.WeeklyTraining(player.Strength, player.WorkEthic);
                    rating = player.Strength;
                    break;
                case "Intelligence":
                    prev = player.Intelligence;
                    if (CurrentLeague.IsTrainingCamp)
                        player.Intelligence = player.TrainingCamp(player.Intelligence, player.WorkEthic);
                    else
                        player.Intelligence = player.WeeklyTraining(player.Intelligence, player.WorkEthic);
                    rating = player.Intelligence;
                    break;
            }
            player.Overall = player.GetOverall;
            player.HasTrained = true;
            
            if (rating > prev)
            {
                var playerHistory = player.PlayerHistoryJson == null ? new List<string>() : player.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add($"{attribute} improved from {prev} to {rating} (+{rating - prev}) on {CurrentLeague.CurDate.ToLongDateString()}.");
                player.PlayerHistoryJson = playerHistory.SerializeToJson();
            }

            _playersTraining.Add(player);
            if (prev == rating)
                _trainingResults.Add($"{player.NameWithPosition}'s {attribute} remained the same at {prev}");
            else
                _trainingResults.Add($"{player.NameWithPosition}'s {attribute} increased from {prev} to {rating}");
        }

        private object _dragSource = null;
        private void listBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var parent = (ListBox)sender;
            _dragSource = parent;
            var data = GetPlayerData(_dragSource, e.GetPosition(parent));
            if (data != null)
                DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
        }

        private static Player GetPlayerData(object source, Point point)
        {
            UIElement element = null;
            if (source is ListBox)
                element = (source as ListBox).InputHitTest(point) as UIElement;
            else
                element = (source as DataGrid).InputHitTest(point) as UIElement;
            if (element != null)
            {
                var data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    if (source is ListBox)
                        data = (source as ListBox).ItemContainerGenerator.ItemFromContainer(element);
                    else
                        data = (source as DataGrid).ItemContainerGenerator.ItemFromContainer(element);

                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;

                    if (element == source)
                        return null;
                }
                if (data != DependencyProperty.UnsetValue)
                    return data as Player;
            }
            return null;
        }

        private void listBox_Drop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(typeof(Player));
            if (_dragSource is ListBox)
                (_dragSource as ListBox).Items.Remove(data);
            else
                (_dragSource as DataGrid).Items.Remove(data);
            if(sender is ListBox)
            {
                var parent = (ListBox)sender;
                parent.Items.Add(data);
            }
            else
            {
                var parent = (DataGrid)sender;
                parent.Items.Add(data);
            }
        }

        private void TeamDatagrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var parent = (DataGrid)sender;
            _dragSource = parent;
            var data = GetPlayerData(_dragSource, e.GetPosition(parent));
            if (data != null)
                DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
        }

        private void AutoTrainBtn_Click(object sender, RoutedEventArgs e)
        {
            _playersTraining = new List<Player>();
            _trainingResults = new List<string>();

            var trainingPlayers = TeamDatagrid.Items.Cast<Player>().ToList();

            foreach (var p in trainingPlayers)
                TrainPlayer(p, p.TrainingAttribute);

            if (_playersTraining.Count > 0)
            {
                Repo.Database.UpdateAllPlayers(_playersTraining.AsEnumerable());
                SendEmail.TrainingResults(Team, _trainingResults);
                IFM.NotifyChange("EmailsCount");
            }

            GameWindow.MainFrame.Refresh();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for GameDay.xaml
    /// </summary>
    public partial class GameDay : UserControl
    {
        public GameDay()
        {
            InitializeComponent();
        }

        public GameDay(List<Game> games)
        {
            InitializeComponent();
            ListOfGames = games;
        }

        private List<Game> ListOfGames { get; }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            CreateGameDayGrid(ListOfGames);
        }

        private void CreateGameDayGrid(IReadOnlyCollection<Game> games)
        {
            var rows = Math.Round(games.Count / 2.0, MidpointRounding.AwayFromZero);
            var col = new ColumnDefinition { Width = new GridLength(635) };
            GameDayGrid.ColumnDefinitions.Add(col);
            var col1 = new ColumnDefinition { Width = new GridLength(635) };
            GameDayGrid.ColumnDefinitions.Add(col1);
            for (var j = 0; j < rows; j++)
            {
                var row = new RowDefinition() { Height = new GridLength(175), };
                GameDayGrid.RowDefinitions.Add(row);
            }

            var r = 0;
            var c = 0;
            foreach (var g in games.OrderBy(game => game.GameDate))
            {
                var grid = new Grid();
                grid.Children.Add(new GameGrid(g) { VerticalAlignment = VerticalAlignment.Center, Width = 500 });
                if (g.Conference == "Championship")
                {
                    Grid.SetRow(grid, r);
                    Grid.SetColumn(grid, 0);
                    Grid.SetColumnSpan(grid, 2);
                }
                else
                {
                    Grid.SetRow(grid, r);
                    Grid.SetColumn(grid, c++);
                }
                GameDayGrid.Children.Add(grid);
                if (c != 2) continue;
                c = 0;
                r++;
            }
        }
    }
}

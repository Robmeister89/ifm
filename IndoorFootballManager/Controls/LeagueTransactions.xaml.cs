﻿using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using SQLite;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for LeagueTransactions.xaml
    /// </summary>
    public partial class LeagueTransactions : UserControl
    {
        public LeagueTransactions()
        {
            InitializeComponent();
        }

        private static SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            HeaderTxt.Text = $"{CurrentLeague.LeagueName} {CurrentLeague.CurrentSeason} Transactions";
            TransactionsGrid.ItemsSource = Database.GetTransactions(CurrentLeague.CurrentSeason).OrderByDescending(t => t.Id);

            TeamsCbox.Items.Add("Entire League");
            var teams = Database.GetTeams().OrderBy(t => t.City).ThenBy(t => t.Mascot).ToList();
            foreach (var t in teams)
            {
                TeamsCbox.Items.Add(t.TeamName);
            }
            TeamsCbox.SelectedIndex = 0;
        }

        private void TeamsCbox_DropDownClosed(object sender, EventArgs e)
        {
            if (TeamsCbox.Text == "Entire League")
                TransactionsGrid.ItemsSource = Database.GetTransactions(CurrentLeague.CurrentSeason).OrderByDescending(t => t.Id);
            else
            {
                var team = Database.GetTeamByName(TeamsCbox.Text);
                if (team != null)
                    TransactionsGrid.ItemsSource = Database.GetTeamTransactions(team.Id, CurrentLeague.CurrentSeason).OrderByDescending(t => t.Id);
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamStrategy.xaml
    /// </summary>
    public partial class TeamStrategy : UserControl
    {
        public TeamStrategy()
        {
            InitializeComponent();
        }

        public TeamStrategy(Team t)
        {
            InitializeComponent();
            Team = t;
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private Team Team { get; set; }
        private static GeneralManager GM => IFM.UserManager;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            PresetsCbox.SelectedIndex = 0;
            if (GM == null || GM.Team == null)
                return;

            Team = GM.Team;
            foreach (var tb in StratGrid.Children.OfType<TextBlock>())
                tb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(GM.Team.SecondaryColor));

            ResetStrategy(Team);
        }

        private void ResetStrategy(Team team)
        {
            var teamStrat = Repo.Database.GetStrategy(team.Id);
            O1st10.Value = teamStrat.O1st10;
            O1stShort.Value = teamStrat.O1stShort;
            O1stLong.Value = teamStrat.O1stLong;
            O2ndShort.Value = teamStrat.O2ndShort;
            O2ndLong.Value = teamStrat.O2ndLong;
            O3rdShort.Value = teamStrat.O3rdShort;
            O3rdLong.Value = teamStrat.O3rdLong;
            O4thShort.Value = teamStrat.O4thShort;
            O4thLong.Value = teamStrat.O4thLong;
            OGoalline.Value = teamStrat.OGoalline;
            D1st10.Value = teamStrat.D1st10;
            D1stShort.Value = teamStrat.D1stShort;
            D1stLong.Value = teamStrat.D1stLong;
            D2ndShort.Value = teamStrat.D2ndShort;
            D2ndLong.Value = teamStrat.D2ndLong;
            D3rdShort.Value = teamStrat.D3rdShort;
            D3rdLong.Value = teamStrat.D3rdLong;
            D4thShort.Value = teamStrat.D4thShort;
            D4thLong.Value = teamStrat.D4thLong;
            DGoalline.Value = teamStrat.DGoalline;
        }

        private string PassPct(double value)
        {
            return (100 - value).ToString();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var slider = sender as Slider;
            var name = slider?.Name;

            var pass = StratGrid.Children.OfType<TextBlock>().First(t => t.Name == name + "P");
            if (slider != null) pass.Text = PassPct(slider.Value);
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            var strategy = new Strategy()
            {
                Id = GM.TeamId,
                O1st10 = Convert.ToInt32(O1st10.Value),
                O1stShort = Convert.ToInt32(O1stShort.Value),
                O1stLong = Convert.ToInt32(O1stLong.Value),
                O2ndShort = Convert.ToInt32(O2ndShort.Value),
                O2ndLong = Convert.ToInt32(O2ndLong.Value),
                O3rdShort = Convert.ToInt32(O3rdShort.Value),
                O3rdLong = Convert.ToInt32(O3rdLong.Value),
                O4thShort = Convert.ToInt32(O4thShort.Value),
                O4thLong = Convert.ToInt32(O4thLong.Value),
                OGoalline = Convert.ToInt32(OGoalline.Value),
                D1st10 = Convert.ToInt32(D1st10.Value),
                D1stShort = Convert.ToInt32(D1stShort.Value),
                D1stLong = Convert.ToInt32(D1stLong.Value),
                D2ndShort = Convert.ToInt32(D2ndShort.Value),
                D2ndLong = Convert.ToInt32(D2ndLong.Value),
                D3rdShort = Convert.ToInt32(D3rdShort.Value),
                D3rdLong = Convert.ToInt32(D3rdLong.Value),
                D4thShort = Convert.ToInt32(D4thShort.Value),
                D4thLong = Convert.ToInt32(D4thLong.Value),
                DGoalline = Convert.ToInt32(DGoalline.Value),
            };

            Repo.Database.SaveStrategy(strategy);
            App.ShowNotification("Success", "Strategy updated.", Notification.Wpf.NotificationType.Success, 5);
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

        private void ResetBtn_Click(object sender, RoutedEventArgs e)
        {
            PresetsCbox.SelectedIndex = 0;
            ResetStrategy(Team);
        }

        private void PresetsCbox_DropDownClosed(object sender, EventArgs e)
        {
            switch(PresetsCbox.Text)
            {
                default:
                    ResetStrategy(Team);
                    break;
                case "Balanced":
                    SetBalanced();
                    break;
                case "Pass More":
                    SetPassMore();
                    break;
                case "Run More":
                    SetRunMore();
                    break;
            }
        }

        private void SetBalanced()
        {
            O1st10.Value = 50;
            O1stShort.Value = 40;
            O1stLong.Value = 60;
            O2ndShort.Value = 40;
            O2ndLong.Value = 60;
            O3rdShort.Value = 40;
            O3rdLong.Value = 70;
            O4thShort.Value = 30;
            O4thLong.Value = 70;
            OGoalline.Value = 50;
            D1st10.Value = 50;
            D1stShort.Value = 40;
            D1stLong.Value = 60;
            D2ndShort.Value = 40;
            D2ndLong.Value = 60;
            D3rdShort.Value = 40;
            D3rdLong.Value = 70;
            D4thShort.Value = 30;
            D4thLong.Value = 70;
            DGoalline.Value = 50;
        }

        private void SetPassMore()
        {
            O1st10.Value = 70;
            O1stShort.Value = 60;
            O1stLong.Value = 80;
            O2ndShort.Value = 60;
            O2ndLong.Value = 80;
            O3rdShort.Value = 60;
            O3rdLong.Value = 90;
            O4thShort.Value = 50;
            O4thLong.Value = 90;
            OGoalline.Value = 70;
            D1st10.Value = 50;
            D1stShort.Value = 40;
            D1stLong.Value = 60;
            D2ndShort.Value = 40;
            D2ndLong.Value = 60;
            D3rdShort.Value = 40;
            D3rdLong.Value = 70;
            D4thShort.Value = 30;
            D4thLong.Value = 70;
            DGoalline.Value = 50;
        }

        private void SetRunMore()
        {
            O1st10.Value = 40;
            O1stShort.Value = 20;
            O1stLong.Value = 50;
            O2ndShort.Value = 20;
            O2ndLong.Value = 50;
            O3rdShort.Value = 20;
            O3rdLong.Value = 50;
            O4thShort.Value = 10;
            O4thLong.Value = 70;
            OGoalline.Value = 30;
            D1st10.Value = 50;
            D1stShort.Value = 40;
            D1stLong.Value = 60;
            D2ndShort.Value = 40;
            D2ndLong.Value = 60;
            D3rdShort.Value = 40;
            D3rdLong.Value = 70;
            D4thShort.Value = 30;
            D4thLong.Value = 70;
            DGoalline.Value = 50;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;

namespace IndoorFootballManager.Controls
{
    /// <summary>
    /// Interaction logic for TeamDepthChart.xaml
    /// </summary>
    public partial class TeamDepthChart : UserControl
    {
        public TeamDepthChart()
        {
            InitializeComponent();
        }

        private IFM GameWindow => Window.GetWindow(this) as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static Team Team => TeamPage.Team;
        private DepthChart _teamChart;

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TeamPhilosophy.Text = PhilosophyStr;
            LoadPlayers();
            _teamChart = Repo.Database.GetDepthChart(Team.Id);
            LoadDepthChart(_teamChart);
        }

        private void LoadPlayers()
        {
            Quarterbacks.ForEach(c => c.ItemsSource = QBs.OrderByDescending(p => p.Overall));
            Runningbacks.ForEach(c => c.ItemsSource = RBs.OrderByDescending(p => p.Overall));            
            Receivers.ForEach(c => c.ItemsSource = WRs.OrderByDescending(p => p.Overall));
            OLinemen.ForEach(c => c.ItemsSource = OLine.OrderByDescending(p => p.Overall));
            DLinemen.ForEach(c => c.ItemsSource = DLine.OrderByDescending(p => p.Overall));
            Linebackers.ForEach(c => c.ItemsSource = LBs.OrderByDescending(p => p.Overall));
            Dbacks.ForEach(c => c.ItemsSource = DBs.OrderByDescending(p => p.Overall));
            Kickers.ForEach(c => c.ItemsSource = Ks);
            KickReturners.ForEach(c => c.ItemsSource = KRs);
        }


        private void LoadDepthChart(DepthChart dc)
        {
            var players = Team.ActiveRoster.ToList();
            var qbs = dc.Quarterback?.DeserializeToList<Player>();
            for (var i = 0; i < 3; i++)
            {
                if (qbs == null || i == qbs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "QB" + (i + 1));
                box.Text = qbs[i]?.NameWithOverall ?? "";
            }

            var rbs = dc.Runningback?.DeserializeToList<Player>();
            for (var i = 0; i < 5; i++)
            {
                if (rbs == null || i == rbs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "RB" + (i + 1));
                box.Text = rbs[i]?.NameWithOverall ?? "";
            }

            var wrs = dc.WideReceiver?.DeserializeToList<Player>();
            for (var i = 0; i < 6; i++)
            {
                if (wrs == null || i == wrs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "WR" + (i + 1));
                box.Text = wrs[i]?.NameWithOverall ?? "";
            }

            var ol = dc.OffensiveLine?.DeserializeToList<Player>();
            for (var i = 0; i < 6; i++)
            {
                if (ol == null || i == ol.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "OL" + (i + 1));
                box.Text = ol[i]?.NameWithOverall ?? "";
            }

            var dl = dc.DefensiveLine?.DeserializeToList<Player>();
            for (var i = 0; i < 6; i++)
            {
                if (dl == null || i == dl.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "DL" + (i + 1));
                box.Text = dl[i]?.NameWithOverall ?? "";
            }

            var lbs = dc.Linebacker?.DeserializeToList<Player>();
            for (var i = 0; i < 5; i++)
            {
                if (lbs == null || i == lbs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "LB" + (i + 1));
                box.Text = lbs[i]?.NameWithOverall ?? "";
            }

            var dbs = dc.DefensiveBack?.DeserializeToList<Player>();
            for (var i = 0; i < 6; i++)
            {
                if (dbs == null || i == dbs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "DB" + (i + 1));
                box.Text = dbs[i]?.NameWithOverall ?? "";
            }

            var krs = dc.KickReturner?.DeserializeToList<Player>();
            for (var i = 0; i < 3; i++)
            {
                if (krs == null || i == krs.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "KR" + (i + 1));
                box.Text = krs[i]?.NameWithOverall ?? "";
            }

            var ks = dc.Kicker?.DeserializeToList<Player>();
            for (var i = 0; i < 3; i++)
            {
                if (ks == null || i == ks.Count)
                    break;
                var box = ChartGrid.Children.OfType<ComboBox>().FirstOrDefault(c => c.Name == "K" + (i + 1));
                box.Text = ks[i]?.NameWithOverall ?? "";
            }
        }


        private static string PhilosophyStr
        {
            get
            {
                switch (Team.OffensivePhilosophyStr)
                {
                    case "ThreeWRs":
                        return "Three Wide Receivers";
                    case "TwoRBs":
                        return "Two Running Backs";
                    default:
                        return "Three Running Backs";
                }
            }
        }

        private Enums.OffensivePhilosophies PhilosophyEnum
        {
            get
            {
                switch (TeamPhilosophy.Text)
                {
                    case "Three Wide Receivers":
                        return Enums.OffensivePhilosophies.ThreeWRs;
                    case "Two Running Backs":
                        return Enums.OffensivePhilosophies.TwoRBs;
                    default:
                        return Enums.OffensivePhilosophies.ThreeRBs;
                }
            }
        }

        private static IEnumerable<Player> QBs => Team.QBs;
        private static IEnumerable<Player> RBs => Team.RBs;
        private static IEnumerable<Player> WRs => Team.WRs;
        private static IEnumerable<Player> OLine => Team.OLine;
        private static IEnumerable<Player> DLine => Team.DLine;
        private static IEnumerable<Player> LBs => Team.LBs;
        private static IEnumerable<Player> DBs => Team.DBs;
        private static IEnumerable<Player> Ks => Team.Ks;
        private static IEnumerable<Player> KRs => Team.Returners;

        public List<ComboBox> Quarterbacks => new List<ComboBox>() { QB1, QB2, QB3 };
        public List<ComboBox> Runningbacks => new List<ComboBox>() { RB1, RB2, RB3, RB4, RB5 };
        public List<ComboBox> Receivers => new List<ComboBox>() { WR1, WR2, WR3, WR4, WR5, WR6 };
        public List<ComboBox> OLinemen => new List<ComboBox>() { OL1, OL2, OL3, OL4, OL5, OL6 };
        public List<ComboBox> DLinemen => new List<ComboBox>() { DL1, DL2, DL3, DL4, DL5, DL6 };
        public List<ComboBox> Linebackers => new List<ComboBox>() { LB1, LB2, LB3, LB4, LB5 };
        public List<ComboBox> Dbacks => new List<ComboBox>() { DB1, DB2, DB3, DB4, DB5, DB6 };
        public List<ComboBox> Kickers => new List<ComboBox>() { K1, K2, K3 };
        public List<ComboBox> KickReturners => new List<ComboBox>() { KR1, KR2, KR3 };

        private async void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            await App.ActivateLoading();
            try
            {
                Team.OffPhilosophy = (int)PhilosophyEnum;
                Repo.Database.SaveTeam(Team);
                var teamChart = new DepthChart
                {
                    TeamId = Team.Id,
                    Quarterback = new List<Player> {
                        QB1.SelectedItem as Player ?? null,
                        QB2.SelectedItem as Player ?? null,
                        QB3.SelectedItem as Player ?? null}.SerializeToJson(),
                    Runningback = new List<Player> {
                        RB1.SelectedItem as Player ?? null,
                        RB2.SelectedItem as Player ?? null,
                        RB3.SelectedItem as Player ?? null,
                        RB4.SelectedItem as Player ?? null,
                        RB5.SelectedItem as Player ?? null}.SerializeToJson(),
                    WideReceiver = new List<Player> { 
                        WR1.SelectedItem as Player ?? null,
                        WR2.SelectedItem as Player ?? null,
                        WR3.SelectedItem as Player ?? null,
                        WR4.SelectedItem as Player ?? null,
                        WR5.SelectedItem as Player ?? null,
                        WR6.SelectedItem as Player ?? null}.SerializeToJson(),
                    OffensiveLine = new List<Player> { 
                        OL1.SelectedItem as Player ?? null,
                        OL2.SelectedItem as Player ?? null,
                        OL3.SelectedItem as Player ?? null,
                        OL4.SelectedItem as Player ?? null,
                        OL5.SelectedItem as Player ?? null,
                        OL6.SelectedItem as Player ?? null}.SerializeToJson(),
                    DefensiveLine = new List<Player> {
                        DL1.SelectedItem as Player ?? null,
                        DL2.SelectedItem as Player ?? null,
                        DL3.SelectedItem as Player ?? null,
                        DL4.SelectedItem as Player ?? null,
                        DL5.SelectedItem as Player ?? null,
                        DL6.SelectedItem as Player ?? null}.SerializeToJson(),
                    Linebacker = new List<Player> { 
                        LB1.SelectedItem as Player ?? null,
                        LB2.SelectedItem as Player ?? null,
                        LB3.SelectedItem as Player ?? null,
                        LB4.SelectedItem as Player ?? null,
                        LB5.SelectedItem as Player ?? null}.SerializeToJson(),
                    DefensiveBack = new List<Player> {
                        DB1.SelectedItem as Player ?? null,
                        DB2.SelectedItem as Player ?? null,
                        DB3.SelectedItem as Player ?? null,
                        DB4.SelectedItem as Player ?? null,
                        DB5.SelectedItem as Player ?? null,
                        DB6.SelectedItem as Player ?? null}.SerializeToJson(),
                    KickReturner = new List<Player> { 
                        KR1.SelectedItem as Player ?? null,
                        KR2.SelectedItem as Player ?? null,
                        KR3.SelectedItem as Player ?? null}.SerializeToJson(),
                    Kicker = new List<Player> { 
                        K1.SelectedItem as Player ?? null,
                        K2.SelectedItem as Player ?? null, 
                        K3.SelectedItem as Player ?? null}.SerializeToJson()
                };
                Repo.Database.SaveDepthChart(teamChart);
                App.ShowNotification("Success", "Depth Chart saved.", Notification.Wpf.NotificationType.Success, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "An error has occurred.", Notification.Wpf.NotificationType.Error, 5);
            }
            finally
            {
                await App.DeactivateLoading();
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow.MainFrame.GoBack();
        }

        private void AutoFillBtn_Click(object sender, RoutedEventArgs e)
        {
            void Cancel() { return; }
            App.ShowMessageBox("Auto Set Roster", "Are you sure you wish to auto set the depth chart?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", AutoFill, "No", Cancel);
        }

        private void AutoFill()
        {
            QB1.SelectedIndex = 0;
            QB2.SelectedIndex = 1;
            QB3.SelectedIndex = 2;
            RB1.SelectedIndex = 0;
            RB2.SelectedIndex = 1;
            RB3.SelectedIndex = 2;
            RB4.SelectedIndex = 3;
            RB5.SelectedIndex = 4;
            WR1.SelectedIndex = 0;
            WR2.SelectedIndex = 1;
            WR3.SelectedIndex = 2;
            WR4.SelectedIndex = 3;
            WR5.SelectedIndex = 4;
            WR6.SelectedIndex = 5;
            OL1.SelectedIndex = 0;
            OL2.SelectedIndex = 1;
            OL3.SelectedIndex = 2;
            OL4.SelectedIndex = 3;
            OL5.SelectedIndex = 4;
            OL6.SelectedIndex = 5;
            DL1.SelectedIndex = 0;
            DL2.SelectedIndex = 1;
            DL3.SelectedIndex = 2;
            DL4.SelectedIndex = 3;
            DL5.SelectedIndex = 4;
            DL6.SelectedIndex = 5;
            LB1.SelectedIndex = 0;
            LB2.SelectedIndex = 1;
            LB3.SelectedIndex = 2;
            LB4.SelectedIndex = 3;
            LB5.SelectedIndex = 4;
            DB1.SelectedIndex = 0;
            DB2.SelectedIndex = 1;
            DB3.SelectedIndex = 2;
            DB4.SelectedIndex = 3;
            DB5.SelectedIndex = 4;
            DB6.SelectedIndex = 5;
            KR1.SelectedIndex = 0;
            KR2.SelectedIndex = 1;
            KR3.SelectedIndex = 2;
            K1.SelectedIndex = 0;
            K2.SelectedIndex = 1;
            K3.SelectedIndex = 2;
        }

        private void ResetBtn_Click(object sender, RoutedEventArgs e)
        {
            if(TeamPhilosophy.Text != PhilosophyStr)
                TeamPhilosophy.Text = PhilosophyStr;
            LoadDepthChart(_teamChart);
        }

        private void DC_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                ((ComboBox)sender).Text = "";
        }
    }
}

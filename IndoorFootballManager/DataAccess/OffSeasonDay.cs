﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.DataAccess
{
    public class OffSeasonDay
    {
        private static League CurrentLeague => IFM.CurrentLeague;
        private static SQLiteConnection Database => Repo.Database;

        public static void ClearEmails(IEnumerable<GeneralManager> managers)
        {
            // delete all emails
            var gms = managers.ToList();
            var emails = gms.Select(manager => Database.GetEmails(manager.Id)).SelectMany(email => email).ToList();
            foreach (var email in emails)
            {
                Database.DeleteEmail(email.Id);
            }
        }

        public static void UpdateExperience(IEnumerable<Player> players)
        {
            var expUpdate = players.Select(p => { p.InjuredReserve = false; p.RosterReserve = false; p.Exp += 1; return p; });
            Database.UpdateAllPlayers(expUpdate);
        }

        public static void EndContracts(IEnumerable<Player> players)
        {
            var cutPlayers = players.Select(p => { p.Salary = 0; p.ContractYears = 0; p.TeamId = 0; return p; });
            Database.UpdateAllPlayers(cutPlayers);
        }

        public static void RolloverContracts(IEnumerable<Player> players)
        {
            var signedPlayers = players.Select(p => { p.ContractYears -= 1; return p; });
            Database.UpdateAllPlayers(signedPlayers);
        }

        public static void ExtendPlayers(IEnumerable<Player> players)
        {
            var extPlayers = players.Select(p =>
            {
                p.ContractYears = p.ExtensionYears;
                p.Salary = p.ExtensionSalary;
                p.ExtensionYears = 0;
                p.ExtensionSalary = 0;
                if (!p.IsFranchised) return p;

                p.IsFranchised = false;
                var team = Database.GetTeamById(p.TeamId);
                team.FranchiseTagUsed = false;
                Database.SaveTeam(team);
                return p;
            });

            Database.UpdateAllPlayers(extPlayers);
        }

        public static void DeclinePlayers(IEnumerable<Player> players)
        {
            var declinedPlayers = new List<Player>();
            foreach (var p in players)
            {
                bool declined;
                var under28 = new Random().Next(0, 4);
                if (p.Age > 25 && p.Age < 28 && under28 == 0)
                    declined = p.CheckDecline;
                else
                    declined = p.CheckDecline;

                if (!declined) continue;

                p.DeclinePlayer();
                declinedPlayers.Add(p);
            }

            if (declinedPlayers.Any())
                Database.UpdateAllPlayers(declinedPlayers.AsEnumerable());
        }

        private static List<Player> _retiredPlayers;
        public static void RetirePlayers(IEnumerable<Player> players)
        {
            _retiredPlayers = new List<Player>();
            var retiredList = new List<Player>();
            var transactions = new List<Transaction>();
            foreach(var p in players)
            {
                bool retired;
                var under28 = new Random().Next(0, 10);
                if (p.Age > 25 && p.Age < 28 && under28 == 0)
                    retired = p.CheckRetirement;
                else
                    retired = p.CheckRetirement;

                if (!retired) continue;

                if (p.TeamId > 0)
                {
                    var team = Database.GetTeamById(p.TeamId);
                    if (!team.IsCpu)
                        retiredList.Add(p);
                    if (p.IsFranchised)
                    {
                        team.FranchiseTagUsed = false;
                        Database.SaveTeam(team);
                    }
                }

                transactions.Add(new Transaction(p.TeamId,
                    CurrentLeague.CurrentSeason,
                    CurrentLeague.CurDate,
                    $"{p.NameWithPosition} has retired on {CurrentLeague.CurDate.ToShortDateString()}."));

                p.ContractYears = 0;
                p.Salary = 0;
                p.ExtensionSalary = 0;
                p.ExtensionYears = 0;
                if (p.IsFranchised)
                {
                    p.IsFranchised = false;
                    p.Team.FranchiseTagUsed = false;
                    Database.SaveTeam(p.Team);
                }
                p.TeamId = -2;
                var playerHistory = p.PlayerHistoryJson == null ? new List<string>() : p.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add("Retired on " + CurrentLeague.CurDate.ToShortDateString());
                p.PlayerHistoryJson = playerHistory.SerializeToJson();
                _retiredPlayers.Add(p);
            }
            
            Database.UpdateAllPlayers(_retiredPlayers.AsEnumerable());
            Database.InsertAll(transactions.AsEnumerable());
            
            SendEmail.PlayersRetired(retiredList);
            IFM.NotifyChange("EmailsCount");
        }

        public static void SaveStandings(int conferences, int divs)
        {
            var newId = 1;
            var historyStandings = new List<StandingsHistory>();
            var teams = new List<Team>();
            var id = 0;
            for (var i = 0; i < conferences; i++)
            {
                for (var j = 0; j < divs; j++)
                {
                    var divStandings = Database.GetTeamsForDivisionStandings(i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2, CurrentLeague.DivisionsList[id++]).ToList();
                    foreach(var t in divStandings)
                    {
                        var hist = new StandingsHistory
                        {
                            Season = t.Season,
                            TeamId = t.Id,
                            TeamName = t.TeamName,
                            Wins = t.Wins,
                            Losses = t.Losses,
                            PointsFor = t.PointsFor,
                            PointsAgainst = t.PointsAgainst,
                            DivWins = t.DivWins,
                            DivLosses = t.DivLosses,
                            ConfWins = t.ConfWins,
                            ConfLosses = t.ConfLosses,
                            HomeWins = t.HomeWins,
                            HomeLosses = t.HomeLosses,
                            AwayWins = t.AwayWins,
                            AwayLosses = t.AwayLosses,
                            Streak = t.Streak,
                            Conference = t.Conference,
                            Division = t.Division,
                        };
                        historyStandings.Add(hist);

                        // reset team standings info
                        t.Season += 1;
                        t.LastSeasonWins = t.Wins;
                        t.LastSeasonLosses = t.Losses;
                        t.LastSeasonSOS = t.SOS;
                        t.LastSeasonFinish = newId;
                        t.Wins = 0;
                        t.Losses = 0;
                        t.PointsFor = 0;
                        t.PointsAgainst = 0;
                        t.DivWins = 0;
                        t.DivLosses = 0;
                        t.ConfWins = 0;
                        t.ConfLosses = 0;
                        t.HomeWins = 0;
                        t.HomeLosses = 0;
                        t.AwayWins = 0;
                        t.AwayLosses = 0;
                        t.Streak = 0;
                        teams.Add(t);
                        newId++;
                    }
                }
            }

            Database.InsertAll(historyStandings.AsEnumerable());
            Database.UpdateAllTeams(teams.AsEnumerable());
        }

        public static void UpdateLastSeasonFinishForNewFormat(int conferences, int divs)
        {
            var newId = 1;
            var teams = new List<Team>();
            var id = 0;
            for (var i = 0; i < conferences; i++)
            {
                for (var j = 0; j < divs; j++)
                {
                    var divStandings = Database.GetTeamsForDivisionStandings(i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2, CurrentLeague.DivisionsList[id++]);
                    foreach (var t in divStandings)
                    {
                        t.LastSeasonFinish = newId;
                        teams.Add(t);
                        newId++;
                    }
                }
            }
            Database.UpdateAllTeams(teams.AsEnumerable());
            DeleteAndCreateGames();
        }

        public static void DeleteAndCreateGames()
        {
            var oldGames = Database.GetGames(CurrentLeague.CurrentSeason);
            foreach (var g in oldGames)
                Database.Delete(g);

            CreateNewScheduleAndDates(CurrentLeague);
        }

        public static void CreateNewScheduleAndDates(League league)
        {
            var divs = league.Conferences * league.Divisions;
            var teams = league.LeagueTeams / divs;
            var format = league.Conferences + "-" + league.Divisions + "-" + teams;
            var mod = (league.CurrentSeason % 2).ToString();
            ParseSchedule(@"Resources/Schedules/" + format + "-schedule-" + mod + ".csv");
            SetDraftDate(league);
            SetGameDates(league, _games.AsEnumerable());
        }

        private static List<Game> _games;

        private static void ParseSchedule(string filePath)
        {
            _games = new List<Game>();
            var text = File.ReadAllLines(filePath);
            var fields = new string[text.Count(), 3];
            var r = 0;

            for (var l = 0; l < text.Count(); l++)
            {
                var cols = text[l].Split(',');
                fields[l, r] = cols[0];
                r++;
                fields[l, r] = cols[1];
                r = 0;
            }

            var teams = Database.GetTeams().ToList();
            for (var k = 1; k < text.Count(); k++)
            {
                var homeTeam = Convert.ToInt32(fields[k, 0]);
                var awayTeam = Convert.ToInt32(fields[k, 1]);

                var game = new Game
                {
                    Season = CurrentLeague.CurrentSeason,
                    HomeTeam = teams.FirstOrDefault(t => t.LastSeasonFinish == homeTeam).Id,
                    AwayTeam = teams.FirstOrDefault(t => t.LastSeasonFinish == awayTeam).Id,
                };
                _games.Add(game);
            }
        }

        private static void SetDraftDate(League league)
        {
            var day1 = new DateTime(league.CurrentSeason, 3, 1);
            for (var i = 0; i < 7; i++)
            {
                if (day1.AddDays(i).DayOfWeek != 0) continue;
                day1 = day1.AddDays(i);
                break;
            }
            league.NewSeasonDate = day1.AddDays(-30);
            league.DraftDate = day1;
            league.TrainingCampDate = day1.AddDays(21);
            Database.SaveLeague(league);
        }

        private static DateTime _gameDate;
        private static void SetGameDates(League league, IEnumerable<Game> games)
        {
            var day1 = new DateTime(league.CurrentSeason, 4, 1);
            for (var i = 0; i < 7; i++)
            {
                if (day1.AddDays(i).DayOfWeek != 0) continue;
                day1 = day1.AddDays(i);
                break;
            }
            league.InSeasonDate = day1.AddDays(14);
            _gameDate = day1.AddDays(21);
            var gamesList = games.ToList();
            var gamesCount = gamesList.Count() / (league.LeagueTeams / 2);
            var gameIndex = 0;
            var seasonEnd = new DateTime();
            for (var i = 0; i < gamesCount; i++)
            {
                for (var j = 0; j < league.LeagueTeams / 2; j++)
                {
                    var g = gamesList.ElementAt(gameIndex);
                    g.GameDate = _gameDate;
                    gameIndex++;
                }
                _gameDate = _gameDate.AddDays(7);
                if (i == gamesCount - 1)
                    seasonEnd = _gameDate.AddDays(30);
            }

            Database.InsertAll(gamesList);

            league.SeasonEndDate = seasonEnd;
            Database.SaveLeague(league);
        }

        public static void DeleteNonActiveContractOffers()
        {
            var offers = Database.GetContractOffers().Where(o => o.Active == false).ToList();
            foreach (var con in offers)
            {
                Database.Delete<ContractOffer>(con.Id);
            }
        }

        public static void SignFreeAgents()
        {
            var contractOffers = Database.GetContractOffers()
                .Where(o => o.Active)
                .OrderByDescending(o => o.Evaluation)
                .GroupBy(o => o.PlayerId)
                .Select(o => o.First());
            foreach (var con in contractOffers)
            {
                var signed = false;
                Team team = null;
                Player player = null;
                var randomChange = new Random().Next(0, 2);
                if (randomChange == 1)
                {
                    signed = true;

                    team = Database.GetTeamById(con.TeamId);
                    player = Database.GetPlayerById(con.PlayerId);
                    player.TeamId = team.Id;
                    player.Salary = con.Salary;
                    player.ContractYears = con.Years;
                    player.Jersey = player.JerseyNumber;
                    var playerHistory = player.PlayerHistoryJson == null ? new List<string>() : player.PlayerHistoryJson.DeserializeToList<string>();
                    playerHistory.Add("Signed by " + team.TeamName + " on " + CurrentLeague.CurDate.ToShortDateString() + " (" + player.Salary.ToString("C") + " / " + player.ContractYears + " years)");
                    player.PlayerHistoryJson = playerHistory.SerializeToJson();

                    var transaction = new Transaction(team.Id,
                        CurrentLeague.CurrentSeason,
                        CurrentLeague.CurDate,
                        $"{player.NameWithPosition} signed with {team.TeamName} on {CurrentLeague.CurDate.ToShortDateString()} on a {player.ContractYears}-year contract for {player.Salary:C0}/year.");

                    Database.SaveTransaction(transaction);

                    Database.SavePlayer(player);
                    PlayerData.DeleteOldContracts(player);
                    if (team.IsCpu == false)
                    {
                        SendEmail.PlayerSigned(team, player);
                        IFM.NotifyChange("EmailsCount");
                    }
                }

                if (!signed) continue;
                var notSigned = Database.GetContractOffers().Where(o => o.PlayerId == con.PlayerId && o.Active).ToList();
                notSigned.Select(c => { c.Active = false; return c; }).ToList().ForEach(c =>
                {
                    var team1 = Database.GetTeamById(c.TeamId);
                    if (team1.IsCpu) return;
                    SendEmail.PlayerDidNotSign(team1, team, player);
                    IFM.NotifyChange("EmailsCount");
                });

                Database.UpdateAll(notSigned.AsEnumerable());
            }
        }

        public static void ClearTransactions() => Database.DeleteTransactions(CurrentLeague.CurrentSeason);




    }
}

﻿using System.Collections.Generic;
using System.Linq;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.DataAccess
{
    public static class TeamData
    {
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        public static Dictionary<string, int> MinPositions => new Dictionary<string, int>
        {
            { "QB", 2 },
            { "RB", 3 },
            { "WR", 4 },
            { "OL", 4 },
            { "DL", 4 },
            { "LB", 3 },
            { "DB", 4 },
            { "K", 1 }
        };

        public static bool IsAtMinimumRequirements(this Team t)
        {
            var players = t.ActiveRoster.ToList();
            return MinPositions.All(min => players.Count(p => p.Pos == min.Key) >= min.Value);
        }

        public static void CpuSetDepthChart(this Team t)
        {
            if (t.Players == null) return;
            var teamChart = new DepthChart
            {
                TeamId = t.Id,
                Quarterback = t.QBs.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                Runningback = t.RBs.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                WideReceiver = t.WRs.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                OffensiveLine = t.OLine.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                DefensiveLine = t.DLine.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                Linebacker = t.LBs.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                DefensiveBack = t.DBs.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                KickReturner = t.Returners.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson(),
                Kicker = t.Ks.Where(p => !p.IsInjured).OrderByDescending(p => p.Overall).ToList().SerializeToJson()
            };
            Database.SaveDepthChart(teamChart);
        }

        public static void UpdateDepthChart(this Team t)
        {
            if (t.Players == null) return;
            var players = t.ActiveRoster.ToList();
            var teamChart = Database.GetDepthChart(t.Id);
            var qbs = teamChart.Quarterback.DeserializeToList<Player>();
            for (var i = 0; i < qbs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => qbs[i] != null && pl.Id == qbs[i].Id);
                if (p != null && p.IsInjured)
                    qbs.RemoveAt(i);
            }
            var rbs = teamChart.Runningback.DeserializeToList<Player>();
            for (var i = 0; i < rbs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => rbs[i] != null && pl.Id == rbs[i].Id);
                if (p != null && p.IsInjured)
                    rbs.RemoveAt(i);
            }
            var wrs = teamChart.WideReceiver.DeserializeToList<Player>();
            for (var i = 0; i < wrs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => wrs[i] != null && pl.Id == wrs[i].Id);
                if (p != null && p.IsInjured)
                    wrs.RemoveAt(i);
            }
            var ol = teamChart.OffensiveLine.DeserializeToList<Player>();
            for (var i = 0; i < ol.Count; i++)
            {
                var p = players.FirstOrDefault(pl => ol[i] != null && pl.Id == ol[i].Id);
                if (p != null && p.IsInjured)
                    ol.RemoveAt(i);
            }
            var dl = teamChart.DefensiveLine.DeserializeToList<Player>();
            for (var i = 0; i < dl.Count; i++)
            {
                var p = players.FirstOrDefault(pl => dl[i] != null && pl.Id == dl[i].Id);
                if (p != null && p.IsInjured)
                    dl.RemoveAt(i);
            }
            var lbs = teamChart.Linebacker.DeserializeToList<Player>();
            for (var i = 0; i < lbs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => lbs[i] != null && pl.Id == lbs[i].Id);
                if (p != null && p.IsInjured)
                    lbs.RemoveAt(i);
            }
            var dbs = teamChart.DefensiveBack.DeserializeToList<Player>();
            for (var i = 0; i < dbs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => dbs[i] != null && pl.Id == dbs[i].Id);
                if (p != null && p.IsInjured)
                    dbs.RemoveAt(i);
            }
            var krs = teamChart.KickReturner.DeserializeToList<Player>();
            for (var i = 0; i < krs.Count; i++)
            {
                var p = players.FirstOrDefault(pl => krs[i] != null && pl.Id == krs[i].Id);
                if (p != null && p.IsInjured)
                    krs.RemoveAt(i);
            }
            var ks = teamChart.Kicker.DeserializeToList<Player>();
            for (var i = 0; i < ks.Count; i++)
            {
                var p = players.FirstOrDefault(pl => ks[i] != null && pl.Id == ks[i].Id);
                if (p != null && p.IsInjured)
                    ks.RemoveAt(i);
            }
            teamChart.Quarterback = qbs.SerializeToJson();
            teamChart.Runningback = rbs.SerializeToJson();
            teamChart.WideReceiver = wrs.SerializeToJson();
            teamChart.OffensiveLine = ol.SerializeToJson();
            teamChart.DefensiveLine = dl.SerializeToJson();
            teamChart.Linebacker = lbs.SerializeToJson();
            teamChart.DefensiveBack = dbs.SerializeToJson();
            teamChart.KickReturner = krs.SerializeToJson();
            teamChart.Kicker = ks.SerializeToJson();
            Database.SaveDepthChart(teamChart);
        }
    }
}

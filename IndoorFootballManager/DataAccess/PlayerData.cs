﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.DataAccess
{
    public static class PlayerData
    {
        private static IFM GameWindow => App.Current.MainWindow as IFM;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static SQLiteConnection Database => Repo.Database;

        internal static void AssignToReserves(this Player p)
        {
            p.RosterReserve = true;
            Database.SavePlayer(p);
        }

        internal static void ActivatePlayer(this Player p)
        {
            p.RosterReserve = false;
            Database.SavePlayer(p);
        }

        internal static void AssignToIr(this Player p)
        {
            p.InjuredReserve = true;
            p.RosterReserve = false;
            Database.SavePlayer(p);
        }

        // save method is called later
        public static void DeclinePlayer(this Player p)
        {
            p.Strength = (int)Math.Floor(p.Strength * RandomDouble(0.9, 1.01));
            p.ArmStrength = (int)Math.Floor(p.ArmStrength * RandomDouble(0.9, 1.01));
            p.Speed = (int)Math.Floor(p.Speed * RandomDouble(0.9, 1.01));
            p.Tackle = (int)Math.Floor(p.Tackle * RandomDouble(0.9, 1.01));
            p.KickStrength = (int)Math.Floor(p.KickStrength * RandomDouble(0.9, 1.01));
            
            if (p.Intelligence >= 100) 
                return;

            p.Intelligence = (int)Math.Round(p.Intelligence * RandomDouble(1.0, 1.05));
            if (p.Intelligence > 100)
                p.Intelligence = 100;
        }

        private static readonly Random Random = new Random();
        private static double RandomDouble(double minValue, double maxValue)
        {
            var next = Random.NextDouble();
            return minValue + (next * (maxValue - minValue));
        }

        private static List<double> DeclineValues
        {
            get
            {
                var values = new List<double> { 1.00, 0.99, 0.98, 0.97, 0.96, 0.95, 0.94, 0.93, 0.92, 0.91, 0.9 };
                return values;
            }
        }

        private static List<double> ProgressValues
        {
            get
            {
                var values = new List<double> { 1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 1.1 };
                return values;
            }
        }

        internal static void CutPlayer(Player player)
        {
            void Release()
            {
                ReleasePlayer(player);
                App.ShowNotification("Player Released", $"{player.Name} has been released.", Notification.Wpf.NotificationType.Information, 5);
                GameWindow.MainFrame.Refresh();
            }

            void Cancel() { return; }
            App.ShowMessageBox("Release Player", $"Are you sure you want to cut {player.NameWithPosition}", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", Release, "No", Cancel);
        }

        internal static void ReleasePlayer(Player player)
        {
            if (CurrentLeague == null)
            {
                return;
            }

            var playerHistory = player.PlayerHistoryJson.DeserializeToList<string>();
            playerHistory.Add("Released by " + player.Team.TeamName + " on " + CurrentLeague.CurDate.ToShortDateString());
            player.PlayerHistoryJson = playerHistory.SerializeToJson();
            var transaction = new Transaction(player.TeamId, 
                                CurrentLeague.CurrentSeason, 
                                CurrentLeague.CurDate, 
                                $"{player.Team.TeamName} released {player.NameWithPosition} on {CurrentLeague.CurDate.ToShortDateString()}.");

            Database.SaveTransaction(transaction);

            SendEmail.PlayerReleased(player.Team, player);
            IFM.NotifyChange("EmailsCount");

            player.TeamId = 0;
            player.Salary = 0;
            player.ContractYears = 0;
            player.InjuredReserve = false;
            player.RosterReserve = false;
            if (player.IsFranchised)
            {
                player.IsFranchised = false;
                player.Team.FranchiseTagUsed = false;
                Database.SaveTeam(player.Team);
            }
            if (player.ExtensionSalary > 0)
            {
                player.ExtensionSalary = 0;
                player.ExtensionYears = 0;
            }
            Database.SavePlayer(player);
        }

        // in-season signing
        internal static void OfferInSeasonContract(Team team, Player player)
        {
            OfferContract(team, player, 1, player.GetFreeAgentSalary);
        }

        internal static void CpuOfferOffSeasonContract(Team team, Player player)
        {
            var years = player.DesiredYears;
            var salary = player.GetNegotiatedSalary;
            OfferContract(team, player,
                years == 1 ? new Random().Next(1, 3) : new Random().Next(years - 1, years + 1),
                new Random().Next(0, 4) == 0 ? Convert.ToInt32(salary * .9) : Convert.ToInt32(salary * 1.12)); 
        }

        internal static void CpuSignInSeason(Team team, Player player, bool reserves)
        {
            player.TeamId = team.Id;
            player.ContractYears = 1;
            player.Salary = player.GetFreeAgentSalary;
            player.RosterReserve = reserves;
            player.InjuredReserve = false;
            player.Jersey = player.JerseyNumber;
            var playerHistory = player.PlayerHistoryJson.DeserializeToList<string>();
            playerHistory.Add($"Signed by {team.TeamName} on {CurrentLeague.CurDate.ToShortDateString()} ({player.Salary:C} / {player.ContractYears} years)");
            player.PlayerHistoryJson = playerHistory.SerializeToJson();
            var transaction = new Transaction(team.Id,
                                CurrentLeague.CurrentSeason,
                                CurrentLeague.CurDate,
                                $"{player.NameWithPosition} signed with {team.TeamName} on {CurrentLeague.CurDate.ToShortDateString()} on a {player.ContractYears}-year contract for {player.Salary:C0}/year.");


            Database.SaveTransaction(transaction);
                
            Database.SavePlayer(player);

            if (!team.IsCpu)
            {
                SendEmail.PlayerSigned(team, player);
                IFM.NotifyChange("EmailsCount");
            }
        }

        internal static void DeleteOldContracts(Player player)
        {
            var offers = Database.GetContractOffers(player.Id);
            foreach (var offer in offers)
            {
                Database.Delete(offer);
            }
        }

        // offer player a contract
        internal static void OfferContract(Team team, Player player, int years, int salary)
        {
            var id = Database.Table<ContractOffer>().Count() != 0
                ? Database.Table<ContractOffer>().Last().Id + 1
                : 1;

            // create offer
            var offer = new ContractOffer()
            {
                Id = id++,
                PlayerId = player.Id,
                TeamId = team.Id,
                Years = years,
                Salary = salary,
                Active = true
            };
            // check existing offers to make sure the player and team don't already have one
            Database.CheckExistingOffers(offer);
            // save new contract offer
            Database.SaveContract(offer);
        }

        internal static void FranchisePlayer(Player player)
        {
            var team = player.Team;
            if (team.FranchiseTagUsed == false)
            {
                void UseTag()
                {
                    player.UseFranchiseTag();
                    App.ShowNotification("Contract Update", $"{player.NameWithPosition} has been franchise tagged.", Notification.Wpf.NotificationType.Information, 5);
                    GameWindow.MainFrame.Refresh();
                }

                void Cancel() { return; }
                App.ShowMessageBox("Franchise Tag Player", $"Are you sure you wish to franchise tag {player.NameWithPosition}?", Notification.Wpf.NotificationType.Notification, "NotificationArea", "Yes", UseTag, "No", Cancel);
            }
        }
                
        public static void ExportPlayers(IEnumerable<Player> players, string label)
        {
            var playersToExport = Repo.ConvertPlayersToDataTable(players);

            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var databaseFilename = $"{databasePath}{leagueName}_{label.Replace(" ", string.Empty)}.csv";

            try
            {
                if (File.Exists(databaseFilename))
                    File.Delete(databaseFilename);

                var sb = new StringBuilder();
                var columnNames = playersToExport.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
                sb.AppendLine(string.Join(",", columnNames));
                foreach (DataRow row in playersToExport.Rows)
                {
                    var fields = row.ItemArray.Take(32).Select(field => field.ToString());
                    sb.AppendLine(string.Join(",", fields));
                }

                File.WriteAllText(databaseFilename, sb.ToString());
                App.ShowNotification("Export Complete", $"{label} Exported.", Notification.Wpf.NotificationType.Information, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Check if the file is open already.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        public static void ImportPlayers(string filePath)
        {
            var id = Database.GetPlayers().Last().Id;
            using (var sr = new StreamReader(filePath))
            {
                var header = sr.ReadLine();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var row = line.Split(',');
                    var p = new Player
                    {
                        Id = ++id,
                        Jersey = Convert.ToInt32(row[1]),
                        FirstName = row[2],
                        LastName = row[3],
                        TeamId = Convert.ToInt32(row[4]),
                        Pos = row[5],
                        DOB = Convert.ToDateTime(row[6]),
                        Exp = Convert.ToInt32(row[7]),
                        Accuracy = Convert.ToInt32(row[8]),
                        ArmStrength = Convert.ToInt32(row[9]),
                        Carry = Convert.ToInt32(row[10]),
                        Speed = Convert.ToInt32(row[11]),
                        Strength = Convert.ToInt32(row[12]),
                        Catching = Convert.ToInt32(row[13]),
                        Block = Convert.ToInt32(row[14]),
                        Blitz = Convert.ToInt32(row[15]),
                        Coverage = Convert.ToInt32(row[16]),
                        Tackle = Convert.ToInt32(row[17]),
                        Intelligence = Convert.ToInt32(row[18]),
                        KickStrength = Convert.ToInt32(row[19]),
                        KickAccuracy = Convert.ToInt32(row[20]),
                        Attitude = Convert.ToInt32(row[21]),
                        Greed = Convert.ToInt32(row[22]),
                        Loyalty = Convert.ToInt32(row[23]),
                        Injury = Convert.ToInt32(row[24]),
                        Leadership = Convert.ToInt32(row[25]),
                        Passion = Convert.ToInt32(row[26]),
                        WorkEthic = Convert.ToInt32(row[27]),
                        College = row[30],
                    };

                    if (p.TeamId > 0)
                    {
                        p.ContractYears = Convert.ToInt32(row[28]);
                        p.Salary = Convert.ToInt32(row[29]);
                    }
                    else
                    {
                        p.TeamId = 0;
                        p.ContractYears = 0;
                        p.Salary = 0;
                    }

                    p.Overall = p.GetOverall;

                    Database.SavePlayer(p);
                }
            }

            App.ShowNotification("Success", "Players imported successfully.", Notification.Wpf.NotificationType.Success, 5);
        }

        public static void ImportRookies(string filePath)
        {
            var id = Database.GetPlayers().Last().Id;
            using (var sr = new StreamReader(filePath))
            {
                var header = sr.ReadLine();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var row = line.Split(',');
                    var p = new Player
                    {
                        Id = ++id,
                        Jersey = Convert.ToInt32(row[1]),
                        FirstName = row[2],
                        LastName = row[3],
                        TeamId = -1,
                        Pos = row[5],
                        DOB = Convert.ToDateTime(row[6]),
                        Exp = 0,
                        Accuracy = Convert.ToInt32(row[8]),
                        ArmStrength = Convert.ToInt32(row[9]),
                        Carry = Convert.ToInt32(row[10]),
                        Speed = Convert.ToInt32(row[11]),
                        Strength = Convert.ToInt32(row[12]),
                        Catching = Convert.ToInt32(row[13]),
                        Block = Convert.ToInt32(row[14]),
                        Blitz = Convert.ToInt32(row[15]),
                        Coverage = Convert.ToInt32(row[16]),
                        Tackle = Convert.ToInt32(row[17]),
                        Intelligence = Convert.ToInt32(row[18]),
                        KickStrength = Convert.ToInt32(row[19]),
                        KickAccuracy = Convert.ToInt32(row[20]),
                        Attitude = Convert.ToInt32(row[21]),
                        Greed = Convert.ToInt32(row[22]),
                        Loyalty = Convert.ToInt32(row[23]),
                        Injury = Convert.ToInt32(row[24]),
                        Leadership = Convert.ToInt32(row[25]),
                        Passion = Convert.ToInt32(row[26]),
                        WorkEthic = Convert.ToInt32(row[27]),
                        College = row[30],
                        ContractYears = 0,
                        Salary = 0
                    };

                    p.Overall = p.GetOverall;

                    Database.SavePlayer(p);
                }
            }

            App.ShowNotification("Success", "Rookies imported successfully.", Notification.Wpf.NotificationType.Success, 5);
        }


    }
}

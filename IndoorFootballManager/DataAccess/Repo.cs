﻿using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace IndoorFootballManager.DataAccess
{
    public static class Repo
    {
        private static readonly object CreateLocker = new object();

        private static SQLiteConnection _db;
        private static string _databaseFilename;

        public static SQLiteConnection Database => _db ?? GetOrCreate();
        
        private static SQLiteConnection GetOrCreate()
        {
            if (Globals.LeagueAbbreviation.IsNullOrEmpty()) 
                return null;
            
            lock (CreateLocker)
            {
                _databaseFilename = Globals.LeagueFilePath;
                return _db ?? (_db = new SQLiteConnection(_databaseFilename));
            }
        }

        public static void Reconnect(string filePath)
        {
            lock (CreateLocker)
            {
                _databaseFilename = filePath;
                _db = new SQLiteConnection(_databaseFilename);
            }
        }

        public static SQLiteConnection Open(string leagueName)
        {
            if (leagueName == null)
                return null;

            lock (CreateLocker)
            {
                var databaseFileName = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\{leagueName}.ifm";
                return new SQLiteConnection(databaseFileName);
            }

        }

        public static SQLiteConnection Create(string leagueName)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\";
            Directory.CreateDirectory(databasePath);

            _databaseFilename = $@"{databasePath}{leagueName}.ifm";

            _db = new SQLiteConnection(_databaseFilename);
            _db.CreateTable<League>();
            _db.CreateTable<Team>();
            _db.CreateTable<Player>();
            _db.CreateTable<DepthChart>();
            _db.CreateTable<GeneralManager>();
            _db.CreateTable<PlayerStats>();
            _db.CreateTable<PlayerPlayoffStats>();
            _db.CreateTable<DraftPick>();
            _db.CreateTable<Draft>();
            _db.CreateTable<Strategy>();
            _db.CreateTable<Game>();
            _db.CreateTable<Email>();
            _db.CreateTable<ContractOffer>();
            _db.CreateTable<StandingsHistory>();
            _db.CreateTable<Award>();
            _db.CreateTable<Record>();
            _db.CreateTable<Transaction>();

            return _db;
        }

        public static async Task<bool> Vacuum(this SQLiteConnection db)
        {
            return db.ExecuteScalar<bool>("VACUUM;");
        }

        public static async void Disconnect()
        {
            if (_db == null) return;
            await _db.Vacuum();
            _db.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            _db = null;
        }
        
        public static DataTable ConvertToStandingsDataTable(IEnumerable<Team> teams)
        {
            var dt = new DataTable();

            dt.Columns.Add("TeamName", typeof(string));
            dt.Columns.Add("Wins", typeof(int));
            dt.Columns.Add("Losses", typeof(int));
            dt.Columns.Add("WinPct", typeof(string));
            dt.Columns.Add("PointsFor", typeof(int));
            dt.Columns.Add("PointsAgainst", typeof(int));
            dt.Columns.Add("HomeRecord", typeof(string));
            dt.Columns.Add("AwayRecord", typeof(string));
            dt.Columns.Add("DivRecord", typeof(string));
            dt.Columns.Add("ConfRecord", typeof(string));
            dt.Columns.Add("Division", typeof(string));
            dt.Columns.Add("Conference", typeof(string));

            var values = new object[12];
            foreach (var team in teams)
            {
                values[0] = team.TeamName;
                values[1] = team.Wins;
                values[2] = team.Losses;
                values[3] = team.WinPct.ToString("N3");
                values[4] = team.PointsFor;
                values[5] = team.PointsAgainst;
                values[6] = team.HomeRecord;
                values[7] = team.AwayRecord;
                values[8] = team.DivRecord;
                values[9] = team.ConfRecord;
                values[10] = team.Division;
                values[11] = team.Conference;
                dt.Rows.Add(values);
            }
            return dt;
        }

        public static DataTable ConvertToHistoricalStandingsDataTable(IEnumerable<StandingsHistory> teams)
        {
            var dt = new DataTable();

            dt.Columns.Add("TeamName", typeof(string));
            dt.Columns.Add("Wins", typeof(int));
            dt.Columns.Add("Losses", typeof(int));
            dt.Columns.Add("WinPct", typeof(string));
            dt.Columns.Add("PointsFor", typeof(int));
            dt.Columns.Add("PointsAgainst", typeof(int));
            dt.Columns.Add("HomeRecord", typeof(string));
            dt.Columns.Add("AwayRecord", typeof(string));
            dt.Columns.Add("DivRecord", typeof(string));
            dt.Columns.Add("ConfRecord", typeof(string));
            dt.Columns.Add("Division", typeof(string));
            dt.Columns.Add("Conference", typeof(string));

            var values = new object[12];
            foreach (var team in teams)
            {
                values[0] = team.TeamName;
                values[1] = team.Wins;
                values[2] = team.Losses;
                values[3] = team.WinPct.ToString("N3");
                values[4] = team.PointsFor;
                values[5] = team.PointsAgainst;
                values[6] = team.HomeRecord;
                values[7] = team.AwayRecord;
                values[8] = team.DivRecord;
                values[9] = team.ConfRecord;
                values[10] = team.Division;
                values[11] = team.Conference;
                dt.Rows.Add(values);
            }
            return dt;
        }
        
        public static DataTable ConvertTeamsToTable(IEnumerable<Team> teams)
        {
            var dt = new DataTable { TableName = "Teams" };
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("City", typeof(string));
            dt.Columns.Add("Mascot", typeof(string));
            dt.Columns.Add("Abbreviation", typeof(string));
            dt.Columns.Add("Conference", typeof(string));
            dt.Columns.Add("Division", typeof(string));
            dt.Columns.Add("Wins", typeof(int));
            dt.Columns.Add("Losses", typeof(int));
            dt.Columns.Add("PointsFor", typeof(int));
            dt.Columns.Add("PointsAgainst", typeof(int));
            dt.Columns.Add("DivWins", typeof(int));
            dt.Columns.Add("DivLosses", typeof(int));
            dt.Columns.Add("ConfWins", typeof(int));
            dt.Columns.Add("ConfLosses", typeof(int));
            dt.Columns.Add("HomeWins", typeof(int));
            dt.Columns.Add("HomeLosses", typeof(int));
            dt.Columns.Add("AwayWins", typeof(int));
            dt.Columns.Add("AwayLosses", typeof(int));
            dt.Columns.Add("Streak", typeof(string));
            dt.Columns.Add("CPU", typeof(bool));
            dt.Columns.Add("PrimaryColor", typeof(string));
            dt.Columns.Add("SecondaryColor", typeof(string));
            dt.Columns.Add("ExportStatus", typeof(string));
            dt.Columns.Add("ExportTime", typeof(string));
            dt.Columns.Add("GameVersion", typeof(string));

            var values = new object[25];
            foreach(var t in teams)
            {
                values[0] = t.Id;
                values[1] = t.City;
                values[2] = t.Mascot;
                values[3] = t.Abbreviation;
                values[4] = t.Conference;
                values[5] = t.Division;
                values[6] = t.Wins;
                values[7] = t.Losses;
                values[8] = t.PointsFor;
                values[9] = t.PointsAgainst;
                values[10] = t.DivWins;
                values[11] = t.DivLosses;
                values[12] = t.ConfWins;
                values[13] = t.ConfLosses;
                values[14] = t.HomeWins;
                values[15] = t.HomeLosses;
                values[16] = t.AwayWins;
                values[17] = t.AwayLosses;
                values[18] = t.StreakTxt;
                values[19] = t.IsCpu;
                values[20] = t.PrimaryColor;
                values[21] = t.SecondaryColor;
                values[22] = t.ExportStatus;
                values[23] = t.ExportTime;
                values[24] = t.GameVersion;

                dt.Rows.Add(values);
            }
            return dt;
        }

        public static DataTable ConvertManagersToTable(IEnumerable<GeneralManager> managers)
        {
            var dt = new DataTable { TableName = "Managers" };
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));
            dt.Columns.Add("DOB", typeof(string));
            dt.Columns.Add("TeamId", typeof(int));
            dt.Columns.Add("TeamName", typeof(string));
            dt.Columns.Add("Commissioner", typeof(bool));

            var values = new object[7];
            foreach(var m in managers)
            {
                values[0] = m.Id;
                values[1] = m.FirstName;
                values[2] = m.LastName;
                values[3] = m.DOB.ToShortDateString();
                values[4] = m.TeamId;
                values[5] = m.TeamId == 0 ? "No Team" : m.Team.TeamName;
                values[6] = m.IsCommissioner;

                dt.Rows.Add(values);
            }
            return dt;
        }

        public static DataTable ConvertPlayersToDataTable(IEnumerable<Player> players)
        {
            var dt = new DataTable { TableName = "Players" };
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Jersey", typeof(int));
            dt.Columns.Add("First", typeof(string));
            dt.Columns.Add("Last", typeof(string));
            dt.Columns.Add("TeamId", typeof(int));
            dt.Columns.Add("Pos", typeof(string));
            dt.Columns.Add("DOB", typeof(string));
            dt.Columns.Add("Exp", typeof(int));
            dt.Columns.Add("Acc", typeof(int));
            dt.Columns.Add("Arm", typeof(int));
            dt.Columns.Add("Car", typeof(int));
            dt.Columns.Add("Spd", typeof(int));
            dt.Columns.Add("Str", typeof(int));
            dt.Columns.Add("Cth", typeof(int));
            dt.Columns.Add("Blk", typeof(int));
            dt.Columns.Add("Blz", typeof(int));
            dt.Columns.Add("Cov", typeof(int));
            dt.Columns.Add("Tck", typeof(int));
            dt.Columns.Add("Int", typeof(int));
            dt.Columns.Add("KSt", typeof(int));
            dt.Columns.Add("KAc", typeof(int));
            dt.Columns.Add("Att", typeof(int));
            dt.Columns.Add("Grd", typeof(int));
            dt.Columns.Add("Loy", typeof(int));
            dt.Columns.Add("Inj", typeof(int));
            dt.Columns.Add("Ldr", typeof(int));
            dt.Columns.Add("Pas", typeof(int));
            dt.Columns.Add("Wrk", typeof(int));
            dt.Columns.Add("Yrs", typeof(int));
            dt.Columns.Add("Salary", typeof(int));
            dt.Columns.Add("College", typeof(string));
            dt.Columns.Add("Overall", typeof(int));

            var values = new object[32];
            foreach (var p in players)
            {
                values[0] = p.Id;
                values[1] = p.Jersey;
                values[2] = p.FirstName;
                values[3] = p.LastName;
                values[4] = p.TeamId;
                values[5] = p.Pos;
                values[6] = p.DOB.ToShortDateString();
                values[7] = p.Exp;
                values[8] = p.Accuracy;
                values[9] = p.ArmStrength;
                values[10] = p.Carry;
                values[11] = p.Speed;
                values[12] = p.Strength;
                values[13] = p.Catching;
                values[14] = p.Block;
                values[15] = p.Blitz;
                values[16] = p.Coverage;
                values[17] = p.Tackle;
                values[18] = p.Intelligence;
                values[19] = p.KickStrength;
                values[20] = p.KickAccuracy;
                values[21] = p.Attitude;
                values[22] = p.Greed;
                values[23] = p.Loyalty;
                values[24] = p.Injury;
                values[25] = p.Leadership;
                values[26] = p.Passion;
                values[27] = p.WorkEthic;
                values[28] = p.ContractYears;
                values[29] = p.Salary;
                values[30] = p.College;
                values[31] = p.Overall;

                dt.Rows.Add(values);
            }
            return dt;
        }

        public static DataTable ConvertToDataTable<TSource>(IEnumerable<TSource> source)
        {
            var props = typeof(TSource).GetProperties();
            var dt = new DataTable();
            dt.Columns.AddRange(props.Select(p => new DataColumn(p.Name, p.PropertyType)).ToArray());
            source.ToList().ForEach(i => dt.Rows.Add(props.Select(p => p.GetValue(i, null)).ToArray()));
            return dt;
        }

        public static IEnumerable<Game> GetAllGames(this SQLiteConnection db) => db.Table<Game>();
        public static IEnumerable<Game> GetGames(this SQLiteConnection db, int season) => db.Table<Game>().Where(g => g.Season == season);
        public static IEnumerable<Game> GetGamesByTeamId(this SQLiteConnection db, int season, int id) => db.Table<Game>().Where(g => (g.HomeTeam == id || g.AwayTeam == id) && g.Season == season);
        public static IEnumerable<Game> GetTvTGames(this SQLiteConnection db, int season, Team team1, Team team2)
        {
            var team1Home = db.Table<Game>().Where(g => g.HomeTeam == team1.Id && g.AwayTeam == team2.Id && g.HasPlayed && g.Season == season);
            var team1Away = db.Table<Game>().Where(g => g.AwayTeam == team1.Id && g.HomeTeam == team2.Id && g.HasPlayed && g.Season == season);
            return team1Home.Concat(team1Away);
        }

        public static Game GetGame(this SQLiteConnection db, int id) => db.Table<Game>().FirstOrDefault(g => g.Id == id);
        public static void SaveGame(this SQLiteConnection db, Game g)
        {
            var saved = false;

            if (g.Id > 0)
                saved = db.Update(g) == 1;

            if (!saved)
                db.Insert(g);
        }

        public static IEnumerable<Player> GetPlayers(this SQLiteConnection db) => db.Table<Player>();
        public static IEnumerable<Player> GetFilteredPlayers(this SQLiteConnection db, int[] exp, int[] arm, int[] acc, int[] str, int[] car, int[] spd, int[] cat, int[] blk, int[] blz, int[] cov, int[] tac, 
            int[] intl, int[] kst, int[] kac, int[] att, int[] grd, int[] loy, int[] inj, int[] lds, int[] pas, int[] wrk)
        {
            var expMin = exp[0]; var expMax = exp[1];
            var armMin = arm[0]; var armMax = arm[1];
            var accMin = acc[0]; var accMax = acc[1];
            var strMin = str[0]; var strMax = str[1];
            var carMin = car[0]; var carMax = car[1];
            var spdMin = spd[0]; var spdMax = spd[1];
            var catMin = cat[0]; var catMax = cat[1];
            var blkMin = blk[0]; var blkMax = blk[1];  
            var blzMin = blz[0]; var blzMax = blz[1];  
            var covMin = cov[0]; var covMax = cov[1];  
            var tacMin = tac[0]; var tacMax = tac[1];  
            var intlMin = intl[0]; var intlMax = intl[1];  
            var kstMin = kst[0]; var kstMax = kst[1];  
            var kacMin = kac[0]; var kacMax = kac[1];  
            var attMin = att[0]; var attMax = att[1];  
            var grdMin = grd[0]; var grdMax = grd[1];  
            var loyMin = loy[0]; var loyMax = loy[1];  
            var injMin = inj[0]; var injMax = inj[1];  
            var ldsMin = lds[0]; var ldsMax = lds[1];  
            var pasMin = pas[0]; var pasMax = pas[1];
            var wrkMin = wrk[0]; var wrkMax = wrk[1];
            return db.Table<Player>()
                .Where(p => p.Exp >= expMin && p.Exp <= expMax)
                .Where(p => p.ArmStrength >= armMin && p.ArmStrength <= armMax)
                .Where(p => p.Accuracy >= accMin && p.Accuracy <= accMax)
                .Where(p => p.Strength >= strMin && p.Strength <= strMax)
                .Where(p => p.Carry >= carMin && p.Carry <= carMax)
                .Where(p => p.Speed >= spdMin && p.Speed <= spdMax)
                .Where(p => p.Catching >= catMin && p.Catching <= catMax)
                .Where(p => p.Block >= blkMin && p.Block <= blkMax)
                .Where(p => p.Blitz >= blzMin && p.Blitz <= blzMax)
                .Where(p => p.Coverage >= covMin && p.Coverage <= covMax)
                .Where(p => p.Tackle >= tacMin && p.Tackle <= tacMax)
                .Where(p => p.Intelligence >= intlMin && p.Intelligence <= intlMax)
                .Where(p => p.KickStrength >= kstMin && p.KickStrength <= kstMax)
                .Where(p => p.KickAccuracy >= kacMin && p.KickAccuracy <= kacMax)
                .Where(p => p.Attitude >= attMin && p.Attitude <= attMax)
                .Where(p => p.Greed >= grdMin && p.Greed <= grdMax)
                .Where(p => p.Loyalty >= loyMin && p.Loyalty <= loyMax)
                .Where(p => p.Injury >= injMin && p.Injury <= injMax)
                .Where(p => p.Leadership >= ldsMin && p.Leadership <= ldsMax)
                .Where(p => p.Passion >= pasMin && p.Passion <= pasMax)
                .Where(p => p.WorkEthic >= wrkMin && p.WorkEthic <= wrkMax);
        }

        public static IEnumerable<Player> GetPlayersByTeamId(this SQLiteConnection db, int teamId) => db.Table<Player>().Where(p => p.TeamId == teamId);
        public static IEnumerable<Player> GetPlayersByPosition(this SQLiteConnection db, string pos) => db.Table<Player>().Where(p => p.Pos == pos);
        public static IEnumerable<Player> GetRookiesByPosition(this SQLiteConnection db, string pos) => db.Table<Player>().Where(p => p.TeamId == -1 && p.Pos == pos);
        public static Player GetPlayerById(this SQLiteConnection db, int id) => db.Table<Player>().FirstOrDefault(p => p.Id == id);
        public static Player GetPlayerByName(this SQLiteConnection db, string name) => db.Table<Player>().FirstOrDefault(p => p.Name == name);
        public static Player GetPlayerByNameAndPosition(this SQLiteConnection db, string name) => db.Table<Player>().FirstOrDefault(p => p.NameWithPosition == name);
        public static void SavePlayer(this SQLiteConnection db, Player player)
        {
            var saved = false;

            if (player.Id > 0)
                saved = db.Update(player) == 1; 

            if (!saved)
                db.Insert(player);
        }

        public static void UpdateAllPlayers(this SQLiteConnection db, IEnumerable<Player> players) => db.UpdateAll(players);
        public static void InsertAllPlayers(this SQLiteConnection db, IEnumerable<Player> players) => db.InsertAll(players);

        public static DepthChart GetDepthChart(this SQLiteConnection db, int id) => db.Table<DepthChart>().FirstOrDefault(d => d.TeamId == id);
        public static void SaveDepthChart(this SQLiteConnection db, DepthChart dc)
        {
            var saved = false;

            if (dc.TeamId > 0)
                saved = db.Update(dc) == 1;
            if (!saved)
                db.Insert(dc);
        }

        public static IEnumerable<GeneralManager> GetManagers(this SQLiteConnection db) => db.Table<GeneralManager>();
        public static GeneralManager GetManagerById(this SQLiteConnection db, int id) => db.Table<GeneralManager>().FirstOrDefault(g => g.Id == id);
        public static GeneralManager GetManagerByTeamId(this SQLiteConnection db, int teamId) => db.Table<GeneralManager>().FirstOrDefault(g => g.TeamId == teamId);
        public static void SaveManager(this SQLiteConnection db, GeneralManager gm)
        {
            var saved = false;

            if (gm.Id > 0)
                saved = db.Update(gm) == 1;

            if (!saved)
                db.Insert(gm);
        }

        public static IEnumerable<Team> GetTeams(this SQLiteConnection db) => db.Table<Team>();
        public static IEnumerable<Team> GetTeamsForStandings(this SQLiteConnection db) => db.Table<Team>().OrderByDescending(t => t.WinPct); // will need to add more sort parameters
        public static IEnumerable<Team> GetTeamsForDivisionStandings(this SQLiteConnection db, string conference, string division)
        {
            var divTeams = db.GetTeams().Where(t => t.Conference == conference && t.Division == division);
            var teams = divTeams.OrderByDescending(t => t.WinPct).ThenByDescending(t => t.DivPct).ToList();
            var updated = new List<Team>();
            var teamsCount = teams.Count;
            for (var i = 0; i < teamsCount; i++)
            {
                var team = teams.Any() ? teams[i] : null;
                if (team == null) // reached end of division
                    break;

                var team2 = teams.Count() > 1 ? teams[i + 1] : null;
                if (team2 == null) // reached end of division
                {
                    updated.Add(team);
                    teams.Remove(team);
                    break;
                }

                if (team.WinPct > team2.WinPct)
                {
                    updated.Add(team);
                    teams.Remove(team);
                    teamsCount--;
                    i--;
                    continue;
                }

                if (team.DivPct == team2.DivPct)
                {
                    var cols = team.VsOpponent(team2).Split('-');
                    var wins = Convert.ToInt32(cols[0]);
                    var losses = Convert.ToInt32(cols[1]);
                    //var winPct = Convert.ToDouble(wins / (wins + losses));
                    if (wins > losses)
                    {
                        updated.Add(team);
                        updated.Add(team2);
                    }
                    else if (losses > wins)
                    {
                        updated.Add(team2);
                        updated.Add(team);
                    }
                    else // losses == wins, move on to next tiebreaker
                    {
                        if (team.ConfPct > team2.ConfPct)
                        {
                            updated.Add(team);
                            updated.Add(team2);
                        }
                        else if (team2.ConfPct > team2.ConfPct)
                        {
                            updated.Add(team2);
                            updated.Add(team);
                        }
                        else // another tie...
                        {
                            if (team.PointsFor > team2.PointsFor)
                            {
                                updated.Add(team);
                                updated.Add(team2);
                            }
                            else if (team2.PointsFor > team.PointsFor)
                            {
                                updated.Add(team2);
                                updated.Add(team);
                            }
                            else // yet another tie...
                            {
                                if (team.PointsAgainst < team2.PointsAgainst)
                                {
                                    updated.Add(team);
                                    updated.Add(team2);
                                }
                                else if (team2.PointsAgainst < team.PointsAgainst)
                                {
                                    updated.Add(team2);
                                    updated.Add(team);
                                }
                                else
                                {
                                    if (team.SOS > team2.SOS)
                                    {
                                        updated.Add(team);
                                        updated.Add(team2);
                                    }
                                    else
                                    {
                                        updated.Add(team2);
                                        updated.Add(team);
                                    }
                                }
                            }
                        }
                    }
                    teams.Remove(team);
                    teams.Remove(team2);
                }
                else // team 1 is not tied with team 2, add team 1 and move on
                {
                    updated.Add(team);
                    teams.Remove(team);
                }
                teamsCount--;
                i--;
            }

            return updated.AsEnumerable();
        }

        public static IEnumerable<Team> GetTeamsForConferenceStandings(this SQLiteConnection db, string conference)
        {
            var confTeams = db.GetTeams().Where(t => t.Conference == conference);
            var teams = confTeams.OrderByDescending(t => t.WinPct).ThenByDescending(t => t.ConfPct).ToList();
            var updated = new List<Team>();
            var teamsCount = teams.Count();
            for (var i = 0; i < teamsCount; i++)
            {
                var team = teams.Any() ? teams[i] : null;
                if (team == null) // reached end of conference
                    break;

                Team team2 = null;
                var teams2 = teams.Where(t => t.WinPct == team.WinPct && t.Id != team.Id).ToList();
                if (teams2.Any()) // teams with the same win pct
                {
                    if (teams2.Count() == 1) // just 2 are tied
                    {
                        team2 = teams2[0];
                        var cols = team.VsOpponent(team2).Split('-');
                        var wins = Convert.ToInt32(cols[0]);
                        var losses = Convert.ToInt32(cols[1]);
                        //var winPct = Convert.ToDouble(wins / (wins + losses));
                        if (wins > losses) // team 1 has the upper hand
                        {
                            updated.Add(team);
                            updated.Add(team2);
                        }
                        else if (losses > wins)
                        {
                            updated.Add(team2);
                            updated.Add(team);
                        }
                        else if (team.ConfPct > team2.ConfPct)
                        {
                            updated.Add(team);
                            updated.Add(team2);
                        }
                        else if (team2.ConfPct > team.ConfPct)
                        {
                            updated.Add(team2);
                            updated.Add(team);
                        }
                        else
                        {
                            if (team.Division == team2.Division && team.DivPct > team2.DivPct)
                            {
                                updated.Add(team);
                                updated.Add(team2);
                            }
                            else if (team.Division == team2.Division && team2.DivPct > team.DivPct)
                            {
                                updated.Add(team2);
                                updated.Add(team);
                            }
                            else
                            {
                                if (team.PointsFor > team2.PointsFor)
                                {
                                    updated.Add(team);
                                    updated.Add(team2);
                                }
                                else if (team2.PointsFor > team.PointsFor)
                                {
                                    updated.Add(team2);
                                    updated.Add(team);
                                }
                                else // yet another tie...
                                {
                                    if (team.PointsAgainst < team2.PointsAgainst)
                                    {
                                        updated.Add(team);
                                        updated.Add(team2);
                                    }
                                    else if (team2.PointsAgainst < team.PointsAgainst)
                                    {
                                        updated.Add(team2);
                                        updated.Add(team);
                                    }
                                    else
                                    {
                                        if (team.SOS > team2.SOS)
                                        {
                                            updated.Add(team);
                                            updated.Add(team2);
                                        }
                                        else
                                        {
                                            updated.Add(team2);
                                            updated.Add(team);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else // more than 2 teams are tied
                    {
                        var tiedTeams = teams.Where(t => t.WinPct == team.WinPct).ToList();
                        var sameDivision = false;
                        for(var j = 0; j < tiedTeams.Count(); j++)
                        {
                            if (j >= tiedTeams.Count() - 1 || tiedTeams[j].Division != tiedTeams[j + 1].Division)
                                continue;
                            sameDivision = true;
                            team = tiedTeams[j];
                        }
                        var divTeams = sameDivision ? tiedTeams.Where(t => t.Division == team.Division && t.Id != team.Id).ToList() : null;
                        if (divTeams != null && divTeams.Any()) // teams in the same division... break the tie between top two and move on to next
                        {
                            team2 = divTeams[0];
                            var cols = team.VsOpponent(team2).Split('-');
                            var wins = Convert.ToInt32(cols[0]);
                            var losses = Convert.ToInt32(cols[1]);
                            var winPct = Convert.ToDouble(wins / (wins + losses));
                            if (wins > 0) // team 1 has the upper hand
                            {
                                updated.Add(team);
                                team2 = null;
                            }
                            else
                            {
                                if (losses > 0)
                                {
                                    updated.Add(team2);
                                    team = null;
                                }
                                else
                                {
                                    if (team.DivPct > team2.DivPct) // team 1 had better div pct
                                    {
                                        updated.Add(team);
                                        team2 = null;
                                    }
                                    else if (team2.DivPct > team.DivPct) // team 2 had better div pct
                                    {
                                        updated.Add(team2);
                                        team = null;
                                    }
                                    else
                                    {
                                        if (team.ConfPct > team2.ConfPct) // go by conference percentage next
                                        {
                                            updated.Add(team);
                                            team2 = null;
                                        }
                                        else if (team2.ConfPct > team.ConfPct)
                                        {
                                            updated.Add(team2);
                                            team = null;
                                        }
                                        else
                                        {
                                            if (team.PointsFor > team2.PointsFor)
                                            {
                                                updated.Add(team);
                                                team2 = null;
                                            }
                                            else if (team.PointsFor == team2.PointsFor)
                                            {
                                                if (team.PointsAgainst < team2.PointsAgainst)
                                                {
                                                    updated.Add(team);
                                                    team2 = null;
                                                }
                                                else if (team2.PointsAgainst < team.PointsAgainst)
                                                {
                                                    updated.Add(team2);
                                                    team = null;
                                                }
                                                else
                                                {
                                                    if (team.SOS > team2.SOS)
                                                    {
                                                        updated.Add(team);
                                                        team2 = null;
                                                    }
                                                    else
                                                    {
                                                        updated.Add(team2);
                                                        team = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                updated.Add(team2);
                                                team = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else // sort by conf record, pointsfor, pointsagainst, sos and be done...
                        {
                            var sortedTeams = teams.Where(t => t.WinPct == team.WinPct).OrderByDescending(t => t.ConfPct)
                                .ThenByDescending(t => t.PointsFor).ThenBy(t => t.PointsAgainst)
                                .ThenByDescending(t => t.SOS).ToList();
                            updated.AddRange(sortedTeams);
                            team = null;
                            team2 = null;
                            teams.RemoveAll(t => sortedTeams.Contains(t));
                        }
                    }
                }
                else
                {
                    updated.Add(team);
                }
                if (team != null) { teams.Remove(team); }
                if (team2 != null) { teams.Remove(team2); }
                teamsCount--;
                i--;
            }

            return updated.AsEnumerable();
        }

        public static Team GetTeamById(this SQLiteConnection db, int id) => db.Table<Team>().FirstOrDefault(t => t.Id == id);
        public static Team GetTeamByName(this SQLiteConnection db, string teamName) => db.Table<Team>().FirstOrDefault(t => t.TeamName == teamName);
        public static Team GetTeamByAbbreviation(this SQLiteConnection db, string abbreviation) =>db.Table<Team>().FirstOrDefault(t => t.Abbreviation == abbreviation);
        public static void SaveTeam(this SQLiteConnection db, Team team)
        {
            var saved = false;

            if (team.Id > 0)
                saved = db.Update(team) == 1;

            if (!saved)
                db.Insert(team);
        }

        public static void UpdateAllTeams(this SQLiteConnection db, IEnumerable<Team> teams) => db.UpdateAll(teams);

        public static void ExportTeam(this SQLiteConnection db, int id, string leagueName)
        {
            var gm = db.Table<GeneralManager>().FirstOrDefault(g => g.TeamId == id);
            var team = db.Table<Team>().FirstOrDefault(t => t.Id == id);
            team.GameVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            team.ExportTime = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            SaveTeam(db, team);
            var players1 = db.Table<Player>().Where(p => p.TeamId == 0).ToList();
            players1.RemoveAll(p => !Converters.PlayerWasReleased(p.PlayerHistoryJson, team.TeamName, team.CurDate.ToShortDateString()));
            var players = db.Table<Player>().Where(p => p.TeamId == id).Concat(players1);
            var contractOffers = db.Table<ContractOffer>().Where(o => o.TeamId == id);
            var emails = db.Table<Email>().Where(e => e.SenderId == gm.Id && !e.Read);
            var transactions = db.Table<Transaction>().Where(x => x.TeamId == id);
            var depthChart = db.Table<DepthChart>().FirstOrDefault(dc => dc.TeamId == id);
            var strategy = db.Table<Strategy>().FirstOrDefault(s => s.Id == id);

            var exportsPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Exports\";
            Directory.CreateDirectory(exportsPath);

            var filename = $@"{exportsPath}{id}.team";
            
            if (File.Exists(filename))
                File.Delete(filename);

            var export = new SQLiteConnection(filename);
            export.CreateTable<GeneralManager>();
            export.CreateTable<Team>();
            export.CreateTable<Player>();
            export.CreateTable<ContractOffer>();
            export.CreateTable<Email>();
            export.CreateTable<Transaction>();
            export.CreateTable<Strategy>();
            export.CreateTable<DepthChart>();

            export.Insert(gm);
            export.Insert(team);
            export.InsertAll(players);
            export.InsertAll(contractOffers);
            export.InsertAll(emails);
            export.InsertAll(transactions);
            export.Insert(strategy);
            export.Insert(depthChart);
            
            export.Close();
        }

        public static void ImportTeam(this SQLiteConnection db, int id, string leagueName)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Exports\";
            Directory.CreateDirectory(databasePath);

            var filename = $@"{databasePath}{id}.team";
            var export = new SQLiteConnection(filename);
            var gm = GetManagerByTeamId(export, id);
            var exportTeam = GetTeamById(export, id);
            var team = GetTeamById(db, id);

            if (exportTeam.CurDate != GetLeague(db, 1).CurDate)
            {
                team.ExportStatus = "Invalid File/Old Export";
                team.ExportTime = exportTeam.ExportTime;
                db.SaveTeam(team);
                return;
            }

            if (exportTeam.GameVersion != Assembly.GetExecutingAssembly().GetName().Version.ToString())
            {
                team.ExportStatus = "Wrong Game Version";
                team.ExportTime = exportTeam.ExportTime;
                db.SaveTeam(team);
                return;
            }

            var players = export.Table<Player>();
            var contractOffers = export.Table<ContractOffer>().Where(o => o.TeamId == id);
            var emails = export.Table<Email>();
            var transactions = export.Table<Transaction>();
            foreach(var em in emails)
            {
                em.Read = true;
                export.SaveEmail(em);
            }
            var strategy = GetStrategy(export, id);
            var depthChart = GetDepthChart(export, id);
            team.FranchiseTagUsed = exportTeam.FranchiseTagUsed;
            team.OffPhilosophy = exportTeam.OffPhilosophy;
            team.ExportStatus = "Successful";
            team.ExportTime = exportTeam.ExportTime;
            db.SaveTeam(team);
            db.Update(gm);
            db.UpdateAll(players);
            foreach (var c in contractOffers)
            {
                db.SaveContract(c);
            }
            var curTransactions = GetTransactions(db, team.Season).ToList();
            var newTransactions = (from t in transactions let x = curTransactions.FirstOrDefault(l => l.Text == t.Text) where x is null select t).ToList();
            db.InsertAll(newTransactions);
            db.InsertAll(emails);
            db.Update(strategy);
            db.Update(depthChart);
            export.Close();
        }

        /*
        public static IEnumerable<Coach> GetCoaches(SQLiteConnection db) => db.Table<Coach>();
        public static Coach GetCoach(SQLiteConnection db, int id) => db.Table<Coach>().FirstOrDefault(c => c.Id == id);
        public static void SaveCoach(SQLiteConnection db, Coach coach)
        {
            var saved = false;

            if (coach.Id > 0)
                saved = db.Update(coach) == 1;

            if (!saved)
                db.Insert(coach);
        }
        */

        public static League GetLeague(this SQLiteConnection db, int id) => db.Table<League>().FirstOrDefault(l => l.Id == id);
        public static void SaveLeague(this SQLiteConnection db, League league)
        {
            var saved = false;

            if (league.Id > 0)
                saved = db.Update(league) == 1;

            if (!saved)
                db.Insert(league);
        }

        public static IEnumerable<Draft> GetDraft(this SQLiteConnection db, int season) => db.Table<Draft>().Where(d => d.Season == season);
        public static Draft GetDraftByPickId(this SQLiteConnection db, int id) => db.Table<Draft>().FirstOrDefault(d => d.DraftPickId == id);
        public static double GetLastDraftSalary(this SQLiteConnection db, int season)
        {
            var lastDraftSalary = GetDraft(db, season).OrderBy(d => d.Salary).FirstOrDefault(d => d.Salary != null);
            if (lastDraftSalary == null)
                return db.GetLeague(1).MaxDraftSalary;
            var salary = lastDraftSalary.Salary;
            return Convert.ToDouble(salary.TrimStart('$'));
        }

        public static void SaveDraft(this SQLiteConnection db, Draft draft)
        {
            var saved = false;

            if (draft.Id > 0)
                saved = db.Update(draft) == 1;

            if (!saved)
                db.Insert(draft);
        }

        public static void UpdateAllDrafts(this SQLiteConnection db, IEnumerable picks) => db.UpdateAll(picks);
        public static IEnumerable<DraftPick> GetPicks(this SQLiteConnection db) => db.Table<DraftPick>();
        public static IEnumerable<DraftPick> GetPicksByTeam(this SQLiteConnection db, int teamId) => db.Table<DraftPick>().Where(d => d.CurrentTeamId == teamId);
        public static DraftPick GetPickById(this SQLiteConnection db, int id) => db.Table<DraftPick>().FirstOrDefault(d => d.Id == id);
        public static void DeletePicks(this SQLiteConnection db, int season)
        {
            var picks = db.Table<DraftPick>().Where(d => d.Season == season).AsEnumerable();
            foreach (var d in picks)
                db.Delete<DraftPick>(d.Id);
        }

        public static void DeleteAllPicks(this SQLiteConnection db, List<int> picks) => db.Table<DraftPick>().Delete(pick => picks.Contains(pick.Id));
        public static void DeleteDraftPick(this SQLiteConnection db, DraftPick pick) => db.Delete(pick.Id);
        public static void InsertAllPicks(this SQLiteConnection db, IEnumerable<DraftPick> picks) => db.InsertAll(picks);
       public static void SaveDraftPick(this SQLiteConnection db, DraftPick pick)
        {
            var saved = false;

            if (pick.Id > 0)
                saved = db.Update(pick) == 1;

            if (!saved)
                db.Insert(pick);
        }

        public static Strategy GetStrategy(this SQLiteConnection db, int teamId) => db.Table<Strategy>().FirstOrDefault(s => s.Id == teamId);
        public static void SaveStrategy(this SQLiteConnection db, Strategy strategy)
        {
            var saved = false;

            if (strategy.Id > 0)
                saved = db.Update(strategy) == 1;

            if (!saved)
                db.Insert(strategy);
        }

        public static IEnumerable<Email> GetEmails(this SQLiteConnection db, int gmId) => db.Table<Email>().Where(e => e.GmId == gmId);
        public static Email GetEmail(this SQLiteConnection db, int id) => db.Table<Email>().FirstOrDefault(e => e.Id == id);
        public static void DeleteEmail(this SQLiteConnection db, int id) => db.Delete<Email>(id);
        public static void SaveEmail(this SQLiteConnection db, Email email)
        {
            var saved = false;

            if (email.Id > 0)
                saved = db.Update(email) == 1;

            if (!saved)
                db.Insert(email);
        }

        public static IEnumerable<ContractOffer> GetContractOffers(this SQLiteConnection db) => db.Table<ContractOffer>();
        public static void CheckExistingOffers(this SQLiteConnection db, ContractOffer con)
        {
            var offers = db.GetContractOffers().Where(o => o.TeamId == con.TeamId).ToList();
            foreach (var offer in offers)
            {
                if (offer.PlayerId != con.PlayerId) continue;
                if (!offer.Active) continue;
                offer.Active = false;
                db.SaveContract(offer);
            }
        }

        public static IEnumerable<ContractOffer>  GetContractOffers(this SQLiteConnection db, int playerId) => db.Table<ContractOffer>().Where(o => o.PlayerId == playerId);
        public static ContractOffer GetContractOffer(this SQLiteConnection db, Team team, Player player) => db.Table<ContractOffer>().FirstOrDefault(o => o.TeamId == team.Id && o.PlayerId == player.Id);
        public static void SaveContract(this SQLiteConnection db, ContractOffer con)
        {
            var saved = false;

            if (con.Id > 0)
                saved = db.Update(con) == 1;

            if (!saved)
                db.Insert(con);
        }

        public static IEnumerable<StandingsHistory> GetHistoricalStandings(this SQLiteConnection db, int year) => db.Table<StandingsHistory>().Where(t => t.Season == year);
        public static IEnumerable<StandingsHistory> GetHistoricalDivisionStandings(this SQLiteConnection db, int year, string division) =>
            GetHistoricalStandings(db, year).Where(t => t.Division == division)
                .OrderByDescending(t => t.WinPct)
                .ThenByDescending(t => t.DivPct)
                .ThenByDescending(t => t.ConfPct);
        public static IEnumerable<StandingsHistory> GetHistoricalConferenceStandings(this SQLiteConnection db, int year, string conference) =>
            GetHistoricalStandings(db, year).Where(t => t.Conference == conference)
                .OrderByDescending(t => t.WinPct)
                .ThenByDescending(t => t.ConfPct);
        public static void SaveStandings(this SQLiteConnection db, StandingsHistory s)
        {
            var saved = false;

            if (s.Id > 0)
                saved = db.Update(s) == 1;

            if (!saved)
                db.Insert(s);
        }

        public static IEnumerable<PlayerGameStats> GetHistoricalGameStats(this SQLiteConnection db, int season) => db.Table<PlayerGameStats>().Where(g => g.Season == season);
        public static IEnumerable<PlayerGameStats> GetAllGameStats(this SQLiteConnection db, int season, bool playoffs) => db.Table<PlayerGameStats>().Where(g => g.Season == season && g.Playoffs == playoffs);
        public static IEnumerable<PlayerGameStats> GetPlayerGameStats(this SQLiteConnection db, int season, int id) => db.Table<PlayerGameStats>().Where(g => g.Season == season && g.PlayerId == id);
        public static IEnumerable<PlayerGameStats> GetTeamGameStats(this SQLiteConnection db, int season, string abbr, bool playoffs) => db.Table<PlayerGameStats>().Where(g => g.Season == season && g.Team == abbr && g.Playoffs == playoffs);
        public static IEnumerable<PlayerGameStats> GetGameStats(this SQLiteConnection db, int gameId) => db.Table<PlayerGameStats>().Where(g => g.GameId == gameId);
        public static void SavePlayerGameStats(this SQLiteConnection db, PlayerGameStats pgs)
        {
            var saved = false;

            if (pgs.Id > 0)
                saved = db.Update(pgs) == 1;

            if (!saved)
                db.Insert(pgs);
        }

        public static IEnumerable<PlayerStats> GetPlayerCareerStats(this SQLiteConnection db, int pid) => db.Table<PlayerStats>().Where(s => s.PlayerId == pid);
        public static IEnumerable<PlayerStats> GetAllPlayerCareerStats(this SQLiteConnection db) => db.Table<PlayerStats>();
        public static IEnumerable<PlayerStats> GetAllPlayerStats(this SQLiteConnection db, int season) => db.Table<PlayerStats>().Where(s => s.Season == season);
        public static IEnumerable<PlayerStats> GetTeamStats(this SQLiteConnection db, int season, string abbr) => db.Table<PlayerStats>().Where(s => s.Season == season && s.Team == abbr);
        public static void SavePlayerStats(this SQLiteConnection db, PlayerStats player)
        {
            var saved = false;

            if (player.Id > 0)
                saved = db.Update(player) == 1;

            if (!saved)
                db.Insert(player);
        }

        public static IEnumerable<PlayerPlayoffStats> GetAllPlayerPlayoffStats(this SQLiteConnection db, int season) => db.Table<PlayerPlayoffStats>().Where(s => s.Season == season);
        public static IEnumerable<PlayerPlayoffStats> GetTeamPlayoffStats(this SQLiteConnection db, int season, string abbr) => db.Table<PlayerPlayoffStats>().Where(s => s.Season == season && s.Team == abbr);
        public static PlayerPlayoffStats GetPlayerPlayoffStats(this SQLiteConnection db, int id) => db.Table<PlayerPlayoffStats>().FirstOrDefault(p => p.PlayerId == id);
        public static void SavePlayerPlayoffStats(this SQLiteConnection db, PlayerPlayoffStats player)
        {
            var saved = false;

            if (player.Id > 0)
                saved = db.Update(player) == 1;

            if (!saved)
                db.Insert(player);
        }

        public static IEnumerable<Record> GetGameRecords(this SQLiteConnection db) => db.Table<Record>().Where(r => r.Type == 0);
        public static Record GetRecord(this SQLiteConnection db, int id) => db.Table<Record>().First(r => r.Id == id);
        public static IEnumerable<Record> GetSeasonRecords(this SQLiteConnection db) => db.Table<Record>().Where(r => r.Type == 1);
        public static IEnumerable<Record> GetCareerRecords(this SQLiteConnection db) => db.Table<Record>().Where(r => r.Type == 2);
        public static void SaveRecord(this SQLiteConnection db, Record record)
        {
            var saved = false;

            if (record.Id > 0)
                saved = db.Update(record) == 1;

            if (!saved)
                db.Insert(record);
        }

        public static IEnumerable<Transaction> GetTransactions(this SQLiteConnection db, int season) => db.Table<Transaction>().Where(x => x.Season == season);
        public static IEnumerable<Transaction> GetTeamTransactions(this SQLiteConnection db, int teamId, int season) => db.Table<Transaction>().Where(x => x.TeamId == teamId && x.Season == season);
        public static void SaveTransaction(this SQLiteConnection db, Transaction transaction)
        {
            var saved = false;

            if (transaction.Id > 0)
                saved = db.Update(transaction) == 1;

            if (!saved)
                db.Insert(transaction);
        }

        public static IEnumerable<int> GetTransactionSeasons(this SQLiteConnection db) =>
            db.Table<Transaction>().Select(x => x.Season).Distinct();

        public static void DeleteTransactions(this SQLiteConnection db, int season) => db.Table<Transaction>().Delete(t => t.Season == season);

    }
}

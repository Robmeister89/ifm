﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using IndoorFootballManager.Models;
using excel = Microsoft.Office.Interop.Excel;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.DataAccess
{
    internal class LeagueData
    {
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        public static void ExportTeamsList(IEnumerable<Team> teams)
        {
            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var filePath = $"{databasePath}{leagueName}_Teams.csv";
            if (File.Exists(filePath))
                File.Delete(filePath);

            var teamsList = Repo.ConvertTeamsToTable(teams);
            var sb = new StringBuilder();
            var columnNames = teamsList.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in teamsList.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(filePath, sb.ToString());
            App.ShowNotification("Success", "Teams Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportManagersList(IEnumerable<GeneralManager> managers)
        {
            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var filePath = $"{databasePath}{leagueName}_Managers.csv";
            if (File.Exists(filePath))
                File.Delete(filePath);

            var teamsList = Repo.ConvertManagersToTable(managers);

            var sb = new StringBuilder();
            var columnNames = teamsList.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in teamsList.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(filePath, sb.ToString());
            App.ShowNotification("Success", "Managers Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportDraft(IEnumerable<Draft> drafts)
        {
            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var filePath = $"{databasePath}{leagueName}_{drafts.First().Season}Draft.csv";

            if (File.Exists(filePath))
                File.Delete(filePath);

            using (var sw = new StreamWriter(filePath))
            {
                drafts.ToList().ForEach(d => sw.WriteLine(d.ToString()));
            }

            App.ShowNotification("Success", $"{drafts.First().Season} Draft Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportGameStats(IEnumerable<PlayerGameStats> stats, int season)
        {
            var statistics = Repo.ConvertToDataTable(stats);

            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var databaseFilename = $"{databasePath}{leagueName}_{season}_PlayerGameStats.csv";

            if (File.Exists(databaseFilename))
                File.Delete(databaseFilename);

            var sb = new StringBuilder();
            var columnNames = statistics.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in statistics.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(databaseFilename, sb.ToString());
            App.ShowNotification("Success", "Player Game Stats Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportSeasonStats(string json, int season)
        {
            var seasonStats = json.DeserializeToList<PlayerStats>();
            var stats = Repo.ConvertToDataTable(seasonStats);

            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var databaseFilename = $"{databasePath}{leagueName}_{season}_PlayerSeasonStats.csv";

            if (File.Exists(databaseFilename))
                File.Delete(databaseFilename);

            var sb = new StringBuilder();
            var columnNames = stats.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in stats.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(databaseFilename, sb.ToString());
            App.ShowNotification("Success", "Player Season Stats Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportCurrentStandings(IEnumerable<Team> teams)
        {
            var stats = Repo.ConvertToStandingsDataTable(teams);
            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var databaseFilename = $"{databasePath}{leagueName}_{CurrentLeague.CurrentSeason}_Standings.txt";

            if (File.Exists(databaseFilename))
                File.Delete(databaseFilename);

            var sb = new StringBuilder();
            var columnNames = stats.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join("\t", columnNames));
            foreach (DataRow row in stats.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join("\t", fields));
            }

            File.WriteAllText(databaseFilename, sb.ToString());
            App.ShowNotification("Success", $"{CurrentLeague.CurrentSeason} Standings Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportHistoricalStandings(IEnumerable<StandingsHistory> teams, int year)
        {
            var stats = Repo.ConvertToHistoricalStandingsDataTable(teams);
            var leagueName = CurrentLeague.Abbreviation;
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
            Directory.CreateDirectory(databasePath);

            var databaseFilename = $"{databasePath}{leagueName}_{year}_Standings.csv";

            if (File.Exists(databaseFilename))
                File.Delete(databaseFilename);

            var sb = new StringBuilder();
            var columnNames = stats.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in stats.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(databaseFilename, sb.ToString());
            App.ShowNotification("Success", $"{year} Standings Exported.", Notification.Wpf.NotificationType.Information, 5);
        }

        public static void ExportExcelStandings(IEnumerable<Team> teams)
        {
            try
            {
                var missingVal = System.Reflection.Missing.Value;
                var stats = Repo.ConvertToStandingsDataTable(teams);

                var leagueName = CurrentLeague.Abbreviation;
                var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
                Directory.CreateDirectory(databasePath);

                var databaseFilename = $"{databasePath}{leagueName}_{CurrentLeague.CurrentSeason}_Standings.xlsx";

                if (File.Exists(databaseFilename))
                    File.Delete(databaseFilename);

                var xls = new excel.Application() { Visible = false, DisplayAlerts = true };
                var xlsWrk = xls.Workbooks.Add(missingVal);
                var xlsSheet = (excel.Worksheet)xlsWrk.ActiveSheet;
                xlsSheet.Name = leagueName + " " + CurrentLeague.CurrentSeason.ToString() + " Standings";
                xlsSheet.Cells[1, 1] = CurrentLeague.LeagueName;
                xlsSheet.Cells[1, 2] = CurrentLeague.CurrentSeason.ToString() + " Standings";
                xlsSheet.Range[xlsSheet.Cells[1, 2], xlsSheet.Cells[stats.Columns.Count]].Merge();
                xlsSheet.Columns.NumberFormat = "@";
                for(var k = 0; k < stats.Columns.Count; k++)
                {
                    xlsSheet.Cells[2, k + 1] = stats.Columns[k].ColumnName;
                }
                for (var i = 0; i <= stats.Rows.Count - 1; i++)
                {
                    for (var j = 0; j <= stats.Columns.Count - 1; j++)
                    {
                        xlsSheet.Cells[i + 3, j + 1] = stats.Rows[i].ItemArray[j].ToString();
                    }
                }

                var xlsRange = xlsSheet.Range[xlsSheet.Cells[1, 1], xlsSheet.Cells[stats.Rows.Count + 2, stats.Columns.Count]];
                xlsRange.EntireColumn.AutoFit();
                var border = xlsRange.Borders;
                border.LineStyle = excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;

                xlsWrk.SaveAs(databaseFilename);
                xlsWrk.Close();
                xls.Quit();
                App.ShowNotification("Success", "Exported standings", Notification.Wpf.NotificationType.Information, 5);
            }
            catch(Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Success", "Export failed.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        public static void ExportExcelStandings(IEnumerable<StandingsHistory> teams, int year)
        {
            try
            {
                var missingVal = System.Reflection.Missing.Value;
                var stats = Repo.ConvertToHistoricalStandingsDataTable(teams);

                var leagueName = CurrentLeague.Abbreviation;
                var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{leagueName}\Output\";
                Directory.CreateDirectory(databasePath);

                var databaseFilename = $"{databasePath}{leagueName}_{year}_Standings.xlsx";

                if (File.Exists(databaseFilename))
                    File.Delete(databaseFilename);

                var xls = new excel.Application() { Visible = false, DisplayAlerts = true };
                var xlsWrk = xls.Workbooks.Add(missingVal);
                var xlsSheet = (excel.Worksheet)xlsWrk.ActiveSheet;
                xlsSheet.Name = leagueName + " " + year.ToString() + " Standings";
                xlsSheet.Cells[1, 1] = CurrentLeague.LeagueName;
                xlsSheet.Cells[1, 2] = year.ToString() + " Standings";
                xlsSheet.Range[xlsSheet.Cells[1, 2], xlsSheet.Cells[stats.Columns.Count]].Merge();
                xlsSheet.Columns.NumberFormat = "@";
                for (var k = 0; k < stats.Columns.Count; k++)
                {
                    xlsSheet.Cells[2, k + 1] = stats.Columns[k].ColumnName;
                }
                for (var i = 0; i <= stats.Rows.Count - 1; i++)
                {
                    for (var j = 0; j <= stats.Columns.Count - 1; j++)
                    {
                        xlsSheet.Cells[i + 3, j + 1] = stats.Rows[i].ItemArray[j].ToString();
                    }
                }

                var xlsRange = xlsSheet.Range[xlsSheet.Cells[1, 1], xlsSheet.Cells[stats.Rows.Count + 2, stats.Columns.Count]];
                xlsRange.EntireColumn.AutoFit();
                var border = xlsRange.Borders;
                border.LineStyle = excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;

                xlsWrk.SaveAs(databaseFilename);
                xlsWrk.Close();
                xls.Quit();
                App.ShowNotification("Success", "Exported standings", Notification.Wpf.NotificationType.Information, 5);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Success", "Export failed.", Notification.Wpf.NotificationType.Error, 5);
            }
        }

        public static void CheckForRecords(IEnumerable<Game> games)
        {
            var tableExists = Database.ExecuteScalar<bool>("SELECT COUNT(*) FROM sqlite_master WHERE name='Record'");
            if (tableExists)
            {
                var records = Database.GetGameRecords().ToArray();
                if (records.Any() && Convert.ToInt32(records.First().RecordValue) > 0)
                    UpdateRecords(games);
                else
                    SetRecords();
            }
            else
                SetRecords();
        }

        public static void SetRecords()
        {
            var teams = Database.GetTeams().ToList();
            var games = Database.GetAllGames().Where(g => g.HasPlayed).ToList();
            var gameRecords = new List<Record>();
            var records = new List<Record>();

            List<PlayerGameStats> gameStats;
            if (games.Any())
            {
                games.ForEach(g =>
                {
                    var teams2 = teams.Where(t => t.Id == g.HomeTeam || t.Id == g.AwayTeam).ToList();
                    gameStats = g.StatsJson.DeserializeToList<PlayerGameStats>().ToList();

                    /* passing */
                    var gs = gameStats.OrderByDescending(gm => gm.PassCmp).First();
                    gameRecords.Add(new Record(1, 0, "Pass Completions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassCmp.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PassAtt).First();
                    gameRecords.Add(new Record(2, 0, "Pass Attempts", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassAtt.ToString()));
                    var att = gameStats.Where(gm => gm.PassAtt > 9).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.CompPct).First();
                        gameRecords.Add(new Record(3, 0, "Completion Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.CompPct * 100).ToString("N1")));
                        gs = att.OrderByDescending(gm => gm.PassYPC).First();
                        gameRecords.Add(new Record(7, 0, "Pass Yards/Completion", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassYPC.ToString("N1")));
                        gs = att.OrderByDescending(gm => gm.QBRating).First();
                        gameRecords.Add(new Record(8, 0, "QB Rating", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.QBRating.ToString("N1")));
                    }
                    else
                    {
                        gameRecords.Add(new Record(3, 0, "Completion Pct", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                        gameRecords.Add(new Record(7, 0, "Pass Yards/Completion", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                        gameRecords.Add(new Record(8, 0, "QB Rating", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                    }
                    gs = gameStats.OrderByDescending(gm => gm.PassYds).First();
                    gameRecords.Add(new Record(4, 0, "Pass Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PassTds).First();
                    gameRecords.Add(new Record(5, 0, "Pass Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassTds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PassInts).First();
                    gameRecords.Add(new Record(6, 0, "Pass Interceptions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassInts.ToString()));

                    /* rushing */
                    gs = gameStats.OrderByDescending(gm => gm.Carries).First();
                    gameRecords.Add(new Record(9, 0, "Rush Attempts", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Carries.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RushYds).First();
                    gameRecords.Add(new Record(10, 0, "Rush Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RushTds).First();
                    gameRecords.Add(new Record(11, 0, "Rush Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushTds.ToString()));
                    att = gameStats.Where(gm => gm.Carries > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.RushAvg).First();
                        gameRecords.Add(new Record(12, 0, "Rush Average", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushAvg.ToString("N1")));
                    }
                    else
                        gameRecords.Add(new Record(12, 0, "Rush Average", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                    gs = gameStats.OrderByDescending(gm => gm.RushLong).First();
                    gameRecords.Add(new Record(13, 0, "Rush Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushLong.ToString()));

                    /* receiving */
                    gs = gameStats.OrderByDescending(gm => gm.Receptions).First();
                    gameRecords.Add(new Record(14, 0, "Receptions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Receptions.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RecYds).First();
                    gameRecords.Add(new Record(15, 0, "Receive Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RecTds).First();
                    gameRecords.Add(new Record(16, 0, "Receive Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecTds.ToString()));
                    att = gameStats.Where(gm => gm.Receptions > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.RecAvg).First();
                        gameRecords.Add(new Record(17, 0, "Reception Average", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecAvg.ToString("N1")));
                    }
                    else
                        gameRecords.Add(new Record(17, 0, "Reception Average", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                    gs = gameStats.OrderByDescending(gm => gm.RecLong).First();
                    gameRecords.Add(new Record(18, 0, "Reception Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecLong.ToString()));

                    /* offense other */
                    gs = gameStats.OrderByDescending(gm => gm.Fumbles).First();
                    gameRecords.Add(gs.Fumbles > 0
                        ? new Record(19, 0, "Fumbles", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.Fumbles.ToString())
                        : new Record(19, 0, "Fumbles", "", "", "", 0, CurrentLeague.CurDate, "0"));
                    gs = gameStats.OrderByDescending(gm => gm.FumblesLost).First();
                    gameRecords.Add(gs.FumblesLost > 0
                        ? new Record(20, 0, "Fumbles Lost", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.FumblesLost.ToString())
                        : new Record(20, 0, "Fumbles Lost", "", "", "", 0, CurrentLeague.CurDate, "0"));
                    gs = gameStats.OrderByDescending(gm => gm.PancakeBlocks).First();
                    gameRecords.Add(new Record(21, 0, "Pancake Blocks", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PancakeBlocks.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.SacksAllowed).First();
                    gameRecords.Add(new Record(22, 0, "Sacks Allowed", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.SacksAllowed.ToString()));

                    /* kick returns */
                    gs = gameStats.OrderByDescending(gm => gm.KickReturns).First();
                    gameRecords.Add(new Record(23, 0, "Return Attempts", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickReturns.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.KickRetYds).First();
                    gameRecords.Add(new Record(24, 0, "Return Yards", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickRetYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.KickReturnTds).First();
                    gameRecords.Add(gs.KickReturnTds == 0
                        ? new Record(25, 0, "Return Touchdowns", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(25, 0, "Return Touchdowns", gs.PlayerName,
                            teams2.First(t => t.Abbreviation == gs.Team).TeamName,
                            teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate,
                            gs.KickReturnTds.ToString()));
                    att = gameStats.Where(gm => gm.KickReturns > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.ReturnAvg).First();
                        gameRecords.Add(new Record(26, 0, "Return Average", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.ReturnAvg.ToString("N1")));
                    }
                    else
                        gameRecords.Add(new Record(26, 0, "Return Average", "", "", "", 0, CurrentLeague.CurDate, "0.0"));
                    gs = gameStats.OrderByDescending(gm => gm.KickReturnLong).First();
                    gameRecords.Add(new Record(27, 0, "Return Long", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickReturnLong.ToString()));

                    /* defense */
                    gs = gameStats.OrderByDescending(gm => gm.Tackles).First();
                    gameRecords.Add(new Record(28, 0, "Tackles", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Tackles.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.Sacks).First();
                    gameRecords.Add(gs.Sacks == 0
                        ? new Record(29, 0, "Sacks", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(29, 0, "Sacks", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.Sacks.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.Ints).First();
                    gameRecords.Add(gs.Ints == 0
                        ? new Record(30, 0, "Interceptions", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(30, 0, "Interceptions", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Ints.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.IntYds).First();
                    gameRecords.Add(gs.IntYds == 0
                        ? new Record(31, 0, "Interception Yards", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(31, 0, "Interception Yards", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.IntYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.ForFumb).First();
                    gameRecords.Add(gs.ForFumb == 0
                        ? new Record(32, 0, "Forced Fumbles", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(32, 0, "Forced Fumbles", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.ForFumb.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FumbRec).First();
                    gameRecords.Add(gs.FumbRec == 0
                        ? new Record(33, 0, "Fumbles Recovered", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(33, 0, "Fumbles Recovered", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.FumbRec.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.DefTds).First();
                    gameRecords.Add(gs.DefTds == 0
                        ? new Record(34, 0, "Defensive Touchdowns", "", "", "", 0, CurrentLeague.CurDate, "0")
                        : new Record(34, 0, "Defensive Touchdowns", gs.PlayerName,
                            teams2.First(t => t.Id == gs.TeamId).TeamName,
                            teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate,
                            gs.DefTds.ToString()));

                    /* kicking */
                    gs = gameStats.OrderByDescending(gm => gm.XPMade).First();
                    gameRecords.Add(new Record(35, 0, "Extra Points Made", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.XPMade.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.XPAtt).First();
                    gameRecords.Add(new Record(36, 0, "Extra Points Attempted", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.XPAtt.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.XpPct).First();
                    gameRecords.Add(new Record(37, 0, "Extra Point Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.XpPct * 100).ToString("N1")));
                    gs = gameStats.OrderByDescending(gm => gm.FGMade).First();
                    gameRecords.Add(new Record(38, 0, "Field Goals Made", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGMade.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FGAtt).First();
                    gameRecords.Add(new Record(39, 0, "Field Goals Attempted", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGAtt.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FgPct).First();
                    gameRecords.Add(new Record(40, 0, "Field Goal Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.FgPct * 100).ToString("N1")));
                    gs = gameStats.OrderByDescending(gm => gm.FGLong).First();
                    gameRecords.Add(new Record(41, 0, "Field Goal Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGLong.ToString()));

                    /* other */
                    gs = gameStats.OrderByDescending(gm => gm.Points).First();
                    gameRecords.Add(new Record(42, 0, "Points", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Points.ToString()));
                });

                records.Add(gameRecords.Where(r => r.Id == 1).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 2).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 3).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                //string compPct = records.Where(r => r.Id == 3).First().RecordValue;
                //compPct = Convert.ToDouble(compPct).ToString("P1", CultureInfo.InvariantCulture);
                //records.Where(r => r.Id == 3).First().RecordValue = compPct;
                records.Add(gameRecords.Where(r => r.Id == 4).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 5).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 6).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 7).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 8).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 9).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 10).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 11).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 12).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 13).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 14).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 15).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 16).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 17).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 18).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 19).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 20).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 21).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 22).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 23).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 24).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 25).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 26).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 27).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 28).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 29).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 30).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 31).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 32).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 33).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 34).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 35).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 36).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 37).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                //string xpPct = records.Where(r => r.Id == 37).First().RecordValue;
                //xpPct = Convert.ToDouble(xpPct).ToString("P1", CultureInfo.InvariantCulture);
                //records.Where(r => r.Id == 37).First().RecordValue = xpPct;
                records.Add(gameRecords.Where(r => r.Id == 38).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 39).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 40).OrderByDescending(r => Convert.ToDouble(r.RecordValue)).First());
                //string fgPct = records.Where(r => r.Id == 40).First().RecordValue;
                //fgPct = Convert.ToDouble(fgPct).ToString("P1", CultureInfo.InvariantCulture);
                //records.Where(r => r.Id == 40).First().RecordValue = fgPct;
                records.Add(gameRecords.Where(r => r.Id == 41).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());
                records.Add(gameRecords.Where(r => r.Id == 42).OrderByDescending(r => Convert.ToInt32(r.RecordValue)).First());

                Database.CreateTable<Record>();
                Database.InsertAll(records.AsEnumerable());
            }
        }

        /// <summary>
        /// Updates league records.
        /// </summary>
        /// <param name="g">The list of games</param>
        public static void UpdateRecords(IEnumerable<Game> g)
        {
            var teams = Database.GetTeams().ToList();
            var records = Database.GetGameRecords().ToList();
            var gameRecords = new List<Record>();
            var games = g.ToList();
            //var gamesMax = new List<PlayerGameStats>(); // maybe add max values to a new list to iterate through only the max values?? would it be faster?
            if (games.Any())
            {
                games.ForEach(x =>
                {
                    var teams2 = teams.Where(t => t.Id == x.HomeTeam || t.Id == x.AwayTeam).ToList();
                    var gameStats = x.StatsJson.DeserializeToList<PlayerGameStats>();

                    /* passing */
                    var gs = gameStats.OrderByDescending(s => s.PassCmp).First();
                    if (gs.PassCmp > Convert.ToInt32(records.First(r => r.Id == 1).RecordValue))
                        gameRecords.Add(new Record(1, 0, "Pass Completions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassCmp.ToString()));
                    gs = gameStats.OrderByDescending(s => s.PassAtt).First();
                    if (gs.PassAtt > Convert.ToInt32(records.First(r => r.Id == 2).RecordValue))
                        gameRecords.Add(new Record(2, 0, "Pass Attempts", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassAtt.ToString()));
                    var att = gameStats.Where(gm => gm.PassAtt > 9).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.CompPct).First();
                        if ((gs.CompPct * 100) > Convert.ToDouble(records.First(r => r.Id == 3).RecordValue))
                            gameRecords.Add(new Record(3, 0, "Completion Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.CompPct * 100).ToString("N1")));
                        gs = att.OrderByDescending(gm => gm.PassYPC).First();
                        if (gs.PassYPC > Convert.ToDouble(records.First(r => r.Id == 7).RecordValue))
                            gameRecords.Add(new Record(7, 0, "Pass Yards/Completion", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassYPC.ToString("N1")));
                        gs = att.OrderByDescending(gm => gm.QBRating).First();
                        if (gs.QBRating > Convert.ToDouble(records.First(r => r.Id == 8).RecordValue))
                            gameRecords.Add(new Record(8, 0, "QB Rating", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.QBRating.ToString("N1")));
                    }
                    gs = gameStats.OrderByDescending(gm => gm.PassYds).First();
                    if (gs.PassYds > Convert.ToInt32(records.First(r => r.Id == 4).RecordValue))
                        gameRecords.Add(new Record(4, 0, "Pass Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PassTds).First();
                    if (gs.PassTds > Convert.ToInt32(records.First(r => r.Id == 5).RecordValue))
                        gameRecords.Add(new Record(5, 0, "Pass Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassTds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PassInts).First();
                    if (gs.PassInts > Convert.ToInt32(records.First(r => r.Id == 6).RecordValue))
                        gameRecords.Add(new Record(6, 0, "Pass Interceptions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PassInts.ToString()));

                    /* rushing */
                    gs = gameStats.OrderByDescending(gm => gm.Carries).First();
                    if (gs.Carries > Convert.ToInt32(records.First(r => r.Id == 9).RecordValue))
                        gameRecords.Add(new Record(9, 0, "Rush Attempts", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Carries.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RushYds).First();
                    if (gs.RushYds > Convert.ToInt32(records.First(r => r.Id == 10).RecordValue))
                        gameRecords.Add(new Record(10, 0, "Rush Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RushTds).First();
                    if (gs.RushTds > Convert.ToInt32(records.First(r => r.Id == 11).RecordValue))
                        gameRecords.Add(new Record(11, 0, "Rush Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushTds.ToString()));
                    att = gameStats.Where(gm => gm.Carries > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.RushAvg).First();
                        if (gs.RushAvg > Convert.ToDouble(records.First(r => r.Id == 12).RecordValue))
                            gameRecords.Add(new Record(12, 0, "Rush Average", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushAvg.ToString("N1")));
                    }
                    gs = gameStats.OrderByDescending(gm => gm.RushLong).First();
                    if (gs.RushLong > Convert.ToInt32(records.First(r => r.Id == 13).RecordValue))
                        gameRecords.Add(new Record(13, 0, "Rush Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RushLong.ToString()));

                    /* receiving */
                    gs = gameStats.OrderByDescending(gm => gm.Receptions).First();
                    if (gs.Receptions > Convert.ToInt32(records.First(r => r.Id == 14).RecordValue))
                        gameRecords.Add(new Record(14, 0, "Receptions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Receptions.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RecYds).First();
                    if (gs.RecYds > Convert.ToInt32(records.First(r => r.Id == 15).RecordValue))
                        gameRecords.Add(new Record(15, 0, "Receive Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.RecTds).First();
                    if (gs.RecTds > Convert.ToInt32(records.First(r => r.Id == 16).RecordValue))
                        gameRecords.Add(new Record(16, 0, "Receive Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecTds.ToString()));
                    att = gameStats.Where(gm => gm.Receptions > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.RecAvg).First();
                        if (gs.RecAvg > Convert.ToDouble(records.First(r => r.Id == 17).RecordValue))
                            gameRecords.Add(new Record(17, 0, "Reception Average", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecAvg.ToString("N1")));
                    }
                    gs = gameStats.OrderByDescending(gm => gm.RecLong).First();
                    if (gs.RecLong > Convert.ToInt32(records.First(r => r.Id == 18).RecordValue))
                        gameRecords.Add(new Record(18, 0, "Reception Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.RecLong.ToString()));

                    /* offense other */
                    gs = gameStats.OrderByDescending(gm => gm.Fumbles).First();
                    if (gs.Fumbles > Convert.ToInt32(records.First(r => r.Id == 19).RecordValue))
                        gameRecords.Add(new Record(19, 0, "Fumbles", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Fumbles.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FumblesLost).First();
                    if (gs.FumblesLost > Convert.ToInt32(records.First(r => r.Id == 20).RecordValue))
                        gameRecords.Add(new Record(20, 0, "Fumbles Lost", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FumblesLost.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.PancakeBlocks).First();
                    if (gs.PancakeBlocks > Convert.ToInt32(records.First(r => r.Id == 21).RecordValue))
                        gameRecords.Add(new Record(21, 0, "Pancake Blocks", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.PancakeBlocks.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.SacksAllowed).First();
                    if (gs.SacksAllowed > Convert.ToInt32(records.First(r => r.Id == 22).RecordValue))
                        gameRecords.Add(new Record(22, 0, "Sacks Allowed", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.SacksAllowed.ToString()));

                    /* kick returns */
                    gs = gameStats.OrderByDescending(gm => gm.KickReturns).First();
                    if (gs.KickReturns > Convert.ToInt32(records.First(r => r.Id == 23).RecordValue))
                        gameRecords.Add(new Record(23, 0, "Return Attempts", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickReturns.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.KickRetYds).First();
                    if (gs.KickRetYds > Convert.ToInt32(records.First(r => r.Id == 24).RecordValue))
                        gameRecords.Add(new Record(24, 0, "Return Yards", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickRetYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.KickReturnTds).First();
                    if (gs.KickReturnTds > Convert.ToInt32(records.First(r => r.Id == 25).RecordValue))
                        gameRecords.Add(new Record(25, 0, "Return Touchdowns", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickReturnTds.ToString()));
                    att = gameStats.Where(gm => gm.KickReturns > 4).ToList();
                    if (att.Any())
                    {
                        gs = att.OrderByDescending(gm => gm.ReturnAvg).First();
                        if (gs.ReturnAvg > Convert.ToDouble(records.First(r => r.Id == 26).RecordValue))
                            gameRecords.Add(new Record(26, 0, "Return Average", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.ReturnAvg.ToString("N1")));
                    }
                    gs = gameStats.OrderByDescending(gm => gm.KickReturnLong).First();
                    if (gs.KickReturnLong > Convert.ToInt32(records.First(r => r.Id == 27).RecordValue))
                        gameRecords.Add(new Record(27, 0, "Return Long", gs.PlayerName, teams2.First(t => t.Abbreviation == gs.Team).TeamName, teams2.First(t => t.Abbreviation != gs.Team).TeamName, gs.Season, gs.GameDate, gs.KickReturnLong.ToString()));

                    /* defense */
                    gs = gameStats.OrderByDescending(gm => gm.Tackles).First();
                    if (gs.Tackles > Convert.ToInt32(records.First(r => r.Id == 28).RecordValue))
                        gameRecords.Add(new Record(28, 0, "Tackles", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Tackles.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.Sacks).First();
                    if (gs.Sacks > Convert.ToInt32(records.First(r => r.Id == 29).RecordValue))
                        gameRecords.Add(new Record(29, 0, "Sacks", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Sacks.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.Ints).First();
                    if (gs.Ints > Convert.ToInt32(records.First(r => r.Id == 30).RecordValue))
                        gameRecords.Add(new Record(30, 0, "Interceptions", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Ints.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.IntYds).First();
                    if (gs.IntYds > Convert.ToInt32(records.First(r => r.Id == 31).RecordValue))
                        gameRecords.Add(new Record(31, 0, "Interception Yards", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.IntYds.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.ForFumb).First();
                    if (gs.ForFumb > Convert.ToInt32(records.First(r => r.Id == 32).RecordValue))
                        gameRecords.Add(new Record(32, 0, "Forced Fumbles", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.ForFumb.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FumbRec).First();
                    if (gs.FumbRec > Convert.ToInt32(records.First(r => r.Id == 33).RecordValue))
                        gameRecords.Add(new Record(33, 0, "Fumbles Recovered", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FumbRec.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.DefTds).First();
                    if (gs.DefTds > Convert.ToInt32(records.First(r => r.Id == 34).RecordValue))
                        gameRecords.Add(new Record(34, 0, "Defensive Touchdowns", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.DefTds.ToString()));


                    /* kicking */
                    gs = gameStats.OrderByDescending(gm => gm.XPMade).First();
                    if (gs.XPMade > Convert.ToInt32(records.First(r => r.Id == 35).RecordValue))
                        gameRecords.Add(new Record(35, 0, "Extra Points Made", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.XPMade.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.XPAtt).First();
                    if (gs.XPAtt > Convert.ToInt32(records.First(r => r.Id == 36).RecordValue))
                        gameRecords.Add(new Record(36, 0, "Extra Points Attempted", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.XPAtt.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.XpPct).First();
                    if ((gs.XpPct * 100) > Convert.ToDouble(records.First(r => r.Id == 37).RecordValue))
                        gameRecords.Add(new Record(37, 0, "Extra Point Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.XpPct * 100).ToString("N1")));
                    gs = gameStats.OrderByDescending(gm => gm.FGMade).First();
                    if (gs.FGMade > Convert.ToInt32(records.First(r => r.Id == 38).RecordValue))
                        gameRecords.Add(new Record(38, 0, "Field Goals Made", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGMade.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FGAtt).First();
                    if (gs.FGMade > Convert.ToInt32(records.First(r => r.Id == 39).RecordValue))
                        gameRecords.Add(new Record(39, 0, "Field Goals Attempted", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGAtt.ToString()));
                    gs = gameStats.OrderByDescending(gm => gm.FgPct).First();
                    if ((gs.FgPct * 100) > Convert.ToDouble(records.First(r => r.Id == 40).RecordValue))
                        gameRecords.Add(new Record(40, 0, "Field Goal Pct", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, (gs.FgPct * 100).ToString("N1")));
                    gs = gameStats.OrderByDescending(gm => gm.FGLong).First();
                    if (gs.FGLong > Convert.ToInt32(records.First(r => r.Id == 41).RecordValue))
                        gameRecords.Add(new Record(41, 0, "Field Goal Long", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.FGLong.ToString()));

                    /* other */
                    gs = gameStats.OrderByDescending(gm => gm.Points).First();
                    if (gs.Points > Convert.ToInt32(records.First(r => r.Id == 42).RecordValue))
                        gameRecords.Add(new Record(42, 0, "Points", gs.PlayerName, teams2.First(t => t.Id == gs.TeamId).TeamName, teams2.First(t => t.Id != gs.TeamId).TeamName, gs.Season, gs.GameDate, gs.Points.ToString()));
                });

                Database.UpdateAll(gameRecords.AsEnumerable());
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using IndoorFootballManager.Models;
using IndoorFootballManager.Services;
using SQLite;

namespace IndoorFootballManager.DataAccess
{
    public class InSeasonDay
    {
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static readonly Random Random = new Random();

        public static void CheckForPlayoffsStart(bool hasStarted)
        {
            if (hasStarted) 
                return;

            var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.Season == CurrentLeague.CurrentSeason).ToList();
            var startPlayoffs = games.All(g => g.HasPlayed);
            
            if (!startPlayoffs) 
                return;

            try
            {
                // allow customization...
                if (CurrentLeague.Conferences == 1)
                {
                    // add something to this later...
                    var conf = CurrentLeague.Conference1;
                    var playoffTeamsCount = CurrentLeague.PlayoffTeams;

                    if (playoffTeamsCount == 2)
                    {
                        var teams = Database.GetTeamsForConferenceStandings(conf).ToList();
                        var g = new Game
                        {
                            Season = CurrentLeague.CurrentSeason,
                            GameDate = games.Last().GameDate.AddDays(7),
                            HomeTeam = teams[0].Id,
                            AwayTeam = teams[1].Id,
                            Playoffs = true,
                            Conference = "Championship",
                            HomeTeamRank = 1,
                            AwayTeamRank = 2
                        };

                        Database.SaveGame(g);
                    }
                    else
                    {
                        var playoffTeams = new List<Team>();
                        var divisionWinners = new List<Team>();
                        var divisions = CurrentLeague.GetConferenceDivisions(conf);
                        var divIds = new List<int>();
                        foreach (var divLead in divisions.Select(div => Database.GetTeamsForDivisionStandings(conf, div).Take(1).First()))
                        {
                            divisionWinners.Add(divLead);
                            divIds.Add(divLead.Id);
                        }

                        playoffTeams.AddRange(divisionWinners.OrderByDescending(t => t.WinPct)
                            .ThenByDescending(t => t.ConfPct).ThenByDescending(t => t.PointsFor)
                            .ThenBy(t => t.PointsAgainst).ThenByDescending(t => t.SOS));
                        var conferenceTeams = Database.GetTeamsForConferenceStandings(conf).Take(playoffTeamsCount)
                            .ToList();
                        var conferenceTeamsNew =
                            conferenceTeams.Where(t => !divIds.Contains(t.Id)).ToList();
                        playoffTeams.AddRange(conferenceTeamsNew);
                        var k = playoffTeams.Count();
                        for (var j = 0; j < playoffTeamsCount / 2; j++)
                        {
                            var g = NewGame(games, playoffTeams, j + 1, k);
                            playoffTeams.RemoveAt(0);
                            playoffTeams.RemoveAt(playoffTeams.Count() - 1);
                            Database.SaveGame(g);
                            k--;
                        }
                    }
                }
                else
                {
                    var playoffTeamsCount = CurrentLeague.PlayoffTeams / 2;
                    if (playoffTeamsCount == 1)
                    {
                        var teams = Database.GetTeamsForStandings().ToList();
                        var g = new Game
                        {
                            Season = CurrentLeague.CurrentSeason,
                            GameDate = games.Last(gm => gm.Playoffs == false).GameDate.AddDays(7),
                            HomeTeam = teams.First().Id,
                            AwayTeam = teams.Last().Id,
                            Playoffs = true,
                            Conference = "Championship",
                            HomeTeamRank = 1,
                            AwayTeamRank = 1
                        };
                        Database.SaveGame(g);
                    }
                    else
                    {
                        for (var i = 0; i < 2; i++)
                        {
                            var conf = i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2;

                            var playoffTeams = new List<Team>();
                            var divisionWinners = new List<Team>();
                            var divisions = CurrentLeague.GetConferenceDivisions(conf);
                            var divIds = new List<int>();
                            foreach (var divLead in divisions.Select(div =>
                                Database.GetTeamsForDivisionStandings(conf, div).Take(1).First()))
                            {
                                divisionWinners.Add(divLead);
                                divIds.Add(divLead.Id);
                            }

                            playoffTeams.AddRange(divisionWinners.OrderByDescending(t => t.WinPct)
                                .ThenByDescending(t => t.ConfPct).ThenByDescending(t => t.PointsFor)
                                .ThenBy(t => t.PointsAgainst).ThenByDescending(t => t.SOS));
                            var conferenceTeams = Database.GetTeamsForConferenceStandings(conf)
                                .Take(playoffTeamsCount).ToList();
                            var conferenceTeamsNew =
                                conferenceTeams.Where(t => !divIds.Contains(t.Id)).ToList();
                            playoffTeams.AddRange(conferenceTeamsNew);

                            if (playoffTeamsCount == 2)
                            {
                                var g = NewGame(games, playoffTeams, 1, 2);
                                Database.SaveGame(g);
                            }
                            else // do something special with 12 teams...
                            {
                                var k = playoffTeams.Count;
                                for (var j = 0; j < playoffTeamsCount / 2; j++)
                                {
                                    var g = NewGame(games, playoffTeams, j + 1, k);
                                    playoffTeams.RemoveAt(0);
                                    playoffTeams.RemoveAt(playoffTeams.Count - 1);
                                    Database.SaveGame(g);
                                    k--;
                                }
                            }
                        }
                    }
                }

                CurrentLeague.IsPlayoffs = true;
                Database.SaveLeague(CurrentLeague);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error",
                    "Something went wrong.\nCheck Log file in the IFM folder in Documents.",
                    Notification.Wpf.NotificationType.Error, 5);
                throw x;
            }
        }

        private static Game NewGame(IEnumerable<Game> games, IEnumerable<Team> teams, int top, int bot)
        {
            var g = new Game
            {
                Season = CurrentLeague.CurrentSeason,
                GameDate = games.Last(gm => gm.Playoffs == false).GameDate.AddDays(7),
                HomeTeam = teams.First().Id,
                AwayTeam = teams.Last().Id,
                Playoffs = true,
                Conference = teams.First().Conference,
                HomeTeamRank = top,
                AwayTeamRank = bot
            };
            return g;
        }
        
        public static void AdvancePlayoffs(bool ready)
        {
            if (!ready) 
                return;

            var games = Database.GetGames(CurrentLeague.CurrentSeason).Where(g => g.Playoffs).ToList();
            var winningTeams = games.Where(g => g.GameDate == games.Last().GameDate).OrderBy(g => g.WinningTeamRank).Select(g => g.WinningTeam).ToList();
            var gameDate = games.Last().GameDate.AddDays(7);

            if (winningTeams.Count() == 2)
            {
                var winningIds = games.Take(2).ToList();
                var g = new Game()
                {
                    Season = CurrentLeague.CurrentSeason,
                    GameDate = gameDate,
                    HomeTeam = winningTeams.First().Id,
                    AwayTeam = winningTeams.Last().Id,
                    Playoffs = true,
                    Conference = "Championship",
                    HomeTeamRank = winningIds.First().WinningTeamRank,
                    AwayTeamRank = winningIds.Last().WinningTeamRank,
                };
                Database.SaveGame(g);
            }
            else
            {
                for (var i = 0; i < CurrentLeague.Conferences; i++)
                {
                    var conf = i == 0 ? CurrentLeague.Conference1 : CurrentLeague.Conference2;

                    var confTeams = winningTeams.Where(t => t.Conference == conf).ToList();
                    var confGames = games.Where(g => g.GameDate == games.Last().GameDate && g.Conference == conf).OrderBy(g => g.WinningTeamRank).ToList();
                    var gamesCount = confGames.Count() / 2;
                    for (var j = 0; j < gamesCount; j++)
                    {
                        var g = new Game
                        {
                            Season = CurrentLeague.CurrentSeason,
                            GameDate = gameDate,
                            HomeTeam = confGames.First().WinningTeamId,
                            AwayTeam = confGames.Last().WinningTeamId,
                            Playoffs = true,
                            Conference = conf,
                            HomeTeamRank = confGames.First().WinningTeamRank,
                            AwayTeamRank = confGames.Last().WinningTeamRank,
                        };

                        confTeams.RemoveAt(0);
                        confTeams.RemoveAt(confTeams.Count - 1);
                        confGames.RemoveAt(0);
                        confGames.RemoveAt(confGames.Count - 1);
                        Database.SaveGame(g);
                    }
                }
            }
        }

        public static void DeleteNonActiveContractOffers()
        {
            var offers = Database.GetContractOffers().Where(o => o.Active == false).ToList();
            foreach (var con in offers)
            {
                Database.Delete<ContractOffer>(con.Id);
            }
        }

        public static void FillCpuRosters()
        {
            var cpuTeams = Database.GetTeams().Where(t => t.IsCpu).Concat(Database.GetManagers().Where(m => m.AssistExtensions).Select(gm => gm.Team)).ToList();
            foreach (var t in cpuTeams)
                t.FillRoster();
        }

        public static void CpuSignPending()
        {
            var cpuTeams = Database.GetTeams().Where(t => t.IsCpu).Concat(Database.GetManagers().Where(m => m.AssistExtensions).Select(gm => gm.Team)).ToList();
            foreach (var team in cpuTeams)
                SignCpuPending(team);
        }

        public static void CpuTrainPlayers()
        {
            var cpuTeams = Database.GetTeams().Where(t => t.IsCpu).ToList();
            foreach (var team in cpuTeams)
                team.CpuTrainPlayers();
        }

        public static void SignFreeAgents()
        {
            var contractOffers = Database.GetContractOffers()
                .Where(o => o.Active == true)
                .OrderBy(o => o.PlayerId)
                .ThenBy(o => o.Team.WinPct)
                .GroupBy(o => o.PlayerId)
                .Select(o => o.First());
            foreach (var con in contractOffers)
            {
                var team = Database.GetTeamById(con.TeamId);
                var player = Database.GetPlayerById(con.PlayerId);
                player.TeamId = team.Id;
                player.Salary = con.Salary;
                player.ContractYears = con.Years;
                player.Jersey = player.JerseyNumber;
                PlayerData.DeleteOldContracts(player);

                con.Active = false;
                Database.SaveContract(con);
                if (!team.IsCpu)
                {
                    SendEmail.PlayerSigned(team, player);
                    IFM.NotifyChange("EmailsCount");
                }

                var playerHistory = player.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add("Signed by " + team.TeamName + " on " + CurrentLeague.CurDate.ToShortDateString() + " (" + player.Salary.ToString("C") + " / " + player.ContractYears.ToString() + " years)");
                player.PlayerHistoryJson = playerHistory.SerializeToJson();
                Database.SavePlayer(player);

                var transaction = new Transaction(team.Id,
                                    CurrentLeague.CurrentSeason,
                                    CurrentLeague.CurDate,
                                    $"{player.NameWithPosition} signed with {team.TeamName} on {CurrentLeague.CurDate.ToShortDateString()} on a {player.ContractYears}-year contract for {player.Salary:C0}/year.");

                Database.SaveTransaction(transaction);
                    
            }
        }

        private static void SignCpuPending(Team team)
        {
            var players = Database.GetPlayersByTeamId(team.Id).Where(p => p.ExtensionYears == 0 && p.ContractYears == 1 && !p.IsFranchised).ToList();
            foreach (var p in from p in players let rand = Random.NextDouble() where rand < p.Evaluation select p)
            {
                p.ExtensionSalary = p.GetNegotiatedSalary;
                p.ExtensionYears = p.DesiredYears;

                var playerHistory = p.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add("Signed an extension with " + team.TeamName + " on " + CurrentLeague.CurDate.ToShortDateString() + " (" + p.ExtensionSalary.ToString("C0") + " / " + p.ExtensionYears.ToString() + " years)");
                p.PlayerHistoryJson = playerHistory.SerializeToJson();
                    
                var transaction = new Transaction(team.Id,
                    CurrentLeague.CurrentSeason,
                    CurrentLeague.CurDate,
                    $"{p.NameWithPosition} signed an extension with {team.TeamName} on {CurrentLeague.CurDate.ToShortDateString()} on a {p.ExtensionYears}-year contract for {p.ExtensionSalary:C0}/year.");
                    
                Database.SaveTransaction(transaction);
            }
            Database.UpdateAllPlayers(players.AsEnumerable());
        }


    }
}

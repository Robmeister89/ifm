﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;

namespace IndoorFootballManager.Services
{
    internal class Generation
    {
        private static League CurrentLeague => IFM.CurrentLeague;
        public List<Player> Players = new List<Player>();

        private readonly int[] _intelligence = { 30, 30, 40, 40, 40, 40, 50, 50, 50, 50, 50, 60, 60, 60, 60, 70, 70, 80 };
        private readonly int[] _rkIntelligence = { 30, 30, 40, 40, 40, 40, 50, 50, 50, 50, 50, 60, 60, 60, 60, 70 };

        private readonly int[] _qbPass = { 70, 70, 70, 70, 80, 80, 80, 90, 90 };
        private readonly int[] _qbIntel = { 60, 60, 70, 70, 70, 70, 80, 90 };
        private readonly int[] _qbStrength = { 20, 20, 30, 30, 30, 30, 40, 40, 40, 40, 50, 50, 60, 60 };
        private readonly int[] _qbCarry = { 30, 40, 50, 50, 50, 60, 60, 60, 70 };
        private readonly int[] _qbSpeed = { 20, 30, 30, 40, 40, 40, 50, 50, 50, 60, 60, 70, 80 };
        private readonly int[] _rkQbPass = { 60, 60, 60, 70, 70, 70, 70, 80, 90 };
        private readonly int[] _rkQbIntel = { 40, 40, 50, 50, 50, 60, 60, 60, 60, 60, 70 };
        private readonly int[] _rkQbStrength = { 20, 20, 20, 30, 30, 30, 30, 30, 40, 40, 40, 50, 60 };
        private readonly int[] _rkQbCarry = { 30, 30, 40, 40, 50, 50, 50, 50, 60 };
        private readonly int[] _rkQbSpeed = { 20, 20, 30, 30, 40, 40, 50, 50, 50, 60, 70 };

        private readonly int[] _rbStrength = { 20, 30, 40, 40, 40, 50, 50, 50, 60, 60, 70, 70 };
        private readonly int[] _rbCarry = { 40, 40, 40, 50, 50, 50, 60, 60, 60, 70, 80, 90 };
        private readonly int[] _rbCatch = { 30, 40, 40, 40, 50, 50, 50, 60, 60, 70, 80 };
        private readonly int[] _rbBlock = { 10, 20, 30, 30, 40, 40, 50, 50, 50, 60, 60, 70 };
        private readonly int[] _rkRbStrength = { 20, 30, 30, 30, 40, 40, 40, 40, 40, 50, 60 };
        private readonly int[] _rkRbCarry = { 40, 50, 50, 50, 50, 50, 60, 60, 60, 60, 60, 60, 70, 70, 80 };
        private readonly int[] _rkRbCatch = { 40, 40, 40, 50, 50, 50, 50, 60, 60, 70 };
        private readonly int[] _rkRbBlock = { 20, 20, 30, 30, 30, 40, 40, 40, 50, 50, 60 };

        private readonly int[] _wrStrength = { 20, 20, 30, 30, 40, 40, 40, 40, 50, 50, 50, 50, 60, 70 };
        private readonly int[] _wrCarry = { 30, 30, 40, 40, 40, 40, 40, 50, 50, 50, 50, 60, 60, 60, 70, 80 };
        private readonly int[] _wrCatch = { 50, 50, 60, 60, 60, 60, 70, 70, 70, 80, 90 };
        private readonly int[] _wrBlock = { 10, 20, 20, 30, 30, 30, 40, 40, 40, 50, 60, 70 };
        private readonly int[] _rkWrStrength = { 20, 30, 30, 30, 40, 40, 40, 40, 40, 50, 50, 50, 60 };
        private readonly int[] _rkWrCarry = { 20, 30, 30, 30, 40, 40, 40, 40, 50, 50, 50, 50, 60 };
        private readonly int[] _rkWrCatch = { 50, 50, 60, 60, 60, 60, 60, 60, 70, 70, 70, 80, 80 };
        private readonly int[] _rkWrBlock = { 10, 10, 20, 20, 30, 30, 40, 40, 40, 50, 60 };

        private readonly int[] _oLineSpeed = { 10, 20, 20, 30, 30, 30, 30, 40, 40, 40, 50, 50, 60, 60 };
        private readonly int[] _rkOLineSpeed = { 10, 10, 20, 20, 20, 30, 30, 30, 30, 40, 40, 40, 40, 50, 50, 60 };

        private readonly int[] _lineStrength = { 60, 60, 60, 70, 70, 70, 70, 80, 80, 80, 90 };
        private readonly int[] _blockBlitz = { 60, 60, 60, 70, 70, 70, 70, 70, 70, 80, 80, 80, 90 }; // used for oline block and dline blitz
        private readonly int[] _rkLineStrength = { 50, 50, 60, 60, 70, 70, 70, 70, 70, 80, 80, 90 };
        private readonly int[] _rkBlockBlitz = { 50, 60, 60, 60, 70, 70, 70, 70, 70, 80, 80, 85 }; // used for oline block and dline blitz

        private readonly int[] _dLineSpeed = { 10, 20, 30, 40, 40, 40, 50, 50, 50, 60, 60, 60 };
        private readonly int[] _dLineTackle = { 30, 40, 50, 50, 60, 60, 60, 60, 70, 70, 70, 80, 90 };
        private readonly int[] _rkDLineSpeed = { 10, 20, 30, 40, 40, 40, 50, 50, 50, 50, 50, 60, 60 };
        private readonly int[] _rkDLineTackle = { 10, 20, 30, 40, 40, 40, 50, 50, 50, 60, 60, 60, 70, 75 };

        private readonly int[] _positionSpeed = { 60, 70, 70, 70, 80, 80, 80, 80, 80, 90, 90 }; //rb, wr, db
        private readonly int[] _rkPositionSpeed = { 60, 70, 70, 70, 80, 80, 90 }; //rb, wr, db

        private readonly int[] _lbCarryCatch = { 20, 30, 30, 40, 40, 40, 50, 50, 50, 60 };
        private readonly int[] _lbSpeed = { 60, 60, 60, 70, 70, 70, 70, 70, 70, 80, 80 };
        private readonly int[] _lbStrength = { 30, 40, 40, 40, 50, 50, 50, 50, 60, 60, 60, 70, 70, 70, 80 };
        private readonly int[] _lbBlitz = { 60, 60, 70, 70, 70, 70, 80, 80, 80, 80, 90 };
        private readonly int[] _lbCoverage = { 30, 40, 50, 50, 50, 50, 60, 60, 60, 60, 60, 70, 70, 70, 80, 80 };
        private readonly int[] _lbTackle = { 40, 50, 50, 50, 50, 60, 60, 60, 60, 70, 70, 70, 70, 70, 70, 80, 80, 80, 90, 90 };
        private readonly int[] _rkLbCarryCatch = { 20, 30, 30, 40, 40, 40, 50 };
        private readonly int[] _rkLbSpeed = { 50, 60, 60, 60, 60, 60, 60, 70, 70, 70, 70, 80 };
        private readonly int[] _rkLbStrength = { 30, 30, 40, 40, 40, 40, 50, 50, 50, 50, 60, 60, 60, 60, 70 };
        private readonly int[] _rkLbBlitz = { 40, 50, 50, 50, 60, 60, 60, 60, 60, 70, 70, 70, 80, 80 };
        private readonly int[] _rkLbCoverage = { 40, 40, 50, 50, 50, 50, 50, 60, 60, 60, 60, 60, 70, 70, 80 };
        private readonly int[] _rkLbTackle = { 40, 50, 50, 50, 60, 60, 60, 60, 70, 70, 70, 70, 80, 85 };

        private readonly int[] _dbStrengthCarryCatch = { 0, 10, 20, 30, 30, 40, 40, 40, 50, 50, 50, 50, 60, 60 };
        private readonly int[] _dbCover = { 50, 60, 60, 60, 60, 70, 70, 70, 70, 70, 80, 80, 80, 90 };
        private readonly int[] _dbTackle = { 0, 10, 20, 30, 40, 40, 40, 40, 50, 50, 50, 60, 60, 60, 70, 75 };
        private readonly int[] _rkDbStrengthCarryCatch = { 0, 10, 20, 30, 30, 30, 40, 40, 40, 40, 40, 50, 50, 60 };
        private readonly int[] _rkDbCover = { 40, 50, 50, 50, 60, 60, 60, 60, 70, 70, 70, 70, 80, 80, 85 };
        private readonly int[] _rkDbTackle = { 10, 20, 30, 30, 30, 40, 40, 40, 50, 50, 50, 50, 60, 60, 60, 70 };

        private readonly int[] _kStrength = { 0, 10, 20, 20, 20, 30, 30, 30, 40, 40, 50, 50 };
        private readonly int[] _kIntelligence = { 30, 40, 50, 50, 50, 60, 60, 60, 60, 70, 70, 80 };
        private readonly int[] _kickStrength = { 60, 70, 70, 70, 70, 70, 80, 80, 80, 80, 80, 80, 90 };
        private readonly int[] _kickAccuracy = { 60, 70, 70, 70, 70, 70, 70, 80, 80, 80, 80, 80, 90 };
        private readonly int[] _kSpeed = { 0, 10, 20, 20, 30, 30, 30, 40, 40, 40, 50, 50 };
        private readonly int[] _rkKStrength = { 0, 10, 20, 20, 20, 30, 30, 30, 40, 40, 40, 50 };
        private readonly int[] _rkKIntelligence = { 30, 40, 40, 50, 50, 50, 50, 60, 60, 60, 60, 70, 70, 70 };
        private readonly int[] _rkKickStrength = { 60, 60, 60, 70, 70, 70, 70, 70, 70, 80, 80, 90 };
        private readonly int[] _rkKickAccuracy = { 60, 60, 70, 70, 70, 70, 70, 70, 80, 80, 90 };
        private readonly int[] _rkKSpeed = { 0, 10, 20, 20, 30, 30, 30, 40, 40, 40, 50, 50 };

        private static int JerseyNumber(string pos)
        {
            var numbers = new List<int>();
            switch (pos)
            {
                case "QB":
                    for (var i = 1; i < 20; i++)
                        numbers.Add(i);
                    break;
                case "RB":
                case "DB":
                    for (var i = 1; i < 50; i++)
                        numbers.Add(i);
                    break;
                case "WR":
                    for (var i = 1; i < 90; i++)
                    {
                        if (i == 20)
                            i = 80;
                        numbers.Add(i);
                    }
                    break;
                case "OL":
                    for (var i = 50; i < 80; i++)
                        numbers.Add(i);
                    break;
                case "DL":
                    for (var i = 50; i < 100; i++)
                    {
                        if (i == 60)
                            i = 70;
                        if (i == 80)
                            i = 90;
                        numbers.Add(i);
                    }
                    break;
                case "LB":
                    for (var i = 1; i < 100; i++)
                    {
                        if (i == 60)
                            i = 90;
                        numbers.Add(i);
                    }
                    break;
                default: // K
                    for (var i = 1; i < 20; i++)
                        numbers.Add(i);
                    break;
            }
            return numbers[new Random().Next(numbers.Count)];
        }

        //GENERATE A LIST OF RANDOM PLAYERS (VETERANS AND ROOKIES) AND PLACE THEM ON RANDOM TEAMS
        //LIST CURRENTLY ASSIGNS RANDOM CONTRACTS AS WELL AND SETS THEIR YEARS LEFT TO JUST 1 FOR RE-SIGNING TESTING
        //FOR LEAGUE GENERATION ALL TEAM IDS WILL BE SET TO 0 FOR THE INAUGURAL DRAFT
        public List<Player> GeneratePlayers(int numberOfTeams)
        {
            var id = Repo.Database.Table<Player>().Count() != 0 ? Repo.Database.Table<Player>().Last().Id : 1;

            var r = new Random();
            var firstNames = File.ReadAllLines("Resources/FirstNames.txt");
            var lastNames = File.ReadAllLines("Resources/LastNames.txt");
            var colleges = File.ReadAllLines("Resources/Colleges.txt");
                       
            var numberOfQb = numberOfTeams * 5;
            var numberOfRb = numberOfTeams * 6;
            var numberOfWr = numberOfTeams * 9;
            var numberOfOl = numberOfTeams * 9;
            var numberOfDl = numberOfTeams * 9;
            var numberOfLb = numberOfTeams * 9;
            var numberOfDb = numberOfTeams * 9;
            var numberOfK = numberOfTeams * 3;

            for (var i = 0; i < numberOfQb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("QB");
                var accuracy = _qbPass[r.Next(_qbPass.Length)] + r.Next(0, 11);
                var armStrength = _qbPass[r.Next(_qbPass.Length)] + r.Next(0, 11);
                var carry = _qbCarry[r.Next(_qbCarry.Length)] + r.Next(0, 11);
                var speed = _qbSpeed[r.Next(_qbSpeed.Length)] + r.Next(0, 11);
                var catching = r.Next(0, 31);
                var tackle = r.Next(0, 31);
                var strength = _qbStrength[r.Next(_qbStrength.Length)] + r.Next(0, 11);
                var intelligence = _qbIntel[r.Next(_qbIntel.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "QB", 0, accuracy, armStrength, strength, carry, speed, catching, 0, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfRb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("RB");
                var carry = _rbCarry[r.Next(_rbCarry.Length)] + r.Next(0, 11);
                var speed = _positionSpeed[r.Next(_positionSpeed.Length)] + r.Next(0, 11);
                var catching = _rbCatch[r.Next(_rbCatch.Length)] + r.Next(0, 11);
                var block = _rbBlock[r.Next(_rbBlock.Length)] + r.Next(0, 11);
                var tackle = r.Next(10, 50);
                var strength = _rbStrength[r.Next(_rbStrength.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "RB", 0, 0, 0, strength, carry, speed, catching, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfWr; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("WR");
                var carry = _wrCarry[r.Next(_wrCarry.Length)] + r.Next(0, 11);
                var speed = _positionSpeed[r.Next(_positionSpeed.Length)] + r.Next(0, 11);
                var catching = _wrCatch[r.Next(_wrCatch.Length)] + r.Next(0, 11);
                var block = _wrBlock[r.Next(_wrBlock.Length)] + r.Next(0, 11);
                var tackle = r.Next(10, 50);
                var strength = _wrStrength[r.Next(_wrStrength.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "WR", 0, 0, 0, strength, carry, speed, catching, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfOl; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("OL");
                var carry = r.Next(0, 50);
                var speed = _oLineSpeed[r.Next(_oLineSpeed.Length)] + r.Next(0, 11);
                var block = _blockBlitz[r.Next(_blockBlitz.Length)] + r.Next(0, 11);
                var tackle = r.Next(0, 40);
                var strength = _lineStrength[r.Next(_lineStrength.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "OL", 0, 0, 0, strength, carry, speed, 0, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfDl; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("DL");
                var carry = r.Next(0, 40);
                var speed = _dLineSpeed[r.Next(_dLineSpeed.Length)] + r.Next(0, 11);
                var blitz = _blockBlitz[r.Next(_blockBlitz.Length)] + r.Next(0, 11);
                var coverage = r.Next(0, 40);
                var tackle = _dLineTackle[r.Next(_dLineTackle.Length)] + r.Next(0, 11);
                var strength = _lineStrength[r.Next(_lineStrength.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "DL", 0, 0, 0, strength, carry, speed, 0, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfLb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("LB");
                var carry = _lbCarryCatch[r.Next(_lbCarryCatch.Length)] + r.Next(0, 11);
                var speed = _lbSpeed[r.Next(_lbSpeed.Length)] + r.Next(0, 11);
                var catching = _lbCarryCatch[r.Next(_lbCarryCatch.Length)] + r.Next(0, 11);
                var blitz = _lbBlitz[r.Next(_lbBlitz.Length)] + r.Next(0, 11);
                var coverage = _lbCoverage[r.Next(_lbCoverage.Length)] + r.Next(0, 11);
                var tackle = _lbTackle[r.Next(_lbTackle.Length)] + r.Next(0, 11);
                var strength = _lbStrength[r.Next(_lbStrength.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "LB", 0, 0, 0, strength, carry, speed, catching, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfDb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("DB");
                var carry = _dbStrengthCarryCatch[r.Next(_dbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var speed = _positionSpeed[r.Next(_positionSpeed.Length)] + r.Next(0, 11);
                var catching = _dbStrengthCarryCatch[r.Next(_dbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var blitz = r.Next(0, 50);
                var coverage = _dbCover[r.Next(_dbCover.Length)] + r.Next(0, 11);
                var tackle = _dbTackle[r.Next(_dbTackle.Length)] + r.Next(0, 11);
                var strength = _dbStrengthCarryCatch[r.Next(_dbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var intelligence = _intelligence[r.Next(_intelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "DB", 0, 0, 0, strength, carry, speed, catching, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfK; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 35);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var exp = age - 20;
                var jersey = JerseyNumber("K");
                var accuracy = r.Next(0, 50);
                var armStrength = r.Next(0, 50);
                var carry = r.Next(0, 40);
                var speed = _kSpeed[r.Next(_kSpeed.Length)] + r.Next(0, 11);
                var catching = r.Next(0, 40);
                var ks = _kickStrength[r.Next(_kickStrength.Length)] + r.Next(0, 11);
                var ka = _kickAccuracy[r.Next(_kickAccuracy.Length)] + r.Next(0, 11);
                var tackle = r.Next(0, 40);
                var strength = _kStrength[r.Next(_kStrength.Length)] + r.Next(0, 11);
                var intelligence = _kIntelligence[r.Next(_kIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, exp, "K", 0, accuracy, armStrength, strength, carry, speed, catching, 0, 0, 0, tackle, intelligence, ks, ka, 0, college, potential));
            }
            return Players;
        }

        //GENERATE A NEW LIST OF ROOKIES
        public List<Player> GenerateRookies(int numberOfTeams)
        {
            var id = Repo.Database.Table<Player>().Count() != 0 ? Repo.Database.Table<Player>().Last().Id : 1;

            var r = new Random();
            var firstNames = File.ReadAllLines("Resources/FirstNames.txt");
            var lastNames = File.ReadAllLines("Resources/LastNames.txt");
            var colleges = File.ReadAllLines("Resources/Colleges.txt");

            var numberOfQb = numberOfTeams * 4;
            var numberOfRb = numberOfTeams * 5;
            var numberOfWr = numberOfTeams * 7;
            var numberOfOl = numberOfTeams * 7;
            var numberOfDl = numberOfTeams * 7;
            var numberOfLb = numberOfTeams * 7;
            var numberOfDb = numberOfTeams * 7;
            var numberOfK = numberOfTeams * 2;

            for (var i = 0; i < numberOfQb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("QB");
                var accuracy = _rkQbPass[r.Next(_rkQbPass.Length)] + r.Next(0, 11);
                var armStrength = _rkQbPass[r.Next(_rkQbPass.Length)] + r.Next(0, 11);
                var carry = _rkQbCarry[r.Next(_rkQbCarry.Length)] + r.Next(0, 11);
                var speed = _rkQbSpeed[r.Next(_rkQbSpeed.Length)] + r.Next(0, 11);
                var catching = r.Next(0, 31);
                var tackle = r.Next(0, 31);
                var strength = _rkQbStrength[r.Next(_rkQbStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkQbIntel[r.Next(_rkQbIntel.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "QB", -1, accuracy, armStrength, strength, carry, speed, catching, 0, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfRb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("RB");
                var carry = _rkRbCarry[r.Next(_rkRbCarry.Length)] + r.Next(0, 11);
                var speed = _rkPositionSpeed[r.Next(_rkPositionSpeed.Length)] + r.Next(0, 11);
                var catching = _rkRbCatch[r.Next(_rkRbCatch.Length)] + r.Next(0, 11);
                var block = _rkRbBlock[r.Next(_rkRbBlock.Length)] + r.Next(0, 11);
                var tackle = r.Next(10, 50);
                var strength = _rkRbStrength[r.Next(_rkRbStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "RB", -1, 0, 0, strength, carry, speed, catching, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfWr; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("WR");
                var carry = _rkWrCarry[r.Next(_rkWrCarry.Length)] + r.Next(0, 11);
                var speed = _rkPositionSpeed[r.Next(_rkPositionSpeed.Length)] + r.Next(0, 11);
                var catching = _rkWrCatch[r.Next(_rkWrCatch.Length)] + r.Next(0, 11);
                var block = _rkWrBlock[r.Next(_rkWrBlock.Length)] + r.Next(0, 11);
                var tackle = r.Next(10, 50);
                var strength = _rkWrStrength[r.Next(_rkWrStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "WR", -1, 0, 0, strength, carry, speed, catching, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfOl; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("OL");
                var carry = r.Next(0, 50);
                var speed = _rkOLineSpeed[r.Next(_rkOLineSpeed.Length)] + r.Next(0, 11);
                var block = _rkBlockBlitz[r.Next(_rkBlockBlitz.Length)] + r.Next(0, 11);
                var tackle = r.Next(0, 40);
                var strength = _rkLineStrength[r.Next(_rkLineStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "OL", -1, 0, 0, strength, carry, speed, 0, block, 0, 0, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfDl; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("DL");
                var carry = r.Next(0, 40);
                var speed = _rkDLineSpeed[r.Next(_rkDLineSpeed.Length)] + r.Next(0, 11);
                var blitz = _rkBlockBlitz[r.Next(_rkBlockBlitz.Length)] + r.Next(0, 11);
                var coverage = r.Next(0, 40);
                var tackle = _rkDLineTackle[r.Next(_rkDLineTackle.Length)] + r.Next(0, 11);
                var strength = _rkLineStrength[r.Next(_rkLineStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "DL", -1, 0, 0, strength, carry, speed, 0, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfLb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("LB");
                var carry = _rkLbCarryCatch[r.Next(_rkLbCarryCatch.Length)] + r.Next(0, 11);
                var speed = _rkLbSpeed[r.Next(_rkLbSpeed.Length)] + r.Next(0, 11);
                var catching = _rkLbCarryCatch[r.Next(_rkLbCarryCatch.Length)] + r.Next(0, 11);
                var blitz = _rkLbBlitz[r.Next(_rkLbBlitz.Length)] + r.Next(0, 11);
                var coverage = _rkLbCoverage[r.Next(_rkLbCoverage.Length)] + r.Next(0, 11);
                var tackle = _rkLbTackle[r.Next(_rkLbTackle.Length)] + r.Next(0, 11);
                var strength = _rkLbStrength[r.Next(_rkLbStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "LB", -1, 0, 0, strength, carry, speed, catching, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfDb; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("DB");
                var carry = _rkDbStrengthCarryCatch[r.Next(_rkDbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var speed = _rkPositionSpeed[r.Next(_rkPositionSpeed.Length)] + r.Next(0, 11);
                var catching = _rkDbStrengthCarryCatch[r.Next(_rkDbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var blitz = r.Next(0, 50);
                var coverage = _rkDbCover[r.Next(_rkDbCover.Length)] + r.Next(0, 11);
                var tackle = _rkDbTackle[r.Next(_rkDbTackle.Length)] + r.Next(0, 11);
                var strength = _rkDbStrengthCarryCatch[r.Next(_rkDbStrengthCarryCatch.Length)] + r.Next(0, 11);
                var intelligence = _rkIntelligence[r.Next(_rkIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "DB", -1, 0, 0, strength, carry, speed, catching, 0, blitz, coverage, tackle, intelligence, 0, 0, 0, college, potential));
            }

            for (var i = 0; i < numberOfK; i++)
            {
                var first = firstNames[r.Next(firstNames.Length)];
                var last = lastNames[r.Next(lastNames.Length)];
                var college = colleges[r.Next(colleges.Length)];
                var age = r.Next(20, 25);
                var years = CurrentLeague.CurDate.AddYears(-age);
                var dob = years.AddDays(r.Next(-365, 1));
                var jersey = JerseyNumber("K");
                var accuracy = r.Next(0, 50);
                var armStrength = r.Next(0, 50);
                var carry = r.Next(0, 40);
                var speed = _rkKSpeed[r.Next(_rkKSpeed.Length)] + r.Next(0, 11);
                var catching = r.Next(0, 40);
                var ks = _rkKickStrength[r.Next(_rkKickStrength.Length)] + r.Next(0, 11);
                var ka = _rkKickAccuracy[r.Next(_rkKickAccuracy.Length)] + r.Next(0, 11);
                var tackle = r.Next(0, 40);
                var strength = _rkKStrength[r.Next(_rkKStrength.Length)] + r.Next(0, 11);
                var intelligence = _rkKIntelligence[r.Next(_rkKIntelligence.Length)] + r.Next(0, 11);
                var potential = r.Next(1, 11) * 10;
                Players.Add(new Player(++id, jersey, first, last, dob, 0, "K", -1, accuracy, armStrength, strength, carry, speed, catching, 0, 0, 0, tackle, intelligence, ks, ka, 0, college, potential));
            }
            return Players;
        }
    }
}

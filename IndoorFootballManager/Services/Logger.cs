﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IndoorFootballManager.Services
{
    public class Logger
    {
        private static string DirectoryPath => $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\";
        private static string FilePath => $"{DirectoryPath}IFM_Log.txt";

        public static async void LogException(Exception x)
        {
            await CheckDirectory;
            var currentContent = await FileContents;

            var sb = new StringBuilder();
            _ = sb.Append("-----------------------------------------------------------------------------");
            _ = sb.Append(Environment.NewLine);
            _ = sb.Append("Date : " + DateTime.Now);
            _ = sb.Append(Environment.NewLine);

            while (x != null)
            {
                _ = sb.Append(x.GetType().FullName);
                _ = sb.Append(Environment.NewLine);
                _ = sb.Append("Message : " + x.Message);
                _ = sb.Append(Environment.NewLine);
                _ = sb.Append("StackTrace : " + x.StackTrace);
                _ = sb.Append(Environment.NewLine);
                x = x.InnerException;
            }

            File.WriteAllText(FilePath, $@"{sb}{currentContent}");
        }

        public static async void LogInformation(string text)
        {
            await CheckDirectory;
            var currentContent = await FileContents;

            var sb = new StringBuilder();
            _ = sb.Append("-----------------------------------------------------------------------------");
            _ = sb.Append(Environment.NewLine);
            _ = sb.Append(text);
            _ = sb.Append(Environment.NewLine);

            File.WriteAllText(FilePath, $@"{sb}{currentContent}");
        }

        private static Task CheckDirectory =>
            Directory.Exists(DirectoryPath)
                ? Task.CompletedTask
                : Task.FromResult(Directory.CreateDirectory(DirectoryPath));

        private static Task<string> FileContents =>
            File.Exists(FilePath)
                ? Task.Run(() =>
                {
                    var currentContent = File.ReadAllText(FilePath);
                    File.Delete(FilePath);
                    return Task.FromResult(currentContent);
                })
                : Task.FromResult(string.Empty);
    }
}

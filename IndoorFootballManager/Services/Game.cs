﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using SQLite;
using System.Xml;
using System.IO;

namespace IndoorFootballManager.Services
{
    public class Game
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Indexed]
        public bool HasPlayed { get; set; } = false;
        public int Season { get; set; }
        public DateTime GameDate { get; set; }
        public int HomeTeam { get; set; }
        public int AwayTeam { get; set; }
        public int Home1st { get; set; }
        public int Away1st { get; set; }
        public int Home2nd { get; set; }
        public int Away2nd { get; set; }
        public int Home3rd { get; set; }
        public int Away3rd { get; set; }
        public int Home4th { get; set; }
        public int Away4th { get; set; }
        public int HomeOT { get; set; }
        public int AwayOT { get; set; }
        public int HomeFinal { get; set; }
        public int AwayFinal { get; set; }
        public string PBP { get; set; }
        public string ScoringPlays { get; set; }
        public bool Playoffs { get; set; }
        public string Conference { get; set; }
        public int HomeTeamRank { get; set; }
        public int AwayTeamRank { get; set; }
        public int WinningTeamRank { get; set; }
        public int WinningTeamId { get; set; }
        public int HomePlaysRun { get; set; }
        public int AwayPlaysRun { get; set; }
        public int PlaysRun { get; set; }
        public double HomeTOP { get; set; }
        public double AwayTOP { get; set; }
        public int Home1stDowns { get; set; }
        public int Away1stDowns { get; set; }
        public int Home3rdDownAtt { get; set; }
        public int Away3rdDownAtt { get; set; }
        public int Home3rdDownComp { get; set; }
        public int Away3rdDownComp { get; set; }
        public int Home4thDownAtt { get; set; }
        public int Away4thDownAtt { get; set; }
        public int Home4thDownComp { get; set; }
        public int Away4thDownComp { get; set; }
        public int HomeTotalYds { get; set; }
        public int AwayTotalYds { get; set; }
        public int HomePassYds { get; set; }
        public int HomePassAtt { get; set; }
        public int HomePassComp { get; set; }
        public int HomePassTd { get; set; }
        public int HomeRushYds { get; set; }
        public int HomeRushAtt { get; set; }
        public int AwayPassYds { get; set; }
        public int AwayPassAtt { get; set; }
        public int AwayPassComp { get; set; }
        public int AwayPassTd { get; set; }
        public int AwayRushYds { get; set; }
        public int AwayRushAtt { get; set; }
        public int HomePenalties { get; set; }
        public int HomePenaltyYds { get; set; }
        public int AwayPenalties { get; set; }
        public int AwayPenaltyYds { get; set; }
        public int HomeFumbles { get; set; }
        public int HomeFumblesLost { get; set; }
        public int HomeInt { get; set; }
        public int AwayFumbles { get; set; }
        public int AwayFumblesLost { get; set; }
        public int AwayInt { get; set; }
        public string StatsJson { get; set; }
        [Obsolete] public byte[] Stats { get; set; }

        public string Display
        {
            get
            {
                try
                {
                    if (AwayTeam == 0) 
                        return string.Empty;

                    var away = Database.GetTeamById(AwayTeam);
                    var home = Database.GetTeamById(HomeTeam);
                    string display;

                    if (!HasPlayed)
                    {
                        display = away.City + " @ " + home.City;
                    }
                    else
                    {
                        if (HomeFinal > AwayFinal)
                        {
                            if (HomeOT + AwayOT > 0)
                                display = GameDate.ToShortDateString() + ": " + home.Abbreviation + " " + HomeFinal + ", " + away.Abbreviation + " " + AwayFinal + " (OT)";
                            else
                                display = GameDate.ToShortDateString() + ": " + home.Abbreviation + " " + HomeFinal + ", " + away.Abbreviation + " " + AwayFinal;
                        }
                        else
                        {
                            if (HomeOT + AwayOT > 0)
                                display = GameDate.ToShortDateString() + ": " + away.Abbreviation + " " + AwayFinal + ", " + home.Abbreviation + " " + HomeFinal + " (OT)";
                            else
                                display = GameDate.ToShortDateString() + ": " + away.Abbreviation + " " + AwayFinal + ", " + home.Abbreviation + " " + HomeFinal;
                        }
                    }

                    return display;
                }
                catch(Exception x)
                {
                    Logger.LogException(x);
                    return string.Empty;
                }
            }
        }

        [Ignore]
        public Team WinningTeam
        {
            get
            {
                var team = Database.GetTeamById(WinningTeamId);
                return team;
            }
            set { }
        }

        [Ignore]
        public string AwayTeamStr
        {
            get
            {
                if (CurrentLeague == null) return string.Empty;
                var away = Database.GetTeamById(AwayTeam).TeamName;
                return away;
            } 
        }

        [Ignore]
        public string AwayTeamAbbr
        {
            get
            {
                if (CurrentLeague == null) return string.Empty;
                var away = Database.GetTeamById(AwayTeam).Abbreviation;
                return away;
            }
        }

        [Ignore]
        public string HomeTeamStr
        {
            get
            {
                if (CurrentLeague == null) return string.Empty;
                var home = Database.GetTeamById(HomeTeam).TeamName;
                return home;
            }
        }

        [Ignore]
        public string HomeTeamAbbr
        {
            get
            {
                if (CurrentLeague == null) return string.Empty;
                var home = Database.GetTeamById(HomeTeam).Abbreviation;
                return home;
            }
        }

        [Ignore]
        public string DateDisplay
        {
            get
            {
                if (!Playoffs) 
                    return $"Game Date: {GameDate.ToShortDateString()}";
                
                return Conference == "Championship"
                    ? $"Championship Date: {GameDate.ToShortDateString()}" 
                    : $"Playoff Date: {GameDate.ToShortDateString()}";
            }
        }

        public Game()
        {

        }

        public Game(int season, int home, int away)
        {
            Season = season;
            HomeTeam = home;
            AwayTeam = away;
        }

        private SQLiteConnection Database => Repo.Database;
        private League CurrentLeague => IFM.CurrentLeague;
        private Team _homeTeam;
        private Team _awayTeam;
        private Strategy _homeStrategy;
        private Strategy _awayStrategy;

        private bool _hasPlayed;
        private string _gameName;
        private int _penaltyMod;
        private bool _useInjuries;
        
        public int homeScore;
        public int[] homeQScore;
        public int awayScore;
        public int[] awayQScore;
        private int _numOt;
        private int _homePlaysRun;
        private int _awayPlaysRun;
        private int _homePenalties;
        private int _homePenaltyYds;
        private int _awayPenalties;
        private int _awayPenaltyYds;
        private double _homeTop;
        private double _awayTop;
        private int _home1StDowns;
        private int _away1StDowns;
        private int _home3RdDownAtt;
        private int _away3RdDownAtt;
        private int _home3RdDownComp;
        private int _away3RdDownComp;
        private int _home4ThDownAtt;
        private int _away4ThDownAtt;
        private int _home4ThDownComp;
        private int _away4ThDownComp;

        public List<PlayerGameStats> PlayersStats;

        internal string pbpLog;
        internal string scoringPlays;

        //private variables used when simming games
        private double _gameTime;
        private double _timeOfPossession;
        private bool _gamePoss; //1 if home, 0 if away
        private int _gameYardLine;
        private int _gameDown;
        private int _gameYardsNeed;
        private bool _playingOt;
        private bool _bottomOt;

        private bool _secondQt;
        private bool _secondHalf;
        private bool _fourthQt;

        private string _homePhilosophy;
        private double _homeCompositeFootIq;
        private double _homeCompositeOlBlock;
        private double _homeRushProf;
        private double _homeRushDef;
        private string _awayPhilosophy;
        private double _awayCompositeFootIq;
        private double _awayCompositeOlBlock;
        private double _awayRushProf;
        private double _awayRushDef;

        private DepthChart _homeChart;
        private List<Player> _homeQBs;
        private List<Player> _homeRBs;
        private List<Player> _homeWRs;
        private List<Player> _homeOl;
        private List<Player> _homeDl;
        private List<Player> _homeLBs;
        private List<Player> _homeDBs;
        private List<Player> _homeKRs;
        private List<Player> _homeKs;

        private DepthChart _awayChart;
        private List<Player> _awayQBs;
        private List<Player> _awayRBs;
        private List<Player> _awayWRs;
        private List<Player> _awayOl;
        private List<Player> _awayDl;
        private List<Player> _awayLBs;
        private List<Player> _awayDBs;
        private List<Player> _awayKRs;
        private List<Player> _awayKs;

        private List<Penalty> _penalties;
        private List<Injury> _injuries;

        private Random _random;

        public Game(Team home, Team away, string name)
        {
            _homeTeam = home;
            _awayTeam = away;
            _gameName = name;
            homeScore = 0;
            homeQScore = new int[10];
            awayScore = 0;
            awayQScore = new int[10];
            _numOt = 0;
            _hasPlayed = false;
        }

        public Game(Team home, Team away)
        {
            Id = 0;
            _homeTeam = home;
            _awayTeam = away;
            HomeTeam = _homeTeam.Id;
            AwayTeam = _awayTeam.Id;
            Season = DateTime.Now.Year;
            GameDate = DateTime.Today;
            _homeTeam.SetDepthChart();
            _awayTeam.SetDepthChart();
            _gameName = "Exhibition Game";
            _useInjuries = false;
            _hasPlayed = false;
        }

        public Game(int id)
        {
            try
            {
                _ = PlayGame(id);
            }
            catch (Exception x)
            {
                Logger.LogException(x);
                App.ShowNotification("Error", "Unable to run game.\nPlease ensure strategy and depth charts are set.", Notification.Wpf.NotificationType.Error, 5);
                throw new Exception();
            }
        }

        private double HomeFieldAdvantage
        {
            get
            {
                //home field advantage
                var compositeFootballIq = (_homeCompositeFootIq - _awayCompositeFootIq) / 5;
                var homeFieldAdvantage =
                    compositeFootballIq > 3
                        ? 3
                        : compositeFootballIq < -3
                            ? -3
                            : compositeFootballIq;
                return _gamePoss ? 3 + homeFieldAdvantage : -homeFieldAdvantage;
            }
        }

        private string ScorePrefix(string type, bool defense = false)
        {
            var possStr = _gamePoss && defense
                ? _awayTeam.Abbreviation
                : !_gamePoss && defense
                    ? _homeTeam.Abbreviation
                    : _gamePoss
                        ? _homeTeam.Abbreviation
                        : _awayTeam.Abbreviation;
            return "\n" + possStr + " | " + type + " | " + ConvGameTime() + " | ";
        }

        private string ScorePostfix => " | " + awayScore + " | " + homeScore;

        private string EventPrefix
        {
            get
            {
                string possStr;
                string defStr;
                if (_gamePoss)
                {
                    possStr = _homeTeam.Abbreviation;
                    defStr = _awayTeam.Abbreviation;
                }
                else
                {
                    possStr = _awayTeam.Abbreviation;
                    defStr = _homeTeam.Abbreviation;
                }
                var yardsNeedAdj = "" + _gameYardsNeed;
                
                if (_gameYardLine + _gameYardsNeed >= 50)
                {
                    yardsNeedAdj = "Goal";
                }

                var gameDownAdj = _gameDown > 4 ? 4 : _gameDown;
                var down = string.Empty;
                var yardLine = _gameYardLine.ToString();
                var sideOfField = possStr;
                if (_gameYardLine > 25)
                {
                    sideOfField = defStr;
                    yardLine = (50 - _gameYardLine).ToString();
                }
                switch (gameDownAdj)
                {
                    case 1:
                        down = "1st";
                        break;
                    case 2:
                        down = "2nd";
                        break;
                    case 3:
                        down = "3rd";
                        break;
                    case 4:
                        down = "4th";
                        break;
                    case -2:
                        down = "Kickoff";
                        break;
                }

                return down == "Kickoff"
                    ? "\n" + defStr + " | " + ConvGameTime() + " | " + sideOfField + " - " + yardLine + " | Kickoff | "
                    : "\n" + possStr + " | " + ConvGameTime() + " | " + sideOfField + " - " + yardLine + " | " + down + " - " + yardsNeedAdj + " | ";
            }
        }

        private int _quarterNumber;

        private string ConvGameTime()
        {
            if (!_playingOt)
            {
                _quarterNumber = (3600 - (int)_gameTime) / 900 + 1;
                if (_gameTime <= 0 && _numOt <= 0)
                { // Prevent Q5 1X:XX from displaying in the game log
                    return "0:00 Q4";
                }
                var minTime = ((int)_gameTime - (900 * (4 - _quarterNumber))) / 60;
                var secTime = (int)_gameTime - (900 * (4 - _quarterNumber)) - (60 * minTime);
                var secStr = secTime < 10 ? $"0{secTime}": $"{secTime}";

                return $"{minTime}:{secStr} Q{_quarterNumber}";
            }

            if (!_bottomOt)
            {
                return $"TOP OT{_numOt}";
            }

            return $"BOT OT{_numOt}";
        }

        private bool _kickOffTeam;
        public async Task PlayGame(int id)
        {
            try
            {
                _random = new Random();
                Game game = null;

                if (_gameName != "Exhibition Game")
                {
                    game = Database.GetGame(id);
                    if (game.HasPlayed)
                    {
                        return;
                    }

                    _gameName = "League Game";
                    _homeTeam = Database.GetTeamById(game.HomeTeam);
                    _awayTeam = Database.GetTeamById(game.AwayTeam);

                    if (_homeTeam.IsAtMinimumRequirements() == false ||
                        _awayTeam.IsAtMinimumRequirements() == false)
                    {
                        App.ShowNotification("Cannot Play Game",
                            $"{_homeTeam.Abbreviation} vs {_awayTeam.Abbreviation} cannot be simulated because one or both teams are below roster minimums.",
                            Notification.Wpf.NotificationType.Error, 5.0);
                        return;
                    }

                    _useInjuries = CurrentLeague.UseInjuries;
                }

                PlayersStats = new List<PlayerGameStats>();
                _injuries = LoadedInjuries;
                _penalties = LoadedPenalties;
                _penaltyMod = CurrentLeague.PenaltyMod;
                _homeStrategy = Database.GetStrategy(_homeTeam.Id);
                _awayStrategy = Database.GetStrategy(_awayTeam.Id);
                SetGameValues(_homeTeam, _awayTeam);

                _gameTime = 3600;
                if (_random.Next(1, 3) == 1)
                {
                    _gamePoss = true;
                    _kickOffTeam = true;
                }
                _gameDown = 1;
                _gameYardsNeed = 10;
                _gameYardLine = 20;

                // Regulation
                while (_gameTime > 0)
                {
                    //play ball!
                    if (_gamePoss)
                        RunPlay(_homeTeam, _awayTeam, _homeStrategy);
                    else
                        RunPlay(_awayTeam, _homeTeam, _awayStrategy);
                }

                // Add last play
                if (homeScore != awayScore)
                {
                    pbpLog += "\n\n---------- END OF THE GAME ----------\n\n";
                    if (homeScore > awayScore)
                        pbpLog += _homeTeam.City + " has defeated " + _awayTeam.City + " by a score of " + homeScore + " - " + awayScore;
                    else
                        pbpLog += _awayTeam.City + " has defeated " + _homeTeam.City + " by a score of " + awayScore + " - " + homeScore;

                    if (!int.TryParse(scoringPlays.Substring(scoringPlays.Length - 1), out _))
                        scoringPlays += ScorePostfix;
                }
                else
                {
                    pbpLog += "\n\n---------- OVERTIME | " + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + " ----------\n";
                }

                //Overtime (if needed)
                if (_gameTime <= 0 && homeScore == awayScore)
                {
                    _playingOt = true;
                    _bottomOt = false;
                    _gamePoss = false;
                    _gameYardLine = 0; //75
                    _numOt++;
                    while (_playingOt)
                    {
                        if (_gamePoss)
                            RunPlay(_homeTeam, _awayTeam, _homeStrategy);
                        else
                            RunPlay(_awayTeam, _homeTeam, _awayStrategy);
                    }

                    pbpLog += "\n\n---------- END OF THE GAME ----------\n\n";
                    if (homeScore > awayScore)
                        pbpLog += _homeTeam.City + " has defeated " + _awayTeam.City + " by a score of " + homeScore + " - " + awayScore;
                    else
                        pbpLog += _awayTeam.City + " has defeated " + _homeTeam.City + " by a score of " + awayScore + " - " + homeScore;

                    if (!int.TryParse(scoringPlays.Substring(scoringPlays.Length - 1), out _))
                        scoringPlays += ScorePostfix;
                }

                if (_gameName != "Exhibition Game")
                {
                    await SaveGameStats();
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                throw new Exception();
            }
        }

        private void SetGameValues(Team h, Team a)
        {
            _homeTeam.SetDepthChart();
            _awayTeam.SetDepthChart();
            _homeChart = Database.GetDepthChart(h.Id);
            _homeQBs = _homeChart.Quarterback.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            if (!_homeQBs.Any())
            {
                // check for fourth QB or use emergency qb
                var qbs = _homeTeam.ActiveRoster.Where(pl => pl.Pos == "QB" && !pl.IsInjured).OrderByDescending(pl => pl.Overall).ToList();
                _homeQBs.Add(qbs.Count > 3 ? qbs[3] : _homeTeam.EmergencyQB);
            }
            _homeRBs = _homeChart.Runningback.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeWRs = _homeChart.WideReceiver.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeOl = _homeChart.OffensiveLine.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeDl = _homeChart.DefensiveLine.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeLBs = _homeChart.Linebacker.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeDBs = _homeChart.DefensiveBack.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeKRs = _homeChart.KickReturner.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeKs = _homeChart.Kicker.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _homeDl.AddRange(_homeLBs.GetRange(1, _homeLBs.Count() - 1));
            _homeLBs.AddRange(_homeDl.GetRange(1, _homeDl.Count() - 1).Where(p => p.Pos != "LB"));
            _homePhilosophy = _homeTeam.OffensivePhilosophyStr;
            _homeCompositeFootIq = _homeTeam.CompositeIntelligence;
            _homeCompositeOlBlock = _homeTeam.CompositeOlBlock;
            _homeRushProf = _homeTeam.RushProf;
            _homeRushDef = _homeTeam.RushDef;

            _awayChart = Database.GetDepthChart(a.Id);
            _awayQBs = _awayChart.Quarterback.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            if (!_awayQBs.Any())
            {
                // check for fourth QB or use emergency qb
                var qbs = _awayTeam.ActiveRoster.Where(pl => pl.Pos == "QB" && !pl.IsInjured).OrderByDescending(pl => pl.Overall).ToList();
                _awayQBs.Add(qbs.Count > 3 ? qbs[3] : _awayTeam.EmergencyQB);
            }
            _awayRBs = _awayChart.Runningback.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayWRs = _awayChart.WideReceiver.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayOl = _awayChart.OffensiveLine.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayDl = _awayChart.DefensiveLine.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayLBs = _awayChart.Linebacker.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayDBs = _awayChart.DefensiveBack.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayKRs = _awayChart.KickReturner.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayKs = _awayChart.Kicker.DeserializeToList<Player>().Where(p => p != null && !p.IsInjured).ToList();
            _awayDl.AddRange(_awayLBs.GetRange(1,_awayLBs.Count() - 1));
            _awayLBs.AddRange(_awayDl.GetRange(1, _awayDl.Count() - 1).Where(p => p.Pos != "LB"));
            _awayPhilosophy = _awayTeam.OffensivePhilosophyStr;
            _awayCompositeFootIq = _awayTeam.CompositeIntelligence;
            _awayCompositeOlBlock = _awayTeam.CompositeOlBlock;
            _awayRushProf = _awayTeam.RushProf;
            _awayRushDef = _awayTeam.RushDef;

            _numOt = 0;
            homeScore = 0;
            homeQScore = new int[10];
            awayScore = 0;
            awayQScore = new int[10];
        }

        private void RunPlay(Team offense, Team defense, Strategy offStrategy)
        {
            QuarterCheck();
            if (_gameDown > 4)
            {
                //gameEventLog += EventPrefix;
                if (!_playingOt)
                {
                    //Log the turnover on downs, reset down and distance, give possession to the defense, exit this RunPlay()
                    pbpLog += "\nTURNOVER ON DOWNS!\n" + offense.City + " failed to convert on " + (_gameDown - 1) + "th down. " + defense.City + " takes over possession on downs.\n";

                    //Turn over on downs, change possession, set to first down and 10 yards to go
                    _gamePoss = !_gamePoss;
                    _gameDown = 1;
                    _gameYardsNeed = 10;
                    //and flip which direction the ball is moving in
                    _gameYardLine = 50 - _gameYardLine;
                }
                else
                {
                    //OT is over for the offense, log the turnover on downs, run ResetForOvertime().
                    pbpLog += "\nTURNOVER ON DOWNS!\n" + offense.City + " failed to convert on " + (_gameDown - 1) + "th down in OT and their possession is over.\n";
                    ResetForOvertime();
                }
            }
            else
            {
                if (_gameTime == 3600)
                {
                    scoringPlays += "TEAM|SCORE|TIME|PLAY|AWAY|HOME";
                    pbpLog += defense.City + " will receive the opening kickoff.\n";
                    _gameDown = -2;
                    if (_kickOffTeam)
                        KickOff(_homeTeam, _awayTeam);
                    else
                        KickOff(_awayTeam, _homeTeam);
                    //gamePoss = !gamePoss;
                }

                // If it's 1st and Goal to go, adjust yards needed to reflect distance for a TD so that play selection reflects actual yards to go
                // If we don't do this, gameYardsNeed may be higher than the actually distance for a TD and suboptimal plays may be chosen
                if (_gameDown == 1 && _gameYardLine >= 41)
                    _gameYardsNeed = 50 - _gameYardLine;

                //Under 30 seconds to play, check that the team with the ball is trailing or tied, do something based on the score difference
                if (_gameTime <= 60 && !_playingOt && ((_gamePoss && (awayScore >= homeScore)) || (!_gamePoss && (homeScore >= awayScore))))
                {
                    //Down by 3 or less, or tied, and you have the ball
                    if (((_gamePoss && (awayScore - homeScore) <= 3) || (!_gamePoss && (homeScore - awayScore) <= 3)) && _gameYardLine < 40) //60
                    {
                        pbpLog += EventPrefix;
                        FieldGoalAtt(offense, defense);
                    }
                    else
                    {
                        pbpLog += EventPrefix;
                        PassingPlay(offense, defense);
                    }
                }
                else if (_gameDown >= 4)
                {
                    if (((_gamePoss && (awayScore - homeScore) > 3) || (!_gamePoss && (homeScore - awayScore) > 3)) && _gameTime < 300)
                    {
                        //go for it since we need 7 to win -- This also forces going for it if down by a TD in BOT OT
                        if (_gameYardsNeed < 3)
                        {
                            pbpLog += EventPrefix;
                            RushingPlay(offense, defense);
                        }
                        else
                        {
                            pbpLog += EventPrefix;
                            PassingPlay(offense, defense);
                        }
                    }
                    else
                    {
                        //4th down
                        if (_gameYardsNeed < 3)
                        {
                            if (_gameYardLine > 30)
                            {
                                pbpLog += EventPrefix;
                                FieldGoalAtt(offense, defense);
                            }
                            else
                            {
                                CheckDown(offense, defense, offStrategy);
                            }
                        }
                        else
                        {
                            if (_gameTime > 300)
                            {
                                pbpLog += EventPrefix;
                                FieldGoalAtt(offense, defense);
                            }
                            else
                            {
                                pbpLog += EventPrefix;
                                PassingPlay(offense, defense);
                            }
                        }
                    }
                }
                else
                {
                    CheckDown(offense, defense, offStrategy);
                }
            }
        }
        
        private void CheckDown(Team offense, Team defense, Strategy offStrategy)
        {
            var chance = Convert.ToInt32(GlobalRandom.NextDouble * 100);
            if (_gameYardsNeed == 10 && _gameDown == 1)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O1st10)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed >= 5 && _gameDown == 1)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O1stLong)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed < 5 && _gameDown == 1)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O1stShort)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed >= 5 && _gameDown == 2)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O2ndLong)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed < 5 && _gameDown == 2)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O2ndShort)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed >= 5 && _gameDown == 3)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O3rdLong)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameYardsNeed < 5 && _gameDown == 3)
            {
                pbpLog += EventPrefix;
                if (chance >= offStrategy.O3rdShort)
                    RushingPlay(offense, defense);
                else
                    PassingPlay(offense, defense);
            }
            else if (_gameTime < 300 || (_gameTime < 2000 && !_secondHalf))
            {
                if (_gameYardsNeed >= 5 && _gameDown == 4)
                {
                    pbpLog += EventPrefix;
                    if (chance >= offStrategy.O4thLong)
                        RushingPlay(offense, defense);
                    else
                        PassingPlay(offense, defense);
                }
                else
                {
                    pbpLog += EventPrefix;
                    if (chance >= offStrategy.O4thShort)
                        RushingPlay(offense, defense);
                    else
                        PassingPlay(offense, defense);
                }
            }
            else switch (_gameDown)
            {
                case 4:
                    pbpLog += EventPrefix;
                    FieldGoalAtt(offense, defense);
                    break;
                case -2 when _playingOt:
                    ResetForOvertime();
                    break;
                case -2 when !_playingOt:
                    KickOff(offense, defense);
                    break;
                default:
                    break;
            }
        }

        private void ResetForOvertime()
        {
            switch (_bottomOt)
            {
                case true when homeScore == awayScore:
                {
                    _gameYardLine = 30;
                    _gameYardsNeed = 10;
                    _gameDown = 1;
                    _numOt++;
                    _gamePoss = (_numOt % 2) == 0;
                    _gameTime = -1;
                    _bottomOt = false;
                    break;
                }
                case false:
                    _gamePoss = !_gamePoss;
                    _gameYardLine = 30;
                    _gameYardsNeed = 10;
                    _gameDown = 1;
                    _gameTime = -1;
                    _bottomOt = true;
                    break;
                default:
                    // game is not tied after both teams had their chance
                    _playingOt = false;
                    break;
            }
        }

        private void PassingPlay(Team offense, Team defense)
        {
            var lineOfScrimmage = _gameYardLine;
            _timeOfPossession = 0.0;

            Player selQb;
            Player selRb;
            List<Player> oLine;
            List<Player> selWRs;
            List<Player> dLinemen;
            List<Player> linebackers;
            List<Player> dBacks;
            double rushDef;
            double compositeOlBlock;

            string logUpdate = null;
            var accepted = false;
            var autoFirst = false;

            if (_gamePoss)
            {
                selQb = _homeQBs[0];
                selRb = _homeRBs[_random.Next(0, _homeRBs.Count)];
                oLine = _homeOl;
                selWRs = _homeWRs;
                compositeOlBlock = _homeCompositeOlBlock;
                dLinemen = _awayDl;
                linebackers = _awayLBs;
                dBacks = _awayDBs;
                rushDef = _awayRushDef;
            }
            else
            {
                selQb = _awayQBs[0];
                selRb = _awayRBs[_random.Next(0, _awayRBs.Count)];
                oLine = _awayOl;
                selWRs = _awayWRs;
                compositeOlBlock = _awayCompositeOlBlock;
                dLinemen = _homeDl;
                linebackers = _homeLBs;
                dBacks = _homeDBs;
                rushDef = _homeRushDef;
            }


            var onField = new Player[2];
            onField[0] = selQb;
            onField[1] = selRb;
            onField = onField.Concat(selWRs).Concat(oLine).Concat(dLinemen).Concat(linebackers).Concat(dBacks).ToArray();
            var r = _random;

            var yardsGain = 0;
            var gotTd = false;
            var gotFumble = false;
            // choose WR to throw to, better WRs more often
            var wr1Pref = Math.Pow(selWRs[0].Catching + selWRs[0].Speed + selWRs[0].Intelligence, 1.06) * GlobalRandom.NextDouble;
            var wr2Pref = Math.Pow(selWRs[1].Catching + selWRs[1].Speed + selWRs[1].Intelligence, 1.03) * GlobalRandom.NextDouble;
            var wr3Pref = Math.Pow(selWRs[2].Catching + selWRs[2].Speed + selWRs[2].Intelligence, 1) * GlobalRandom.NextDouble;
            var wr4Pref = Math.Pow(selRb.Catching + selRb.Speed + selRb.Intelligence, .98) * GlobalRandom.NextDouble;
            // give backups a chance ???
            var wr5Pref = selWRs.Count > 3 ? Math.Pow(selWRs[3].Catching + selWRs[3].Speed + selWRs[3].Intelligence, 0.96) * GlobalRandom.NextDouble : 0.0;
            var wr6Pref = selWRs.Count > 4 ? Math.Pow(selWRs[4].Catching + selWRs[4].Speed + selWRs[4].Intelligence, 0.93) * GlobalRandom.NextDouble : 0.0;
            var wrs = new List<double>() { wr1Pref, wr2Pref, wr3Pref, wr4Pref, wr5Pref, wr6Pref };
            
            Player selWr;
            Player selCb;
            dBacks.OrderByDescending(p => p.Coverage);

            if (wr1Pref == wrs.Max())
            {
                selWr = selWRs[0];
                selCb = dBacks[0];
            }
            else if (wr2Pref == wrs.Max())
            {
                selWr = selWRs[1];
                selCb = dBacks[1]; 
            }
            else if (wr3Pref == wrs.Max())
            {
                selWr = selWRs[2];
                selCb = dBacks[2];
            }
            else if (wr5Pref != 0.0 && wr5Pref == wrs.Max())
            {
                selWr = selWRs[3];
                selCb = dBacks[1];
            }
            else if (wr6Pref != 0.0 && wr6Pref == wrs.Max())
            {
                selWr = selWRs[4];
                selCb = dBacks[2];
            }
            else
            {
                selWr = selRb;
                selCb = linebackers[0];
            }
            
            var penalty = _random.Next(0, 10) > 4 && CheckPenalty(true, "pass", selQb, selRb, selWr, oLine, dLinemen, linebackers, dBacks);
            if (penalty)
            {
                ProcessPresnap(_penalty);
                _penalty = null;
                return;
            }

            if (_gamePoss)
            {
                _homePlaysRun++;
                switch (_gameDown)
                {
                    case 3:
                        _home3RdDownAtt++;
                        break;
                    case 4:
                        _home4ThDownAtt++;
                        break;
                }
            }
            else
            {
                _awayPlaysRun++;
                switch (_gameDown)
                {
                    case 3:
                        _away3RdDownAtt++;
                        break;
                    case 4:
                        _away4ThDownAtt++;
                        break;
                }
            }

            var penalty2 = _random.Next(0, 10) > 4 && CheckPenalty(false, "pass", selQb, selRb, selWr, oLine, dLinemen, linebackers, dBacks);

            //get how much pressure there is on qb, check if sack
            var pressureOnQb = (rushDef * 2 - compositeOlBlock - HomeFieldAdvantage) / 380;
            var sackChance = GlobalRandom.NextDouble * 10;
            if (sackChance < pressureOnQb)
            {
                var injury = _useInjuries != false && CheckInjury(onField);

                if (penalty2)
                {
                    if (_penalty != null && _penalty.Team == "Offense")
                        QbSack();
                    else
                        ProcessAfterPlay(_penalty, lineOfScrimmage, true, false, false, false, yardsGain, out logUpdate, out accepted, out autoFirst);
                    _penalty = null;
                    if (!injury) return;
                    pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                    _injuredPlayer = null;

                    return;
                }

                QbSack();
                if (!injury) return;
                pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                _injuredPlayer = null;
                return;
            }

            var pancakeChance = compositeOlBlock * GlobalRandom.NextDouble;
            if(pancakeChance > compositeOlBlock / 3)
            {
                var randOl = oLine[_random.Next(0, 3)];
                var chance = randOl.Block * GlobalRandom.NextDouble;
                if (chance > randOl.Block / 6)
                    PlayersStats.Add(new PlayerGameStats { PlayerId = randOl.Id, PlayerName = randOl.Name, TeamId = randOl.TeamId, PancakeBlocks = 1 });
            }

            //check for int
            var intChance = (pressureOnQb + ((dBacks[0].Overall + dBacks[1].Overall) / 2) - (2 * (selQb.Accuracy * .75) + selQb.Intelligence + 110) / 4) / 310;
            if (intChance < 0.015)
            {
                intChance = 0.015;
            }
            if (GlobalRandom.NextDouble < intChance)
            {
                if (penalty2)
                {
                    if (_penalty != null && _penalty.Team == "Offense")
                        QbInterception(offense, defense, selQb, selCb);
                    else
                        ProcessAfterPlay(_penalty, lineOfScrimmage, false, true, false, false, yardsGain, out logUpdate, out accepted, out autoFirst);
                    _penalty = null;

                    var injury = _useInjuries && CheckInjury(onField);
                    if (!injury) return;
                    pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                    _injuredPlayer = null;

                    return;
                }

                //Interception
                QbInterception(offense, defense, selQb, selCb);
                return;
            }

            //throw ball, check for completion
            var completion = Math.Abs(((HomeFieldAdvantage + Normalize(selQb.Accuracy) + Normalize(selWr.Catching) - Normalize(selCb.Coverage)) / 2 - pressureOnQb) / 75); 
            var compChance = GlobalRandom.NextDouble;
            if (compChance < completion)
            {
                var dropChance = (100 - selWr.Catching) / 3;
                if (100 * GlobalRandom.NextDouble < dropChance)
                {
                    var injury = _useInjuries != false && CheckInjury(onField);
                    if (injury)
                    {
                        pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                        _injuredPlayer = null;
                    }

                    //drop
                    _gameDown++;
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Drops = 1 });
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassAtt = 1 });

                    pbpLog += selWr.Name + " dropped the pass from " + selQb.Name + ".";

                    _timeOfPossession = 25 * GlobalRandom.NextDouble;

                    // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                    if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;

                    if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                        _timeOfPossession = _gameTime - 2700;
                    else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                        _timeOfPossession = _gameTime - 1800;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                        _timeOfPossession = _gameTime - 900;


                    _gameTime -= _timeOfPossession;
                    if (_gamePoss)
                        _homeTop += _timeOfPossession;
                    else
                        _awayTop += _timeOfPossession;

                    return;
                }
                else
                {
                    //no drop
                    yardsGain = (int)(((Normalize(selQb.ArmStrength) + Normalize(selWr.Speed) - Normalize(selCb.Speed)) * GlobalRandom.NextDouble / 2.7) * .85);
                    //see if receiver can get yards after catch
                    var escapeChance = (Normalize(selWr.Catching) * 2.5 - selCb.Tackle - dBacks[2].Overall) * GlobalRandom.NextDouble;
                    if (escapeChance > 92 || GlobalRandom.NextDouble > 0.95)
                    {
                        yardsGain += Convert.ToInt32(3 + selWr.Speed * GlobalRandom.NextDouble / 5); //3
                    }
                    if (escapeChance > 75 && GlobalRandom.NextDouble < (0.1 / 200))
                    {
                        //wr escapes for TD
                        yardsGain += 50; 
                    }

                    //add yardage
                    _gameYardLine += yardsGain;

                    if (_gameYardLine >= 50) 
                    { //TD!
                        var injury = _useInjuries != false && CheckInjury(onField);
                        if (injury)
                        {
                            pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                            _injuredPlayer = null;
                        }

                        if (penalty2)
                        {
                            ProcessAfterPlay(_penalty, lineOfScrimmage, false, false, false, true, yardsGain, out logUpdate, out accepted, out autoFirst);
                            if (_penalty != null && _penalty.Team == "Offense")
                                return;
                        }

                        yardsGain -= _gameYardLine - 50;
                        _gameYardLine = 47;
                        AddPointsQuarter(6);

                        pbpLog += selQb.Name + " completed the " + yardsGain + " yard pass to " + selWr.Name + ".";
                        if (logUpdate != null)
                            pbpLog += logUpdate;

                        PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassCmp = 1, PassYds = yardsGain, PassTds = 1, PassFirst = 1 });
                        PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Receptions = 1, RecYds = yardsGain, RecTds = 1, RecFirst = 1 });

                        if (yardsGain >= 20)
                        {
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, Pass20 = 1 });
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Rec20 = 1 });
                        }

                        gotTd = true;

                        if (_gamePoss)
                        {
                            switch (_gameDown)
                            {
                                case 3:
                                    _home3RdDownComp++;
                                    break;
                                case 4:
                                    _home4ThDownComp++;
                                    break;
                            }

                            _home1StDowns++;
                        }
                        else
                        {
                            switch (_gameDown)
                            {
                                case 3:
                                    _away3RdDownComp++;
                                    break;
                                case 4:
                                    _away4ThDownComp++;
                                    break;
                            }

                            _away1StDowns++;
                        }

                    }
                    else
                    {
                        if (penalty2)
                        {
                            ProcessAfterPlay(_penalty, lineOfScrimmage, false, false, false, false, yardsGain, out logUpdate, out accepted, out autoFirst);
                            if (_penalty != null && _penalty.Team == "Offense")
                                return;
                        }

                        Player tackler;
                        if (yardsGain > 4 && yardsGain < 10)
                        {
                            tackler = GlobalRandom.NextDouble > 0.4 ? linebackers[_random.Next(0, 2)] : dBacks[_random.Next(0, 3)];
                        }
                        else
                        {
                            tackler = yardsGain <= 4
                                ? GlobalRandom.NextDouble > 0.4 ? dLinemen[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)]
                                : GlobalRandom.NextDouble > 0.2 ? dBacks[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)];
                        }

                        PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Tackles = 1 });

                        pbpLog += selQb.Name + " completed the " + yardsGain + " yard pass to " + selWr.Name + ".";
                        if (logUpdate != null)
                            pbpLog += logUpdate;

                        PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassCmp = 1, PassYds = yardsGain });
                        PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Receptions = 1, RecYds = yardsGain });
                        if (yardsGain >= 20)
                        {
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, Pass20 = 1 });
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Rec20 = 1 });
                        }

                        //check for fumble
                        var fumChance = (tackler.Strength + selCb.Strength) / selWr.Carry;
                        if (accepted == false && GlobalRandom.NextDouble < fumChance / (155 + GlobalRandom.NextDouble))
                        {
                            PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, ForFumb = 1 });
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, Fumbles = 1 });

                            pbpLog += "\nFUMBLE!\n";
                            //Fumble!
                            if (_random.Next(1, 10) > 6)
                            {
                                pbpLog += defense.City + " has recovered the fumble and will take possession!\n";

                                gotFumble = true;
                                if (yardsGain > 4 && yardsGain < 10)
                                {
                                    tackler = GlobalRandom.NextDouble > 0.4 ? linebackers[_random.Next(0, 2)] : dBacks[_random.Next(0, 3)];
                                }
                                else
                                {
                                    tackler = yardsGain <= 4
                                        ? GlobalRandom.NextDouble > 0.4 ? dLinemen[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)]
                                        : GlobalRandom.NextDouble > 0.2 ? dBacks[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)];
                                }


                                PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, FumblesLost = 1 });
                                PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, ForFumb = 1 });
                            }
                            else
                            {
                                gotFumble = false;
                                pbpLog += offense.City + " recovered the fumble and will keep possession.";

                            }
                        }
                    }

                    if (!gotTd && !gotFumble)
                    {
                        var injury = _useInjuries != false && CheckInjury(onField);
                        if (injury)
                        {
                            pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                            _injuredPlayer = null;
                        }

                        //check downs if there wasn't fumble or TD
                        _gameYardsNeed -= yardsGain;

                        if (_gameYardsNeed <= 0)
                        {
                            // Only set new down and distance if there wasn't a TD
                            _gameDown = 1;
                            _gameYardsNeed = 10;

                            PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassFirst = 1 });
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selWr.Id, PlayerName = selWr.Name, TeamId = selWr.TeamId, RecFirst = 1 });

                            if (_gamePoss)
                            {
                                switch (_gameDown)
                                {
                                    case 3:
                                        _home3RdDownComp++;
                                        break;
                                    case 4:
                                        _home4ThDownComp++;
                                        break;
                                }

                                _home1StDowns++;
                            }
                            else
                            {
                                switch (_gameDown)
                                {
                                    case 3:
                                        _away3RdDownComp++;
                                        break;
                                    case 4:
                                        _away4ThDownComp++;
                                        break;
                                }

                                _away1StDowns++;
                            }

                        }
                        else
                            _gameDown++;


                        if (autoFirst || (accepted && _penalty != null && _penalty.Text == "Facemask"))
                        {
                            _gameDown = 1;
                            _gameYardsNeed = 10;
                        }

                    }
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassAtt = 1 });
                }
            }
            else
            {
                if (penalty2)
                {
                    ProcessAfterPlay(_penalty, lineOfScrimmage, false, false, false, false, yardsGain, out logUpdate, out accepted, out autoFirst);
                    if (_penalty != null && _penalty.Team == "Offense")
                    {
                        _penalty = null;
                        return;
                    }
                    _penalty = null;
                }

                PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassAtt = 1 });

                var breakupChance = selCb.Coverage * GlobalRandom.NextDouble;
                if (breakupChance > selCb.Coverage * GlobalRandom.NextDouble / 5)
                {
                    pbpLog += selQb.Name + " pass to " + selWr.Name + " broken up by " + selCb.Name + ".";
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selCb.Id, PlayerName = selCb.Name, TeamId = selCb.TeamId, PassBreakups = 1 });
                }
                else
                    pbpLog += selQb.Name + " pass to " + selWr.Name + " falls incomplete.";

                _gameDown++;
                //Incomplete pass stops the clock, so just run time for how long the play took, then move on

                _timeOfPossession = 5 + 15 * GlobalRandom.NextDouble;
                // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                    _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                    _timeOfPossession -= 5 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _homeTop += _timeOfPossession;
                else
                    _awayTop += _timeOfPossession;

                if (autoFirst)
                {
                    _gameDown = 1;
                    _gameYardsNeed = 10;
                }

                return;
            }

            if (gotFumble)
            {
                if (!_playingOt)
                {
                    _gameDown = 1;
                    _gameYardsNeed = 10;
                    _gamePoss = !_gamePoss;
                    _gameYardLine = 50 - _gameYardLine;

                    _timeOfPossession = 20 * GlobalRandom.NextDouble;

                    // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                    if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;

                    if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                        _timeOfPossession = _gameTime - 2700;
                    else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                        _timeOfPossession = _gameTime - 1800;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                        _timeOfPossession = _gameTime - 900;

                    _gameTime -= _timeOfPossession;
                    if (_gamePoss)
                        _homeTop += _timeOfPossession;
                    else
                        _awayTop += _timeOfPossession;

                    return;
                }
                else
                {
                    ResetForOvertime();
                    return;
                }
            }

            if (gotTd)
            {
                scoringPlays += ScorePrefix("TD") + selWr.ShortName + " " + yardsGain + "-yd pass from " + selQb.ShortName;
                _timeOfPossession = _random.Next(20,45) + 65 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                { // home possession
                    homeScore += 6;
                    _homeTop += _timeOfPossession;
                }
                else
                {
                    awayScore += 6;
                    _awayTop += _timeOfPossession;
                }

                pbpLog += "\nTOUCHDOWN!\n";

                KickXp(offense, defense);
                if (!_playingOt)
                    KickOff(offense, defense);
                else
                    ResetForOvertime();
                return;
            }

            _timeOfPossession = _random.Next(10, 25) + 25 * GlobalRandom.NextDouble;

            // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
            if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;

            if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                _timeOfPossession = _gameTime - 2700;
            else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                _timeOfPossession = _gameTime - 1800;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                _timeOfPossession = _gameTime - 900;

            _gameTime -= _timeOfPossession;
            if (_gamePoss)
                _homeTop += _timeOfPossession;
            else
                _awayTop += _timeOfPossession;
        }

        private void RushingPlay(Team offense, Team defense)
        {
            var lineOfScrimmage = _gameYardLine;
            _timeOfPossession = 0.0;
            string offPhilosophy;
            var gotTd = false;
            Player selQb;
            Player selRb;
            List<Player> rBs;
            List<Player> selWRs;
            List<Player> oLine;
            List<Player> dLinemen;
            List<Player> linebackers;
            List<Player> dBacks;

            double rb1Pref;
            double rb2Pref;
            double rb3Pref;
            double compositeOlBlock;
            double rushDef;
            double rushProf;
            string logUpdate = null;
            var accepted = false;
            var autoFirst = false;

            if (_gamePoss)
            {
                selQb = _homeQBs[0];
                rBs = _homeRBs;
                oLine = _homeOl;
                selWRs = _homeWRs;
                offPhilosophy = _homePhilosophy;
                compositeOlBlock = _homeCompositeOlBlock;
                rushProf = _homeRushProf;
                dLinemen = _awayDl;
                linebackers = _awayLBs;
                dBacks = _awayDBs;
                rushDef = _awayRushDef;

            }
            else
            {
                selQb = _awayQBs[0];
                rBs = _awayRBs;
                oLine = _awayOl;
                selWRs = _awayWRs;
                offPhilosophy = _awayPhilosophy;
                compositeOlBlock = _awayCompositeOlBlock;
                rushProf = _awayRushProf;
                dLinemen = _homeDl;
                linebackers = _homeLBs;
                dBacks = _homeDBs;
                rushDef = _homeRushDef;
            }

            if (offPhilosophy == "ThreeWRs")
            {
                rb1Pref = Math.Pow(rBs[0].Overall, 1.3) * GlobalRandom.NextDouble;
                rb2Pref = Math.Pow(rBs[1].Overall, 1.05) * GlobalRandom.NextDouble;
                rb3Pref = Math.Pow(selQb.Speed, 1) * GlobalRandom.NextDouble;
                if (rb1Pref > rb2Pref && rb1Pref > rb3Pref)
                    selRb = rBs[0];
                else if (rb2Pref > rb1Pref && rb2Pref > rb3Pref)
                    selRb = rBs[1];
                else
                    selRb = selQb;
            }
            else if (offPhilosophy == "TwoRBs")
            {
                rb1Pref = Math.Pow(rBs[0].Overall, 1.1) * GlobalRandom.NextDouble;
                rb2Pref = Math.Pow(rBs[1].Overall, 1.05) * GlobalRandom.NextDouble;
                rb3Pref = Math.Pow(selQb.Speed, 1) * GlobalRandom.NextDouble;
                if (rb1Pref > rb2Pref && rb1Pref > rb3Pref)
                    selRb = rBs[0];
                else if (rb2Pref > rb1Pref && rb2Pref > rb3Pref)
                    selRb = rBs[1];
                else
                    selRb = selQb;
            }
            else
            {
                rb1Pref = Math.Pow(rBs[0].Overall, 1.2) * GlobalRandom.NextDouble;
                rb2Pref = Math.Pow(rBs[1].Overall, 1.15) * GlobalRandom.NextDouble;
                if (rBs[2] != null)
                    rb3Pref = Math.Pow(rBs[2].Overall, 1.05) * GlobalRandom.NextDouble;
                else
                    rb3Pref = 0;
                if (rb1Pref > rb2Pref && rb1Pref > rb3Pref)
                    selRb = rBs[0];
                else if (rb2Pref > rb1Pref && rb2Pref > rb3Pref)
                    selRb = rBs[1];
                else
                    selRb = rBs[2];
            }

            var penalty = _random.Next(0, 10) > 4 && CheckPenalty(true, "run", selQb, selRb, selWRs[_random.Next(0, 3)], oLine, dLinemen, linebackers, dBacks);
            if (penalty)
            {
                ProcessPresnap(_penalty);
                _penalty = null;
                return;
            }

            var onField = new Player[2];
            onField[0] = selQb;
            onField[1] = selRb;
            onField = onField.Concat(selWRs).Concat(oLine).Concat(dLinemen).Concat(linebackers).Concat(dBacks).ToArray();

            if (_gamePoss)
            {
                _homePlaysRun++;
                switch (_gameDown)
                {
                    case 3:
                        _home3RdDownAtt++;
                        break;
                    case 4:
                        _home4ThDownAtt++;
                        break;
                }
            }
            else
            {
                _awayPlaysRun++;
                switch (_gameDown)
                {
                    case 3:
                        _away3RdDownAtt++;
                        break;
                    case 4:
                        _away4ThDownAtt++;
                        break;
                }
            }

            var pancakeChance = compositeOlBlock * GlobalRandom.NextDouble;
            if (pancakeChance > compositeOlBlock / 3)
            {
                var randOl = oLine[_random.Next(0, 3)];
                var chance = randOl.Block * GlobalRandom.NextDouble;
                if (chance > randOl.Block / 5)
                    PlayersStats.Add(new PlayerGameStats { PlayerId = randOl.Id, PlayerName = randOl.Name, TeamId = randOl.TeamId, PancakeBlocks = 1 });
            }

            var blockAdv = compositeOlBlock - rushDef + (GlobalRandom.NextDouble + 3);
            var yardsGain = (int)(((selRb.Speed / 7) + blockAdv + HomeFieldAdvantage) * GlobalRandom.NextDouble * 0.5); // * 2 //(double)offense.teamStratOff.RYB / 2 - (double)defense.teamStratDef.RYB / 2);
            if (yardsGain < 2)
                yardsGain += _random.Next(-1, 3);
            else
            {
                //break free from tackles
                if (GlobalRandom.NextDouble < Math.Abs((0.2 / (rushProf - rushDef + GlobalRandom.NextDouble) / (GlobalRandom.NextDouble + 2)))) //(offense.teamStratOff.RAB - (double)defense.teamStratDef.RYB / 2) / 50))
                    yardsGain +=  Convert.ToInt32(selRb.Speed / 6 * GlobalRandom.NextDouble);
            }
            var penalty2 = _random.Next(0, 10) > 4 && CheckPenalty(false, "run", selQb, selRb, selWRs[_random.Next(0, 3)], oLine, dLinemen, linebackers, dBacks);

            //add yardage
            _gameYardLine += yardsGain;
            if (_gameYardLine >= 50)
            { //TD!
                if (penalty2)
                {
                    ProcessAfterPlay(_penalty, lineOfScrimmage, false, false, false, true, yardsGain, out logUpdate, out accepted, out autoFirst);
                    if (_penalty != null && _penalty.Team == "Offense")
                    {
                        _penalty = null;
                        return;
                    }
                    _penalty = null;
                }

                AddPointsQuarter(6);
                yardsGain -= (_gameYardLine - 50);

                _gameYardLine = 47; //3 yards from endzone? 
                if (_gamePoss)
                { // home possession
                    homeScore += 6;
                }
                else
                {
                    awayScore += 6;
                }

                gotTd = true;
            }
            
            //check downs if there wasn't TD
            if (!gotTd)
            {
                //check downs 
                _gameYardsNeed -= yardsGain;
                if (_gameYardsNeed <= 0)
                {
                    if (_gamePoss)
                    {
                        switch (_gameDown)
                        {
                            case 3:
                                _home3RdDownComp++;
                                break;
                            case 4:
                                _home4ThDownComp++;
                                break;
                        }

                        _home1StDowns++;
                    }
                    else
                    {
                        switch (_gameDown)
                        {
                            case 3:
                                _away3RdDownComp++;
                                break;
                            case 4:
                                _away4ThDownComp++;
                                break;
                        }

                        _away1StDowns++;
                    }

                    // Only set new down and distance if there wasn't a TD
                    _gameDown = 1;
                    _gameYardsNeed = 10;

                    PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, RushFirst = 1 });

                }
                else
                {
                    _gameDown++;
                }
            }


            if (gotTd)
            {
                //if (penalty2_)
                //{
                //    ProcessAfterPlay(penalty, lineOfScrimmage, false, false, false, true, yardsGain, out logUpdate, out accepted, out autoFirst);
                //    if (penalty != null && penalty.Team == "Offense")
                //    {
                //        penalty = null;
                //        return;
                //    }
                //    else
                //        penalty = null;
                //}

                PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, Carries = 1, RushYds = yardsGain, RushTds = 1, RushFirst = 1 });
                if (yardsGain >= 20)
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, Rush20 = 1 });

                if (_gamePoss)
                {
                    switch (_gameDown)
                    {
                        case 3:
                            _home3RdDownComp++;
                            break;
                        case 4:
                            _home4ThDownComp++;
                            break;
                    }

                    _home1StDowns++;
                }
                else
                {
                    switch (_gameDown)
                    {
                        case 3:
                            _away3RdDownComp++;
                            break;
                        case 4:
                            _away4ThDownComp++;
                            break;
                    }

                    _away1StDowns++;
                }

                pbpLog += selRb.Name + " rushed for " + yardsGain + " yards.\nTOUCHDOWN!\n";
                scoringPlays += ScorePrefix("TD") + selRb.ShortName + " " + yardsGain + "-yd run";

                _timeOfPossession = _random.Next(0, 34) + 55 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _homeTop += _timeOfPossession;
                else
                    _awayTop += _timeOfPossession;

                var injury = _useInjuries && CheckInjury(onField);
                if (injury)
                {
                    pbpLog += _injuredPlayer.ShortNameWithPosition + " has been injured.";
                    _injuredPlayer = null;
                }

                if (logUpdate != null)
                    pbpLog += logUpdate;

                KickXp(offense, defense);
                if (!_playingOt)
                    KickOff(offense, defense);
                else
                    ResetForOvertime();
            }
            else
            {
                pbpLog += selRb.Name + " rushed for " + yardsGain + " yards.";

                Player tackler;
                if (yardsGain > 4 && yardsGain < 10)
                {
                    tackler = GlobalRandom.NextDouble > 0.4 ? linebackers[_random.Next(0, 2)] : dBacks[_random.Next(0, 3)];
                }
                else
                {
                    tackler = yardsGain <= 4
                        ? GlobalRandom.NextDouble > 0.4 ? dLinemen[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)]
                        : GlobalRandom.NextDouble > 0.2 ? dBacks[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)];
                }

                PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Tackles = 1 });
                PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, Carries = 1, RushYds = yardsGain });
                if (yardsGain >= 20)
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, Rush20 = 1 });

                _timeOfPossession = _random.Next(20, 30) + 20 * GlobalRandom.NextDouble;

                switch (yardsGain < 1)
                {
                    // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                    case true when !_secondHalf && _gameTime - _timeOfPossession < 1860:
                    case true when !_fourthQt && _gameTime - _timeOfPossession < 60:
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                        break;
                }

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _homeTop += _timeOfPossession;
                else
                    _awayTop += _timeOfPossession;

                if (penalty2)
                {
                    ProcessAfterPlay(_penalty, lineOfScrimmage, false, false, false, false, yardsGain, out logUpdate, out accepted, out autoFirst);
                    if (_penalty != null && _penalty.Team == "Offense")
                    {
                        _penalty = null;
                        return;
                    }
                    else
                        _penalty = null;
                }

                if (logUpdate != null)
                    pbpLog += logUpdate;

                if (autoFirst)
                {
                    _gameDown = 1;
                    _gameYardsNeed = 10;
                }

                //check for Strength
                var fumChance = (tackler.Strength + defense.RushDef) / selRb.Carry;
                if (accepted == false && GlobalRandom.NextDouble < fumChance / (155 + GlobalRandom.NextDouble))
                {
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, Fumbles = 1 });
                    PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, ForFumb = 1 });
                    pbpLog += "\nFUMBLE!\n";

                    if (_random.Next(1, 10) > 6)
                    {
                        if (yardsGain >= 4)
                        {
                            if (GlobalRandom.NextDouble > 0.4)
                                tackler = dLinemen[_random.Next(0, 3)];
                            else
                                tackler = linebackers[_random.Next(0, 2)];
                        }
                        else if (yardsGain > 4 && yardsGain < 10)
                        {
                            tackler = GlobalRandom.NextDouble > 0.4 ? linebackers[_random.Next(0, 2)] : dBacks[_random.Next(0, 3)];
                        }
                        else
                        {
                            tackler = GlobalRandom.NextDouble > 0.2 ? dBacks[_random.Next(0, 3)] : linebackers[_random.Next(0, 2)];
                        }

                        PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, FumblesLost = 1 });
                        PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, FumbRec = 1 });
                        pbpLog += defense.City + " has recovered the fumble and will take possession!\n";
                        if (!_playingOt)
                        {
                            _gameDown = 1;
                            _gameYardsNeed = 10;
                            _gamePoss = !_gamePoss;
                            _gameYardLine = 50 - _gameYardLine;
                        }
                        else
                        {
                            ResetForOvertime();
                        }
                    }
                    else
                    {
                        pbpLog += offense.City + " recovered the fumble and will keep possession.";
                    }
                }
                var injury = _useInjuries && CheckInjury(onField);
                if (!injury) return;
                pbpLog += "\n" + _injuredPlayer.ShortNameWithPosition + " has been injured.";
                _injuredPlayer = null;
            }
        }

        // TODO: implement penalties and injuries
        private void FieldGoalAtt(Team offense, Team defense)
        {
            var lineOfScrimmage = _gameYardLine;
            _timeOfPossession = 0.0;
            Player selK;
            Player selKr;
            Player selQb;
            Player selRb;
            List<Player> selOl;
            List<Player> selDl;
            List<Player> selLBs;
            List<Player> selDBs;
            if (!_gamePoss)
            {
                selK = _awayKs[0];
                selQb = _awayQBs[0];
                selOl = _awayOl;
                selRb = _awayRBs[0];
                selKr = _homeKRs[0];
                selDl = _homeDl;
                selLBs = _homeLBs;
                selDBs = _homeDBs;
            }
            else
            {
                selK = _homeKs[0];
                selQb = _homeQBs[0];
                selRb = _homeRBs[0];
                selOl = _homeOl;
                selKr = _awayKRs[0];
                selDl = _awayDl;
                selLBs = _awayLBs;
                selDBs = _awayDBs;
            }

            var penalty = _random.Next(0, 6) > 4 && CheckPenalty(true, "kick", selQb, selRb, null, selOl, selDl, selLBs, selDBs);
            if (penalty)
            {
                ProcessPresnap(_penalty);
                _penalty = null;
                return;
            }

            var fgDistRatio = Math.Pow((60 - _gameYardLine) / 50.0, 2);
            var fgAccRatio = Math.Pow((60 - _gameYardLine) / 50.0, 1.25);
            var fgDistChance = selK.KickStrength - fgDistRatio * (60 + _gameYardLine * 2);
            var fgAccChance = selK.KickAccuracy - fgAccRatio * (60 + _gameYardLine * 2);
            if (fgDistChance >= 10 && fgAccChance * GlobalRandom.NextDouble > 8)
            {
                // made the fg
                if (_gamePoss)
                { // home possession
                    homeScore += 3;
                }
                else
                {
                    awayScore += 3;
                }

                PlayersStats.Add(new PlayerGameStats { PlayerId = selK.Id, PlayerName = selK.Name, TeamId = selK.TeamId, FGAtt = 1, FGMade = 1, FGLong = (65 - _gameYardLine) });
                pbpLog += selK.Name + " made the " + (65 - _gameYardLine) + " yard field goal.\n";
                pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";
                
                scoringPlays += ScorePrefix("FG") + selK.ShortName + " " + (65 - _gameYardLine) + "-yd field goal" + ScorePostfix;
                AddPointsQuarter(3);

                if (!_playingOt)
                {
                    _timeOfPossession = _random.Next(10, 40);

                    if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                        _timeOfPossession = _gameTime - 2700;
                    else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                        _timeOfPossession = _gameTime - 1800;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                        _timeOfPossession = _gameTime - 900;

                    _gameTime -= _timeOfPossession;
                    if (_gamePoss)
                        _homeTop += _timeOfPossession;
                    else
                        _awayTop += _timeOfPossession;

                    _gameDown = -2;
                    _gameYardsNeed = 0;
                    KickOff(offense, defense);
                }
                else
                {
                    ResetForOvertime();
                }

            }
            else
            {
                //miss
                pbpLog += selK.Name + " missed the " + (65 - _gameYardLine) + " yard field goal.\n";
                PlayersStats.Add(new PlayerGameStats { PlayerId = selK.Id, PlayerName = selK.Name, TeamId = selK.TeamId, FGAtt = 1 });

                if (!_playingOt)
                {
                    _timeOfPossession = _random.Next(10, 40);

                    // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                    if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                        _timeOfPossession -= 5 * GlobalRandom.NextDouble;

                    if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                        _timeOfPossession = _gameTime - 2700;
                    else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                        _timeOfPossession = _gameTime - 1800;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                        _timeOfPossession = _gameTime - 900;

                    _gameTime -= _timeOfPossession;
                    if (_gamePoss)
                        _homeTop += _timeOfPossession;
                    else
                        _awayTop += _timeOfPossession;

                    var tooShort = fgDistChance > 35.00;
                    ReturnPlay(offense, defense, selK, selKr, tooShort);
                }
                else
                {
                    ResetForOvertime();
                }
            }
        }

        private void KickXp(Team offense, Team defense)
        {
            Player selQb;
            Player selRb;
            Player selWr;
            Player selCb;
            Player selK;
            if (_gamePoss)
            {
                selQb = _homeQBs[0];
                selRb = _homeRBs[0];
                selWr = _homeWRs[_random.Next(0, 2)];
                selK = _homeKs[0];
                selCb = _awayDBs[_random.Next(0, 2)];
            }
            else
            {
                selQb = _awayQBs[0];
                selRb = _awayRBs[0];
                selWr = _awayWRs[_random.Next(0, 2)];
                selK = _awayKs[0];
                selCb = _homeDBs[_random.Next(0, 2)];
            }

            if ((_numOt >= 3) || (((_gamePoss && (awayScore - homeScore) == 2) || (!_gamePoss && (homeScore - awayScore) == 2)) && _gameTime < 300))
            {
                //go for 2
                if (GlobalRandom.NextDouble <= 0.50)
                {
                    //rushing
                    var blockAdv = offense.CompositeOlBlock - defense.RushDef;
                    var yardsGain = (int)((selRb.Speed + blockAdv) * GlobalRandom.NextDouble / 6);
                    if (yardsGain > 5)
                    {
                        if (_gamePoss)
                        { // home possession
                            homeScore += 2;
                        }
                        else
                        {
                            awayScore += 2;
                        }
                        AddPointsQuarter(2);
                        pbpLog += selRb.Name + " rushed for the 2pt conversion.";
                        pbpLog += "\n" + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";

                        scoringPlays += " (" + selRb.Name + " 2-pt run)" + ScorePostfix;

                    }
                    else
                    {
                        pbpLog += selRb.Name + " stopped at the line of scrimmage, failed the 2pt conversion.\n";
                        pbpLog += "\n" + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";

                        scoringPlays += " (" + selRb.Name + " 2-pt run failed)" + ScorePostfix;
                    }
                }
                else
                {
                    var pressureOnQb = defense.RushDef * 2 - offense.CompositeOlBlock;
                    var completion = (Normalize(selQb.Accuracy) + selWr.Catching - selCb.Coverage) / 2 - pressureOnQb / 380;
                    var compChance = 100 * GlobalRandom.NextDouble;
                    if (compChance < completion)
                    {
                        if (_gamePoss)
                        { // home possession
                            homeScore += 2;
                        }
                        else
                        {
                            awayScore += 2;
                        }
                        AddPointsQuarter(2);
                        pbpLog += selQb.Name + " completed the pass to " + selQb.Name + " for the 2pt conversion.\n";
                        pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";

                        scoringPlays += " (" + selQb.Name + " 2-pt pass)" + ScorePostfix;
                    }
                    else
                    {
                        pbpLog += selQb.Name + "'s pass incomplete to " + selWr.Name + " for the failed 2pt conversion.\n";
                        pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";

                        scoringPlays += " (" + selQb.Name + " 2-pt pass failed)" + ScorePostfix;
                    }
                }
            }
            else
            {
                //kick XP
                if (GlobalRandom.NextDouble * 100 < 23 + selK.KickAccuracy && GlobalRandom.NextDouble > 0.075)
                {
                    //made XP
                    if (_gamePoss)
                    { // home possession
                        homeScore += 1;
                    }
                    else
                    {
                        awayScore += 1;
                    }
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selK.Id, PlayerName = selK.Name, TeamId = selK.TeamId, XPAtt = 1, XPMade = 1 });
                    pbpLog += selK.Name + " made the XP.\n";
                    pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";
                    scoringPlays += " (" + selK.Name + " XP)" + ScorePostfix;
                    AddPointsQuarter(1);
                }
                else
                {
                    // missed XP
                    pbpLog += selK.Name + " missed the XP.\n";
                    pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";
                    scoringPlays += " (" + selK.Name + " XP failed)" + ScorePostfix;
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selK.Id, PlayerName = selK.Name, TeamId = selK.TeamId, XPAtt = 1 });
                }
            }
            
            _gameDown = -2;
            _gameYardsNeed = 0;
        }

        // TODO: implement penalties and injuries
        private void KickOff(Team offense, Team defense)
        {
            _timeOfPossession = 0.0;
            _gameYardLine = 0;
            var gotTd = false;
            if (_gameTime <= 0 || (_gameTime <= 2700 && !_secondQt) || (_gameTime <= 1800 && !_secondHalf) || (_gameTime <= 900 && !_fourthQt))
            {
                return;
            }
            else
            {
                Player selK;
                Player selKr;
                var defenders = new List<Player>();
                if (_gamePoss)
                {
                    selK = _homeKs[0];
                    selKr = _awayKRs[0];
                    defenders.AddRange(_homeDl);
                    defenders.AddRange(_homeLBs);
                    defenders.AddRange(_homeDBs);
                }
                else
                {
                    selK = _awayKs[0];
                    selKr = _homeKRs[0];
                    defenders.AddRange(_awayDl);
                    defenders.AddRange(_awayLBs);
                    defenders.AddRange(_awayDBs);
                }

                pbpLog += EventPrefix;
                //Decide whether to onside kick. Only if losing but within 8 points with < 3 min to go
                if (_gameTime < 180 && ((_gamePoss && (awayScore - homeScore) <= 8 && (awayScore - homeScore) > 0) || (!_gamePoss && (homeScore - awayScore) <= 8 && (homeScore - awayScore) > 0)))
                {
                    // Yes, do onside
                    if (selK.KickAccuracy * GlobalRandom.NextDouble > 60 || GlobalRandom.NextDouble < 0.1)
                    {
                        //Success!
                        pbpLog += selK.Name + " successfully executes onside kick! " + offense.City + " has possession!";
                    }
                    else
                    {
                        // Fail
                        pbpLog += selK.Name + " failed the onside kick and lost possession.";
                        _gamePoss = !_gamePoss;
                    }
                    _gameYardLine = _random.Next(20, 30); //50
                    _gameDown = 1;
                    _gameYardsNeed = 10;

                    var tackler = defenders[_random.Next(0, defenders.Count)];
                    PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Tackles = 1 });

                    _timeOfPossession = _random.Next(0, 5) + 5 * GlobalRandom.NextDouble;

                    if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                        _timeOfPossession = _gameTime - 2700;
                    else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                        _timeOfPossession = _gameTime - 1800;
                    else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                        _timeOfPossession = _gameTime - 900;

                    _gameTime -= _timeOfPossession;
                    if (_gamePoss)
                        _homeTop += _timeOfPossession;
                    else
                        _awayTop += _timeOfPossession;
                }
                else
                {   // figure in a very tiny chance of a rouge  (very very small chance!) using kick strength and accuracy
                    var start = _random.Next(-5, 5);
                    _gameYardLine = start + (50 - ((selK.KickStrength / 2) - (int)((selKr.Speed / 5.0) * GlobalRandom.NextDouble)));
                    _gameYardLine += _random.Next(0, 50) > 47 ? Convert.ToInt32(Math.Round(selKr.Speed * GlobalRandom.NextDouble)) : 0;
                    if (start < 0)
                        start *= -1;
                    var yardsGain = start + _gameYardLine;
                    if (yardsGain > 55)
                        yardsGain = 55;

                    if (_gameYardLine >= 50) 
                    { //TD!
                        AddPointsQuarter(6, true);

                        PlayersStats.Add(new PlayerGameStats { PlayerId = selKr.Id, PlayerName = selKr.Name, TeamId = selKr.TeamId, KickReturns = 1, KickRetYds = yardsGain, KickReturnTds = 1 });
                        pbpLog += $"{selKr.Name} returned the kickoff {yardsGain} yards for a touchdown!\n";
                        scoringPlays += $"{ScorePrefix("TD",true)}{selKr.ShortName} {yardsGain}-yd return";

                        _gameYardLine = 47;
                        if (!_gamePoss)
                        { // home possession
                            homeScore += 6;
                        }
                        else
                        {
                            awayScore += 6;
                        }
                        _timeOfPossession = 35 * GlobalRandom.NextDouble;

                        if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                            _timeOfPossession = _gameTime - 2700;
                        else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                            _timeOfPossession = _gameTime - 1800;
                        else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                            _timeOfPossession = _gameTime - 900;

                        _gameTime -= _timeOfPossession;
                        if (_gamePoss)
                            _awayTop += _timeOfPossession;
                        else
                            _homeTop += _timeOfPossession;
                        gotTd = true;
                    }

                    //check downs if there wasn't TD
                    if (gotTd)
                    {
                        _gamePoss = !_gamePoss;
                        KickXp(defense, offense);
                        KickOff(defense, offense);
                    }
                    else
                    {
                        if (_gameYardLine <= 0)
                        {
                            _gameYardLine = 5;
                            pbpLog += "Kickoff was downed in the endzone. Touchback";

                        }
                        else
                        {
                            if (_gameYardLine == 25)
                                pbpLog += $"{selKr.Name} returned the kickoff {yardsGain} yards to midfield.";
                            else if (_gameYardLine > 25)
                                pbpLog += $"{selKr.Name} returned the kickoff {yardsGain} yards to the {defense.City} {50 - _gameYardLine} yardline.";
                            else
                                pbpLog += $"{selKr.Name} returned the kickoff {yardsGain} yards to their own {_gameYardLine} yardline.";
                            PlayersStats.Add(new PlayerGameStats { PlayerId = selKr.Id, PlayerName = selKr.Name, TeamId = selKr.TeamId, KickReturns = 1, KickRetYds = yardsGain });
                        }
                        _gameDown = 1;
                        _gameYardsNeed = 10;

                        var tackler = defenders[_random.Next(0, defenders.Count)];
                        PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Tackles = 1 });

                        _timeOfPossession = 5 + 35 * GlobalRandom.NextDouble;

                        if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                            _timeOfPossession = _gameTime - 2700;
                        else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                            _timeOfPossession = _gameTime - 1800;
                        else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                            _timeOfPossession = _gameTime - 900;

                        _gameTime -= _timeOfPossession;
                        if (_gamePoss)
                            _awayTop += _timeOfPossession;
                        else
                            _homeTop += _timeOfPossession;
                        _gamePoss = !_gamePoss;
                    }
                }
            }
        }

        private void ReturnPlay(Team offense, Team defense, Player selK, Player selKr, bool tooShort)
        {
            if (tooShort)
            {
                _gameYardLine = 5;
                _gameDown = 1;
                _gameYardsNeed = 10;
                pbpLog += "Field goal was kicked beyond the endzone. Touchback\n";
                _gamePoss = !_gamePoss;
                return;
            }

            _timeOfPossession = 0.0;
            var gotTd = false;
            var defenders = new List<Player>();
            if (!_gamePoss)
            {
                defenders.AddRange(_awayDl);
                defenders.AddRange(_awayLBs);
                defenders.AddRange(_awayDBs);
            }
            else
            {
                defenders.AddRange(_homeDl);
                defenders.AddRange(_homeLBs);
                defenders.AddRange(_homeDBs);
            }
            var start = _random.Next(-5, 3);
            _gameYardLine = start + (50 - ((selK.KickStrength / 2) - (int)(selKr.Speed / 5.0 * GlobalRandom.NextDouble)));
            _gameYardLine += _random.Next(0, 50) > 47 ? Convert.ToInt32(Math.Round(selKr.Speed * GlobalRandom.NextDouble)) : 0;
            if (start < 0)
                start *= -1;
            var yardsGain = start + _gameYardLine;
            if (yardsGain > 55)
                yardsGain = 55;

            if (_gameYardLine >= 50) 
            { //TD!
                AddPointsQuarter(6, true);

                PlayersStats.Add(new PlayerGameStats { PlayerId = selKr.Id, PlayerName = selKr.Name, TeamId = selKr.TeamId, KickReturns = 1, KickRetYds = yardsGain, KickReturnTds = 1 });
                pbpLog += $"{selKr.Name} returned the missed field goal {yardsGain} yards for a touchdown!\n";
                scoringPlays += $"{ScorePrefix("TD", true)}{selKr.ShortName} {yardsGain}-yd return";

                _gameYardLine = 47; //3 yards from endzone? 
                if (!_gamePoss)
                { // home possession
                    homeScore += 6;
                }
                else
                {
                    awayScore += 6;
                }
                _timeOfPossession = 35 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _awayTop += _timeOfPossession;
                else
                    _homeTop += _timeOfPossession;
                gotTd = true;
            }

            //check downs if there wasn't TD
            if (gotTd)
            {
                _gamePoss = !_gamePoss;
                KickXp(defense, offense); 
                KickOff(defense, offense);
            }
            else
            {
                if (_gameYardLine <= 0)
                {
                    _gameYardLine = 5;
                    pbpLog += "Missed field goal was downed in the endzone. Touchback\n";
                }
                else
                {
                    if (_gameYardLine > 25)
                        pbpLog += $"{selKr.Name} returned the missed field goal {yardsGain} yards to the {defense.City} {(50 - _gameYardLine)} yardline.\n";
                    else
                        pbpLog += $"{selKr.Name} returned the missed field goal {yardsGain} yards to their own {_gameYardLine} yardline.\n";
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selKr.Id, PlayerName = selKr.Name, TeamId = selKr.TeamId, KickReturns = 1, KickRetYds = yardsGain });
                }
                _gameDown = 1;
                _gameYardsNeed = 10;

                _timeOfPossession = 30 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _awayTop += _timeOfPossession;
                else
                    _homeTop += _timeOfPossession;
                _gamePoss = !_gamePoss;
            }
        }

        private void QbSack()
        {
            _timeOfPossession = 0.0;
            var yards = _random.Next(1, 10);
            _gameYardsNeed += yards;
            _gameYardLine -= yards;
            Player selQb = null;
            Player selRb = null;
            List<Player> oLine;
            var defenders = new List<Player>();
            if (_gamePoss)
            {
                selQb = _homeQBs[0];
                selRb = _homeRBs[_random.Next(0, 2)];
                oLine = _homeOl;
                defenders.AddRange(_awayDl.ToList());
                defenders.AddRange(_awayLBs.ToList());
                defenders.AddRange(_awayDBs.ToList());
            }
            else
            {
                selQb = _awayQBs[0];
                selRb = _awayRBs[_random.Next(0, 2)];
                oLine = _awayOl;
                defenders.AddRange(_homeDl.ToList());
                defenders.AddRange(_homeLBs.ToList());
                defenders.AddRange(_homeDBs.ToList());
            }
            defenders.OrderByDescending(p => p.Blitz).ThenByDescending(p => p.Speed).ThenByDescending(p => p.Tackle);
            var sackChance = new [] { 1, 1, 1, 1, 2, 2, 2, 3, 3, 4 };
            var index = sackChance[_random.Next(sackChance.Length)];
            var tackler = defenders[index - 1];
            PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Tackles = 1, Sacks = 1 });

            switch (index)
            {
                case 1:
                    PlayersStats.Add(new PlayerGameStats { PlayerId = oLine[2].Id, PlayerName = oLine[2].Name, TeamId = oLine[2].TeamId, SacksAllowed = 1 });
                    break;
                case 2:
                    PlayersStats.Add(new PlayerGameStats { PlayerId = oLine[1].Id, PlayerName = oLine[1].Name, TeamId = oLine[1].TeamId, SacksAllowed = 1 });
                    break;
                case 3:
                    PlayersStats.Add(new PlayerGameStats { PlayerId = oLine[0].Id, PlayerName = oLine[0].Name, TeamId = oLine[0].TeamId, SacksAllowed = 1 });
                    break;
                default:
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selRb.Id, PlayerName = selRb.Name, TeamId = selRb.TeamId, SacksAllowed = 1 });
                    break;
            }

            if (_gameYardLine > 0)
                pbpLog += selQb.Name + " was sacked for a " + yards + " yard loss by " + tackler.ShortNameWithPosition + ".";

            else if (_gameYardLine < 0)
            {
                // Safety!
                _timeOfPossession = 20 + 30 * GlobalRandom.NextDouble;

                // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
                if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                    _timeOfPossession -= 5 * GlobalRandom.NextDouble;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                    _timeOfPossession -= 5 * GlobalRandom.NextDouble;

                if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                    _timeOfPossession = _gameTime - 2700;
                else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                    _timeOfPossession = _gameTime - 1800;
                else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                    _timeOfPossession = _gameTime - 900;

                _gameTime -= _timeOfPossession;
                if (_gamePoss)
                    _homeTop += _timeOfPossession;
                else
                    _awayTop += _timeOfPossession;

                Safety(selQb, tackler);
                return;
            }

            _gameDown++; // Advance gameDown after checking for Safety, otherwise game log reports Safety occurring one down later than it did

            //Similar amount of time as rushing, minus some in-play time -- sacks are faster (usually)
            _timeOfPossession = 25 + 35 * GlobalRandom.NextDouble;

            // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
            if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;

            if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                _timeOfPossession = _gameTime - 2700;
            else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                _timeOfPossession = _gameTime - 1800;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                _timeOfPossession = _gameTime - 900;

            _gameTime -= _timeOfPossession;
            if (_gamePoss)
                _homeTop += _timeOfPossession;
            else
                _awayTop += _timeOfPossession;
        }

        private void Safety(Player selQb, Player tackler)
        {
            AddPointsQuarter(2, true);
            if (_gamePoss)
                awayScore += 2;
            else
                homeScore += 2;

            pbpLog += selQb.Name + " was sacked in the endzone by " + tackler.ShortNameWithPosition + ".\nSAFETY!\n";
            pbpLog += _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + "\n";
            PlayersStats.Add(new PlayerGameStats { PlayerId = tackler.Id, PlayerName = tackler.Name, TeamId = tackler.TeamId, Safeties = 1 });
            scoringPlays += ScorePrefix("SAF", true) + tackler.ShortName + " safety sack" + ScorePostfix;

            if (!_playingOt)
            {
                if (_gamePoss)
                {
                    KickOff(_homeTeam, _awayTeam);
                }
                else
                {
                    KickOff(_awayTeam, _homeTeam);
                }
            }
            else
            {
                ResetForOvertime();
            }
        }

        private void QbInterception(Team offense, Team defense, Player selQb, Player selCb)
        {
            PlayersStats.Add(new PlayerGameStats { PlayerId = selQb.Id, PlayerName = selQb.Name, TeamId = selQb.TeamId, PassAtt = 1, PassInts = 1 });
            _timeOfPossession = 0.0;

            pbpLog += selQb.Name + " was intercepted by " + selCb.Name + ".\nINTERCEPTION!\n";

            _timeOfPossession = 10 + 35 * GlobalRandom.NextDouble;

            // account for clock stoppage inside of 1:00 of the halves by adding time back on the clock
            if (!_secondHalf && _gameTime - _timeOfPossession < 1860)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 60)
                _timeOfPossession -= 5 * GlobalRandom.NextDouble;

            if (!_secondQt && _gameTime - _timeOfPossession < 2700)
                _timeOfPossession = _gameTime - 2700;
            else if (!_secondHalf && _gameTime - _timeOfPossession < 1800)
                _timeOfPossession = _gameTime - 1800;
            else if (!_fourthQt && _gameTime - _timeOfPossession < 900)
                _timeOfPossession = _gameTime - 900;

            _gameTime -= _timeOfPossession;
            if (_gamePoss)
                _homeTop += _timeOfPossession;
            else
                _awayTop += _timeOfPossession;

            if (!_playingOt)
            {

                _gameDown = 1;
                _gameYardsNeed = 10;
                _gamePoss = !_gamePoss;

                _gameYardLine += _random.Next(1, 50); // get random yard downfield
                if (_gameYardLine > 58)
                    _gameYardLine = _random.Next(50, 59); // set interception in the endzone somewhere

                if (_gameYardLine > 25)
                    _gameYardLine = 50 - _gameYardLine; // reset (flip) field if over 25

                var start = _gameYardLine; // return starts at gameYardLine
                var yardsGain = (int)(selCb.Speed * GlobalRandom.NextDouble * 2) / 10; // get yards of return
                _gameYardLine = start + yardsGain; // set gameYardLine
                _gameYardLine += _random.Next(0, 50) > 47 ? Convert.ToInt32(Math.Round(selCb.Speed * GlobalRandom.NextDouble)) : 0;

                if (yardsGain > 55)
                    yardsGain = 55;

                if (_gameYardLine <= 0)
                {
                    _gameYardLine = 5;
                    pbpLog += defense.Abbreviation + " " + selCb.ShortNameWithPosition + " intercepted the pass and was tackled in the endzone for a touchback.\n";
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selCb.Id, PlayerName = selCb.Name, TeamId = selCb.TeamId, Ints = 1, IntYds = 0 });
                }
                else if (_gameYardLine >= 50) // touchdown
                {
                    AddPointsQuarter(6, true);
                    pbpLog += selCb.ShortNameWithPosition + " returned the interception " + yardsGain + " yards for a touchdown!\n";
                    scoringPlays += ScorePrefix("TD", true) + selCb.ShortName + " " + yardsGain + "-yard interception return.";
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selCb.Id, PlayerName = selCb.Name, TeamId = selCb.TeamId, Ints = 1, IntYds = yardsGain, DefTds = 1 });

                    if (_gamePoss)
                    {
                        homeScore += 6;
                        KickXp(defense, offense);
                    }
                    else
                    {
                        awayScore += 6;
                        KickXp(defense, offense);
                    }
                    
                    if (!_playingOt)
                        KickOff(defense, offense);
                    else
                        ResetForOvertime();
                }
                else
                {
                    var yardLine = _gameYardLine;
                    if (_gameYardLine > 25)
                        yardLine = 50 - _gameYardLine;

                    pbpLog += defense.Abbreviation + " " + selCb.ShortNameWithPosition + " returned the interception " + yardsGain + " yards to the " + yardLine + " yard line.\n";
                    PlayersStats.Add(new PlayerGameStats { PlayerId = selCb.Id, PlayerName = selCb.Name, TeamId = selCb.TeamId, Ints = 1, IntYds = yardsGain });
                }
            }
            else
            {
                PlayersStats.Add(new PlayerGameStats { PlayerId = selCb.Id, PlayerName = selCb.Name, TeamId = selCb.TeamId, Ints = 1 });
                ResetForOvertime();
            }
        }

        private void AddPointsQuarter(int points, bool defense = false)
        {
            var home = _gamePoss && !defense || !_gamePoss && defense;
            if (_gameTime >= 2700)
            {
                if (home)
                    homeQScore[0] += points;
                else
                    awayQScore[0] += points;
            }
            else if (_gameTime >= 1800)
            {
                if (home)
                    homeQScore[1] += points;
                else
                    awayQScore[1] += points;
            }
            else if (_gameTime >= 900)
            {
                if (home)
                    homeQScore[2] += points;
                else
                    awayQScore[2] += points;
            }
            else if (_numOt == 0)
            {
                if (home)
                    homeQScore[3] += points;
                else
                    awayQScore[3] += points;
            }
            else
            {
                if (3 + _numOt < 10)
                {
                    if (home)
                        homeQScore[3 + _numOt] += points;
                    else
                        awayQScore[3 + _numOt] += points;
                }
                else
                {
                    if (home)
                        homeQScore[9] += points;
                    else
                        awayQScore[9] += points;
                }
            }
        }

        private void QuarterCheck()
        {
            if (_gameTime <= 2700 && !_secondQt)
            {
                _gameTime = 2700;
                _secondQt = !_secondQt;
                pbpLog += "\n\n---------- SECOND QUARTER | " + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + " ----------\n";
            }

            else if (_gameTime <= 1800 && !_secondHalf)
            {
                _secondHalf = !_secondHalf;
                _gameTime = 1800;
                _gameDown = -2;
                pbpLog += "\n\n---------- HALFTIME | " + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + " ----------\n";

                if (_kickOffTeam && _gamePoss)
                {
                    _gamePoss = !_gamePoss;
                    pbpLog += _homeTeam.City + " will receive the second half kickoff.\n";
                    KickOff(_homeTeam, _awayTeam);
                }
                else if (_kickOffTeam && !_gamePoss)
                {
                    pbpLog += _homeTeam.City + " will receive the second half kickoff.\n";
                    KickOff(_homeTeam, _awayTeam);
                }
                else if (!_kickOffTeam && _gamePoss)
                {
                    pbpLog += _awayTeam.City + " will receive the second half kickoff.\n";
                    KickOff(_awayTeam, _homeTeam);
                }
                else
                {
                    _gamePoss = !_gamePoss;
                    pbpLog += _awayTeam.City + " will receive the second half kickoff.\n";
                    KickOff(_awayTeam, _homeTeam);
                }
            }

            else if (_gameTime <= 900 && !_fourthQt)
            {
                _gameTime = 900;
                _fourthQt = !_fourthQt;
                pbpLog += "\n\n---------- FOURTH QUARTER | " + _homeTeam.Abbreviation + " " + homeScore + " - " + _awayTeam.Abbreviation + " " + awayScore + " ----------\n";
            }
        }

        private static double Normalize(double rating)
        {
            return (100 + rating) / 2;
        }

        private static List<Penalty> LoadedPenalties
        {
            get
            {
                var penalties = new List<Penalty>();
                var xml = new XmlDocument();
                xml.Load(@"Resources/Penalties.xml");
                var penNodes = xml.SelectNodes("Penalties/Penalty");
                if (penNodes != null)
                {
                    penalties.AddRange(penNodes.Cast<XmlNode>()
                        .Select(item => new Penalty(Convert.ToBoolean(item.Attributes["Presnap"].Value),
                            item.Attributes["Team"].Value, Convert.ToBoolean(item.Attributes["LossOfDown"].Value),
                            item.Attributes["PlayType"].Value, Convert.ToBoolean(item.Attributes["AutoFirst"].Value),
                            Convert.ToInt32(item.Attributes["Yards"].Value), item.Attributes["Text"].Value,
                            Convert.ToInt32(item.Attributes["Frequency"].Value))));
                }

                return penalties;
            }
        }

        private static List<Injury> LoadedInjuries
        {
            get
            {
                var injuries = new List<Injury>();
                var text = File.ReadAllLines(@"Resources/Injuries.csv");
                var x = 1;
                while(x < text.Length)
                {
                    var col = text[x].Split(',');
                    var injury = new Injury()
                    {
                        MinTime = Convert.ToInt32(col[0]),
                        MaxTime = Convert.ToInt32(col[1]),
                        CareerEnding = Convert.ToInt32(col[2]) != 0,
                        Diagnosis = col[3],
                        Frequency = Convert.ToInt32(col[4]),
                    };
                    injuries.Add(injury);
                    x++;
                }
                return injuries;
            }
        }

        private Penalty _penalty;
        private Player _penPlayer;

        private bool CheckPenalty(bool presnap, string playtype, Player selQb, Player selRb, Player selWr, List<Player> oLine, List<Player> dLine, List<Player> lineBackers, List<Player> defBacks)
        {
            var pens = new List<Penalty>();
            var frequency = _random.Next(0, 5);
            switch (playtype)
            {
                case "pass":
                    pens.AddRange(_penalties.Where(p => p.Presnap == presnap && p.Frequency > frequency && (p.PlayType == "" || p.PlayType == "Pass")));
                    break;
                case "kick":
                    pens.AddRange(_penalties.Where(p => p.Presnap == presnap && p.Frequency > frequency && p.PlayType == "Kick"));
                    break;
                default:
                    pens.AddRange(_penalties.Where(p => p.Presnap == presnap && p.Frequency > frequency && p.PlayType == ""));
                    break;
            }
            if (pens.Any())
            {
                var randPenalty = pens[_random.Next(0, pens.Count)];
                var posGroup = randPenalty.PosGroup;
                var pos = posGroup[_random.Next(0, posGroup.Length)];
                Player player;
                if (randPenalty.Team == "Offense")
                {
                    switch (pos)
                    {
                        case "QB":
                            player = selQb;
                            break;
                        case "RB":
                            player = selRb;
                            break;
                        case "WR":
                            player = selWr;
                            break;
                        default:
                        {
                            player = randPenalty.Text == "Snap infraction" ? oLine[0] : oLine[_random.Next(0, 3)];
                            break;
                        }
                    }
                }
                else
                {
                    switch (pos)
                    {
                        case "DL":
                            player = dLine[_random.Next(0, 3)];
                            break;
                        case "LB":
                            player = lineBackers[_random.Next(0, 2)];
                            break;
                        default:
                            player = defBacks[_random.Next(0, 3)];
                            break;
                    }
                }
                var attCheck = (player.Attitude + 10) * GlobalRandom.NextDouble;
                if (attCheck < _penaltyMod)
                {
                    _penalty = randPenalty;
                    _penPlayer = player;
                    return true;
                }

                _penalty = null;
                _penPlayer = null;
                return false;
            }
            return false;
        }

        private void ProcessPresnap(Penalty pen)
        {
            if (pen != null)
            {
                int yards;
                if (pen.Team == "Offense")
                {
                    if (_gameYardLine - _penalty.Yards <= 0)
                        yards = _penalty.Yards / 2;
                    else
                        yards = _penalty.Yards;

                    _gameYardLine -= yards;
                    if (_gameYardLine <= 0)
                    {
                        if (_gameYardLine == 0)
                            yards = 0;
                        _gameYardLine = 0;
                    }
                    _gameYardsNeed += yards;
                    if (_penalty.LossOfDown)
                        _gameDown++;

                    pbpLog += _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the offense, " + yards + " yard(s) - replay the down.";
                }
                else
                {
                    if (_gameYardLine + _penalty.Yards >= 50)
                        yards = _penalty.Yards / 2;
                    else
                        yards = _penalty.Yards;
                    _gameYardLine += yards;
                    if (_gameYardLine >= 50)
                        _gameYardLine = 50;

                    _gameYardsNeed -= yards;

                    if (_penalty.AutoFirst)
                    {
                        _gameDown = 1;
                        _gameYardsNeed = 10;
                        pbpLog += _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - automatic first down.";
                    }
                    else if (_gameYardsNeed <= 0)
                    {
                        _gameDown = 1;
                        if (_gameYardLine >= 41)
                            _gameYardsNeed = 50 - _gameYardLine;
                        else
                            _gameYardsNeed = 10;
                        pbpLog += _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - penalty results in a first down.";
                    }
                    else
                    {
                        pbpLog += _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - replay the down.";
                    }
                }

                if (_gamePoss && pen.Team == "Offense")
                {
                    _homePenalties++;
                    _homePenaltyYds += yards;
                }
                else if (_gamePoss && pen.Team == "Defense")
                {
                    _awayPenalties++;
                    _awayPenaltyYds += yards;
                }
                else if (!_gamePoss && pen.Team == "Offense")
                {
                    _awayPenalties++;
                    _awayPenaltyYds += yards;
                }
                else
                {
                    _homePenalties++;
                    _homePenaltyYds += yards;
                }
            }
            // had some issues with penalties being repeated over and over.. set these to null here so that shouldn't happen
            _penalty = null;
            _penPlayer = null;
        }

        private void ProcessAfterPlay(Penalty pen, int los, bool gotSack, bool gotInt, bool gotFum, bool gotTd, int yardsGain, out string log, out bool accepted, out bool autoFirst)
        {
            log = null;
            accepted = true;
            autoFirst = false;
            if (pen != null)
            {
                int yards;
                if (pen.Team == "Offense")
                {
                    if (gotSack || gotFum || gotInt)
                        return;

                    else
                    {
                        _gameYardLine = los;
                        if (_gameYardLine - _penalty.Yards <= 0)
                            yards = _penalty.Yards / 2;
                        else
                            yards = _penalty.Yards;

                        _gameYardLine -= yards;
                        if (_gameYardLine <= 0)
                        {
                            if (_gameYardLine == 0)
                                yards = 0;
                            _gameYardLine = 0;
                        }
                        _gameYardsNeed += yards;
                        if (_penalty.LossOfDown)
                            _gameDown++;

                        pbpLog += _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the offense, " + yards + " yard(s) - replay the down.";
                    }
                }
                else
                {
                    if (gotTd)
                    {
                        accepted = false;
                        return;
                    }

                    if (gotSack || gotInt || gotFum)
                        _gameYardLine = los;

                    if (_gameYardLine + yardsGain + _penalty.Yards >= 50)
                        yards = _penalty.Yards / 2;
                    else if (pen.Text == "Defensive pass interference")
                    {
                        var newYard = yardsGain + _gameYardLine;
                        if(newYard >= 50) // occurred in endzone, place ball at 2 yard line (48);
                            yards = (newYard - 48) - yardsGain;
                        else
                            yards = yardsGain;
                    }
                    else
                        yards = _penalty.Yards;

                    if (yardsGain > yards)
                    {
                        yards = yardsGain;
                        accepted = false;
                    }
                    if (_penalty.Text == "Facemask")
                    {
                        accepted = true;
                    }

                    if (accepted)
                    {
                        _gameYardLine += yards;
                        if (_gameYardLine >= 50)
                            _gameYardLine = 50;
                    }
                    //else
                    //{
                    //    gameYardLine += yardsGain;
                    //}

                    _gameYardsNeed -= yards;

                    if (_penalty.AutoFirst && accepted)
                    {
                        autoFirst = true;
                        _gameDown = 1;
                        _gameYardsNeed = 10;
                        log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - automatic first down.";
                        //gameEventLog += "\n" + penalty.Text + " on " + penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - automatic first down.";
                    }
                    else
                    {
                        if (pen.Text == "Facemask")
                        {
                            if (_gameYardLine >= 41)
                            {
                                yards = 3;
                                _gameYardsNeed -= 3;
                            }
                            else
                            {
                                yards = 5;
                                _gameYardsNeed -= 5;
                            }
                        }

                        if (_gameYardsNeed <= 0)
                        {
                            _gameDown = 1;
                            if(_gameYardLine >= 41)
                                _gameYardsNeed = 50 - _gameYardLine;
                            else
                                _gameYardsNeed = 10;
                            if (!accepted)
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense was declined. Result of the play is a first down.";
                            //gameEventLog += "\n" + penalty.Text + " on " + penPlayer.NameWithPosition + " of the defense was declined. Result of the play is a first down.";
                            else if (pen.Text == "Facemask")
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, 5 yards will be added on to the end of the run.";
                            else
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - penalty results in a first down.";
                            //gameEventLog += "\n" + penalty.Text + " on " + penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - penalty results in a first down.";
                        }
                        else
                        {
                            _gameDown--;
                            if (!accepted)
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense was declined.";
                            //gameEventLog += penalty.Text + " on " + penPlayer.NameWithPosition + " of the defense was declined. Replay the down.";
                            else if (pen.Text == "Facemask")
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, 5 yards will be added on to the end of the run.";
                            else
                                log = "\n" + _penalty.Text + " on " + _penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s).";
                            //gameEventLog += "\n" + penalty.Text + " on " + penPlayer.NameWithPosition + " of the defense, " + yards + " yard(s) - replay the down.";
                        }
                    }
                }

                if (_gamePoss && pen.Team == "Offense")
                {
                    _homePenalties++;
                    _homePenaltyYds += yards;
                }
                else if (_gamePoss && pen.Team == "Defense")
                {
                    _awayPenalties++;
                    _awayPenaltyYds += yards;
                }
                else if (!_gamePoss && pen.Team == "Offense")
                {
                    _awayPenalties++;
                    _awayPenaltyYds += yards;
                }
                else
                {
                    _homePenalties++;
                    _homePenaltyYds += yards;
                }
            }
            // had some issues with penalties being repeated over and over.. set these to null here so that shouldn't happen
            _penPlayer = null;

        }

        private Player _injuredPlayer;

        public bool CheckInjury(Player[] players)
        {
            var injured = false;
            var p = players[_random.Next(0, players.Count())];
            var injRate = (p.Injury + 10) * GlobalRandom.NextDouble;
            if (injRate < 1 - GlobalRandom.NextDouble)
                injured = true;

            if (!injured) return false;

            var severity = _random.Next(0, 5);
            var frequencyInjuries = _injuries.Where(i => i.Frequency > severity).ToList();
            var randInjury = frequencyInjuries[_random.Next(0, frequencyInjuries.Count() - 1)];
            var pos = p.Pos;
            _injuredPlayer = p;

            p.IsInjured = true;
            p.InjuryTxt = randInjury.Diagnosis;
            p.InjuryDuration = _random.Next(randInjury.MinTime, randInjury.MaxTime);
            Database.SavePlayer(p);
            
            if (p.TeamName == _homeTeam.TeamName)
            {
                switch (pos)
                {
                    case "QB":
                        _homeQBs.Remove(p);
                        if(!_homeQBs.Any())
                        {
                            var qbs = _homeTeam.ActiveRoster.Where(pl => pl.Pos == "QB").OrderByDescending(pl => pl.Overall).ToList();
                            _homeQBs.Add(qbs[3] ?? _homeTeam.EmergencyQB);
                        }
                        break;
                    case "RB":
                        _homeRBs.Remove(p);
                        break;
                    case "WR":
                        _homeWRs.Remove(p);
                        break;
                    case "OL":
                        _homeOl.Remove(p);
                        break;
                    case "DL":
                        _homeDl.Remove(p);
                        break;
                    case "LB":
                        _homeLBs.Remove(p);
                        break;
                    case "DB":
                        _homeDBs.Remove(p);
                        break;
                    default:
                        _homeKs.Remove(p);
                        // if array is empty now (most teams only carry 1 kicker) put in an emergency kicker
                        if (!_homeKs.Any())
                            _homeKs.Add(_homeTeam.EmergencyKicker);
                        break;
                }
            }
            else
            {
                switch (pos)
                {
                    case "QB":
                        _awayQBs.Remove(p);
                        if (!_awayQBs.Any())
                        {
                            var qbs = _awayTeam.ActiveRoster.Where(pl => pl.Pos == "QB").OrderByDescending(pl => pl.Overall).ToList();
                            _awayQBs.Add(qbs[3] ?? _awayTeam.EmergencyQB);
                        }
                        break;
                    case "RB":
                        _awayRBs.Remove(p);
                        break;
                    case "WR":
                        _awayWRs.Remove(p);
                        break;
                    case "OL":
                        _awayOl.Remove(p);
                        break;
                    case "DL":
                        _awayDl.Remove(p);
                        break;
                    case "LB":
                        _awayLBs.Remove(p);
                        break;
                    case "DB":
                        _awayDBs.Remove(p);
                        break;
                    default:
                        _awayKs.Remove(p);
                        // if array is empty now (most teams only carry 1 kicker) put in an emergency kicker
                        if (!_awayKs.Any())
                            _awayKs.Add(_awayTeam.EmergencyKicker);
                        break;
                }
            }
            return true;
        }

        public Task SaveGameStats()
        {
            PBP = pbpLog;
            ScoringPlays = scoringPlays;
            Home1st = homeQScore[0];
            Away1st = awayQScore[0];
            Home2nd = homeQScore[1];
            Away2nd = awayQScore[1];
            Home3rd = homeQScore[2];
            Away3rd = awayQScore[2];
            Home4th = homeQScore[3];
            Away4th = awayQScore[3];

            var homeOt = 0;
            for (var i = 0; i < 6; i++)
                homeOt += homeQScore[i + 4];

            var awayOt = 0;
            for (var i = 0; i < 6; i++)
                awayOt += awayQScore[i + 4];

            HomeOT = homeOt;
            AwayOT = awayOt;
            HomeFinal = homeScore;
            AwayFinal = awayScore;

            var gameStats = PlayersStats.GroupBy(g => g.PlayerId).Select(p => new PlayerGameStats
            {
                Season = Season,
                GameId = Id,
                Playoffs = Playoffs,
                GameDate = GameDate,
                TeamId = p.First().TeamId,
                Team = HomeTeam == p.First().TeamId ? _homeTeam.Abbreviation : _awayTeam.Abbreviation,
                PlayerName = p.First().PlayerName,
                PlayerId = p.First().PlayerId,
                PassAtt = p.Sum(c => c.PassAtt),
                PassCmp = p.Sum(c => c.PassCmp),
                PassYds = p.Sum(c => c.PassYds),
                PassTds = p.Sum(c => c.PassTds),
                PassInts = p.Sum(c => c.PassInts),
                Pass20 = p.Sum(c => c.Pass20),
                PassFirst = p.Sum(c => c.PassFirst),
                PassLong = p.Max(c => c.PassYds),
                Carries = p.Sum(c => c.Carries),
                RushYds = p.Sum(c => c.RushYds),
                RushTds = p.Sum(c => c.RushTds),
                Rush20 = p.Sum(c => c.Rush20),
                RushFirst = p.Sum(c => c.RushFirst),
                RushLong = p.Max(c => c.RushYds),
                Receptions = p.Sum(c => c.Receptions),
                RecYds = p.Sum(c => c.RecYds),
                RecTds = p.Sum(c => c.RecTds),
                Rec20 = p.Sum(c => c.Rec20),
                RecFirst = p.Sum(c => c.RecFirst),
                RecLong = p.Max(c => c.RecYds),
                Drops = p.Sum(c => c.Drops),
                Fumbles = p.Sum(c => c.Fumbles),
                FumblesLost = p.Sum(c => c.FumblesLost),
                Tackles = p.Sum(c => c.Tackles),
                Sacks = p.Sum(c => c.Sacks),
                ForFumb = p.Sum(c => c.ForFumb),
                FumbRec = p.Sum(c => c.FumbRec),
                Ints = p.Sum(c => c.Ints),
                IntYds = p.Sum(c => c.IntYds),
                Safeties = p.Sum(c => c.Safeties),
                DefTds = p.Sum(c => c.DefTds),
                FGAtt = p.Sum(c => c.FGAtt),
                FGMade = p.Sum(c => c.FGMade),
                FGLong = p.Max(c => c.FGLong),
                XPAtt = p.Sum(c => c.XPAtt),
                XPMade = p.Sum(c => c.XPMade),
                Rouges = p.Sum(c => c.Rouges),
                KickReturns = p.Sum(c => c.KickReturns),
                KickRetYds = p.Sum(c => c.KickRetYds),
                KickReturnTds = p.Sum(c => c.KickReturnTds),
                KickReturnLong = p.Max(c => c.KickRetYds),
                PancakeBlocks = p.Sum(c => c.PancakeBlocks),
                SacksAllowed = p.Sum(c => c.SacksAllowed),
            }).ToList();

            StatsJson = gameStats.SerializeToJson();

            var homePlayers = gameStats.Where(p => p.TeamId == HomeTeam).ToList();
            var awayPlayers = gameStats.Where(p => p.TeamId == AwayTeam).ToList();

            HomeTOP = _homeTop;
            HomeTotalYds = homePlayers.Sum(p => p.PassYds + p.RushYds + p.KickRetYds);
            HomePlaysRun = _homePlaysRun;
            Home1stDowns = _home1StDowns;
            Home3rdDownAtt = _home3RdDownAtt;
            Home3rdDownComp = _home3RdDownComp;
            Home4thDownAtt = _home4ThDownAtt;
            Home4thDownComp = _home4ThDownComp;
            HomePassAtt = homePlayers.Sum(p => p.PassAtt);
            HomePassComp = homePlayers.Sum(p => p.PassCmp);
            HomePassYds = homePlayers.Sum(p => p.PassYds);
            HomePassTd = homePlayers.Sum(p => p.PassTds);
            HomeInt = homePlayers.Sum(p => p.PassInts);
            HomeRushAtt = homePlayers.Sum(p => p.Carries);
            HomeRushYds = homePlayers.Sum(p => p.RushYds);
            HomeFumbles = homePlayers.Sum(p => p.Fumbles);
            HomeFumblesLost = homePlayers.Sum(p => p.FumblesLost);
            HomePenaltyYds = _homePenaltyYds;
            HomePenalties = _homePenalties;

            AwayTOP = _awayTop;
            AwayTotalYds = awayPlayers.Sum(p => p.PassYds + p.RushYds + p.KickRetYds);
            AwayPlaysRun = _awayPlaysRun;
            Away1stDowns = _away1StDowns;
            Away3rdDownAtt = _away3RdDownAtt;
            Away3rdDownComp = _away3RdDownComp;
            Away4thDownAtt = _away4ThDownAtt;
            Away4thDownComp = _away4ThDownComp;
            AwayPassAtt = awayPlayers.Sum(p => p.PassAtt);
            AwayPassComp = awayPlayers.Sum(p => p.PassCmp);
            AwayPassYds = awayPlayers.Sum(p => p.PassYds);
            AwayPassTd = awayPlayers.Sum(p => p.PassTds);
            AwayInt = awayPlayers.Sum(p => p.PassInts);
            AwayRushAtt = awayPlayers.Sum(p => p.Carries);
            AwayRushYds = awayPlayers.Sum(p => p.RushYds);
            AwayFumbles = awayPlayers.Sum(p => p.Fumbles);
            AwayFumblesLost = awayPlayers.Sum(p => p.FumblesLost);
            AwayPenaltyYds = _awayPenaltyYds;
            AwayPenalties = _awayPenalties;

            PlaysRun = _homePlaysRun + _awayPlaysRun;
            HasPlayed = true;

            if (!Playoffs)
            {
                //game over, add wins and losses
                if (homeScore > awayScore)
                {
                    _homeTeam.Wins++;
                    _awayTeam.Losses++;
                    _homeTeam.HomeWins++;
                    _awayTeam.AwayLosses++;
                    if (_homeTeam.Streak >= 0)
                        _homeTeam.Streak++;
                    else
                        _homeTeam.Streak = 1;
                    if (_awayTeam.Streak <= 0)
                        _awayTeam.Streak--;
                    else
                        _awayTeam.Streak = -1;
                    if (_homeTeam.Division == _awayTeam.Division)
                    {
                        _homeTeam.DivWins++;
                        _awayTeam.DivLosses++;
                    }

                    if (_homeTeam.Conference == _awayTeam.Conference)
                    {
                        _homeTeam.ConfWins++;
                        _awayTeam.ConfLosses++;
                    }

                    WinningTeamId = HomeTeam;
                }
                else
                {
                    _homeTeam.Losses++;
                    _awayTeam.Wins++;
                    _homeTeam.HomeLosses++;
                    _awayTeam.AwayWins++;
                    if (_homeTeam.Streak <= 0)
                        _homeTeam.Streak--;
                    else
                        _homeTeam.Streak = -1;
                    if (_awayTeam.Streak >= 0)
                        _awayTeam.Streak++;
                    else
                        _awayTeam.Streak = 1;
                    if (_homeTeam.Division == _awayTeam.Division)
                    {
                        _homeTeam.DivLosses++;
                        _awayTeam.DivWins++;
                    }

                    if (_homeTeam.Conference == _awayTeam.Conference)
                    {
                        _homeTeam.ConfLosses++;
                        _awayTeam.ConfWins++;
                    }

                    WinningTeamId = AwayTeam;
                }

                // Add points/opp points
                _homeTeam.PointsFor += homeScore;
                _homeTeam.PointsAgainst += awayScore;
                _awayTeam.PointsFor += awayScore;
                _awayTeam.PointsAgainst += homeScore;
            }
            else
            {
                if (homeScore > awayScore)
                {
                    WinningTeamRank = HomeTeamRank;
                    WinningTeamId = HomeTeam;
                }
                else
                {
                    WinningTeamRank = AwayTeamRank;
                    WinningTeamId = AwayTeam;
                }
            }

            Database.SaveGame(this);
            Database.SaveTeam(_homeTeam);
            Database.SaveTeam(_awayTeam);

            return Task.CompletedTask;
        }

        public void ExportHtml()
        {
            if (!HasPlayed) 
                return;

            var date = GameDate.ToShortDateString();
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{CurrentLeague.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{Season}\Games\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}{Id}.html";
            if (File.Exists(file))
                return;

            var gameStats = StatsJson.DeserializeToList<PlayerGameStats>();
            var homeStats = gameStats.Where(g => g.TeamId == HomeTeam).ToList();
            var awayStats = gameStats.Where(g => g.TeamId == AwayTeam).ToList();

            var awayYpc = (double)AwayRushYds / (double)AwayRushAtt;
            var homeYpc = (double)HomeRushYds / (double)HomeRushAtt;
            var awayKRs = awayStats.Sum(p => p.KickReturns);
            var homeKRs = homeStats.Sum(p => p.KickReturns);
            var awayRetYds = awayStats.Sum(p => p.KickRetYds);
            var homeRetYds = homeStats.Sum(p => p.KickRetYds);
            var awayRetYpc = (double)awayRetYds / (double)awayKRs;
            var homeRetYpc = (double)homeRetYds / (double)homeKRs;
            var awayXpMade = awayStats.Sum(p => p.XPMade);
            var homeXpMade = homeStats.Sum(p => p.XPMade);
            var awayXpAtt = awayStats.Sum(p => p.XPAtt);
            var homeXpAtt = homeStats.Sum(p => p.XPAtt);
            var awayXpPct = (double)awayXpMade / (double)awayXpAtt;
            var homeXpPct = (double)homeXpMade / (double)homeXpAtt;
            var awayFgMade = awayStats.Sum(p => p.FGMade);
            var homeFgMade = homeStats.Sum(p => p.FGMade);
            var awayFgAtt = awayStats.Sum(p => p.FGAtt);
            var homeFgAtt = homeStats.Sum(p => p.FGAtt);
            var awayFgPct = (double)awayFgMade / (double)awayFgAtt;
            var homeFgPct = (double)homeFgMade / (double)homeFgAtt;
            var away3RdPct = Away3rdDownAtt == 0 ? 0.0 : (double)Away3rdDownComp / (double)Away3rdDownAtt;
            var home3RdPct = Home3rdDownAtt == 0 ? 0.0 : (double)Home3rdDownComp / (double)Home3rdDownAtt;
            var away4ThPct = Away4thDownAtt == 0 ? 0.0 : (double)Away4thDownComp / (double)Away4thDownAtt;
            var home4ThPct = Home4thDownAtt == 0 ? 0.0 : (double)Home4thDownComp / (double)Home4thDownAtt;

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{CurrentLeague.Abbreviation} - {AwayTeamStr} @ {HomeTeamStr} ({GameDate.ToLongDateString()})</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{HtmlHelpers.MenuForGames(CurrentLeague)}");
                sw.WriteLine("</br></br>");
                sw.WriteLine($"<h2>{AwayTeamStr} @ {HomeTeamStr} - {GameDate.ToLongDateString()}</h2>");
                sw.WriteLine("<div class=\"w3-container\">");
                sw.WriteLine("<div class=\"w3-row\">");
                sw.WriteLine("<a href=\"javascript:void(0)\" onclick=\"openTab(event, 'Box');\">");
                sw.WriteLine("<div class=\"w3-third tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red\">Box Score</div>");
                sw.WriteLine("</a>");
                sw.WriteLine("<a href=\"javascript:void(0)\" onclick=\"openTab(event, 'PBP');\">");
                sw.WriteLine("<div class=\"w3-third tablink w3-bottombar w3-hover-light-grey w3-padding\">Play by Play</div>");
                sw.WriteLine("</a>");
                sw.WriteLine("<a href=\"javascript:void(0)\" onclick=\"openTab(event, 'Pstats');\">");
                sw.WriteLine("<div class=\"w3-third tablink w3-bottombar w3-hover-light-grey w3-padding\">Player Statistics</div>");
                sw.WriteLine("</a>");
                sw.WriteLine("</div>");

                sw.WriteLine("<div id=\"Box\" class=\"w3-container w3-animate-zoom _name\" style=\"display:block\">");
                sw.WriteLine("</br>");
                sw.WriteLine("<table style=\"width: 40%; float: left; margin-left: auto; margin-right: auto; margin-bottom: 15px;\">");
                sw.WriteLine("<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">BOX SCORE</th></tr>");
                sw.WriteLine("<tr><th>Team</th><th>1st</th><th>2nd</th><th>3rd</th><th>4th</th><th>OT</th><th>Final</th></tr>");
                sw.WriteLine($"<tr><td>{AwayTeamAbbr}</td><td>{Away1st}</td><td>{Away2nd}</td><td>{Away3rd}</td><td>{Away4th}</td><td>{AwayOT}</td><td>{AwayFinal}</td><tr>");
                sw.WriteLine($"<tr><td>{HomeTeamAbbr}</td><td>{Home1st}</td><td>{Home2nd}</td><td>{Home3rd}</td><td>{Home4th}</td><td>{HomeOT}</td><td>{HomeFinal}</td><tr>");
                sw.WriteLine("</table>");

                sw.WriteLine("<table style=\"width: 55%; float: right; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine("<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">SCORING DRIVES</th></tr>");
                sw.WriteLine($"<tr><th>Team</th><th>Score</th><th>Time</th><th>Play</th><th>{AwayTeamAbbr}</th><th>{HomeTeamAbbr}</th></tr>");
                var scores = ScoringPlays.Split('\n').ToList();
                scores.RemoveAt(0);
                scores.ForEach(x =>
                {
                    var row = x.Split('|');
                    sw.WriteLine($"<tr><td>{row[0]}</td><td>{row[1]}</td><td>{row[2]}</td><td>{row[3]}</td><td>{row[4]}</td><td>{row[5]}</td></tr>");
                });
                sw.WriteLine("</table>");

                sw.WriteLine("<table style=\"width: 40%; float: left; margin-left: auto; margin-right: auto; margin-top: 15px;\">");
                sw.WriteLine("<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">TEAM STATS</th></tr>");
                sw.WriteLine($"<tr><th>Stat Category</th><th>{AwayTeamAbbr}</th><th>{HomeTeamAbbr}</th></tr>");
                sw.WriteLine($"<tr><td>Yards Gained</td><td>{AwayTotalYds}</td><td>{HomeTotalYds}</td></tr>");
                sw.WriteLine($"<tr><td>Rushing Yards</td><td>{AwayRushYds}</td><td>{HomeRushYds}</td></tr>");
                sw.WriteLine($"<tr><td>Attempts</td><td>{AwayRushAtt}</td><td>{HomeRushAtt}</td></tr>");
                sw.WriteLine($"<tr><td>Yards Per Carry</td><td>{awayYpc:N1}</td><td>{homeYpc:N1}</td></tr>");
                sw.WriteLine($"<tr><td>Passing Yards</td><td>{AwayPassYds}</td><td>{HomePassYds}</td></tr>");
                sw.WriteLine($"<tr><td>Comp/Att</td><td>{AwayPassComp}/{AwayPassAtt}</td><td>{HomePassComp}/{HomePassAtt}</td></tr>");
                sw.WriteLine($"<tr><td>TD-to-INT Ratio</td><td>{AwayPassTd}-{AwayInt}</td><td>{HomePassTd}-{HomeInt}</td></tr>");
                sw.WriteLine($"<tr><td>Kick Returns</td><td>{awayKRs}-{awayRetYds}</td><td>{homeKRs}-{homeRetYds}</td></tr>");
                sw.WriteLine($"<tr><td>Yards Per Return</td><td>{awayRetYpc:N1}</td><td>{homeRetYpc:N1}</td></tr>");
                sw.WriteLine($"<tr><td>XP Made/Attempted</td><td>{awayXpMade}/{awayXpAtt} ({awayXpPct:P1})</td><td>{homeXpMade}/{homeXpAtt} ({homeXpPct:P1})</td></tr>");
                sw.WriteLine($"<tr><td>FG Made/Attempted</td><td>{awayFgMade}/{awayFgAtt} ({awayFgPct:P1})</td><td>{homeFgMade}/{homeFgAtt} ({homeFgPct:P1})</td></tr>");
                sw.WriteLine($"<tr><td>Penalties</td><td>{AwayPenalties}</td><td>{HomePenalties}</td></tr>");
                sw.WriteLine($"<tr><td>Penalty Yards</td><td>{AwayPenaltyYds}</td><td>{HomePenaltyYds}</td></tr>");
                sw.WriteLine($"<tr><td>3rd Down Efficiency</td><td>{Away3rdDownComp}/{Away3rdDownAtt} ({away3RdPct:P1})</td><td>{Home3rdDownComp}/{Home3rdDownAtt} ({home3RdPct:P1})</td></tr>");
                sw.WriteLine($"<tr><td>4th Down Efficiency</td><td>{Away4thDownComp}/{Away4thDownAtt} ({away4ThPct:P1})</td><td>{Home4thDownComp}/{Home4thDownAtt} ({home4ThPct:P1})</td></tr>");
                sw.WriteLine($"<tr><td>Turnovers</td><td>{AwayFumblesLost + AwayInt}</td><td>{HomeFumblesLost + HomeInt}</td></tr>");
                sw.WriteLine($"<tr><td>Time of Possession</td><td>{TimeSpan.FromSeconds(AwayTOP):mm\\:ss}</td><td>{TimeSpan.FromSeconds(HomeTOP):mm\\:ss}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");

                sw.WriteLine("<div id=\"PBP\" class=\"w3-container w3-animate-zoom _name\" style=\"display:none\">");
                sw.WriteLine("</br>");
                sw.WriteLine("<table style=\"width: 80%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine("<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">PLAY BY PLAY</th></tr>");

                var lines = PBP.Split('\n').ToList();
                lines.ForEach(x =>
                {
                    var row = x.Split('|');
                    if (row.Length == 2)
                    {
                        var txt = string.Join("-", row);
                        sw.WriteLine($"<tr><td style=\"text-align:left;\" colspan=\"100%\">{txt}</td></tr>");
                    }
                    else if (row.Length == 1)
                        sw.WriteLine($"<tr><td style=\"text-align:left;\" colspan=\"100%\">{row[0]}</td></tr>");
                    else
                        sw.WriteLine($"<tr><td>{row[0]}</td><td>{row[1]}</td><td>{row[2]}</td><td>{row[3]}</td><td style=\"text-align:left;\">{row[4]}</td></tr>");
                });
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");

                sw.WriteLine("<div id=\"Pstats\" class=\"w3-container w3-animate-zoom _name\" style=\"display:none\">");
                // passing tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} PASSING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>COMP</th><th>ATT</th><th>CMP %</th><th>YARDS</th><th>TDS</th><th>INTS</th><th>AVG/CMP</th><th>QBR</th></tr>");
                foreach (var a in awayStats.Where(p => p.PassAtt > 0).OrderByDescending(p => p.PassYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PassCmp}</td><td>{a.PassAtt}</td><td>{a.CompPct:P1}</td><td>{a.PassYds}</td><td>{a.PassTds}</td><td>{a.PassInts}</td><td>{a.PassYPC:N1}</td><td>{a.QBRating:N1}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>COMP</th><th>ATT</th><th>CMP %</th><th>YARDS</th><th>TDS</th><th>INTS</th><th>AVG/CMP</th><th>QBR</th></tr>");
                foreach (var a in homeStats.Where(p => p.PassAtt > 0).OrderByDescending(p => p.PassYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PassCmp}</td><td>{a.PassAtt}</td><td>{a.CompPct:P1}</td><td>{a.PassYds}</td><td>{a.PassTds}</td><td>{a.PassInts}</td><td>{a.PassYPC:N1}</td><td>{a.QBRating:N1}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // rushing tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} RUSHING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>ATT</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                foreach (var a in awayStats.Where(p => p.Carries > 0).OrderByDescending(p => p.RushYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Carries}</td><td>{a.RushYds}</td><td>{a.RushTds}</td><td>{a.RushAvg:N1}</td><td>{a.RushLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} RUSHING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>ATT</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                foreach (var a in homeStats.Where(p => p.Carries > 0).OrderByDescending(p => p.RushYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Carries}</td><td>{a.RushYds}</td><td>{a.RushTds}</td><td>{a.RushAvg:N1}</td><td>{a.RushLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // receiving tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} RECEIVING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>REC</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                foreach (var a in awayStats.Where(p => p.Receptions > 0).OrderByDescending(p => p.RecYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Receptions}</td><td>{a.RecYds}</td><td>{a.RecTds}</td><td>{a.RecAvg:N1}</td><td>{a.RecLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} RECEIVING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>REC</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                foreach (var a in homeStats.Where(p => p.Receptions > 0).OrderByDescending(p => p.RecYds))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Receptions}</td><td>{a.RecYds}</td><td>{a.RecTds}</td><td>{a.RecAvg:N1}</td><td>{a.RecLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // blocking tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} BLOCKING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>PANCAKE BLOCKS</th><th>SACKS ALLOWED</th></tr>");
                foreach (var a in awayStats.Where(p => p.PancakeBlocks > 0 || p.SacksAllowed > 0).OrderByDescending(p => p.PancakeBlocks))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PancakeBlocks}</td><td>{a.SacksAllowed}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} BLOCKING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>PANCAKE BLOCKS</th><th>SACKS ALLOWED</th></tr>");
                foreach (var a in homeStats.Where(p => p.PancakeBlocks > 0 || p.SacksAllowed > 0).OrderByDescending(p => p.PancakeBlocks))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PancakeBlocks}</td><td>{a.SacksAllowed}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // returns tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} RETURN STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>RET</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th></tr>");
                foreach (var a in awayStats.Where(p => p.KickReturns > 0).OrderByDescending(p => p.KickReturns))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.KickReturns}</td><td>{a.KickRetYds}</td><td>{a.KickReturnTds}</td><td>{a.ReturnAvg:N1}</td><td>{a.KickReturnLong}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} RETURN STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>RET</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th></tr>");
                foreach (var a in homeStats.Where(p => p.KickReturns > 0).OrderByDescending(p => p.KickReturns))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.KickReturns}</td><td>{a.KickRetYds}</td><td>{a.KickReturnTds}</td><td>{a.ReturnAvg:N1}</td><td>{a.KickReturnLong}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // defense tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} DEFENSIVE STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>TACKLES</th><th>SACKS</th><th>INTS</th><th>INT YDS</th><th>FF</th><th>FR</th><th>TDS</th></tr>");
                foreach (var a in awayStats.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0).OrderByDescending(p => p.Tackles))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Tackles}</td><td>{a.Sacks}</td><td>{a.Ints}</td><td>{a.IntYds}</td><td>{a.ForFumb}</td><td>{a.ForFumb}</td><td>{a.DefTds}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} DEFENSIVE STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>TACKLES</th><th>SACKS</th><th>INTS</th><th>INT YDS</th><th>FF</th><th>FR</th><th>TDS</th></tr>");
                foreach (var a in homeStats.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0).OrderByDescending(p => p.Tackles))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Tackles}</td><td>{a.Sacks}</td><td>{a.Ints}</td><td>{a.IntYds}</td><td>{a.ForFumb}</td><td>{a.ForFumb}</td><td>{a.DefTds}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                // kicking tables
                sw.WriteLine("<div class=\"row\">");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{AwayTeamAbbr} KICKING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>XPM</th><th>XPA</th><th>XP%</th><th>FGM</th><th>FGA</th><th>FG%</th><th>LONG</th><th>PTS</th></tr>");
                foreach (var a in awayStats.Where(p => p.XPAtt > 0 || p.FGAtt > 0).OrderByDescending(p => p.Points))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.XPMade}</td><td>{a.XPAtt}</td><td>{a.XpPct:P1}</td><td>{a.FGMade}</td><td>{a.FGAtt}</td><td>{a.FgPct:P1}</td><td>{a.FGLong}</td><td>{a.Points}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("<div class=\"column\">");
                sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto;\">");
                sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{HomeTeamAbbr} KICKING STATS</th></tr>");
                sw.WriteLine($"<tr><th>PLAYER</th><th>XPM</th><th>XPA</th><th>XP%</th><th>FGM</th><th>FGA</th><th>FG%</th><th>LONG</th><th>PTS</th></tr>");
                foreach (var a in homeStats.Where(p => p.XPAtt > 0 || p.FGAtt > 0).OrderByDescending(p => p.Points))
                    sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.XPMade}</td><td>{a.XPAtt}</td><td>{a.XpPct:P1}</td><td>{a.FGMade}</td><td>{a.FGAtt}</td><td>{a.FgPct:P1}</td><td>{a.FGLong}</td><td>{a.Points}</td></tr>");
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                sw.WriteLine("</div>");
                sw.WriteLine("</div>");

                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");
            }
        }
    }
}

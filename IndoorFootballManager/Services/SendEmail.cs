﻿using System.Collections.Generic;
using System.Linq;
using IndoorFootballManager.Models;
using IndoorFootballManager.DataAccess;
using SQLite;

namespace IndoorFootballManager.Services
{
    public static class SendEmail
    {
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;

        public static void NewManager(GeneralManager gm)
        {
            var team = Database.GetTeamById(gm.TeamId);
            if (team != null)
            {
                var email = new Email
                {
                    GmId = gm.Id,
                    TeamId = gm.TeamId,
                    Subject = "Welcome, " + gm.FullName + "!",
                    Message = "From: Team President\n" +
                        "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                        "Welcome to the " + team.TeamName + ", " + gm.FullName + "!\n" +
                        "\nWe're very glad you're on board and we look forward to you making an impact on our team." +
                        "\n\nSincerely,\n" +
                        "Team President",
                    CanReply = false,
                    SendDate = CurrentLeague.CurDate,
                    Read = true
                };
                Database.SaveEmail(email);
            }
        }

        public static void DraftStart(GeneralManager gm)
        {
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = "Today is Draft Day!",
                Message = "From: Team President\n" +
                "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                "Today is the official start of the " + CurrentLeague.CurDate.Year.ToString() + " Draft.\n" + 
                "Be sure to get your draft board ready. Its going to be a long night!" +
                "\n\nSincerely,\n" +
                "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = true
            };
            Database.SaveEmail(email);
        }

        public static void CampStart(GeneralManager gm)
        {
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = "Training Camp Starts Today!",
                Message = "From: Team President\n" +
                "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                "Today is the official start of Training Camp! Its an entire week of training for our team.\n" +
                "Be sure to all of our guys pass their physicals and get some good training in all week long!" +
                "\n\nSincerely,\n" +
                "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = true
            };
            Database.SaveEmail(email);
        }

        public static void RegularSeasonStart(GeneralManager gm)
        {
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = "Regular Season Kicks Off Today!",
                Message = "From: Team President\n" +
                "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                "Today is the official start of the " + CurrentLeague.CurDate.Year.ToString() + " Regular Season.\n" +
                "You have been preparing for this for months. Don't let me down. I know your guys are ready, so lets go out and win a championship!\n\n" +
                "P.S: Don't forget to cut your roster down to " + CurrentLeague.MaxRoster.ToString() + " and our reserves cannot be more than " + CurrentLeague.ReserveRoster.ToString() + " players. " + 
                "Also, do not forget you cannot be over the salary cap." +
                "\n\nSincerely,\n" +
                "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = true
            };
            Database.SaveEmail(email);
        }

        public static void SeasonEnd(GeneralManager gm)
        {
            var team = Database.GetTeamById(gm.TeamId);
            string emailAdd;
            if (team.PlayoffWins > 0 && team.PlayoffLosses == 0)
                emailAdd = "Congratulations on winning the " + CurrentLeague.LeagueName + " Championship! You had a tremendous season and I hope we can repeat again next season.\n";
            else if (team.WinPct > 0.75 && team.PlayoffLosses == 1)
                emailAdd = "Looks like you had a solid season going but we couldn't manage to make it to the Championship.\n";
            else if (team.WinPct > 0.75 && team.PlayoffLosses == 0)
                emailAdd = "We had a really good season but we didn't make the playoffs. That's a disappointment but we will move forward.\n";
            else if (team.WinPct > 0.5 && team.PlayoffLosses == 1)
                emailAdd = "Well, we had a good season making the playoffs, but we can do better. Lets regroup and get back into the playoffs next season.\n";
            else if (team.WinPct > 0.5 && team.PlayoffLosses == 0)
                emailAdd = "All I can say is at least we had a winning record. Lets do better next season and make the playoffs.\n";
            else
                emailAdd = "I must say I am very disappointed in this season's results. I didn't hire you to perform poorly. Lets figure out what went wrong and turn this team around!\n";

            emailAdd += "Having said that, let us take a quick look at your records from this season:\n" +
                    "Regular Season: " + team.TeamRecord + "\n" +
                    "Playoffs: " + team.PlayoffRecord + "\n" +
                    "Home: " + team.HomeRecord + "\n" +
                    "Away: " + team.AwayRecord + "\n" +
                    "Conference: " + team.ConfRecord + "\n" +
                    "Division: " + team.DivRecord + "\n\n" +
                    "Whether you had a great season or a bad one. Lets go into the off-season and make improvements to our team! If you have not completed contract extensions please make sure we get those taken care of before the end season date is here." +
                    "\n\nSincerely,\n" +
                    "Team President";

            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = "End of Season Recap",
                Message = "From: Team President\n" +
                "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                "Today marks the end of the " + CurrentLeague.CurDate.Year.ToString() + " Season.\n" + emailAdd,
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = true
            };
            Database.SaveEmail(email);
        }

        public static void ManagerToManager(GeneralManager sender, GeneralManager receiver, string subject, string message)
        {
            var email = new Email
            {
                GmId = receiver.Id,
                SenderId = sender.Id,
                TeamId = receiver.TeamId,
                Subject = subject,
                Message = message,
                CanReply = true,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }

        public static void IndividualTraining(Team team, Player player, string result)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = $"{player.NameWithPosition} Training Results from {CurrentLeague.CurDate.ToShortDateString()}",
                Message = "From: Training Staff\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    "Here is his result from the last session:\n" +
                    result,
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false,
            };
            Database.SaveEmail(email);
        }

        public static void TrainingResults(Team team, List<string> results)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = $"Training Results from {CurrentLeague.CurDate.ToShortDateString()}",
                Message = "From: Training Staff\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    "Here are the results from our last session:\n" +
                    string.Join("\n", results),
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false,
            };
            Database.SaveEmail(email);
        }

        public static void PlayerSigned(Team team, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = player.NameWithPosition + " has signed",
                Message = "From: Team President\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    player.Name + " has decided to sign with our team.\n" +
                    "\nWe're fortunate to have him on our squad. Lets get him a jersey, a helmet and then out on the practice field!" +
                    "\n\nSincerely,\n" +
                    "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }

        public static void PlayerDidNotSign(Team team, Team signed, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = player.NameWithPosition + " signed elsewhere",
                Message = "From: Team President\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    player.Name + " has decided to sign with " + signed.TeamName + " instead.\n" +
                    "\nWe wish him only success with his new team." +
                    "\n\nSincerely,\n" +
                    "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }

        public static void PlayerResigned(Team team, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = player.NameWithPosition + " has re-signed",
                Message = "From: Team President\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    player.Name + " has decided to re-sign with our team.\n" +
                    "\nWe look forward to seeing " + player.Name + " continue to progress and get better." +
                    "\n\nSincerely,\n" +
                    "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }

        public static void PlayerReleased(Team team, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            if (gm != null)
            {
                var email = new Email
                {
                    GmId = gm.Id,
                    TeamId = gm.TeamId,
                    Subject = player.NameWithPosition + " has been released",
                    Message = "From: Team President\n" +
                        "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                        player.Name + " has been released by our team.\n" +
                        "\nWe wish " + player.Name + " all the best in his future endeavors." +
                        "\n\nSincerely,\n" +
                        "Team President",
                    CanReply = false,
                    SendDate = CurrentLeague.CurDate,
                    Read = false
                };
                Database.SaveEmail(email);
            }
        }

        public static void PlayerFranchised(Team team, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = player.NameWithPosition + " has been franchised",
                Message = "From: Team President\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    "We have applied our franchise tag on " +player.Name + "\n" +
                    "He will be with us at least for next season. Lets try to sign him long term, though.\n" +
                    "\n\nSincerely,\n" +
                    "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }

        public static void PlayersTraded(Team team, Player player)
        {

        }

        public static void PlayersRetired(List<Player> players)
        {
            var teamIds = new List<int>();
            foreach (var p in players.Where(p => p.TeamId > 0 && !teamIds.Contains(p.TeamId)))
            {
                teamIds.Add(p.TeamId);
            }
            foreach (var id in teamIds)
            {
                var gm = Database.GetManagerByTeamId(id);
                var namesList = players.Where(pl => pl.TeamId == id).Aggregate(string.Empty, (current, p) => current + p.NameWithPosition + "\n");
                if (namesList == string.Empty) continue;
                var email = new Email
                {
                    GmId = gm.Id,
                    TeamId = gm.TeamId,
                    Subject = "Player Retirements",
                    Message = "From: Team President\n" +
                              "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                              "The following players have decided to retire from the " + CurrentLeague.LeagueName + " and our team.\n" +
                              namesList +
                              "\nThey will all be sorely missed both on and off the field. We wish them all the very best in retirement." +
                              "\n\nSincerely,\n" +
                              "Team President",
                    CanReply = false,
                    SendDate = CurrentLeague.CurDate,
                    Read = false
                };
                Database.SaveEmail(email);
            }
        }

        public static void PlayerRetired(Team team, Player player)
        {
            var gm = Database.GetManagerByTeamId(team.Id);
            var email = new Email
            {
                GmId = gm.Id,
                TeamId = gm.TeamId,
                Subject = player.NameWithPosition + " has retired!",
                Message = "From: Team President\n" +
                    "Date: " + CurrentLeague.CurDate.ToShortDateString() + "\n\n" +
                    player.Name + " has decided to retire from the " + CurrentLeague.LeagueName + ".\n" +
                    "\nHe will be sorely missed both on and off the field. We wish him the best in his retirement." +
                    "\n\nSincerely,\n" +
                    "Team President",
                CanReply = false,
                SendDate = CurrentLeague.CurDate,
                Read = false
            };
            Database.SaveEmail(email);
        }






    }
}

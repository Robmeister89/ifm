﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using SQLite;

namespace IndoorFootballManager.Services
{
    public static class ClassExtensions
    {
        private static SQLiteConnection Database => Repo.Database;
        private static League CurrentLeague => IFM.CurrentLeague;
        private static List<string> _trainingResults;

        public static void CpuTrainPlayers(this Team team)
        {
            _trainingResults = new List<string>();

            var trainingPlayers = team.Players.Where(p => !p.IsInjured).ToList();

            foreach (var p in trainingPlayers)
            {
                p.TrainPlayer(p.TrainingAttribute);
            }

            if (!trainingPlayers.Any()) 
                return;
            Database.UpdateAllPlayers(trainingPlayers.AsEnumerable());

            if (team.Manager == null) 
                return;

            SendEmail.TrainingResults(team, _trainingResults);
            IFM.NotifyChange("EmailsCount");
        }

        private static void TrainPlayer(this Player player, string attribute)
        {
            int prev;
            int rating;
            switch (attribute)
            {
                case "Accuracy":
                case "Pass Accuracy":
                    prev = player.Accuracy;
                    player.Accuracy = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Accuracy, player.WorkEthic) : player.WeeklyTraining(player.Accuracy, player.WorkEthic);
                    rating = player.Accuracy;
                    break;
                case "ArmStrength":
                case "Pass Strength":
                    prev = player.ArmStrength;
                    player.ArmStrength = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.ArmStrength, player.WorkEthic) : player.WeeklyTraining(player.ArmStrength, player.WorkEthic);
                    rating = player.ArmStrength;
                    break;
                case "Carry":
                    prev = player.Carry;
                    player.Carry = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Carry, player.WorkEthic) : player.WeeklyTraining(player.Carry, player.WorkEthic);
                    rating = player.Carry;
                    break;
                case "Speed":
                    prev = player.Speed;
                    player.Speed = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Speed, player.WorkEthic) : player.WeeklyTraining(player.Speed, player.WorkEthic);
                    rating = player.Speed;
                    break;
                case "Catching":
                    prev = player.Catching;
                    player.Catching = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Catching, player.WorkEthic) : player.WeeklyTraining(player.Catching, player.WorkEthic);
                    rating = player.Catching;
                    break;
                case "Block":
                case "Blocking":
                    prev = player.Block;
                    player.Block = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Block, player.WorkEthic) : player.WeeklyTraining(player.Block, player.WorkEthic);
                    rating = player.Block;
                    break;
                case "Blitz":
                case "Blitzing":
                    prev = player.Blitz;
                    player.Blitz = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Blitz, player.WorkEthic) : player.WeeklyTraining(player.Blitz, player.WorkEthic);
                    rating = player.Blitz;
                    break;
                case "KickAccuracy":
                case "Kick Accuracy":
                    prev = player.KickAccuracy;
                    player.KickAccuracy = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.KickAccuracy, player.WorkEthic) : player.WeeklyTraining(player.KickAccuracy, player.WorkEthic);
                    rating = player.KickAccuracy;
                    break;
                case "KickStrength":
                case "Kick Strength":
                    prev = player.KickStrength;
                    player.KickStrength = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.KickStrength, player.WorkEthic) : player.WeeklyTraining(player.KickStrength, player.WorkEthic);
                    rating = player.KickStrength;
                    break;
                case "Coverage":
                    prev = player.Coverage;
                    player.Coverage = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Coverage, player.WorkEthic) : player.WeeklyTraining(player.Coverage, player.WorkEthic);
                    rating = player.Coverage;
                    break;
                case "Tackle":
                case "Tackling":
                    prev = player.Tackle;
                    player.Tackle = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Tackle, player.WorkEthic) : player.WeeklyTraining(player.Tackle, player.WorkEthic);
                    rating = player.Tackle;
                    break;
                case "Strength":
                    prev = player.Strength;
                    player.Strength = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Strength, player.WorkEthic) : player.WeeklyTraining(player.Strength, player.WorkEthic);
                    rating = player.Strength;
                    break;
                default:
                case "Intelligence":
                    prev = player.Intelligence;
                    player.Intelligence = CurrentLeague.IsTrainingCamp ? player.TrainingCamp(player.Intelligence, player.WorkEthic) : player.WeeklyTraining(player.Intelligence, player.WorkEthic);
                    rating = player.Intelligence;
                    break;
            }
            player.Overall = player.GetOverall;
            player.HasTrained = true;

            if (rating > prev)
            {
                var playerHistory = player.PlayerHistoryJson == null ? new List<string>() : player.PlayerHistoryJson.DeserializeToList<string>();
                playerHistory.Add($"{attribute} improved from {prev} to {rating} (+{rating - prev}) on {CurrentLeague.CurDate.ToLongDateString()}.");
                player.PlayerHistoryJson = playerHistory.SerializeToJson();
            }

            _trainingResults.Add(prev == rating
                ? $"{player.NameWithPosition}'s {attribute} remained the same at {prev}"
                : $"{player.NameWithPosition}'s {attribute} increased from {prev} to {rating}");
        }

        public static async Task PlayGame(this Game g) => await g.PlayGame(g.Id);
        
        public static Task SaveGameStats(this Game game, bool isPlayoffs)
        {
            var gameStats = game.StatsJson.DeserializeToList<PlayerGameStats>();

            if (!isPlayoffs)
            {
                List<PlayerStats> playerStats = null;
                var seasonStats = Database.GetAllPlayerStats(CurrentLeague.CurrentSeason).FirstOrDefault();
                if (seasonStats != null)
                    playerStats = seasonStats.StatsJson.DeserializeToList<PlayerStats>();
                else
                {
                    seasonStats = new PlayerStats { Season = game.Season, };
                    playerStats = new List<PlayerStats>();
                }

                foreach (var p in gameStats)
                {
                    var ps = playerStats.FirstOrDefault(s => s.PlayerId == p.PlayerId && s.Team == p.Team);
                    if (ps != null)
                    {
                        ps.Season = p.Season;
                        ps.Team = p.Team;
                        ps.TeamId = p.TeamId;
                        ps.PlayerName = p.PlayerName;
                        ps.PlayerId = p.PlayerId;
                        ps.PassAtt += p.PassAtt;
                        ps.PassCmp += p.PassCmp;
                        ps.PassYds += p.PassYds;
                        ps.PassTds += p.PassTds;
                        ps.PassInts += p.PassInts;
                        ps.Pass20 += p.Pass20;
                        ps.PassFirst += p.PassFirst;
                        if (ps.PassLong < p.PassLong) { ps.PassLong = p.PassLong; }
                        ps.Carries += p.Carries;
                        ps.RushYds += p.RushYds;
                        ps.RushTds += p.RushTds;
                        ps.Rush20 += p.Rush20;
                        ps.RushFirst += p.RushFirst;
                        if (ps.RushLong < p.RushLong) { ps.RushLong = p.RushLong; }
                        ps.Receptions += p.Receptions;
                        ps.RecYds += p.RecYds;
                        ps.RecTds += p.RecTds;
                        ps.Rec20 += p.Rec20;
                        ps.RecFirst += p.RecFirst;
                        if (ps.RecLong < p.RecLong) { ps.RecLong = p.RecLong; }
                        ps.Drops += p.Drops;
                        ps.Fumbles += p.Fumbles;
                        ps.FumblesLost += p.FumblesLost;
                        ps.Tackles += p.Tackles;
                        ps.Sacks += p.Sacks;
                        ps.ForFumb += p.ForFumb;
                        ps.FumbRec += p.FumbRec;
                        ps.Ints += p.Ints;
                        ps.IntYds += p.IntYds;
                        ps.Safeties += p.Safeties;
                        ps.DefTds += p.DefTds;
                        ps.FGAtt += p.FGAtt;
                        ps.FGMade += p.FGMade;
                        if (ps.FGLong < p.FGLong) { ps.FGLong = p.FGLong; }
                        ps.XPAtt += p.XPAtt;
                        ps.XPMade += p.XPMade;
                        ps.Rouges += p.Rouges;
                        ps.KickReturns += p.KickReturns;
                        ps.KickRetYds += p.KickRetYds;
                        ps.KickReturnTds += p.KickReturnTds;
                        if (ps.KickReturnLong < p.KickReturnLong) { ps.KickReturnLong = p.KickReturnLong; }
                        ps.PancakeBlocks += p.PancakeBlocks;
                        ps.SacksAllowed += p.SacksAllowed;
                    }
                    else
                    {
                        ps = new PlayerStats
                        {
                            Season = p.Season,
                            Team = p.Team,
                            TeamId = p.TeamId,
                            PlayerName = p.PlayerName,
                            PlayerId = p.PlayerId,
                            PassAtt = p.PassAtt,
                            PassCmp = p.PassCmp,
                            PassYds = p.PassYds,
                            PassTds = p.PassTds,
                            PassInts = p.PassInts,
                            Pass20 = p.Pass20,
                            PassFirst = p.PassFirst,
                            PassLong = p.PassLong,
                            Carries = p.Carries,
                            RushYds = p.RushYds,
                            RushTds = p.RushTds,
                            Rush20 = p.Rush20,
                            RushFirst = p.RushFirst,
                            RushLong = p.RushLong,
                            Receptions = p.Receptions,
                            RecYds = p.RecYds,
                            RecTds = p.RecTds,
                            Rec20 = p.Rec20,
                            RecFirst = p.RecFirst,
                            RecLong = p.RecLong,
                            Drops = p.Drops,
                            Fumbles = p.Fumbles,
                            FumblesLost = p.FumblesLost,
                            Tackles = p.Tackles,
                            Sacks = p.Sacks,
                            ForFumb = p.ForFumb,
                            FumbRec = p.FumbRec,
                            Ints = p.Ints,
                            IntYds = p.IntYds,
                            Safeties = p.Safeties,
                            DefTds = p.DefTds,
                            FGAtt = p.FGAtt,
                            FGMade = p.FGMade,
                            FGLong = p.FGLong,
                            XPAtt = p.XPAtt,
                            XPMade = p.XPMade,
                            Rouges = p.Rouges,
                            KickReturns = p.KickReturns,
                            KickRetYds = p.KickRetYds,
                            KickReturnTds = p.KickReturnTds,
                            KickReturnLong = p.KickReturnLong,
                            PancakeBlocks = p.PancakeBlocks,
                            SacksAllowed = p.SacksAllowed,
                        };
                        playerStats.Add(ps);
                    }
                }
                seasonStats.StatsJson = playerStats.SerializeToJson();
                Database.SavePlayerStats(seasonStats);
            }
            else
            {
                List<PlayerPlayoffStats> playerStats = null;
                var playoffStats = Database.GetAllPlayerPlayoffStats(CurrentLeague.CurrentSeason).FirstOrDefault();
                if (playoffStats != null)
                    playerStats = playoffStats.StatsJson.DeserializeToList<PlayerPlayoffStats>();
                else
                {
                    playoffStats = new PlayerPlayoffStats { Season = game.Season, };
                    playerStats = new List<PlayerPlayoffStats>();
                }

                foreach (var p in gameStats)
                {
                    var ps = playerStats.FirstOrDefault(s => s.PlayerId == p.PlayerId && s.Team == p.Team);
                    if (ps != null)
                    {
                        ps.Season = p.Season;
                        ps.Team = p.Team;
                        ps.TeamId = p.TeamId;
                        ps.PlayerName = p.PlayerName;
                        ps.PlayerId = p.PlayerId;
                        ps.PassAtt += p.PassAtt;
                        ps.PassCmp += p.PassCmp;
                        ps.PassYds += p.PassYds;
                        ps.PassTds += p.PassTds;
                        ps.PassInts += p.PassInts;
                        ps.Pass20 += p.Pass20;
                        ps.PassFirst += p.PassFirst;
                        if (ps.PassLong < p.PassLong) { ps.PassLong = p.PassLong; }
                        ps.Carries += p.Carries;
                        ps.RushYds += p.RushYds;
                        ps.RushTds += p.RushTds;
                        ps.Rush20 += p.Rush20;
                        ps.RushFirst += p.RushFirst;
                        if (ps.RushLong < p.RushLong) { ps.RushLong = p.RushLong; }
                        ps.Receptions += p.Receptions;
                        ps.RecYds += p.RecYds;
                        ps.RecTds += p.RecTds;
                        ps.Rec20 += p.Rec20;
                        ps.RecFirst += p.RecFirst;
                        if (ps.RecLong < p.RecLong) { ps.RecLong = p.RecLong; }
                        ps.Drops += p.Drops;
                        ps.Fumbles += p.Fumbles;
                        ps.FumblesLost += p.FumblesLost;
                        ps.Tackles += p.Tackles;
                        ps.Sacks += p.Sacks;
                        ps.ForFumb += p.ForFumb;
                        ps.FumbRec += p.FumbRec;
                        ps.Ints += p.Ints;
                        ps.IntYds += p.IntYds;
                        ps.Safeties += p.Safeties;
                        ps.DefTds += p.DefTds;
                        ps.FGAtt += p.FGAtt;
                        ps.FGMade += p.FGMade;
                        if (ps.FGLong < p.FGLong) { ps.FGLong = p.FGLong; }
                        ps.XPAtt += p.XPAtt;
                        ps.XPMade += p.XPMade;
                        ps.Rouges += p.Rouges;
                        ps.KickReturns += p.KickReturns;
                        ps.KickRetYds += p.KickRetYds;
                        ps.KickReturnTds += p.KickReturnTds;
                        if (ps.KickReturnLong < p.KickReturnLong) { ps.KickReturnLong = p.KickReturnLong; }
                        ps.PancakeBlocks += p.PancakeBlocks;
                        ps.SacksAllowed += p.SacksAllowed;
                    }
                    else
                    {
                        ps = new PlayerPlayoffStats
                        {
                            Season = p.Season,
                            Team = p.Team,
                            TeamId = p.TeamId,
                            PlayerName = p.PlayerName,
                            PlayerId = p.PlayerId,
                            PassAtt = p.PassAtt,
                            PassCmp = p.PassCmp,
                            PassYds = p.PassYds,
                            PassTds = p.PassTds,
                            PassInts = p.PassInts,
                            Pass20 = p.Pass20,
                            PassFirst = p.PassFirst,
                            PassLong = p.PassLong,
                            Carries = p.Carries,
                            RushYds = p.RushYds,
                            RushTds = p.RushTds,
                            Rush20 = p.Rush20,
                            RushFirst = p.RushFirst,
                            RushLong = p.RushLong,
                            Receptions = p.Receptions,
                            RecYds = p.RecYds,
                            RecTds = p.RecTds,
                            Rec20 = p.Rec20,
                            RecFirst = p.RecFirst,
                            RecLong = p.RecLong,
                            Drops = p.Drops,
                            Fumbles = p.Fumbles,
                            FumblesLost = p.FumblesLost,
                            Tackles = p.Tackles,
                            Sacks = p.Sacks,
                            ForFumb = p.ForFumb,
                            FumbRec = p.FumbRec,
                            Ints = p.Ints,
                            IntYds = p.IntYds,
                            Safeties = p.Safeties,
                            DefTds = p.DefTds,
                            FGAtt = p.FGAtt,
                            FGMade = p.FGMade,
                            FGLong = p.FGLong,
                            XPAtt = p.XPAtt,
                            XPMade = p.XPMade,
                            Rouges = p.Rouges,
                            KickReturns = p.KickReturns,
                            KickRetYds = p.KickRetYds,
                            KickReturnTds = p.KickReturnTds,
                            KickReturnLong = p.KickReturnLong,
                            PancakeBlocks = p.PancakeBlocks,
                            SacksAllowed = p.SacksAllowed,
                        };
                        playerStats.Add(ps);
                    }
                }
                playoffStats.StatsJson = playerStats.SerializeToJson();
                Database.SavePlayerPlayoffStats(playoffStats);
            }

            return Task.CompletedTask;
        }
    }

    public static class DoubleExtensions
    {
        public static double RatingValue(this double value)
        {
            return value > 2.375
                ? 2.375
                : value < 0
                    ? 0
                    : value;
        }
    }

    public static class ListExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> src) => src == null || !src.Any();
    }

    public static class StringExtensions
    {
        public static string ToTitleCase(this string str)
        {
            if (str.IsNullOrEmpty())
                return string.Empty;

            var textInfo = CultureInfo.CurrentCulture.TextInfo;
            return textInfo.ToTitleCase(str);
        }
    }
}

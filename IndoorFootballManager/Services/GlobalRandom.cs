﻿using System;

namespace IndoorFootballManager.Services
{
    internal static class GlobalRandom
    {
        private static Random _randomInstance;
        public static double NextDouble
        {
            get
            {
                if (_randomInstance == null)
                    _randomInstance = new Random();

                return _randomInstance.NextDouble();
            }
        }
    }
}

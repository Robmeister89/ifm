﻿using IndoorFootballManager.DataAccess;
using IndoorFootballManager.Models;
using SQLite;
using System;
using System.IO;
using System.Linq;

namespace IndoorFootballManager.Services
{
    public class HtmlHelpers
    {
        private static SQLiteConnection Database => Repo.Database;

        public static void CopyCss(string path) => File.Copy(@"Resources\styles.css", path, true);
        public static void CopyJquery(string path) => File.Copy(@"Resources\jquery.js", path, true);
        public static void CopyJs(string path) => File.Copy(@"Resources\scripts.js", path, true);
        public static void CopySortTable(string path) => File.Copy(@"Resources\sorttable.js", path, true);

        public static string Menu(League league)
        {
            return "<ul>\n" +
                "<li><a href=\"./Index.html\"><i class=\"fas fa-home fa-lg\"></i> Home</a></li>\n" +
                "<li><a href=\"./Managers.html\"><i class=\"fas fa-headset fa-lg\"></i> Managers</a></li>\n" +
                "<li><a href=\"./Teams.html\"><i class=\"fas fa-clipboard fa-lg\"></i> Teams</a></li>\n" +
                "<li><a href=\"./Transactions.html\"><i class=\"fas fa-exchange-alt\"></i> Transactions</a></li>\n" +
                "<li><a href=\"./Standings.html\"><i class=\"fas fa-list-ol fa-lg\"></i> Standings</a></li>\n" +
                "<li><a href=\"./SeasonStats.html\"><i class=\"fas fa-sort-amount-up fa-lg\"></i> Season Stats</a></li>\n" +
                "<li><a href=\"./Games.html\"><i class=\"fas fa-football-ball fa-lg\"></i> Schedule</a></li>\n" +
                $"<li style=\"float: right; padding: 14px 16px;\">{league.LeagueName}</li>\n" +
                "</ul>";
        }

        public static string MenuForGames(League league)
        {
            return "<ul>\n" +
                "<li><a href=\"../Index.html\"><i class=\"fas fa-home fa-lg\"></i> Home</a></li>\n" +
                "<li><a href=\"../Managers.html\"><i class=\"fas fa-headset fa-lg\"></i> Managers</a></li>\n" +
                "<li><a href=\"../Teams.html\"><i class=\"fas fa-clipboard fa-lg\"></i> Teams</a></li>\n" +
                "<li><a href=\"../Transactions.html\"><i class=\"fas fa-exchange-alt\"></i> Transactions</a></li>\n" +
                "<li><a href=\"../Standings.html\"><i class=\"fas fa-list-ol fa-lg\"></i> Standings</a></li>\n" +
                "<li><a href=\"../SeasonStats.html\"><i class=\"fas fa-sort-amount-up fa-lg\"></i> Season Stats</a></li>\n" +
                "<li><a href=\"../Games.html\"><i class=\"fas fa-football-ball fa-lg\"></i> Schedule</a></li>\n" +
                $"<li style=\"float: right; padding: 14px 16px;\">{league.LeagueName}</li>\n" +
                "</ul>";
        }

        public static void ExportIndex(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Index.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Home Page</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("</br></br>");
                sw.WriteLine("<div class=\"w3-animate-zoom\">");
                sw.WriteLine($"<h2>Welcome to the {league.LeagueName} Website for the {league.CurrentSeason} Season!</h2>");
                sw.WriteLine($"<h3>This index page is under construction. Please use a link on the menu to navigate.</h3>");

                sw.WriteLine("</body>");
                sw.WriteLine("</div>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportTeamsHtml(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Teams.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Teams List</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("</br></br>");
                sw.WriteLine("<div class=\"w3-animate-zoom\">");
                sw.WriteLine($"<h2>{league.LeagueName} {league.CurrentSeason} Teams List</h2>");

                sw.WriteLine("<table style=\"width: 70%; margin-left: auto; margin-right: auto; margin-top: 15px;\">");
                sw.WriteLine("<tr><th>ID</th><th>CITY</th><th>MASCOT</th><th>ABBR.</th><th>CONFERENCE</th><th>DIVISION</th><th>EXPORT STATUS</th><th>EXPORT DATE/TIME</th></tr>");

                var teams = Database.GetTeams().OrderBy(t => t.City).ThenBy(t => t.Mascot).ToList();
                teams.ForEach(x =>
                {
                    string status = null;
                    if (x.ExportStatus == "Successful")
                        status = "<i style=\"color: green\" class=\"fas fa-check-square fa-lg\"></i>";
                    else
                        status = "<i style=\"color: tomato\" class=\"fas fa-times-circle fa-lg\"></i>";
                    sw.WriteLine($"<tr><td>{x.Id}</td><td>{x.City}</td><td>{x.Mascot}</td><td>{x.Abbreviation}</td><td>{x.Conference}</td><td>{x.Division}</td><td>{status}</td><td>{x.ExportTime}</td></tr>");
                });
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");

                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportManagersHtml(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Managers.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Managers List</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("</br></br>");
                sw.WriteLine("<div class=\"w3-animate-zoom\">");
                sw.WriteLine($"<h2>{league.LeagueName} {league.CurrentSeason} Managers List</h2>");

                sw.WriteLine("<table style=\"width: 70%; margin-left: auto; margin-right: auto; margin-top: 15px;\">");
                sw.WriteLine("<tr><th>ID</th><th>FIRST NAME</th><th>LAST NAME</th><th>DATE OF BIRTH</th><th>TEAM</th><th>COMMISSIONER</th></tr>");

                var managers = Database.GetManagers().OrderBy(m => m.LastName).ThenBy(m => m.FirstName).ToList();
                managers.ForEach(x =>
                {
                    string status = null;
                    if (x.IsCommissioner)
                        status = "<i style=\"color: green\" class=\"fas fa-check-square fa-lg\"></i>";
                    else
                        status = "<i style=\"color: tomato\" class=\"fas fa-times-circle fa-lg\"></i>";
                    string team = null;
                    if (x.TeamId == 0)
                        team = "No Team";
                    else
                        team = x.Team.TeamName;
                    sw.WriteLine($"<tr><td>{x.Id}</td><td>{x.FirstName}</td><td>{x.LastName}</td><td>{x.DOB.ToShortDateString()}</td><td>{team}</td><td>{status}</td></tr>");
                });
                sw.WriteLine("</table>");
                sw.WriteLine("</div>");

                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportTransactions(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Transactions.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Schedule</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("<div class=\"w3-container w3-animate-zoom\" style=\"margin-top: 50px\">");
                sw.WriteLine($"<h2>{league.LeagueName} {league.CurrentSeason} Transactions</h2>");

                sw.WriteLine("<table style=\"width: 60%; margin-left: auto; margin-right: auto; margin-bottom: 15px; box-shadow: 0px 0px 3px; border: none\">");
                sw.WriteLine($"<tr><th colspan=\"100%\">Transactions</th></tr>");
                var transactions = Database.GetTransactions(league.CurrentSeason).OrderByDescending(t => t.Id).ToList();
                transactions.ForEach(x =>
                {
                    sw.WriteLine($"<tr><td style=\"width: 100px;\">{x.Text}</td></tr>");
                });
                sw.WriteLine("</table>");

                sw.WriteLine("</div>");
                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportGamesList(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Games.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Schedule</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("<div class=\"w3-container w3-animate-zoom\" style=\"margin-top: 50px\">");
                sw.WriteLine($"<h2>{league.LeagueName} {league.CurrentSeason} Schedule</h2>");

                var games = Database.GetGames(league.CurrentSeason).ToList();
                DateTime? curGameDate = null;
                games.ForEach(x =>
                {
                    if (curGameDate == null || curGameDate != x.GameDate)
                    {
                        if (curGameDate != null)
                            sw.WriteLine("</table>");

                        curGameDate = x.GameDate;
                        sw.WriteLine("<table style=\"width: 60%; margin-left: auto; margin-right: auto; margin-bottom: 15px; box-shadow: 0px 0px 3px; border: none\">");
                        sw.WriteLine($"<tr><th colspan=\"100%\">{curGameDate.Value.ToShortDateString()}</th></tr>");
                    }
                    if (x.HasPlayed)
                    {
                        if (x.AwayOT > 0 || x.HomeOT > 0)
                            sw.WriteLine($"<tr><td style=\"width: 100px;\"><b>FINAL</b></td><td>{x.AwayTeamStr} <b>{x.AwayFinal}</b> - {x.HomeTeamStr} <b>{x.HomeFinal} (OT)</b></td><td style=\"width: 150px;\"><a href=\"./Games/{x.Id}.html\" style=\"text-decoration: none;\"><b>VIEW RECAP</b></a></td></tr>");
                        else
                            sw.WriteLine($"<tr><td style=\"width: 100px;\"><b>FINAL</b></td><td>{x.AwayTeamStr} <b>{x.AwayFinal}</b> - {x.HomeTeamStr} <b>{x.HomeFinal}</b></td><td style=\"width: 150px;\"><a href=\"./Games/{x.Id}.html\" style=\"text-decoration: none;\"><b>VIEW RECAP</b></a></td></tr>");
                    }
                    else
                    {
                        sw.WriteLine($"<tr><td colspan=\"100%\">{x.AwayTeamStr} @ {x.HomeTeamStr} ({x.GameDate.ToShortDateString()})</td></tr>");
                    }
                });
                sw.WriteLine("</table>");

                sw.WriteLine("</div>");
                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportStandingsHtml(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}Standings.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Standings</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                sw.WriteLine("</br></br>");
                sw.WriteLine("<div class=\"w3-animate-zoom\">");
                sw.WriteLine($"<h2>{league.LeagueName} {league.CurrentSeason} Standings</h2>");

                if (league.Conferences == 1)
                {
                    for(var i = 0; i < league.Divisions; i++)
                    {
                        sw.WriteLine("<table style=\"width: 70%; margin-left: auto; margin-right: auto; margin-top: 15px;\">");
                        sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{league.DivisionsList[i]} Standings</th></tr>");
                        sw.WriteLine("<tr><th style=\"width:300px\">TEAM</th><th>WINS</th><th>LOSSES</th><th>PCT</th><th>PF</th><th>PA</th><th>HOME</th><th>AWAY</th><th>DIV</th><th>CONF</th><th>STREAK</th></tr>");

                        var teams = Database.GetTeamsForDivisionStandings(league.Conference1, league.DivisionsList[i]).ToList();
                        teams.ForEach(x =>
                        {
                            sw.WriteLine($"<tr><td>{x.TeamName}</td><td>{x.Wins}</td><td>{x.Losses}</td><td>{x.WinPct:P1}</td><td>{x.PointsFor}</td><td>{x.PointsAgainst}</td><td>{x.HomeRecord}</td><td>{x.AwayRecord}</td><td>{x.DivRecord}</td><td>{x.ConfRecord}</td><td>{x.StreakTxt}</td></tr>");
                        });
                        sw.WriteLine("</table>");
                    }

                }
                else
                {
                    var divs = league.Divisions;
                    sw.WriteLine("<div class=\"row\">");
                    for(var i = 0; i < 2; i++)
                    {
                        sw.WriteLine("<div class=\"column\">");
                        var conference = i == 0 ? league.Conference1 : league.Conference2;
                        for (var j = 0; j < divs; j++)
                        {
                            var id = j + i;
                            if (i == 1)
                                id = j + divs;

                            sw.WriteLine("<table style=\"width: 90%; margin-left: auto; margin-right: auto; margin-top: 15px;\">");
                            sw.WriteLine($"<tr><th style=\"background-color: #ccc;\" colspan=\"100%\">{league.DivisionsList[id]} Standings</th></tr>");
                            sw.WriteLine("<tr><th style=\"width:300px\">TEAM</th><th>WINS</th><th>LOSSES</th><th>PCT</th><th>PF</th><th>PA</th><th>HOME</th><th>AWAY</th><th>DIV</th><th>CONF</th><th>STREAK</th></tr>");

                            var teams = Database.GetTeamsForDivisionStandings(conference, league.DivisionsList[id]).ToList();
                            teams.ForEach(x =>
                            {
                                sw.WriteLine($"<tr><td>{x.TeamName}</td><td>{x.Wins}</td><td>{x.Losses}</td><td>{x.WinPct:P1}</td><td>{x.PointsFor}</td><td>{x.PointsAgainst}</td><td>{x.HomeRecord}</td><td>{x.AwayRecord}</td><td>{x.DivRecord}</td><td>{x.ConfRecord}</td><td>{x.StreakTxt}</td></tr>");
                            });
                            sw.WriteLine("</table>");
                        }
                        sw.WriteLine("</div>");
                    }
                    sw.WriteLine("</div>");
                }
                sw.WriteLine("</div>");
                sw.WriteLine("</body>");
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");

            }
        }

        public static void ExportSeasonStats(League league)
        {
            var databasePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{league.Abbreviation}\";
            var outputPath = $@"{databasePath}html\{league.CurrentSeason}\";
            Directory.CreateDirectory(outputPath);

            var file = $@"{outputPath}SeasonStats.html";
            if (File.Exists(file))
                File.Delete(file);

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("<!DOCTYPE html>");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine($"<title>{league.Abbreviation} {league.CurrentSeason} Season Statistics</title>");
                sw.WriteLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"../styles.css\"/>");
                sw.WriteLine("<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">");
                sw.WriteLine("<script src=\"https://kit.fontawesome.com/ad88de2c0e.js\" crossorigin=\"anonymous\"></script>");
                sw.WriteLine("<script src=\"../scripts.js\"></script>");
                sw.WriteLine("<script src=\"../jquery.js\"></script>");
                sw.WriteLine("<script src=\"../sorttable.js\"></script>");
                sw.WriteLine("</head>");
                sw.WriteLine("<body>");
                sw.WriteLine("<button onclick=\"topFunction()\" id=\"topBtn\" title=\"Go to top\">Top</button>");
                sw.WriteLine($"{Menu(league)}");
                var leagueStats = Database.GetAllPlayerStats(league.CurrentSeason).FirstOrDefault();
                if (leagueStats != null)
                {
                    var seasonStats = leagueStats.StatsJson.DeserializeToList<PlayerStats>();
                    sw.WriteLine("<div class=\"w3-container\" style=\"margin-top: 50px\">");
                    sw.WriteLine("<div class=\"w3-sidebar w3-bar-block w3-light-grey w3-card\" style=\"width:130px; margin-left: -16px;\">");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink w3-red\" onclick=\"openSideTab(event, 'Passing')\">Passing</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Rushing')\">Rushing</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Receiving')\">Receiving</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Blocking')\">Blocking</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Returns')\">Returns</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Defense')\">Defense</button>");
                    sw.WriteLine("<button class=\"w3-bar-item w3-button sidetablink\" onclick=\"openSideTab(event, 'Kicking')\">Kicking</button>");
                    sw.WriteLine("</div>");
                    sw.WriteLine("<div id=\"Passing\" class=\"w3-container _name w3-animate-zoom\" style=\"display: block\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Passing Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>COMP</th><th>ATT</th><th>CMP %</th><th>YARDS</th><th>TDS</th><th>INTS</th><th>AVG/CMP</th><th>QBR</th></tr>");
                    var passing = seasonStats.Where(p => p.PassAtt > 0).OrderByDescending(p => p.PassYds).ToList();
                    passing.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PassCmp}</td><td>{a.PassAtt}</td><td>{a.CompPct:P1}</td><td>{a.PassYds}</td><td>{a.PassTds}</td><td>{a.PassInts}</td><td>{a.PassYPC:N1}</td><td>{a.QBRating:N1}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Rushing\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Rushing Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>ATT</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                    var rushing = seasonStats.Where(p => p.Carries > 0).OrderByDescending(p => p.RushYds).ToList();
                    rushing.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Carries}</td><td>{a.RushYds}</td><td>{a.RushTds}</td><td>{a.RushAvg:N1}</td><td>{a.RushLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Receiving\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Receiving Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>REC</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th><th>FUMBLES</th><th>FUMB LOST</th></tr>");
                    var receiving = seasonStats.Where(p => p.Receptions > 0).OrderByDescending(p => p.RecYds).ToList();
                    receiving.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Receptions}</td><td>{a.RecYds}</td><td>{a.RecTds}</td><td>{a.RecAvg:N1}</td><td>{a.RecLong}</td><td>{a.Fumbles}</td><td>{a.FumblesLost}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Blocking\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Blocking Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>PANCAKE BLOCKS</th><th>SACKS ALLOWED</th></tr>");
                    var blocking = seasonStats.Where(p => p.SacksAllowed > 0 || p.PancakeBlocks > 0).OrderByDescending(p => p.PancakeBlocks).ToList();
                    blocking.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.PancakeBlocks}</td><td>{a.SacksAllowed}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Returns\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Kick Return Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>RET</th><th>YARDS</th><th>TDS</th><th>AVG</th><th>LONG</th></tr>");
                    var returns = seasonStats.Where(p => p.KickReturns > 0).OrderByDescending(p => p.KickRetYds).ToList();
                    returns.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.KickReturns}</td><td>{a.KickRetYds}</td><td>{a.KickReturnTds}</td><td>{a.ReturnAvg:N1}</td><td>{a.KickReturnLong}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Defense\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Defensive Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>TACKLES</th><th>SACKS</th><th>INTS</th><th>INT YDS</th><th>FF</th><th>FR</th><th>TDS</th></tr>");
                    var defense = seasonStats.Where(p => p.Tackles > 0 || p.Sacks > 0 || p.Ints > 0).OrderByDescending(p => p.Tackles).ToList();
                    defense.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.Tackles}</td><td>{a.Sacks}</td><td>{a.Ints}</td><td>{a.IntYds}</td><td>{a.ForFumb}</td><td>{a.ForFumb}</td><td>{a.DefTds}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("<div id=\"Kicking\" class=\"w3-container _name w3-animate-zoom\" style=\"display: none\">");
                    sw.WriteLine($"<h2>{league.Abbreviation} {league.CurrentSeason} Kicking Statistics</h2>");
                    sw.WriteLine("<table class=\"sortable\" style=\"width: 90%; margin-left: 150px; margin-right: auto; margin-top: 15px;\">");
                    sw.WriteLine($"<tr><th>PLAYER</th><th>XPM</th><th>XPA</th><th>XP%</th><th>FGM</th><th>FGA</th><th>FG%</th><th>LONG</th><th>PTS</th></tr>");
                    var kicking = seasonStats.Where(p => p.XPAtt > 0 || p.FGAtt > 0).OrderByDescending(p => p.Points).ToList();
                    kicking.ForEach(a =>
                    {
                        sw.WriteLine($"<tr><td>{a.PlayerName}</td><td>{a.XPMade}</td><td>{a.XPAtt}</td><td>{a.XpPct:P1}</td><td>{a.FGMade}</td><td>{a.FGAtt}</td><td>{a.FgPct:P1}</td><td>{a.FGLong}</td><td>{a.Points}</td></tr>");
                    });
                    sw.WriteLine("</table>");
                    sw.WriteLine("</div>");

                    sw.WriteLine("</div>");
                    sw.WriteLine("</body>");
                }
                sw.WriteLine("<footer class=\"w3-animate-zoom\" style=\"margin-left: auto; margin-right: auto; text-align: center; padding-top: 50px;\"><b>Copyright © Indoor Football Manager. All Rights Reserved.</b></footer>");
                sw.WriteLine("</html>");
            }
        }




    }
}

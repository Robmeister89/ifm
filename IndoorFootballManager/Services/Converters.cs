﻿using IndoorFootballManager.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace IndoorFootballManager.Services
{
    public static class Converters
    {
        public static string SerializeToJson<T>(this T obj) => obj == null ? string.Empty : JsonConvert.SerializeObject(obj);
        public static List<T> DeserializeToList<T>(this string json) => json == null ? new List<T>() : JsonConvert.DeserializeObject<List<T>>(json);

        public static byte[] SaveFromFile(string path)
        {
            using (var ms = new MemoryStream())
            {
                var img = Image.FromFile(path);
                img.Save(ms, img.RawFormat);
                return ms.ToArray();
            }
        }

        public static byte[] SaveImage(ImageSource imageSource)
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            byte[] bytes;

            if (!(imageSource is BitmapSource bitmapSource))
            {
                return null;
            }

            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public static ImageSource LoadImage(byte[] logo)
        {
            using (var ms = new MemoryStream(logo))
            {
                return BitmapFrame.Create(ms, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
        }

        public static string EncryptDecrypt(string szPlainText, int szEncryptionKey)
        {
            var szInputStringBuild = new StringBuilder(szPlainText);
            var szOutStringBuild = new StringBuilder(szPlainText.Length);
            for (var iCount = 0; iCount < szPlainText.Length; iCount++)
            {
                var text = szInputStringBuild[iCount];
                text = (char)(text ^ szEncryptionKey);
                szOutStringBuild.Append(text);
            }
            return szOutStringBuild.ToString();
        }

        public static bool PlayerWasReleased(string json, string teamName, string datetime)
        {
            return !string.IsNullOrEmpty(json)
                   && json.DeserializeToList<string>().Last().StartsWith($"Released by {teamName} on {datetime}");
        }
        
        public static List<Player> ConvertByteToPlayers(byte[] bytes)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();
            mStream.Write(bytes, 0, bytes.Length);
            mStream.Position = 0;
            var myObject = binFormatter.Deserialize(mStream) as List<Player>;
            return myObject;
        }

        public static List<string> ConvertByteToStringList(byte[] bytes)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();
            mStream.Write(bytes, 0, bytes.Length);
            mStream.Position = 0;
            var myObject = binFormatter.Deserialize(mStream) as List<string>;
            return myObject;
        }

        public static List<PlayerGameStats> ConvertByteToGameStats(byte[] bytes)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();
            mStream.Write(bytes, 0, bytes.Length);
            mStream.Position = 0;
            var myObject = binFormatter.Deserialize(mStream) as List<PlayerGameStats>;
            return myObject;
        }

        public static List<PlayerStats> ConvertByteToSeasonStats(byte[] bytes)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();
            mStream.Write(bytes, 0, bytes.Length);
            mStream.Position = 0;
            var myObject = binFormatter.Deserialize(mStream) as List<PlayerStats>;
            return myObject;
        }

        public static List<PlayerPlayoffStats> ConvertByteToPlayoffStats(byte[] bytes)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();
            mStream.Write(bytes, 0, bytes.Length);
            mStream.Position = 0;
            var myObject = binFormatter.Deserialize(mStream) as List<PlayerPlayoffStats>;
            return myObject;
        }
    }
}

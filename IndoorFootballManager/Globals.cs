﻿using System;

namespace IndoorFootballManager
{
    public static class Globals
    {
        public static string LeagueAbbreviation { get; set; }

        public static string LeagueDirectory
        {
            get
            {
                if (LeagueAbbreviation != null)
                    return $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Indoor Football Manager\Leagues\{LeagueAbbreviation}\";
                else
                    return null;
            }
        }

        public static string LeagueFilePath
        {
            get
            {
                if (LeagueAbbreviation != null)
                    return $"{LeagueDirectory}{LeagueAbbreviation}.ifm";
                else
                    return null;
            }
        }
    }
}
